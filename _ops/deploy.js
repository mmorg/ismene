import fs from 'fs'
import chalk from 'chalk'
import { $, execa } from 'execa'
import YAML from 'yaml'

// dashemploi/_ops/website/
// node deploy.js

/*
# preliminary docker config, authentication and project setup
gcloud auth configure-docker europe-west1-docker.pkg.dev
gcloud auth login
gcloud config set project dashemploi
*/

// @TODO: a better way to define root
const projectRoot = '..'

const conf = {
  app: {
    // @TODO: get the .name from the inquirer package selection (get .path also)
    name: 'rome4-api',
    path: 'services/rome4-api',
  },
  env: {
    path: '_ops/_env'
  },
  image: {
    registryPath: `europe-west1-docker.pkg.dev/dashemploi/dashemploi-images`,
    path: `jobs-and-skills/rome4-api`
  },
  build: {
    testLocal: false,
  },
  out: {
    envVar: `_ops/_temp/env_vars`,
    deploy: `_ops/_temp/deploy_asset`,
  }
}
const target = 'dev'
// @TODO: update the others target 
// const target = 'preprod' 
// const target = 'prod' 

const cwd = `${projectRoot}/`

// execa command configuration to get script's output
const c = $({ stdio: 'inherit' })


log('Decrypt the env file')
const getConfCrypt = () => `${conf.env.path}/${target}.config.yaml`
const getDecryptConf = () => `${projectRoot}/${conf.out.envVar}/${target}.config.yaml`
await execa('sops', [`--decrypt`, `${getConfCrypt()}`], { cwd }).pipeStdout(`${getDecryptConf()}`)


log('Deploy the service for docker consumption')


workaround('=> Start to Temp define the pnpm-workspace to only include The deployed service in the workspace')
/**
 * This is a workaround for this bug: 
 * if there is more than one "services" the pnpm deploy do not work. This error is thrown: 
##  ENOENT  ENOENT: no such file or directory, unlink '/home/flowww/dev/mindmatcher/mmorg/ismene/_ops/_temp/deploy_asset/rome4-api/node_modules/@mmorg/rdfx-graphql'
  * @TODO: create a reproduction : 
possible linked issues: 
  * https://github.com/pnpm/pnpm/issues/6697
  * there is something to fix ? To hook ? in deploy command ? https://github.com/pnpm/pnpm/blob/8920dbce383f6b50942f23c7d029da5f807af5ce/releasing/plugin-commands-deploy/src/deploy.ts

 */

// move the actual file 
const pathSource = 'pnpm-workspace.yaml'
const pathTarget = 'pnpm-workspace.yaml.BACKUP'

const file = fs.readFileSync(`../${pathSource}`, 'utf8')
const y = YAML.parseDocument(file)
await execa('mv', [pathSource, pathTarget], { cwd })

try {
  const servicesPath = 'services/*'
  // change the workspace content and save it temporally 
  const visitor = (key, node, path) => {
    if (!node.value) return
    if (node.value === servicesPath) {
      node.value = conf.app.path
      return node
    }
  }
  YAML.visit(y, visitor)

  const temp_content = YAML.stringify(y)

  console.log('For deployment this instruction', servicesPath, 'was replaced by', conf.app.path, '. Temp Yaml file content: ')
  console.log(temp_content)
  fs.writeFileSync(`../${pathSource}`, temp_content)

  console.log('Start to deploy the project')
  const deployTarget = `${conf.out.deploy}/${conf.app.name}`
  await execa('pnpm', [`--filter=${conf.app.name}`, '--prod', 'deploy', deployTarget], { cwd: `${projectRoot}/`, stdio: 'inherit' })

} catch (e) {
  console.log('There is a error during file processing')
  throw e
} finally {
  workaround('<= Restaure workpackage file to it\'s original place')
  await execa('mv', [pathTarget, pathSource], { cwd })
}



log('Start building the container')
const env = {
  T: target,
  DEPLOY_NAME: conf.app.name,
  CONF_FOLDER: conf.out.envVar,
}
const cwd_docker = 'container/'
const dockerOps = { env, cwd: cwd_docker }
await c(dockerOps)`docker compose build dashemploi`

if (conf.build.testLocal) {
  console.log('@TODO: find a way to get the script continue when ctrl+c')
  try {
    const r = await c(dockerOps)`docker compose up dashemploi`
    console.log(r)
  } catch (e) {

    console.log('End of local testing, continue')
  }
}


log('Tag and push the image to google')
// const registryTag = `europe-west1-docker.pkg.dev/dashemploi/dashemploi-images/dashemploi:${target}`
const registryTag = `${conf.image.registryPath}/${conf.image.path}:${target}`
await c`docker tag dashemploi ${registryTag}`
await c`docker push ${registryTag}`

log('Start deploy the image')
// const serviceName = `dashemploi-${target}`
const serviceName = `${conf.app.name}-${target}`
await c`gcloud run deploy ${serviceName} --image=${registryTag} --region=europe-west1`


function log(text, ...others) {
  console.log('\n', chalk.bgBlue(text), ...others)
}

function workaround(text, ...others) {
  console.log('\n', chalk.bgRed(text), ...others)
}