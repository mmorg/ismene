
# Set-up your dev environment
@TODO: update and adapt
```
cd _ops
node devSetup.js
```

# Deploy to online-dev instance 

```
cd _ops
node deploy.js
```

- url: https://profile-api-dev-wyew76oo4a-ew.a.run.app/


# Remarks:
- this ops package is copy-pasted into `5-minutes-profile` also. @TODO: create a reusable package from one or the other