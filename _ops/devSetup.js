import { execa } from 'execa'
import chalk from 'chalk'

const projectRoot = '..'

const conf = {
  app: {
    // @TODO: get the .name from the inquirer package selection (get .path also)
    name : 'rome4-api',
    path : 'services/rome4-api',
  },
  env: {
    path: '_ops/_env'
  },
}

const target = 'local'


const cryptPath = `${conf.env.path}/${target}.config.yaml`
const decryptPath = `${projectRoot}/${conf.app.path}/_confs/config.yaml`

log(`Decrypt the *${target}* env file to ${decryptPath}`)

await execa('sops', [`--decrypt`,`${cryptPath}`], {cwd: `${projectRoot}/` } ).pipeStdout(`${decryptPath}`)

function log(text, ...others) {
  console.log('\n', chalk.bgBlue(text), ...others)
}