
import { saveJson } from '@mmorg/fsutils'
import attachProperty from './src/attachProperty.js'
import klass from './src/romeClasses.js'
import prefix, { uri } from './src/romeNS.js'
import pickClass from './src/pickClass.js'
import paths from './_confs/paths.js'

// cd gatsby 
// node data/rome/updateRomeOntology.js

const romePath = paths.current

const {
  skos_concept,

  rome_workContext,
  rome_workHours,
  rome_workConditions,
  rome_workStructureType,
  rome_workTravel,
  rome_workBeneficiary,

  rome_competency, // most generic for skills

  rome_skillDomain,
  rome_stake,
  rome_goal,
  rome_skill,

  rome_knowHowDomain,
  rome_knowledgeCategory,
  rome_knowledge,

  rome_softSkillDomain,

  rome_field,
  rome_sector,
  rome_job,
  rome_position,
} = klass

updateRomeOntology()

function updateRomeOntology() {

  const graph = [getOntologyEntity()]

  // *****************
  // work environments
  const work_class = getClasses(skos_concept, [rome_workContext])
  work_class.push(...getClasses(rome_workContext, [
    rome_workHours,
    rome_workConditions,
    rome_workStructureType,
    rome_workTravel,
    rome_workBeneficiary,
  ]))

  console.log('@TODO: extract this pick to a layer processing')
  // pick some classes from skos:
  const picked = pickClass(['skos:Concept', 'skos:Collection', 'skos:member', 'skos:memberOf'])

  // ***************************
  // competencies / skills trees 
  const classifications_class = getClasses(skos_concept, [
    // skills
    rome_skillDomain,
    rome_stake,
    rome_goal,
    rome_skill,
    // savoir-faires
    rome_knowHowDomain,
    // knowledges  
    rome_knowledgeCategory,
    rome_knowledge,

    // soft skills
    rome_softSkillDomain, //@TODO: rename softSkill
  ])

  //************* 
  // Jobs processing
  const job_class = getClasses(skos_concept, [rome_field, rome_sector, rome_job, rome_position])

  // Hierachy attachment
  const classToAttach = [
    ...work_class,
    ...classifications_class,
    ...job_class,
  ]
  const attach_skos = attachProperty(classToAttach, ['skos:prefLabel', 'skos:broader', 'skos:narrower'])
  const jobEntity = job_class.find(e => e.id === rome_job)
  const attach_memberOf = attachProperty([jobEntity], ['skos:memberOf'])
  
  ///// **************
  const domain = [rome_job]
  const range = [rome_skill, rome_knowHowDomain]
  const prop = {
    id: 'rome:competence',
    type: ['rdf:Property'],
    label: [{
      "@language": "en",
      "@value": "Competencie / Skill (Rome)"
    }],
    domain: domain,
    range,
  }

  // competence
  // knowledge



  graph.push(...picked)
  graph.push(...classToAttach)
  graph.push(...attach_skos)
  graph.push(...attach_memberOf)
  graph.push(prop)
  console.log('Count of entities in graph:', graph.length)

  const context = getContext()
  const data = {
    ...context,
    graph,
  }

  saveJson(data, romePath)
}

function getClasses(subClassOf, entities) {
  const agg = []

  for (const e of entities) {
    agg.push(getClass(subClassOf, e))
  }
  return agg
}

function getClass(subClassOf, entity) {
  const template = {
    id: entity,
    type: ['rdfs:Class'],
    subClassOf: [subClassOf],
    label: getLabel(),
    comment: getComment()
  }

  return template

  /** @todo: find a better way to get `label` and `comment`  */
  function getLabel() {
    const uriPath = entity.split(':')[1]
    const path = uriPath.split('/')
    const last = path.pop()
    const parent = path.pop()
    const label = `${last} ${parent === 'onto' ? '' : `(${parent})`}`
    return [
      { '@language': 'en', '@value': label },
      { '@language': 'fr', '@value': label }
    ]
  }
  function getComment() {
    return [
      { '@language': 'en', '@value': '__definition_to_add_here__' },
      { '@language': 'fr', '@value': '__definition_à_ajouter_ici__' }
    ]
  }
}

function getOntologyEntity() {
  return {
    "id": "rome:Ontology",
    "type": [
      "owl:Ontology"
    ],
    // "contributor": [
    //   "Dave Beckett",
    //   "Nikki Rogers",
    //   "Participants in W3C's Semantic Web Deployment Working Group."
    // ],
    // "creator": [
    //   "Alistair Miles",
    //   "Sean Bechhofer"
    // ],
    "dct:description": {
      "@language": "en",
      "@value": "The RDF ontology for ROME terminology."
    },
    "title_dct": [
      {
        "@language": "en",
        "@value": "ROME Ontology"
      }
    ]
  }
}



// full context to lighten after fist version
function getContext() {
  return {
    "@context": {
      "id": "@id",
      "graph": {
        "@id": "@graph",
        "@container": "@set"
      },
      "type": {
        "@id": "@type",
        "@container": "@set"
      },
      "rdf": "http://www.w3.org/1999/02/22-rdf-syntax-ns#",
      "first": {
        "@id": "rdf:first",
        "@type": "@id",
        "@container": "@set"
      },
      "rest": {
        "@id": "rdf:rest",
        "@type": "@id",
        "@container": "@set"
      },
      "rdfs": "http://www.w3.org/2000/01/rdf-schema#",
      "domain": {
        "@id": "rdfs:domain",
        "@type": "@id",
        "@container": "@set"
      },
      "range": {
        "@id": "rdfs:range",
        "@type": "@id",
        "@container": "@set"
      },
      "label": {
        "@id": "rdfs:label",
        "@container": "@set"
      },
      "comment": {
        "@id": "rdfs:comment",
        "@container": "@set"
      },
      "subClassOf": {
        "@id": "rdfs:subClassOf",
        "@type": "@id",
        "@container": "@set"
      },
      "subPropertyOf": {
        "@id": "rdfs:subPropertyOf",
        "@type": "@id",
        "@container": "@set"
      },
      "isDefinedBy": {
        "@id": "rdfs:isDefinedBy",
        "@container": "@set"
      },
      "seeAlso": {
        "@id": "rdfs:seeAlso",
        "@container": "@set"
      },
      "owl": "http://www.w3.org/2002/07/owl#",
      "deprecated": {
        "@id": "owl:deprecated",
        "@container": "@set"
      },
      "inverseOf": {
        "@id": "owl:inverseOf",
        "@type": "@id",
        "@container": "@set"
      },
      "disjointWith": {
        "@id": "owl:disjointWith",
        "@type": "@id",
        "@container": "@set"
      },
      "propertyDisjointWith": {
        "@id": "owl:propertyDisjointWith",
        "@type": "@id",
        "@container": "@set"
      },
      "unionOf": {
        "@id": "owl:unionOf",
        "@type": "@id",
        "@container": "@set"
      },
      "versionInfo": {
        "@id": "owl:versionInfo",
        "@container": "@set"
      },
      "priorVersion": {
        "@id": "owl:priorVersion",
        "@container": "@set"
      },
      "imports": {
        "@id": "owl:imports",
        "@container": "@set"
      },
      "versionIRI": {
        "@id": "owl:versionIRI",
        "@container": "@set"
      },
      "maxCardinality": {
        "@id": "owl:maxCardinality",
        "@container": "@set"
      },
      "dct": "http://purl.org/dc/terms/",
      "contributor": {
        "@id": "dct:contributor",
        "@container": "@set"
      },
      "creator": {
        "@id": "dct:creator",
        "@container": "@set"
      },
      "abstract": {
        "@id": "dct:abstract",
        "@container": "@set"
      },
      "title_dct": {
        "@id": "dct:title",
        "@container": "@set"
      },
      "created": {
        "@id": "dct:created",
        "@container": "@set"
      },
      "modified": {
        "@id": "dct:modified",
        "@container": "@set"
      },
      "issued": {
        "@id": "dct:issued",
        "@container": "@set"
      },
      "license": {
        "@id": "dct:license",
        "@container": "@set"
      },
      "skos": "http://www.w3.org/2004/02/skos/core#",
      "gendj": "https://gen.competencies.be/terms/digitalJobs/",
      "inScheme": {
        "@id": "skos:inScheme",
        "@type": "@id",
        "@container": "@set"
      },
      "prefLabel": {
        "@id": "skos:prefLabel",
        "@container": "@set"
      },
      "altLabel": {
        "@id": "skos:altLabel",
        "@container": "@set"
      },
      "narrower": {
        "@id": "skos:narrower",
        "@type": "@id",
        "@container": "@set"
      },
      "broader": {
        "@id": "skos:broader",
        "@type": "@id",
        "@container": "@set"
      },
      "member": {
        "@id": "skos:member",
        "@type": "@id",
        "@container": "@set"
      },
      "definition": {
        "@id": "skos:definition",
        "@container": "@set"
      },
      "scopeNote": {
        "@id": "skos:scopeNote",
        "@container": "@set"
      },
      "example": {
        "@id": "skos:example",
        "@container": "@set"
      },
      "historyNote": {
        "@id": "skos:historyNote",
        "@container": "@set"
      },
      "foaf": "http://xmlns.com/foaf/0.1/",
      "mbox": {
        "@id": "foaf:mbox",
        "@container": "@set"
      },
      "qb": "http://purl.org/linked-data/cube#",
      "dv": "http://www.w3.org/2003/g/data-view#",
      "namespaceTransformation": {
        "@id": "dv:namespaceTransformation",
        "@container": "@set"
      },
      "mmo": "https://ontologies.mindmatcher.org/carto/",
      "mnx": "http://ns.mnemotix.com/ontologies/2019/8/generic-model/",
      "hasAccessPrivilege": {
        "@id": "mnx:hasAccessPrivilege",
        "@type": "@id",
        "@container": "@set"
      },
      "hasScope": {
        "@id": "mnx:hasScope",
        "@type": "@id",
        "@container": "@set"
      },
      "oep": "http://ontology.datasud.fr/openemploi/",
      "sioc": "http://rdfs.org/sioc/ns#",
      "prov": "http://www.w3.org/ns/prov#",
      "geop": "http://www.opengis.net/ont/geosparql#",
      "sch": "http://schema.org/",
      "address": {
        "@id": "sch:address",
        "@container": "@set"
      },
      "stardDate": {
        "@id": "sch:stardDate",
        "@container": "@set"
      },
      "author": {
        "@id": "sch:author",
        "@container": "@set"
      },
      "image": {
        "@id": "sch:image",
        "@container": "@set"
      },
      "provider": {
        "@id": "sch:provider",
        "@container": "@set"
      },
      "publisher": {
        "@id": "sch:publisher",
        "@container": "@set"
      },
      "url": {
        "@id": "sch:url",
        "@container": "@set"
      },
      "name": {
        "@id": "sch:name",
        "@container": "@set"
      },
      "logo": {
        "@id": "sch:logo",
        "@container": "@set"
      },
      "identifier": {
        "@id": "sch:identifier",
        "@type": "@id",
        "@container": "@set"
      },
      "sch_contributor": {
        "@id": "sch:contributor",
        "@type": "@id",
        "@container": "@set"
      },
      "description": {
        "@id": "sch:description",
        "@container": "@set"
      },
      "openBadge": "https://w3id.org/openbadges#",
      "badge": {
        "@id": "openBadge:badge",
        "@type": "@id",
        "@container": "@set"
      },
      "issuer": {
        "@id": "openBadge:issuer",
        "@type": "@id",
        "@container": "@set"
      },
      "issuedOn": {
        "@id": "openBadge:issuedOn",
        "@container": "@set"
      },
      "recipient": {
        "@id": "openBadge:recipient",
        "@container": "@set"
      },
      "openBadge_image": {
        "@id": "openBadge:image",
        "@container": "@set"
      },
      "openBadge_name": {
        "@id": "openBadge:name",
        "@container": "@set"
      },
      "openBadge_url": {
        "@id": "openBadge:url",
        "@container": "@set"
      },
      "criteria": {
        "@id": "openBadge:criteria",
        "@container": "@set"
      },
      "xsd": "http://www.w3.org/2001/XMLSchema#",
      "sh": "http://www.w3.org/ns/shacl#",
      "vs": "http://www.w3.org/2003/06/sw-vocab-status/ns#",
      "nfr": "https://nodefr.competencies.be/nfr/",
      "aPourNiveau": {
        "@id": "nfr:aPourNiveau",
        "@container": "@set"
      },
      "chargeDeTravail": {
        "@id": "nfr:chargeDeTravail",
        "@container": "@set"
      },
      "devise": {
        "@id": "nfr:devise",
        "@container": "@set"
      },
      "montant": {
        "@id": "nfr:montant",
        "@container": "@set"
      },
      "typeDActivite": {
        "@id": "nfr:typeDActivite",
        "@container": "@set"
      },
      "dateDeCreation": {
        "@id": "nfr:dateDeCreation",
        "@container": "@set"
      },
      "ori": "https://ori.competencies.be/ori/",
      "experienceStructure": {
        "@id": "ori:experienceStructure",
        "@type": "@id",
        "@container": "@set"
      },
      "experienceType": {
        "@id": "ori:experienceType",
        "@type": "@id",
        "@container": "@set"
      },
      "public": {
        "@id": "ori:public",
        "@type": "@id",
        "@container": "@set"
      },
      "role": {
        "@id": "ori:role",
        "@type": "@id",
        "@container": "@set"
      },
      "tache": {
        "@id": "ori:tache",
        "@type": "@id",
        "@container": "@set"
      },
      "hobby": {
        "@id": "ori:hobby",
        "@type": "@id",
        "@container": "@set"
      },
      "bloom": {
        "@id": "ori:bloom",
        "@type": "@id",
        "@container": "@set"
      },
      "nodeKind": {
        "@id": "sh:nodeKind",
        "@type": "@id",
        "@container": "@set"
      },
      "path": {
        "@id": "sh:path",
        "@type": "@id",
        "@container": "@set"
      },
      "parameter": {
        "@id": "sh:parameter",
        "@type": "@id",
        "@container": "@set"
      },
      "datatype": {
        "@id": "sh:datatype",
        "@type": "@id",
        "@container": "@set"
      },
      "declare": {
        "@id": "sh:declare",
        "@type": "@id",
        "@container": "@set"
      },
      "propertyValidator": {
        "@id": "sh:propertyValidator",
        "@type": "@id",
        "@container": "@set"
      },
      "jsLibrary": {
        "@id": "sh:jsLibrary",
        "@type": "@id",
        "@container": "@set"
      },
      "property": {
        "@id": "sh:property",
        "@type": "@id",
        "@container": "@set"
      },
      "targetClass": {
        "@id": "sh:targetClass",
        "@type": "@id",
        "@container": "@set"
      },
      "suggestedShapesGraph": {
        "@id": "sh:suggestedShapesGraph",
        "@container": "@set"
      },
      "jsFunctionName": {
        "@id": "sh:jsFunctionName",
        "@container": "@set"
      },
      "message": {
        "@id": "sh:message",
        "@container": "@set"
      },
      "sh_description": {
        "@id": "sh:description",
        "@container": "@set"
      },
      "sh_name": {
        "@id": "sh:name",
        "@container": "@set"
      },
      "optional": {
        "@id": "sh:optional",
        "@container": "@set"
      },
      "namespace": {
        "@id": "sh:namespace",
        "@container": "@set"
      },
      "jsLibraryURL": {
        "@id": "sh:jsLibraryURL",
        "@container": "@set"
      },
      "om": "https://ontomodel.competencies.be/ontomodel/",
      "index": {
        "@id": "om:index",
        "@type": "@id",
        "@container": "@set"
      },
      "records": {
        "@id": "om:records",
        "@type": "@id",
        "@container": "@set"
      },
      "icon": {
        "@id": "om:icon"
      },
      "ii": "https://mm.competencies.be/indexInstruction/",
      "instruction": {
        "@id": "ii:instruction",
        "@type": "@id",
        "@container": "@set"
      },
      "mms": "https://ismene.competencies.be/mms/",
      "isInCollectionOrScheme": {
        "@id": "mms:isInCollectionOrScheme",
        "@type": "@id",
        "@container": "@set"
      },
      "term_status": {
        "@id": "vs:term_status",
        "@container": "@set"
      },
      "esco": "http://data.europa.eu/esco/model#",
      [prefix]: uri
    },
  }

}