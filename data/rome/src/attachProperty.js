import skosGraph from './skosGraph.js'

/**
 * 
 * @param {Object[]} entity 
 * @param {string[]} propertyId 
 * @returns Object[]
 */
export default function attachProperty(entity, propertyId) {
  const graph = skosGraph()
  const domainId = entity.map(({ id }) => id)
  const agg = []
  for (const pId of propertyId) {
    const p = graph.findById(pId)
    p.domain.push(...domainId)
    agg.push(p)
  }
  return agg
}