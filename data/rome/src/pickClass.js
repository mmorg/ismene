import skosGraph from './skosGraph.js';

export default function pickClass(entityIds){
  const graph = skosGraph()
  return entityIds.map( id => graph.findById(id))
}