import prefix from './romeNS.js'

// this file is a copy paste from: https://gitlab.com/mmorg/jobs-and-skills/-/blob/7ead4d33a809074dff1d7a89896bc875fe3cad25/services/rome/src/romeV4/romeClasses.js


// utility types : 
const skos_concept = 'skos:Concept'
const skos_collection = 'skos:Collection'
// rome4 types 

// competencies
const rome_competency = `${prefix}:onto/Competency`
const rome_skill = `${prefix}:onto/Skill`
const rome_knowledge = `${prefix}:onto/Knowledge`
const rome_goal = `${prefix}:onto/Goal`
const rome_stake = `${prefix}:onto/Stake`
const rome_skillDomain = `${prefix}:onto/SkillDomain`
const rome_knowHowDomain = `${prefix}:onto/KnowHowDomain`
const rome_knowledgeCategory = `${prefix}:onto/KnowledgeCategory`
const rome_softSkillDomain = `${prefix}:onto/SoftSkillDomain`

// work context
const rome_workContext = `${prefix}:onto/WorkContext`
const rome_workHours = `${rome_workContext}/Hours`
const rome_workConditions= `${rome_workContext}/Conditions`
const rome_workStructureType = `${rome_workContext}/StructureType`
const rome_workTravel = `${rome_workContext}/Travel`
const rome_workBeneficiary = `${rome_workContext}/Beneficiary`

// jobs 
const rome_employ = `${prefix}:onto/Employment`

const rome_field = `${rome_employ}/Field`
// grand domaine
const rome_sector = `${rome_employ}/Sector`
// domaine professionnels, faire un lien gendj:Sector
const rome_job = `${rome_employ}/Job`
// métier, faire un lien avec gendj:Job
const rome_position = `${rome_employ}/Position`
// appellation, faire un lien avec gendj:Position




export default {
  // utilities
  skos_concept,
  skos_collection,

  // competencies
  rome_competency,
  rome_skill,
  rome_knowledge,
  rome_goal,
  rome_stake,
  rome_skillDomain,
  rome_knowHowDomain,
  rome_knowledgeCategory,
  rome_softSkillDomain,
  
  // work context
  rome_workContext,
  rome_workHours,
  rome_workConditions,
  rome_workStructureType,
  rome_workTravel,
  rome_workBeneficiary,
  
  // Employement
  rome_field,
  rome_sector,
  rome_job,
  rome_position,
}