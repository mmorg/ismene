import { readJson } from '@mmorg/fsutils'
import getGraph from '../../../packages/rdfx-graphql/src/rdfx-graph/getGraph.js'
const skos = readJson('./data/skos/__exports__/gl-compact:skos_1.3.0.jsonld')

let cache = null
/**
 * 
 * @returns 
 */
export default function skosGraph(){
  if(!cache) cache = getGraph(skos)
  return cache 
}