import { dirname } from 'path'
import { fileURLToPath } from 'url'
const __dirname = dirname(fileURLToPath(import.meta.url))

const packageRoot = `${__dirname}/..`

const paths = {
  current: `${packageRoot}/rome-4.0.0.jsonld`,
}

export default paths