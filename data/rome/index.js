import { dirname } from 'path'
import { fileURLToPath } from 'url'
const __dirname = dirname(fileURLToPath(import.meta.url))
import { readJson } from '@mmorg/fsutils'
import paths from './_confs/paths.js'

export const ontology = readJson(paths.current)

export default ontology 
