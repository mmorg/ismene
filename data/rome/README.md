
```
cd gatsby
```

1/ Manual writing of the ontology and generation: 
@TODO: improve the implementation of all properties
```
node data/rome/updateRomeOntology.js
```

@TODO: see if others steps are to implement because rome is really specific
2/ indexation of the ontology ==> Layerize the ontology 
```
node src/bin/integrate mu -f '**/rome/*.jsonld' -g 'data/rome/romeGenerator.js' -u
```
- files are saved in `__layers__`

==> use the export structure of JAS to integrate limited skos

3/ Exports of the layerized graphs 
```
node src/bin/compactLayers export -p 'data/skos' -u
```
- files are saved in `__exports__`
