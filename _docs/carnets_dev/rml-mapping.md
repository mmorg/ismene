
# Exploration des technologies RML pour le mapping xsd -> rdfs

* Les modèles de données HR-XML sont exprimés en xsd et json-schema. 

* Une solution "maison" pour la transformation des fichiers xsd en rdfs avait été réalisé par Mindmatcher. Cette solution bien que fonctionnelle, ne propose pas un niveau de généricité suffisant pour adresser les différents cas présentés par les modèles xsd identifés dans le cadre du data-lab. 

* L'université de Ghent en belgique publie des normes et des librairies pour réaliser des mappings via le language "rml". 

* Plusieurs outils sont disponibles pour réaliser les différentes étapes d'un traitement RML, mais ces outils ne sont actuellement pas intégrés et ont des niveaux de maturités suffisant mais pas industriel. 

* Les travaux réalisés dans le cadre de cette mise en place ont consistés en : 
  * la découverte et la qualification des technologies RML proposées
  * le "packaging" des différents outils proposés en une application intégrée
  * la réalisation d'un "bridge" pour permettre la création de "fonction RML" en Javascript et non en Java
  * la rémontée de bugs et d'améliorations auprès de l'équipe RML (voir ci-dessous)

## Détail des tickets RML réalisés et à faire :

* [Iri function outputs](https://github.com/RMLio/yarrrml-parser/issues/140)
* [Parse XML with namespace](https://github.com/RMLio/rmlmapper-java/issues/132)
* [Fix wrapper.execute when rml:source is a path](https://github.com/RMLio/rmlmapper-java-wrapper-js/issues/23)
* [Configuration pour les tests](https://github.com/RMLio/rmlmapper-java/issues/135)

* a faire : ticket sur la position de `sources` & `validation yaml`
* a faire : ticket sur la gestion des `full_path` see note in yarrrml


## Détail sur le "rml-node-bridge"

* les fonctions rml sont actuellement designées pour être des fonctions Java. Ceci pose un problème dans la capacité à créer facilement des fonctions spécifiques. 
* Afin de pallier cette limitation une function rml d'execution de script JS à été créée. 
* Ce rml-node-bridge est une fonctionnalité qui pourrait être partagée à la communauté après une opération de préparation à la publication
