

# publish a new local version of the rml-bridge: 
```
docker-compose exec align /apps/align/rml-bridge/bridgeBuild.sh
```


# rationale : 


* les fonctions rml sont actuellement designées pour être des fonctions Java. Ceci pose un problème dans la capacité à créer facilement des fonctions spécifiques. 
* Afin de pallier cette limitation une function rml d'execution de script JS à été créée. 
* Ce rml-node-bridge est une fonctionnalité qui pourrait être partagée à la communauté après une opération de préparation à la publication



# bibliography : 

* pour executer un script js depuis java: https://docs.oracle.com/javase/10/scripting/java-scripting-api.htm#JSJSG112


* pour l'intégration des transformations dans node js : https://github.com/RMLio/solid-rml-store
