
# research plugin for @todo management in vs-code.

* Très intéressant et qui permet de faire des widget spécifiques:
  * https://marquee.stateful.com/docs/customWidgets
* 

* la gestion d'un fichier de todo 
  * https://marketplace.visualstudio.com/items?itemName=fabiospampinato.vscode-todo-plus

* pour la gestion de commentaire dans les docs:
  * https://github.com/Gruntfuggly/todo-tree
  


* juste de l'hightlight: 
  * https://marketplace.visualstudio.com/items?itemName=wayou.vscode-todo-highlight
  * 

* 


* très intéressante mais prise en compte de gitlab en attente et proprio et peut être a l'abandon: https://marketplace.visualstudio.com/items?itemName=Stepsize.stepsize
  - sur la prise en compte de gitlab: https://feedback.stepsize.com/tech-debt-management/p/gitlab-integration





# VSCode: suggestion de configuration : 

* Pour cette (extension par exemple)[https://marketplace.visualstudio.com/items?itemName=Stepsize.stepsize]: 
```
Drop this snippet to your repository's .vscode/extensions.json file:
{
  "recommendations": ["stepsize.stepsize"]
}
```
