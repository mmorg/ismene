


# Question sur l'organisation des classes par l'ontologie Skos: 
* objectif: permettre de réprésenter une organisation des classes autre que celle hierachique des sous-classes

* questions ouvertes : 
 * la création de conceptSchemes avec des classes ratachées est une bonne manière de faire ? 
 * doit-on ajouter un type "skos:Concept" aux rdfs:class pour pouvoir les ajouter dans le "inscheme" ? 

* documentations :
  * intéressant sur certains aspects mais ne répond pas à la problématique : https://hal.archives-ouvertes.fr/hal-00948932/document
  * some mixes of owl and skos, but not the way we want: 
https://www.w3.org/2006/07/SWD/SKOS/skos-and-owl/master.html#Hybrids
  * sur la correspondance entre les classes de définition: 
https://www.w3.org/2006/07/SWD/wiki/SkosDesign/ConceptSemantics-Patterns.html

