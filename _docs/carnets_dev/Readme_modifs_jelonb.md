### Modifications:

##Issue 9-changement-de-la-couleur-principale :
* White background color for top Tab.
* Title in red center in the middle.
* Logo Mindmatcher in top-left of it.
* Add DEV|API Icons on top right.

#Files modified:
gatsby/src/components/dashibiden/DashLayout.js
gatsby/src/gatsby-theme-material-ui-top-layout/theme.js

#Insertion of a component:
gatsby/src/components/dashibiden/dialogDev.js