
# Categorize text 
* https://www.elastic.co/guide/en/elasticsearch/reference/current/search-aggregations-bucket-categorize-text-aggregation.html#search-aggregations-bucket-categorize-text-aggregation

* un exemple intéressant sur les `title` de GenScan: 
https://07a3e15f3a2c4048aa41aca62fbf0a56.europe-west3.gcp.cloud.es.io:9243/app/dev_tools#/console
```
GET /.ent-search-engine-documents-genscan-v1-stock-prod/_search
{
   "size":0,
   "aggs":{
      "categories": {
      "categorize_text": {
        "field": "title"
      }
    }
   }
}
```
- peut aussi être fait sur le champs `contenu_formation`
- les paramètres de la requête doivent être affinés pour avoir de meilleures catégories. 
