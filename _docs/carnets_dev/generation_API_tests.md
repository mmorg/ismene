# url chelous : 

https://localhost/admin#/actions


# A tester : 
* skos:Concept and collection creation 
* dataType 
* class & property creation


## A/ test intégration skos et questions

* voir fichier : https://docs.google.com/document/d/1EIGu9w5NztenEClrUESPf7SEca4nezf6I4R65G3DKIw/edit#



## test api 

* pour une requête get 
```
curl -X 'GET'   'https://localhost/concept_schemes?page=1'   -H 'accept: application/ld+json' -k
```
* pour récupérer le contexte:
```
curl -X 'GET'   'https://localhost/contexts/ConceptScheme'   -H 'accept: application/ld+json' -k
```
* pour récupérer le vocab 
```
curl -X 'GET'   'https://localhost/docs.jsonld#'   -H 'accept: application/ld+json' -k
```


!!!!!!!!! RESTART HERE !!! 
* faire un script de génération automatique avec pour test les fichiers-target de : demo/vocabularies_data + paragraphe dessous
* ajouter le skos:narrower
* faire un script d'alimentation du référentiel gen 
    * impact du multitypage ? 
    * gestion par collections ? 
* script d'import du rdfs 


# Script de génération du "modèle api-plateform" 

Faire le modifications avec en tête: 
- la gestion par layers
- l'organisation cible des dossiers


=> mettre rdfs:Class en premier
=> rajoute skos:Concept.subClassOf = rdfs:Ressource
=> gestion de l'héritage des propriétés : 
  * rajouter range et domain = skos:concept a toutes les propriétés
=> faire un check sur les domains et props vide (vérification point précédent)
=> plus tard: intégration des shacl skos 

conceptScheme
  :: gestion des .definition & .example la documentation api et admin ? 
-> hasTopConcept

skos:prefLabel
    $ comment @en en plusieurs lignes
-> subPropertyOf : rdfs:label

==>  skos:broader = 3 niveaux de sub-property 
  * => script à faire: reporter les ".range" et ".domain" au dernier enfant 
==> `skos:semanticRelation` suppression des range/domain
==> a ajouter par layer: rdfs:label.domain += ["skos:Collection", "skos:ConceptScheme"]


- pour plus tard

==> nouvelle organisation des dossiers de données: 
ontoPrefix
    archives
    sources
    layers
    renovationScript
    exports
        gatsby
        api-platform
    README.md

=> layers contains file named like 

# intégration UI doc swagger 

* 1/ extraire la documentation de l'api 
```
## pour extraction du docker 
docker-compose exec php bin/console api:openapi:export --output=/srv/vocabularies_data/swagger_docs.json
## pour copie dans le dossier de processing
# @TODO: update with final folder
mv vocabularies_data/swagger_docs.json open-api-doc/
```

* 2/ affichage dans le visualisateur: 
- apiplateform utilise https://github.com/swagger-api/swagger-ui en version bundle
- mais il existe une version React : https://www.npmjs.com/package/swagger-ui-react


* 3/ faire le filtre  des contenus de l'api pour n'afficher que ceux correspondant à l'entitée sur la page. 
