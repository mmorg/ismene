
* L'enjeux est le stockage des entities rdf avec une `unref_dep = 2` dans app-search / elastic index

* Une opportunitée est la possibilté de wrapper les index elastic dans app-search. 
  - voir cette documentation: https://www.elastic.co/guide/en/app-search/current/elasticsearch-engines.html

* Ce `wrap` permet de gérer les "sous-objets" via une normalisation. Mais cette normalisation ne garde pas la relation entre les objets si c'est le type `object field` qui est utilisé. 
* Cette relation est conservée si c'est le type `nested field` qui est utilisé. Mais l'utilisation de ce type est "couteuse" et [limitée à 50](https://www.elastic.co/guide/en/elasticsearch/reference/8.6/nested.html)

* ==> ces options doivent être étudiées plus en détail. En première intention, c'est une normalisation compatible avec les `object fields` qui est utilisée par défaut dans le writer app-search.

