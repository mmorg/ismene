

# intérêt de l'ontologie pivot : 

* The motivating scenario is about many stakeholders with an interoperability need. To avoid the definition of point-to-point conversions, an any-to-one centralized mapping approach based on Semantic Web technologies (and a reference ontology used as global conceptual model) offers the following advantages:
  * Let stakeholders keep using their current legacy systems
  * To obtain interoperability with other actors in the ecosystem, a stakeholder only needs to define lifting mappings from the adopted standard to the reference ontology, and lowering mappings from the reference ontology to the standard
  * Knowledge graph as an additional valuable product of the conversion


* source : https://github.com/cefriel/chimera#goals
