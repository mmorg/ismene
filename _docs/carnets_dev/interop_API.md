# fonctionnement de l'api Ismene

## Visiontrust 

* Environnement de test : https://staging.visionstrust.com
* Environnement de prod : https://www.visionstrust.com/

2 comptes : 
* MindMatcher
* MindMatcher-Test-Export

# Cloud run 

* instance prod test/prod: https://ismene-gsodvw6jdq-ew.a.run.app

* url de gestion google: https://console.cloud.google.com/run/detail/europe-west1/ismene/metrics?organizationId=185421944977&project=ismene&supportedpurview=project

# Intégration sur l'environnement de test : 

* https://staging.visionstrust.com/web/testenv
   - puis `Tester le protocole d'intéropérabilité`

* logs cloud run : https://console.cloud.google.com/run/detail/europe-west1/ismene/logs?organizationId=185421944977&project=ismene&supportedpurview=project

## les urls de l'api : 

### dev : 
http://localhost:1111/api/consent/exp
http://localhost:1111/api/consent/imp
http://localhost:1111/api/data/exp
http://localhost:1111/api/data/imp

curl -X POST http://localhost:1111/api/consent/exp

### test du endpoint consent import 

- local test:
curl -X POST http://localhost:1111/api/consent/imp \
   -H 'Content-Type: application/json' \
   -d '{"serviceExportUrl":"https://httpbin.org/post","signedConsent":"sign_concent","dataImportUrl":"data_import_url"}'


- prod test:
curl -X POST https://ismene-gsodvw6jdq-ew.a.run.app/api/consent/imp \
   -H 'Content-Type: application/json' \
   -d '{"serviceExportUrl":"https://httpbin.org/post","signedConsent":"sign_concent","dataImportUrl":"data_import_url"}'


- generic test:
curl -X POST https://httpbin.org/post \
   -H 'Content-Type: application/json' \
   -d '{"login":"my_login","password":"my_password"}'

## Test rml integration 

* http://localhost:1111/api/rml





# DEV : 

* 1/ build & local test 
```
docker compose build ismene
```

* test :
```
docker compose up ismene
curl -X POST http://localhost:9000/api/consent/exp
```

# MEP 
```
./gatsby/buildAndPublish-interop.sh
```


