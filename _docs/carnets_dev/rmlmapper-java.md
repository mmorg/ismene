
# update the rmlmapper.java

* execute : `npm run download:rmlmapper`


## hack
* pour tester une fonction en particulier : 
```
mvn test -Dtest=Mapper_XML_Test#evaluate___NS___XML
```

###  build & update jar

* while https://github.com/RMLio/rmlmapper-java/pull/134 not merged,
```sh
# build
cd /home/flowww/dev/mindmatcher/datalab-s/rmlmapper-java
mvn install -Pautonomous

# copy to main jar
mv ../ontologies/rml/rmlmapper.jar ../ontologies/rml/rmlmapper{{VERSION}}.jar 
cp target/rmlmapper{{VERSION2}}.jar ../ontologies/rml/rmlmapper.jar 
```



# Documentations sur les fonctions par défault 

* déclaration principal: https://github.com/RMLio/rmlmapper-java/blob/master/src/main/resources/functions_grel.ttl
* déclaration annexe: https://github.com/RMLio/rmlmapper-java/blob/master/src/main/resources/functions_idlab.ttl
* site de référencement en cours: 


* documentation pour l'intégration de fonctions : 
  * https://stackoverflow.com/questions/66414992/rml-and-fno-fails-to-run-together
  * des exemples (mais attention au ns ex:): https://rml.io/yarrrml/spec/#functions
  

* pour l'ajout et la recherche de fonctions fno : https://fno.io/hub/search


# Questions 
* extraction de plusieurs triples à partir d'une information ne semble pas possible, même avec une fonction specifique: 
  *https://stackoverflow.com/questions/61751174/is-there-a-solution-in-rml-for-multiple-complex-entities-in-one-data-element-ce



# autre outils : 

* Solution intégrée et packagée par une université pour la création et la publication de mappings : 
  * https://d2s.semanticscience.org/docs/
  * a revoir, définition d'une "notion de translator" : 
  * https://d2s.semanticscience.org/docs/translator-prototypes-registry

* rml2shacl : 
  * a voir ce dont il s'agit https://github.com/RMLio/RML2SHACL
    * => attendre de voir comment ça bouge

* une implementation en js qui permet de créer des fonctions js, mais qui n'est pas conforme à l'ensemble des specs : https://github.com/semantifyit/RocketRML
* en nodejs: pour générer du rml à partir d'un example (repository qui date de 4 ans) : https://github.com/RMLio/example2rml


# bibliography:

* chercheur autour du rml : https://rbdd.cnrs.fr/spip.php?article134&lang=fr
