
# nutsheel : 

* on 20211206 

**pour les classes**
* qui fonctionne: 
  * http://localhost:1111/hrrdf/CatalogType
  * http://localhost:1111/hrrdf/SpecifiedCompetencyType
  * http://localhost:1111/hrrdf/AdditionalItemType

* qui bug : 
  * http://localhost:1111/hrrdf/ReportType: 2 propriétés qui range "Report" est à vérifier ("report" est ok)
  * http://localhost:1111/hrrdf/__allowances__Type__ : pas de propriétés, et peut être pas à garder ! 

**pour les propriétés**
* qui fonctionne: 
  * http://localhost:1111/hrrdf/assessedCompetencies
  * http://localhost:1111/hrrdf/accomodationTypeCodes
  * http://localhost:1111/hrrdf/accessPoint

* qui bug:
  * http://localhost:1111/hrrdf/basisCode : avec une description mais pas de range, ni de ER ==> sans doute a cause d'un pointage vers une liste : RemunerationBasisCodeList
  * http://localhost:1111/hrrdf/visa : problème sur l'ER diagram
  * http://localhost:1111/hrrdf/AccountBasedPlanSetup : propriété sans domain (vérifier si il y en a d'autres). Problème au niveau du modèle xml ??²





* Détail to checks : 


# on Classes :
## Case 1 :
* actual jsonld output : 
```json 
* {"id":"hrrdf:AdditionalItemType","type":["rdfs:Class"],
"comment":["Structure to define additional items between trading partners."],
"label":["AdditionalItemType"]},
```

* source file = `rml/data/HR-XML/4.3.0 FINAL/Assessments/xml/CatalogType.xsd`
* test file = [inline definition extract](rml/src/rml-integrated/__fixtures__/CatalogType.xml) 

* object's property (hasProperties) : 
  * => génération : ok
  * => display    : a tester
* property that range (isRangedBy):
  * => generation : 
  * => display    : 

* c'est le problème général des "unnamed complexType"

## Case 2 :

* acutal json ld
```json
{"id":"hrrdf:ReportType","type":["rdfs:Class"],
"comment":["Represents an Assessments Report for HR Open Standards."],
"label":["ReportType"]},
```

* multiples <xsd:complexType> roots in the same file, but only one <xsd:element name="Report">
    * exemple file : `rml/data/HR-XML/4.3.0 FINAL/Assessments/xml/ReportType.xsd`
    
# case 3 : 
* problème de generation pour les classes définies en `xsd:simpleType` 


# Properties

# Case 1 
* problème lié au "classes cas 3" avec un impact sur le range de `{"id":"hrrdf:accomodationTypeCodes"}`

* fichier source : 
rml/data/HR-XML/4.3.0 FINAL/Common/xml/base/CodeType.xsd

* test = 
rml/src/rml-integrated/rdfize.test.js#
Generate good outputs from the `Classes.yaml` mapping on CodeType

* status : ok

