# problème source : 

* 2 problèmes : 
  * 1/ taille du menu dans les pages d'entitées : explostion de la taille des pages : ok résolu
  * 2/ taille du composant "menu" non efficace pour la navigation : see : https://gitlab.com/datalab-s/ontologies/-/issues/11
  

## problème de la taille des pages a cause du mega-menu : 
* du fait du nombre d'entités du modèle hrrdf (2K+) la génération complète du menu en jsx créait des fichiers de 2,5M. Cette taille par fichier rend la taille totale de la gérénation > 2,5G. 
Or la limite pour les gitlab pages est de 1G. Mais aussi, le téléchargement de pages si grosses n'est pas optimal pour un chargement rapide. 
Une solution décalant la génération du menu du "build time" vers le "browser time" permet de résoudre le problème de taille de page puisque elles sont maintenant entre 13 et 19K  (à comparer aux 2,5M). 

Cependant la génération complète coté navigateur prose un lag dans l'affichage du menu : le temps de génération du jsx est trop long au vu du menu. 
[L'issue suivante](https://gitlab.com/datalab-s/ontologies/-/issues/11) doit être traitée pour permettre une amélioration du menu est le rendre plus facilement utilisable.


# Options étudiées et choix
* pour une implementation manuelle : https://www.pluralsight.com/guides/how-to-implement-infinite-scrolling-with-reactjs


* les autres options: 
  * react-infinite-scroll-list: pas mal mais pas maintenu, code simple : https://github.com/samouss/react-infinite-scroll-list
  * react-simple-infinite-loading : peut maintenu et complexe car basé sur un composant "react-window"(https://github.com/bvaughn/react-window) 
  * react-window-infinite-loader: même écosystème que le précédent. trop private et pas assez maintenu : https://www.npmjs.com/package/react-window-infinite-loader
  * react-infinite-scroller : ancien et pas maintenu : https://www.npmjs.com/package/react-infinite-scroller
  * react-infinite-scroll-component: maintenu et actif pas sur l'intersectionObserver, et code compliqué : https://github.com/ankeetmaini/react-infinite-scroll-component
  * react-infinite-scroll : abandonné
  * react-loading-infinite-scroller: plus téléchargé mais aussi à l'abandon: https://github.com/danbovey/react-infinite-scroller
  * react-infinite-scroll-hook: recent et qui semble maintenu, basé sur l'observer: https://www.npmjs.com/package/react-infinite-scroll-hook

* selection des options : 

* 1/ react-infinite-scroll-hook
* 2/ react-infinite-scroll-component
* 3/ implémentation manuelle