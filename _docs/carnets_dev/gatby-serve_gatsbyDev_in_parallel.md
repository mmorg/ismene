* run `gatsby-dev` & `gatsby serve` in parallel
  * see comment https://github.com/gatsbyjs/gatsby/discussions/1878#discussioncomment-1835559
  * require: this patch: gatsby/patches/gatsby+4.3.0.patch
    * goal : arrêter de supprimer tout le dossier public, mais que le contenu
