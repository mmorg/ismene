
# Commands

```sh
docker-compose exec gatsby npm install husky -D

docker-compose exec gatsby npm set-script prepare "cd .. && husky install gatsby/.husky"

docker-compose exec gatsby npm run prepare

touch gatsby/.husky/pre-push
chmod u+x gatsby/.husky/pre-push
# mv gatsby/.husky/pre-commit gatsby/.husky/pre-push
# ==> copy the following content in the file
```

# Script Content

``` sh
#!/usr/bin/env sh
cd gatsby

red='\033[0;31m'
green='\033[0;32m'
yellow='\033[0;33m'
no_color='\033[0m'

echo "\n${yellow}=========================================${no_color}"
echo "${yellow}Executing Tests as a Git hook on 'pre-push'${no_color}"
echo "${yellow}=========================================${no_color}"

if docker-compose exec -T gatsby npm test; then
    echo "\n${green}=========================================${no_color}"
    echo "${green}Tests were SUCCESSFUL!${no_color}. Exiting hook."
    echo "${green}=========================================${no_color}"

    exit 0
fi

echo "\n${red}=========================================${no_color}"
>&2 echo "${red}Some Tests has ERRORS. Check the logs.${no_color}"
echo "${red}=========================================${no_color}"
exit 1
```
