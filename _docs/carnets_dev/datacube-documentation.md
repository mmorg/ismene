
# datacube : 
* une vielle documentation : https://linkeddata.rs/sites/default/files/attachments/LOD2_Tool_RDF_DataCube%20_Validation_for_submission_final.pdf



# libraires autour du rdf datacube 

## table2qb

* description : https://github.com/swirr/ltable2qb
* exemple détaillé : https://github.com/Swirrl/table2qb/tree/master/examples/employment
  * dossier avec les fichiers de sortis (cubes de tests) : https://github.com/Swirrl/table2qb/tree/master/examples/employment
  * un autre exemple: https://github.com/Swirrl/table2qb/tree/master/examples/regional-trade

* un autre exemple, mais ancien et compliqué: https://github.com/Swirrl/cubiql/blob/master/doc/table2qb-cubiql.md


## autres librairies swirrl : 

***ontologies swirrl***
* peut être des trucs à recupérer, design du site ancien: https://ontologies.publishmydata.com/
  * le repository source: https://github.com/Swirrl/pmd-ontologies/tree/master/src/vocabs

***ecriture sparql*** : https://github.com/Swirrl/grafter

***données géospaciales***: un projet actuellement en cours apparement, en lien avec : https://ogcapi.ogc.org/, revoir le repository : https://github.com/Swirrl/ogc-api-prototype
