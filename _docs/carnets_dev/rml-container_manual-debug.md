
* test library cli : 

==> ?? rml/src/rml-integrated/__fixtures__ ??


* test in-script integration: 


==> remove yarrrmlExec test 


@TODO: update doc and code to get a step by step debbuging capability
* yarrml to rml : `@TODO: retrieve the command`
* rml execution: `docker-compose exec rml ./scripts/rmlExec.sh -m rml-rules/rules-3.rml.ttl`

==>
docker-compose exec rml ./test-script/rmlExec.sh -m rml-rules/rules-3.rml.ttl

Others examples of rml exection: 
```
# mapping avec utilisation de la fonction "par defaut" `toUpperCase` : 
docker-compose exec rml ./scripts/rmlExec.sh -m rml-rules/rules-4.rml.ttl

# mapping avec utilisation de la fonction `string_length` dans la librairie `GrelFunctionsTEST.jar`. Cette librairie est une copie renommée de `GrelFunctions.jar`
docker-compose exec rml ./scripts/rmlExec.sh -f rml-rules/functions_local.ttl -m rml-rules/rules-5.rml.ttl
```
