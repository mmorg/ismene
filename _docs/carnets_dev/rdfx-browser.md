



# @TODO : 

* display : zone de search dans les propriétés
* display : gestion de la conservation de l'état du menu entre les navigation et scroll sur le menu courant


* gen : fix the xsd:functions RML cases


* gen : gestion du "domaine" hrrdf: 
  * ajout du "dossier domaine" dans l'url exemple `hrrdf:Assessments#AssessmentType` pour les classes. Idem pour les propriétés ??
  * cas special du dossier commun a traiter; 2 points particuliers : 
    * 1 sous-niveau de dossier
    * un nommage sans majuscule des dossiers

  
* display : intégration d'un badge `classes ou propriété`
  * exemple :
```js
 <CardHeader
        avatar={
          <Avatar
            sx={{ bgcolor: red[500] }}
            aria-label="recipe"
            variant="square"
            sx={{ width: 80, height: 50 }}
          >
            Classe
          </Avatar>
        }
        action={
          <IconButton aria-label="settings">
            <MoreVertIcon />
          </IconButton>
        }
        title="Shrimp and Chorizo Paella"
        subheader="September 14, 2016"
      />
```
  * trouver où, peut être ici mais à checker: gatsby/src/components/dashorm/dashNodeComponents.js



* creation d'un composant genéric LiteralsComponent
  * voir : gatsby/src/components/dashorm/cardComponents/ConceptSchemeLiteralsComponent.js


* (gatsby) gestion de l'ouverture du menu : 2 propriétés: 
  * opens: [id1, id2] : an array of items id opened
  * current : idx : the current id : use a scroll to function

* (gatsby) pour l'optimisation du nombre de cores pour le build, voir à la fin de cette page : https://www.gatsbyjs.com/docs/how-to/local-development/environment-variables/#reserved-environment-variables
  * GATSBY_CPU_COUNT


* (gatsby) @TODO: remove dep @mui-treasury/components when test is no more usefull



# stats on graphs

* statistiques sur les graphs: 
  * un exemple de stats "standardisées": https://www.w3.org/TR/hcls-dataset/
  * peut être mettre ces informations plutot sous forme de datacube non ? 




# A intégrer

* configuration de l'interface de __rdfs-explorer__
@TODO: improve documentation

* DashTreeLayout => transformer en DashPageLayout qui consite juste en la config du `dashTree` 
  * concernant le menu: trouver un moyen d'avoir un menu par défaut au niveau du composant parent et d'avoir un moyen d'influencer ce menu dans le composant enfant `DashPageLayout`
  * ==> restart here: gatsby/src/components/dashorm/DashTreeLayout6.js


* Structuration des `{rdfs}Component` 
  * ces composants sont les "cartes" du dashboard qui sont positionnées par la configuration `dashTree`
  * ces composants doivent exposer d'une variable exportée `query` qui définit un `fragment` GraphQL. C'est cette requête qui est exectuée pour fournir les données au composant dans la variable `props.data`. Exemple : 
  ```js
  export const query = graphql`
    fragment ClassLiteralsComponent on Query{
        ...
    }
  `
  ``` 
  * voir cet [exemple de composant](gatsby/src/components/dashorm/cardComponents/ClassLiteralsComponent.js)




@TODO : au niveau de la génération : 
* ajouter le ns schema.org
* prendre en compte les mapping de "range" (exemple "nfr:Literal" => "schema:Text")
* ==> fixer le nfr ns (/ a la fin) : "nfr": "https://gitlab.com/mmorg/nodefr-2/",

# organisation du menu : 

* voir les questions par rapport à la gestion d'un affichage "humain" des classes dans le menu par rapport à la structure du RDFS. Et sur le mix de différentes ontologies pour gérer cette organisation. Voir : documentation/carnets_dev/rationnale_class-organization.md


# Inspirations : 

* une autre lib potentielle pour les uml:
* https://github.com/skanaar/nomnoml
