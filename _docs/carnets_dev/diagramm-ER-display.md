
==> retrouver le bookmark sur le "language visuel" des ontologies.

# pour les diagramm 

* exemple avec de nombreux rangedBy objects: 
  * http://localhost:1111/hrrdf/AttachmentType/



* https://hub.docker.com/r/plantuml/plantuml-server

* un exemple à tester : 
  * http://localhost:1111/hrrdf/NameType/


## notes sur reactflow:

* représentation sous forme de mindmap: https://codesandbox.io/s/ozvzx?file=/src/MindMap.tsx:3382-3396


* advanced nodes: https://codesandbox.io/s/30fre?file=/src/index.js:51-52

* pour la construction de ER diagram with Crow's Foot notation notation:
  * need to wait the reactflowv10+ 
  * documentation:
    * https://mermaid-js.github.io/mermaid/#/entityRelationshipDiagram
    * https://en.wikipedia.org/wiki/Entity%E2%80%93relationship_model

* intégration d'une zone dans le diagramme: a utiliser pour la légende : 
  * https://codesandbox.io/s/30fre
  * ==> il faut cliquer sur la croix.
   


## documentation sur la modélisation ER 


* très bon article à relire: https://en.wikipedia.org/wiki/Entity%E2%80%93relationship_model


* autre: pour la modélisation de data: les 3 schémas : https://en.wikipedia.org/wiki/Three-schema_approach

