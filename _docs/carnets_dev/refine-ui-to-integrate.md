
* multi-level selector: dans l'exemple de la résidence: https://ant.design/components/form#components-form-demo-register

* ajout de plusieurs langstring pour une propriété: https://ant.design/components/form#components-form-demo-dynamic-form-item

* pour la gestion de différentes langues lors de l'édition : https://refine.dev/docs/advanced-tutorials/data-provider/strapi-v4/#locale

* création d'un data-provider: https://refine.dev/docs/tutorial/understanding-dataprovider/create-dataprovider/

* ajout de filtres sur une table: https://refine.dev/docs/tutorial/adding-crud-pages/antd/adding-sort-and-filters/

==> modification du logo : 
  - qui est appellé "Title" : https://refine.dev/docs/api-reference/core/hooks/refine/useTitle/
  - pour une modification plus profonde du layout, utiliser le swizzle et : 
  - https://refine.dev/docs/api-reference/antd/customization/antd-custom-layout/