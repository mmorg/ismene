
* 2 problèmes : 
  * 1/ taille du menu dans les pages d'entitées : explostion de la taille des pages : ok résolu
  * 2/ taille du composant "menu" non efficace pour la navigation : see : https://gitlab.com/datalab-s/ontologies/-/issues/11
  

# 1rs problem : 
# ok: problème des tailles de pages : 
* du fait du nombre d'entités du modèle hrrdf (2K+) la génération complète du menu en jsx créait des fichiers de 2,5M. Cette taille par fichier rend la taille totale de la gérénation > 2,5G. 
Or la limite pour les gitlab pages est de 1G. Mais aussi, le téléchargement de pages si grosses n'est pas optimal pour un chargement rapide. 
Une solution décalant la génération du menu du "build time" vers le "browser time" permet de résoudre le problème de taille de page puisque elles sont maintenant entre 13 et 19K  (à comparer aux 2,5M). 

Cependant la génération complète coté navigateur prose un lag dans l'affichage du menu : le temps de génération du jsx est trop long au vu du menu. 
[L'issue suivante](https://gitlab.com/datalab-s/ontologies/-/issues/11) doit être traitée pour permettre une amélioration du menu est le rendre plus facilement utilisable.
