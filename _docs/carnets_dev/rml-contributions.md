

# Ticket rml a faire et ok : 
* a faire : ticket sur la position de `sources` & `validation yaml`
* a faire : ticket sur la gestion des `full_path` see note in yarrrml
* a faire: ticket sur la configuration des tests, cf: `rmlmapper-java/pom.xml`
* a faire : ticket sur l'execution des fonctions xpath 

* [Iri function outputs](https://github.com/RMLio/yarrrml-parser/issues/140)
* [Parse XML with namespace](https://github.com/RMLio/rmlmapper-java/issues/132)
* [Fix wrapper.execute when rml:source is a path](https://github.com/RMLio/rmlmapper-java-wrapper-js/issues/23)


