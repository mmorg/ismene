
# a revoir hrrdf: 

* définition du "search" : rml/data/hrrdf/4.3.0/Common/jsonld/search

* question des diff toujours différents sur certaines entitées: voir ce test dans yarrrml2rdf: EmployeeType -> SpecialClassificationTypeArray
 * --> c'est sans doute une histoire de string encoding
 


# tranformation hrrdf à partir des json


* pour le shacl: un exemple de required: ontologies existantes/ontologies xml/HR-XML/4.3.0 FINAL/Common/json/base/YearMonthPeriodType.json

* a revoir: une définition d'object 'inline' dans le items[type=array]: ontologies existantes/ontologies xml/HR-XML/4.3.0 FINAL/Common/json/profile/LicenseType.json

* avec une `minproperties` & des `not.required` : ontologies existantes/ontologies xml/HR-XML/4.3.0 FINAL/Common/json/work/ScheduleType.json

# generation du shacl pour hrrdf :

* required : see TimecardType / QuantityAmount
* 'minItems' : "The violations or transgressions against the license"


# Open questions / issues : 

* issues list: https://gitlab.com/mmorg/hrrdf/-/issues

## partis pris: 

* traitement des *TypeArray => //syntactic sugar// for jsonSchema, this is by default in RDF that any range can be an array.


## !!! mise à jour et re-synchonisation avec datalab-s
* faire un diff entre la branche [nodefr-state](https://gitlab.com/datalab-s/ontologies/-/tree/nodefr-state) et la branche courante sur datalab-s
  * au niveau du gatsby 
  * au niveau de la version RML a remettre dans nodefr

----

# pour l'autogénération hrrdf 

* une propriété intéressante : id: 'hrrdf:applicationSms',


# A noter et à partager : 

* il y a dans hr-xml un sous-dossier de recruiting qui s'appelle JDX. 
  * J'ai fait une recherche rapide, et c'est à confirmer, mais il semblerait que ce soit la "ré-intégration" d'un travail de rdfisation des offres d'emploi réalisé par la fondation de la chambre du commerce US. 
    * un billet assez long (que j'ai lu que par partie), mais qui est intéressant : https://www.t3networkhub.org/post/jdx-a-schema-for-job-data-exchange
    * le "site officiel" du JDX: https://www.uschamberfoundation.org/jdx/job-schema
==> on ne pourrais pas utiliser directement la version rdfs ?


