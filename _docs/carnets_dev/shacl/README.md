
* pour la définition des classes abstraites : https://datashapes.org/dash#abstract-classes
  * => a modifier dans nodefr

# extension shacl-js 

* Dash: https://datashapes.org/dash#abstract
  * https://github.com/TopQuadrant/shacl-js/blob/master/vocabularies/dash.ttl

* exemple de déclaration et d'instanciation d'un paramètre: 
  * https://www.w3.org/TR/shacl-af/ dans le paragraphe "3.2 SPARQL-based Target Types"

* exemple de déclaration d'un contraintComponent : https://www.w3.org/TR/shacl/#an-example-sparql-based-constraint-component


* Travaux de définition d'une contrainte 'isInCollectionOrScheme' : voir les travaux en cours dans `gatsby/data/mmShapes`


* A revoir : 
- pour la transformation, les "triple rules" qui sont décrit ici: https://www.w3.org/TR/shacl-af/


* documentation : 
- très bonne présentation des "composants de base" du shacl en pdf et ici https://www.topquadrant.com/shacl-features-and-specifications/
