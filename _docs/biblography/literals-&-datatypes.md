
* list of build in datatypes : https://www.w3.org/TR/rdf11-concepts/#section-Datatypes

* for language-tagged string the datatype to use is : http://www.w3.org/1999/02/22-rdf-syntax-ns#langString
  * https://www.w3.org/TR/rdf11-concepts/#dfn-language-tagged-string
  