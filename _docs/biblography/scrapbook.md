
* OWL : 
  * ttl definition : cf file (check if owl2)
  * https://www.w3.org/TR/owl2-primer/
  * https://www6.inrae.fr/mia-paris/content/download/4478/42224/version/1/file/cours+OWL.pdf (cf file)
  * https://www.w3.org/TR/owl-guide/#owl_minCardinality
    * attention : ancienne version, mais toujours d'actualité sur certains points
  * converter xml / json-ld : https://www.easyrdf.org/converter


* Les différents models d'inspiration : 
* https://www.dublincore.org/specifications/dublin-core/dcmi-terms/
  * et la version RDF : https://www.dublincore.org/specifications/dublin-core/dcmi-terms/dublin_core_terms.rdf
* https://ec.europa.eu/esco/resources/data/static/model/html/model.xhtml
* http://pub.tenforce.com/schemas/iso25964/skos-thes#status
  * pour la définition du status : 

* prov-o : https://www.w3.org/TR/prov-o/
* 


* primary source of inspiration for the nodeFr-2 model : https://gist.github.com/stain/7690362

