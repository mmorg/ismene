// @ts-ignore
import { describe, expect, it, beforeAll } from '@jest/globals'
import { graphql, GraphQLSchema } from 'graphql'
import { entities2graphql, getDataLoader } from '@mmorg/rdfx-graphql'
import elasticConf from './_confs/elasticConf.js'
import ontology from './_confs/ontology.js'
import {jest} from '@jest/globals'
// cd services/jobs-and-skills-api
// pnpm test -- --watch queries.online.test.js


describe('Data test : query multiple levels normalized or not', () => {
  let makeQuery

  beforeAll(() => {
    const schemaDefinition = entities2graphql(ontology)
    const schema = new GraphQLSchema(schemaDefinition)

    // this is real online dataLoader
    const dataLoader = getDataLoader(elasticConf.current)

    makeQuery = async (query, variables) => graphql({
      schema,
      contextValue: { dataLoader },
      source: query,
      variableValues: variables,
    })
  })

  it('should resolve prefLabels of Skill Tree', async () => {
    jest.setTimeout(40000)
    const query = `
    query getSkillsTree {
      allSkill {
        id
        prefLabel {
          value
          language
        }
        broader {
          id
          prefLabel {
            value
            language
          }
          broader {
            prefLabel {
              value
              language
            }
          }
        }
      }
    }`

    const variableValues = {
      // "page": 1,
      // "limit": 10,
      // "sort": "id:asc",
    }

    const result = await makeQuery(query, variableValues)

    // @TODO: define a filter or/and order to be sure to always get this skill first
    const skillfff = result.data.allSkill[0]
    expect(skillfff.id).toBe('skill:fff74a70-4f82-4949-9c3b-b335904cf927')
    expect(skillfff.prefLabel[0]).toEqual({
      value: 'placer des garde-corps et des bastaings',
      language: 'fr'
    })
    const broaderfff = skillfff.broader[0]
    // see : https://dashemploi.kb.europe-west1.gcp.cloud.es.io:9243/app/enterprise_search/app_search/engines/jobs-skills-fr-dev-4/documents/skill%3AS6.8.2
    expect(broaderfff.id).toBe('skill:S6.8.2')
    expect(broaderfff.prefLabel[0]).toEqual({
      value: 'mettre en place des pièces à usiner ou des matériaux',
      language: 'fr'
    })

  })

  it('should also resolve prefLabels of more generic Tree like skos:Concept', async () => {

    const query = `
    query genericConceptTree {
      allConcept {
        id
        prefLabel {
          language
          value
        }
        broader {
          id
          prefLabel {
            value
            language
          }
        }
      }
    }`

    const variableValues = {
      // "page": 1,
      // "limit": 10,
      // "sort": "id:asc",
    }

    const result = await makeQuery(query, variableValues)

    // @TODO: define a filter or and order to be sure to always get this skill first
    const skillfff = result.data.allConcept[0]
    expect(skillfff.id).toBe('skill:fff74a70-4f82-4949-9c3b-b335904cf927')
    expect(skillfff.prefLabel[0]).toEqual({
      value: 'placer des garde-corps et des bastaings',
      language: 'fr'
    })
    const broaderfff = skillfff.broader[0]
    // see : https://dashemploi.kb.europe-west1.gcp.cloud.es.io:9243/app/enterprise_search/app_search/engines/jobs-skills-fr-dev-4/documents/skill%3AS6.8.2
    expect(broaderfff.id).toBe('skill:S6.8.2')
    expect(broaderfff.prefLabel[0]).toEqual({
      value: 'mettre en place des pièces à usiner ou des matériaux',
      language: 'fr'
    })

  })

  it.todo('Make a query to link esco:Occupation and esco:Skill. But this need a fix in the esco model generation like stated here: gatsby/data/esco/README.md')
})
