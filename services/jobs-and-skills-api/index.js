// @ts-ignore
import { ApolloServer } from "@apollo/server";
// @ts-ignore
import { startStandaloneServer } from "@apollo/server/standalone";
// @ts-ignore
import { readJson } from "@mmorg/fsutils";
import elasticConf from "./_confs/elasticConf.js";

// This is a code first schema adapted from :
// https://github.com/AstrumU/graphql-authz/blob/main/examples/packages/apollo-server-code-first/src/index.ts

import { GraphQLObjectType, GraphQLSchema } from "graphql";
import { entities2graphql, getDataLoader } from "@mmorg/rdfx-graphql";
import { ld2aggregate } from "@mmorg/rdfx-graphql-aggregate";
import ontology from "./_confs/ontology.js";

// Ontology
// @TODO: for now it's a minimal version of skos. A layer & compact should be done for jas (esco+rome+pcs specifics)
// -- skos
const ontoFolder_agg = `skos-1.3.0-gen_v1`;
const ontoPath_agg = `./data/${ontoFolder_agg}/v1.jsonld`;
const onto_agg = readJson(ontoPath_agg);

const schemaDefinition = entities2graphql(ontology);
// @TODO: Gregg: is this still valid ? Anyway, this should load another ontology no ?
const aggregateQueries = ld2aggregate(onto_agg);

schemaDefinition.query = new GraphQLObjectType({
  ...schemaDefinition.query.toConfig(),
  fields: {
    ...schemaDefinition.query.toConfig().fields,
    ...aggregateQueries,
  },
});

// console.log(schemaDefinition.query.toConfig().fields)

const schema = new GraphQLSchema(schemaDefinition);

// @TODO: clean the dataLoaderImplementation
const dataLoader = getDataLoader(elasticConf.current, ontology);
// The ApolloServer constructor requires two parameters: your schema
// definition and your set of resolvers.
const server = new ApolloServer({
  schema,
});

// Passing an ApolloServer instance to the `startStandaloneServer` function:
//  1. creates an Express app
//  2. installs your ApolloServer instance as middleware
//  3. prepares your app to handle incoming requests
const { url } = await startStandaloneServer(server, {
  listen: { port: 5010 },
  // @ts-ignore
  context: async (args) => {
    // console.log('In the context')
    return {
      dataLoader,
    };
  },
});

console.log(`🚀  Server ready at: ${url}`);
