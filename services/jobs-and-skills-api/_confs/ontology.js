// @ts-ignore
import { readJson } from "@mmorg/fsutils";

const ontoPath = `../../data/rome/rome-4.0.0.jsonld`;
const ontology = readJson(ontoPath);
export default ontology;
