import elasticKeys from './elasticKeys.json' assert {type: 'json'}

const {baseUrl,  apiKey} = elasticKeys

const prod = {
  apiKey,
  baseUrl, 
  engineName: 'xxxx',
}

const preProd = {
  apiKey,
  baseUrl,
  engineName: 'xxx'
}

const dev = {
  apiKey,
  baseUrl,
  engineName: 'jobs-skills-fr-dev-4'
}

const current = dev

const elasticConf = { current }
export default elasticConf

