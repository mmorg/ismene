import elasticKeys from './elasticKeys.json'

const {cloudInstance: baseUrl,  apiKey} = elasticKeys

const prod = {
  apiKey,
  baseUrl, 
  engineName: 'xxxx',
}

const preProd = {
  apiKey,
  baseUrl,
  engineName: 'xxx'
}

const dev = {
  apiKey,
  baseUrl,
  engineName: 'rdfx-gen-v1-dev'
}

const current = dev

const elasticConf = { current }
export default elasticConf

