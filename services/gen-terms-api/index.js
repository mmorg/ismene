// @ts-ignore
import { ApolloServer } from '@apollo/server'
// @ts-ignore
import { startStandaloneServer } from '@apollo/server/standalone'
// @ts-ignore
import { readJson } from '@mmorg/fsutils'

// This is a code first schema adapted from : 
// https://github.com/AstrumU/graphql-authz/blob/main/examples/packages/apollo-server-code-first/src/index.ts

import {
  GraphQLSchema
} from 'graphql';
import {entities2graphql, getDataLoader} from '@mmorg/rdfx-graphql'


// Ontology
const ontoFolder = `skos-1.3.0-gen_v1`
const ontoPath = `./data/${ontoFolder}/skos_X.X.X_gl-realm-compacted:gen_v1.jsonld`
const onto = readJson(ontoPath) 

const schemaDefinition = entities2graphql(onto)
const schema = new GraphQLSchema(schemaDefinition)

// @TODO: clean the dataLoaderImplementation
const dataLoader = getDataLoader()
 

// The ApolloServer constructor requires two parameters: your schema
// definition and your set of resolvers. 
const server = new ApolloServer({
  schema,
});

// Passing an ApolloServer instance to the `startStandaloneServer` function:
//  1. creates an Express app
//  2. installs your ApolloServer instance as middleware
//  3. prepares your app to handle incoming requests
const { url } = await startStandaloneServer(server, {
  listen: { port: 5000 },
  // @ts-ignore
  context: async (args) => {
    // console.log('In the context')
    return { 
      dataLoader, 
    }
  }
});

console.log(`🚀  Server ready at: ${url}`);