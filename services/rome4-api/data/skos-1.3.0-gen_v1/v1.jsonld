{
	"@context": {
		"id": "@id",
		"graph": {
			"@id": "@graph",
			"@container": "@set"
		},
		"type": {
			"@id": "@type",
			"@container": "@set"
		},
		"rdfs": "http://www.w3.org/2000/01/rdf-schema#",
		"dct": "http://purl.org/dc/terms/",
		"qb": "http://purl.org/linked-data/cube#",
		"domain": {
			"@id": "rdfs:domain",
			"@type": "@id",
			"@container": "@set"
		},
		"range": {
			"@id": "rdfs:range",
			"@type": "@id",
			"@container": "@set"
		},
		"label": {
			"@id": "rdfs:label",
			"@container": "@set"
		},
		"comment": {
			"@id": "rdfs:comment",
			"@container": "@set"
		},
		"subPropertyOf": {
			"@id": "rdfs:subPropertyOf",
			"@type": "@id",
			"@container": "@set"
		},
		"isDefinedBy": {
			"@id": "rdfs:isDefinedBy",
			"@container": "@set"
		},
		"seeAlso": {
			"@id": "rdfs:seeAlso",
			"@container": "@set"
		},
		"title": {
			"@id": "dct:title",
			"@container": "@set"
		},
		"description": {
			"@id": "sch:description",
			"@container": "@set"
		},
		"owl": "http://www.w3.org/2002/07/owl#",
		"inverseOf": {
			"@id": "owl:inverseOf",
			"@type": "@id",
			"@container": "@set"
		},
		"disjointWith": {
			"@id": "owl:disjointWith",
			"@type": "@id",
			"@container": "@set"
		},
		"contributor": {
			"@id": "dct:contributor",
			"@container": "@set"
		},
		"creator": {
			"@id": "dct:creator",
			"@container": "@set"
		},
		"skos": "http://www.w3.org/2004/02/skos/core#",
		"inScheme": {
			"@id": "skos:inScheme",
			"@type": "@id",
			"@container": "@set"
		},
		"definition": {
			"@id": "skos:definition",
			"@container": "@set"
		},
		"scopeNote": {
			"@id": "skos:scopeNote",
			"@container": "@set"
		},
		"example": {
			"@id": "skos:example",
			"@container": "@set"
		},
		"sch": "http://schema.org/",
		"xsd": "http://www.w3.org/2001/XMLSchema#",
		"ex": "http://example.com#",
		"stats": "http://example.com/stats#"
	},
	"graph": [
		{
			"id": "skos:Ontology",
			"type": [
				"owl:Ontology"
			],
			"contributor": [
				"Dave Beckett",
				"Nikki Rogers",
				"Participants in W3C's Semantic Web Deployment Working Group."
			],
			"creator": [
				"Alistair Miles",
				"Sean Bechhofer"
			],
			"dct:description": {
				"@language": "en",
				"@value": "An RDF vocabulary for describing the basic structure and content of concept schemes such as thesauri, classification schemes, subject heading lists, taxonomies, 'folksonomies', other types of controlled vocabulary, and also concept schemes embedded in glossaries and terminologies."
			},
			"title": [
				{
					"@language": "en",
					"@value": "SKOS Vocabulary"
				}
			],
			"seeAlso": [
				{
					"id": "http://www.w3.org/TR/skos-reference/"
				}
			]
		},
		{
			"id": "skos:Collection",
			"type": [
				"owl:Class",
				"rdfs:Class"
			],
			"isDefinedBy": [
				{
					"id": "http://www.w3.org/2004/02/skos/core"
				}
			],
			"label": [
				{
					"@language": "en",
					"@value": "Collection"
				}
			],
			"disjointWith": [
				"skos:Concept",
				"skos:ConceptScheme"
			],
			"definition": [
				{
					"@language": "en",
					"@value": "A meaningful collection of concepts."
				}
			],
			"inScheme": [
				"skos:scheme_for_rdfs:Class_generated"
			],
			"scopeNote": [
				{
					"@language": "en",
					"@value": "Labelled collections can be used where you would like a set of concepts to be displayed under a 'node label' in the hierarchy."
				}
			]
		},
		{
			"id": "skos:Concept",
			"type": [
				"owl:Class",
				"rdfs:Class"
			],
			"isDefinedBy": [
				{
					"id": "http://www.w3.org/2004/02/skos/core"
				}
			],
			"label": [
				{
					"@language": "en",
					"@value": "Concept"
				}
			],
			"definition": [
				{
					"@language": "en",
					"@value": "An idea or notion; a unit of thought."
				}
			],
			"inScheme": [
				"skos:scheme_for_rdfs:Class_generated"
			]
		},
		{
			"id": "skos:altLabel",
			"type": [
				"rdf:Property",
				"owl:AnnotationProperty"
			],
			"comment": [
				{
					"@language": "en",
					"@value": "The range of skos:altLabel is the class of RDF plain literals."
				},
				{
					"@language": "en",
					"@value": "skos:prefLabel, skos:altLabel and skos:hiddenLabel are pairwise disjoint properties."
				}
			],
			"isDefinedBy": [
				{
					"id": "http://www.w3.org/2004/02/skos/core"
				}
			],
			"label": [
				{
					"@language": "en",
					"@value": "alternative label"
				}
			],
			"subPropertyOf": [
				"rdfs:label"
			],
			"definition": [
				{
					"@language": "en",
					"@value": "An alternative lexical label for a resource."
				}
			],
			"example": [
				{
					"@language": "en",
					"@value": "Acronyms, abbreviations, spelling variants, and irregular plural/singular forms may be included among the alternative labels for a concept. Mis-spelled terms are normally included as hidden labels (see skos:hiddenLabel)."
				}
			],
			"inScheme": [
				"skos:scheme_for_rdf:Property_generated"
			],
			"domain": [
				"rdfs:Resource",
				"skos:Collection",
				"skos:Concept",
				"skos:ConceptScheme",
				"skos:OrderedCollection"
			],
			"range": [
				"rdfs:Literal"
			]
		},
		{
			"id": "skos:broader",
			"type": [
				"rdf:Property",
				"owl:ObjectProperty"
			],
			"comment": [
				{
					"@language": "en",
					"@value": "Broader concepts are typically rendered as parents in a concept hierarchy (tree)."
				}
			],
			"isDefinedBy": [
				{
					"id": "http://www.w3.org/2004/02/skos/core"
				}
			],
			"label": [
				{
					"@language": "en",
					"@value": "has broader"
				}
			],
			"subPropertyOf": [
				"skos:broaderTransitive",
				"skos:semanticRelation"
			],
			"inverseOf": [
				"skos:narrower"
			],
			"definition": [
				{
					"@language": "en",
					"@value": "Relates a concept to a concept that is more general in meaning."
				}
			],
			"inScheme": [
				"skos:scheme_for_rdf:Property_generated"
			],
			"scopeNote": [
				{
					"@language": "en",
					"@value": "By convention, skos:broader is only used to assert an immediate (i.e. direct) hierarchical link between two conceptual resources."
				}
			],
			"domain": [
				"skos:Concept"
			],
			"range": [
				"skos:Concept"
			]
		},
		{
			"id": "skos:definition",
			"type": [
				"rdf:Property",
				"owl:AnnotationProperty"
			],
			"isDefinedBy": [
				{
					"id": "http://www.w3.org/2004/02/skos/core"
				}
			],
			"label": [
				{
					"@language": "en",
					"@value": "definition"
				}
			],
			"subPropertyOf": [
				"skos:note"
			],
			"definition": [
				{
					"@language": "en",
					"@value": "A statement or formal explanation of the meaning of a concept."
				}
			],
			"inScheme": [
				"skos:scheme_for_rdf:Property_generated"
			],
			"domain": [
				"skos:Concept",
				"rdfs:Resource",
				"owl:Thing",
				"skos:Collection",
				"skos:ConceptScheme",
				"skos:OrderedCollection"
			],
			"range": [
				"xsd:string",
				"rdf:langString",
				"rdf:HTML",
				"rdf:XMLLiteral"
			]
		},
		{
			"id": "skos:member",
			"type": [
				"rdf:Property",
				"owl:ObjectProperty"
			],
			"domain": [
				"skos:Collection"
			],
			"isDefinedBy": [
				{
					"id": "http://www.w3.org/2004/02/skos/core"
				}
			],
			"label": [
				{
					"@language": "en",
					"@value": "has member"
				}
			],
			"range": [
				"skos:Collection",
				"skos:Concept"
			],
			"definition": [
				{
					"@language": "en",
					"@value": "Relates a collection to one of its members."
				}
			],
			"inScheme": [
				"skos:scheme_for_rdf:Property_generated"
			]
		},
		{
			"id": "skos:narrower",
			"type": [
				"rdf:Property",
				"owl:ObjectProperty"
			],
			"comment": [
				{
					"@language": "en",
					"@value": "Narrower concepts are typically rendered as children in a concept hierarchy (tree)."
				}
			],
			"isDefinedBy": [
				{
					"id": "http://www.w3.org/2004/02/skos/core"
				}
			],
			"label": [
				{
					"@language": "en",
					"@value": "has narrower"
				}
			],
			"subPropertyOf": [
				"skos:narrowerTransitive",
				"skos:semanticRelation"
			],
			"inverseOf": [
				"skos:broader"
			],
			"definition": [
				{
					"@language": "en",
					"@value": "Relates a concept to a concept that is more specific in meaning."
				}
			],
			"inScheme": [
				"skos:scheme_for_rdf:Property_generated"
			],
			"scopeNote": [
				{
					"@language": "en",
					"@value": "By convention, skos:broader is only used to assert an immediate (i.e. direct) hierarchical link between two conceptual resources."
				}
			],
			"domain": [
				"skos:Concept"
			],
			"range": [
				"skos:Concept"
			]
		},
		{
			"id": "skos:prefLabel",
			"type": [
				"rdf:Property",
				"owl:AnnotationProperty"
			],
			"comment": [
				{
					"@language": "en",
					"@value": "A resource has no more than one value of skos:prefLabel per language tag, and no more than one value of skos:prefLabel without language tag."
				},
				{
					"@language": "en",
					"@value": "The range of skos:prefLabel is the class of RDF plain literals."
				},
				{
					"@language": "en",
					"@value": "skos:prefLabel, skos:altLabel and skos:hiddenLabel are pairwise\n      disjoint properties."
				}
			],
			"isDefinedBy": [
				{
					"id": "http://www.w3.org/2004/02/skos/core"
				}
			],
			"label": [
				{
					"@language": "en",
					"@value": "preferred label"
				}
			],
			"subPropertyOf": [
				"rdfs:label"
			],
			"definition": [
				{
					"@language": "en",
					"@value": "The preferred lexical label for a resource, in a given language."
				}
			],
			"inScheme": [
				"skos:scheme_for_rdf:Property_generated"
			],
			"domain": [
				"rdfs:Resource",
				"skos:Collection",
				"skos:Concept",
				"skos:ConceptScheme",
				"skos:OrderedCollection"
			],
			"range": [
				"rdfs:Literal"
			]
		},
		{
			"id": "skos:scheme_for_rdfs:Class_generated",
			"type": [
				"skos:ConceptScheme",
				"om:defaultScheme"
			],
			"title": [
				{
					"@language": "en",
					"@value": "Default Level"
				},
				{
					"@language": "fr",
					"@value": "Niveau par défaut"
				}
			],
			"description": [
				{
					"@language": "en",
					"@value": "Default scheme for rdfs:Class - autogenerated"
				},
				{
					"@language": "fr",
					"@value": "Scheme par défaut pour les rdfs:Class - autogenéré"
				}
			]
		},
		{
			"id": "skos:scheme_for_rdf:Property_generated",
			"type": [
				"skos:ConceptScheme",
				"om:defaultScheme"
			],
			"title": [
				{
					"@language": "en",
					"@value": "Default Level"
				},
				{
					"@language": "fr",
					"@value": "Niveau par défaut"
				}
			],
			"description": [
				{
					"@language": "en",
					"@value": "Default scheme for rdf:Property - autogenerated"
				},
				{
					"@language": "fr",
					"@value": "Scheme par défaut pour les rdf:Property - autogenéré"
				}
			]
		},
		{
			"id": "skos:memberOf",
			"type": [
				"rdf:Property",
				"owl:ObjectProperty"
			],
			"domain": [
				"skos:Collection",
				"skos:Concept"
			],
			"range": [
				"skos:Collection"
			],
			"label": [
				{
					"@language": "en",
					"@value": "MemberOf"
				}
			],
			"definition": [
				{
					"@language": "en",
					"@value": "Relates members to collections"
				}
			]
		},
		{
	        "id": "ex:JobsAndSkillsCube",
	        "type": [
	            "qb:DataStructureDefinition"
	        ],
	        "qb:dimension": [
	            "broader",
	            "match_with__id"
	        ],
	        "qb:measure": [
	            "count"
	        ]
	    },
	    {
	        "id": "stats:broader",
	        "type": [
	            "rdf:Property",
                "qb:DimensionProperty"
	        ]
	    },
	    {
	        "id": "stats:narrower",
	        "type": [
	            "rdf:Property",
                "qb:DimensionProperty"
	        ]
	    },
	    {
	        "id": "stats:type",
	        "type": [
	            "rdf:Property",
                "qb:DimensionProperty"
	        ]
	    }
	]
}
