# rome4-api

## 1.0.3

### Patch Changes

- Updated dependencies
  - @mmorg/rdfx-graphql@1.2.0

## 1.0.2

### Patch Changes

- Updated dependencies
  - @mmorg/rdfx-graphql@1.1.1

## 1.0.1

### Patch Changes

- Updated dependencies
  - @mmorg/rdfx-graphql@1.1.0
