// @ts-ignore
import { describe, expect, it, beforeAll } from '@jest/globals'
import { graphql, GraphQLSchema } from 'graphql'
import { entities2graphql, getDataLoader } from '@mmorg/rdfx-graphql'
import elasticConf from './_confs/elasticConf.js'
import ontology from './_confs/ontology.js'
import { jest } from '@jest/globals'
// cd services/jobs-and-skills-api
// pnpm test -- --watch queries.online.test.js

let makeQuery

jest.setTimeout(40000)

beforeAll(() => {
  const schemaDefinition = entities2graphql(ontology)
  const schema = new GraphQLSchema(schemaDefinition)

  // this is real online dataLoader
  const dataLoader = getDataLoader(elasticConf.current, ontology)

  makeQuery = async (query, variables) => graphql({
    schema,
    contextValue: { dataLoader },
    source: query,
    variableValues: variables,
  })
})

describe('Data test : query multiple levels normalized or not', () => {

  it('should resolve a skos:Collection with members', async () => {
    jest.setTimeout(40000)
    const query = `
    query rome4AllCollection($start: Int, $limit: Int) {
      allCollection(start: $start, limit: $limit) {
        id
        prefLabel {
          value
          language
        },
        member {
          id
          prefLabel {
            value
          }
        }
      }
    }
    `

    const variableValues = {
      start: 1,
      limit: 2,
    }

    const result = await makeQuery(query, variableValues)

    // Define "structural tests" to not be dependant of the returned value
    const collection1 = result.data.allCollection[0]
    expect(collection1.member.length > 0).toBe(true)
    const member0 = collection1.member[0]
    expect(member0.prefLabel).not.toBeUndefined()

  })


})

describe('Content test: test hierarchies of data', () => {
  const lineStarters = ['', '*', '-', '~', '>', '§', '%']
  const printQuery = (entities, level) => {
    const localLines = []
    for (const e of entities) {
      localLines.push(`${' '.repeat(2 * level)}${lineStarters[level]} ${e.prefLabel[0].value}`)
      // do recurse
      if (e.narrower && e.narrower.length) {
        const lines = printQuery(e.narrower, level + 1)
        localLines.push(...lines)
      }

    }
    return localLines
  }
  const printResult = (data) => {
    // display 
    const lines = []
    const keys = Object.keys(data)
    for (const k of keys) {
      lines.push(`${k} : `)

      const level = 1
      const entities = data[k]
      lines.push(...printQuery(entities, level))
    }

    const display = lines.join('\n')
    console.log(display)
      
  }

  it('should export the mutitples work-environments:', async () => {
    jest.setTimeout(40000)
    const query = `
    query rome4WorkConditions {
      allBeneficiary {
        id
        prefLabel {
          value
        }
      }
      allConditions {
        prefLabel {
          value
        }
      }
      allHours {
        prefLabel {
          value
        }
      }
      allStructureType {
        prefLabel {
          value
        }
      }
      allTravel {
        prefLabel {
          value
        }
      }
    }    
    `

    const variableValues = {
      start: 1,
      // limit: 2, 
    }

    const result = await makeQuery(query, variableValues)

    const { data } = result
    expect(Object.keys(data).length).toBe(5)
    expect(data.allConditions.length).toBe(10)

    // printResult(data)
  })

  it('get all the level 2 (stake) of a level1 (skillDomain):', async () => {
    jest.setTimeout(50000)
    const query = `
    query rome4SkillsTree {
      allSkillDomain {
        id
        prefLabel {
          value
        }
        narrower {
          id
          prefLabel {
            value
          }
        }
      }
    }      
    `

    const variableValues = {
      start: 1,
      limit: 1, 
    }

    const result = await makeQuery(query, variableValues)
    
    const { data } = result
    expect(Object.keys(data).length).toBe(1)

    // printResult(data)

    // this skillDomain group "Savoir-faire" & "Savoir-être profesionnel"
    expect(data.allSkillDomain.length).toBe(6)

    // the first one is the "Savoir-être professionel"
    const [cooperationAndAction, pilotageDomain] = data.allSkillDomain

    expect(cooperationAndAction.narrower.length).toBe(1)
    // that the 2nd one "Pilotage, Gestion, Cadre réglementaire" has 4 children
    expect(pilotageDomain.narrower.length).toBe(6)


  })

  it.todo('Restaure this test when filtering is okay')
  it.skip('should export a "Domain" and his tree of skills:', async () => {
    jest.setTimeout(50000)
    const query = `
    query rome4SkillsTree {
      allSkillDomain {
        id
        prefLabel {
          value
        }
        narrower {
          id
          prefLabel {
            value
          }
          narrower {
            id
            prefLabel {
              value
            }
            narrower {
              id
              prefLabel {
                value
              }
              narrower {
                id
                prefLabel {
                  value
                }
                narrower {
                  id
                }
              }
            }
          }
        }
      }
    }      
    `

    const variableValues = {
      start: 1,
      limit: 1, 
    }

    const result = await makeQuery(query, variableValues)

    const { data } = result
    expect(Object.keys(data).length).toBe(1)

    printResult(data)

    // @TODO : theses are specific tests for a bug, to be extracted to a specific test that filter by id 
    // expect(data.allConditions.length).toBe(10)

    expect(data.allSkillDomain.length).toBe(1)

    // ces 2 ne doivent pas y être : 
    // Réaliser un retour d'expériences
    // Piloter une activité ? 



  })

  // @TODO: create a describe for the knowledges
  // indexes lists: 
  // 1 : Habilitations
  // 2 : Certifications
  // 3 : Techniques manuelles, artisanales, industrielles
  // 4 : Autres techniques professionnelles
  // 5 : Véhicules
  // 6 : Normes et procédés (!!! il n'y a pas de narrower)
  // 7 : Produits finis, services
  // 8 : Outils, machines, équipement matériel
  // 9 : Mode, textile et accessoires
  // 10 : Aliments et spécialités culinaires
  // ...
  it('Get the certifications', async () =>{
    // @TODO: do a search by id, to be sure to get this one 

    const query = `
    query rome4Knowledge($limit: Int, $start: Int) {
      allKnowledgeCategory(limit: $limit, start: $start) {
        id
        prefLabel {
          value
        }
        narrower {
          prefLabel {
            value
          }
        }
      }
    }      
    `

    const variableValues = {
      "start": 2,
      "limit": 1,
    }

    const result = await makeQuery(query, variableValues)

    const { data } = result
    // printResult(data)
    const { allKnowledgeCategory } = data
    const [certif] = allKnowledgeCategory
    expect(certif.prefLabel[0].value).toBe('Certifications')
    expect(certif.narrower.length).toBe(212)
  })
})