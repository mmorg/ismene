// @ts-ignore
import { ApolloServer } from '@apollo/server'
// @ts-ignore
import { startStandaloneServer } from '@apollo/server/standalone'
// @ts-ignore
// import { readJson } from '@mmorg/fsutils'
import {
  GraphQLObjectType,
  GraphQLSchema
} from 'graphql'
import { entities2graphql, getDataLoader } from '@mmorg/rdfx-graphql'
// import { ld2aggregate } from '@mmorg/rdfx-graphql-aggregate'
import config from './_confs/config.js'
import ontology from './_confs/ontology.js'
import loadWorldGraph from './_confs/loadWorldGraph.js'

 
const schemaDefinition = entities2graphql(ontology)

// Ontology aggregation feature:
// @TODO: Gregg: is this still valid ? Anyway, this should load another ontology no ? 
// const ontoFolder_agg = `skos-1.3.0-gen_v1`
// const ontoPath_agg = `./data/${ontoFolder_agg}/v1.jsonld`
// const onto_agg = readJson(ontoPath_agg)
// const aggregateQueries = ld2aggregate(onto_agg)

schemaDefinition.query = new GraphQLObjectType({
  ...schemaDefinition.query.toConfig(),
  fields: {
    ...schemaDefinition.query.toConfig().fields,
    // ...aggregateQueries 
  }
})

// console.log(schemaDefinition.query.toConfig().fields)

const schema = new GraphQLSchema(schemaDefinition)


// @TODO: clean the dataLoaderImplementation
const worldGraph = loadWorldGraph(ontology)
const dataLoader = getDataLoader(config.elastic, ontology, worldGraph)
// The ApolloServer constructor requires two parameters: your schema
// definition and your set of resolvers.
const server = new ApolloServer({
  schema,
});

// Passing an ApolloServer instance to the `startStandaloneServer` function:
//  1. creates an Express app
//  2. installs your ApolloServer instance as middleware
//  3. prepares your app to handle incoming requests
const { url } = await startStandaloneServer(server, {
  listen: { port: config.port },
  // @ts-ignore
  context: async (args) => {
    // console.log('In the context')
    return {
      dataLoader,
    }
  }
});

console.log(`🚀  Server ready at: ${url}`);
