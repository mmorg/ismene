import { dirname } from 'path'
import { fileURLToPath } from 'url'
const __dirname = dirname(fileURLToPath(import.meta.url))
import { readJson } from '@mmorg/fsutils'


export const ontology = readJson(`${__dirname}/rome-4.0.0.jsonld`)

export default ontology 
