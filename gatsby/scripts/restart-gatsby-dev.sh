#!/bin/sh

docker-compose exec gatsby npm run clean

# do a touch to restart nodemon
docker-compose exec gatsby touch gatsby-config.js

