#!/bin/sh

docker compose exec gatsby bash -c "cd /dev-deps/rml-processor/rml && yalc publish"
docker compose exec gatsby yalc update @mmorg/rml-processor
# @TODO: see how to skip the install and full gatsby reload
# docker compose exec gatsby npm install