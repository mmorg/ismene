import type { GatsbyNode } from 'gatsby'
import path from 'path'
import { slash } from 'gatsby-core-utils'
import prepare4GatsbyNodes from './src/jsonld2Nodes/prepare4GatsbyNodes'
import getSlug from './src/components/componentUtils/getSlug' 
import compactRdfs from './src/jsonld2Nodes/compactRdfs'
import pageFragmentsParser from './src/componentFragmentsParser/pageFragmentsParser'
import chalk from 'chalk'
// import * as pMap from 'p-map/index.js'     
import getGatsbyNodes from './src/gatsbyUtils/getGatsbyNodes.js'
   
 
// enable pooling in docker  
// source: https://github.com/gatsbyjs/gatsby/issues/4109#issuecomment-667628363
// webpack doc: 
// - https://webpack.js.org/configuration/watch/#watchoptions-poll
// - https://webpack.js.org/configuration/watch/#watchoptionsignored
// pointers source: https://github.com/laradock/laradock/issues/1259#issuecomment-345540652
export function onCreateWebpackConfig({ actions }) {
  const watchConf = {
    watchOptions: {
      aggregateTimeout: 500, // delay before reloading
      poll: 1000, // enable polling since fsevents are not supported in docker
      ignored: ['**/node_modules', '**/public']
    }
  }  
  actions.setWebpackConfig(watchConf) 
}


/** 
 *  ======== Start Graphql config part ========
 */

// this is a test for solving graphql recursive self result when an array of link is null in jsonLD.
//    this is due to : https://www.gatsbyjs.com/docs/query-filters/#nulls-and-partial-paths
const linkPropsToForce = [
  { type: 'rdfs:Class', properties: ['subClassOf'] },
  { type: 'skos:Concept', properties: ['broader', 'narrower'] }
]

export async function createSchemaCustomization({ actions }) {
  const { createTypes } = actions
  // This only add possible missing properties
  // @TODO: make a graphql type generator from ontologies models (rdfs, skos,...)
  // and others "virtual" properties to get filtering on them.
  //    if not declared it `createTypes` create resolvers don't set-up filters. see at the end of this paragraph: https://www.gatsbyjs.com/docs/reference/graphql-data-layer/schema-customization/#createresolvers-api

  // definition of "Entity" level :
  //  -> this is the basic substract when all the
  const typeDefs = `
    # utility interface to be able to query multiples entities types
    # @TODO: rename it as RdfsResource
    interface Entity implements Node {
      id: ID!
      # @TODO ? this property should not have to be repeated in implementers right ?
      inScheme: [skos_ConceptScheme] @link
      label: [langString]
    }

    # base type for langString
    type langString implements Node {
      _language: String @proxy(from: "@language")
      _value: String @proxy(from: "@value")
    }

    type rdfs_Datatype implements Node & Entity {
      inScheme: [skos_ConceptScheme] @link
      label: [langString]
    }

    type rdf_Property implements Node & Entity {
      status: [String]
      domain: [rdfs_Class] @link
      range : [Entity] @link
      example: [langString] 
      inScheme: [skos_ConceptScheme] @link
      label: [langString]
    }

    type rdfs_Class implements Node & Entity {
      subClassOf: [rdfs_Class] @link
      status: [String]
      example: [langString]
      isAbstract: Boolean
      inScheme: [skos_ConceptScheme] @link
      label: [langString]
      # @TODO: declare other custom resolvers
      isParentClass: Boolean
    }

    ###### Start skos part

    interface mm_ConceptContainer implements Node {
      id: ID!
    }

    type skos_Collection implements Node & mm_ConceptContainer {
      member: [skos_Concept] @link
      label: [langString]
      comment: [langString]
    }

    type skos_ConceptScheme implements Node & mm_ConceptContainer {
      types: [String]
      hasInScheme: [Entity]
      title: [langString]
    }

    type skos_Concept implements Node & Entity {
      prefLabel: [langString]
      broader: [skos_Concept] @link
      narrower: [skos_Concept] @link
      inScheme: [skos_ConceptScheme] @link
      label: [langString]
    }

    #### start sh part
    type sh_PropertyShape implements Node {
      path: [Node] @link
      maxCount: Int
      minCount: Int
    }

    #### start owl part
    type owl_Ontology implements Node {
      index: [om_OntoIndex] @link
      sch_contributor: [sch_Organisation] @link
      contributor: [langString]
      creator: [langString]
      #modified: [langString]
      versionInfo: [String]
    }

    type om_OntoIndex implements Node {
      records: [mm_ConceptContainer] @link
    }

    #### @TODO: retaure this
    #### start sh part
    type sch_Organisation implements Node {
      logo: [sch_URL] @link
    }

    type sch_URL implements Node {
      propertyToDefine: [String]
    }
  `
  createTypes(typeDefs)
}

export async function createResolvers({ createResolvers }) {
  const resolvers = {
    rdf_Property: {
      hasPropertyShape: {
        type: ['sh_PropertyShape'],
        async resolve(source, args, context, info) {
          const { entries } = await context.nodeModel.findAll({
            query: {
              filter: {
                path: { eq: source.id },
              },
            },
            type: 'sh_PropertyShape',
          })
          return entries
        },
      },
    },
    // add properties to classes
    rdfs_Class: {
      // classTree related resolvers
      hasSubClasses: {
        //@TODO: there is a bug for xsd:string and like entities, always retruning itself
        type: ['rdfs_Class'],
        async resolve(source, args, context, info) {
          const { entries } = await context.nodeModel.findAll({
            type: 'rdfs_Class',
            query: {
              filter: {
                subClassOf: { elemMatch: { id: { eq: source.id } } },
              },
            },

          })
          return entries
        },
      },
      isParentClass: {
        type: 'Boolean',
        async resolve(source, args, context, info) {
          console.log(source)
          if (!source.subClassOf) console.warn("Fix this for the new structure")
          if (!source.subClassOf) return false
          //@TODO: a more robust test with all the possible values : 'rdfs:Resource', 'owl:Thing'
          return source.subClassOf.includes('sch:Thing')
        },
      },

      // propertyGraph related resolvers
      hasProperties: {
        type: ['rdf_Property'],
        async resolve(source, args, context, info) {
          const { entries } = await context.nodeModel.findAll({
            query: {
              filter: {
                domain: { elemMatch: { id: { eq: source.id } } },
              },
            },
            type: 'rdf_Property',
          })
          return entries
        },
      },
      isRangedBy: {
        type: ['rdf_Property'],
        async resolve(source, args, context, info) {
          const { entries } = await context.nodeModel.findAll({
            query: {
              filter: {
                range: { elemMatch: { id: { eq: source.id } } },
              },
            },
            type: 'rdf_Property',
          })
          return entries
        },
      }
    },

    //// @TODO: restaure 
    skos_ConceptScheme: {
      hasInScheme: {
        type: ['Entity'], //['rdfs_Class'],
        async resolve(source, args, context, info) {
          const { entries } = await context.nodeModel.findAll({
            query: {
              filter: {
                inScheme: { elemMatch: { id: { eq: source.id } } },
                // inScheme: { eq: source.id },
              },
            },
            type: 'Entity', //'rdfs_Class',
          })
          return entries
        },
      },
    },
  }
  createResolvers(resolvers)
}

/**
 *  ======== End Graphql config part ========
 */
 
// @TODO : checking configurations of ontologies by :
// 1/ file number and name
// 2/ good structure
// documentation :
// https://www.gatsbyjs.com/docs/reference/config-files/gatsby-node/#onPostBootstrap
// https://www.gatsbyjs.com/docs/reference/config-files/node-api-helpers/#getNodesByType
/*
export async function onPostBootstrap({ getNodes, getNodesByType, ...params}){

  console.log('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
  console.log(params)
  // restart here : get files nodes and check that the 2 files are present
  console.log(getNodesByType('File'))
 
}
*/
export const createPages : GatsbyNode["createPages"] = async ( {
  graphql,
  actions,
}) => {

  const { createPage } = actions

  console.log('==== start page creation') 

  const gatsbyFn = { createPage, graphql } 
 
  // 1/ create rdfs classes
  const dashTreeTemplate6Path = path.resolve(`src/components/dashorm/DashTreeLayout6.js`)
  await createEntitiesPages('allRdfsClass', dashTreeTemplate6Path, gatsbyFn, false)

  // 2/ create rdfs properties
  const dashTreeTemplate7Path = path.resolve(`src/components/dashorm/DashTreeLayout7.js`)
  await createEntitiesPages('allRdfProperty', dashTreeTemplate7Path, gatsbyFn, false)

  // 3/ create conceptScheme pages
  const dashLayoutConceptScheme = path.resolve(`src/components/dashorm/DashLayoutConceptScheme.js`)
  await createEntitiesPages('allSkosConceptScheme', dashLayoutConceptScheme, gatsbyFn, false)

  console.warn('@TODO: restart here !')
  
  // 4/ create skos:Concept pages
  const dashLayoutConcept = path.resolve(`src/components/dashorm/DashLayoutConcept.js`)
  await createEntitiesPages('allSkosConcept', dashLayoutConcept, gatsbyFn, false)

  // 5/ create rdfs:Datatype pages
  const dashLayoutRdfsDatatype = path.resolve(`src/components/dashorm/DashLayoutRdfsDatatype.js`)
  await createEntitiesPages('allRdfsDatatype', dashLayoutRdfsDatatype, gatsbyFn, false)

  // 6/ create om:OntoIndex pages
  const dashLayoutOmOntoIndex = path.resolve(`src/components/dashorm/DashLayoutOmOntoIndex.js`)
  await createEntitiesPages('allOmOntoIndex', dashLayoutOmOntoIndex, gatsbyFn, false)

  // 7/ create owl:Ontology pages
  const dashLayoutOwlOntology = path.resolve(`src/components/dashorm/DashLayoutOwlOntology.js`)
  await createEntitiesPages('allOwlOntology', dashLayoutOwlOntology, gatsbyFn, false)
  
  // 8/ create skos:Collection pages
  const dashLayoutSkosCollection = path.resolve(`src/components/dashorm/DashLayoutSkosCollection.js`)
  await createEntitiesPages('allSkosCollection', dashLayoutSkosCollection, gatsbyFn, false)
  
  console.log('=== end page creation')
}

async function createEntitiesPages(
  graphqlAllKey : String, 
  entityTemplatePath: String, 
  gatsbyFn: Function, 
  isTest = false
  ) {
  const { createPage, graphql } = gatsbyFn 

  // A/ get the node IDs we want to process
  // @TODO: extract this out of this function and pass an array of id to this function
  //      : or extract the main key value from the template ?
  const query = `
  query MyQuery {
   ${graphqlAllKey} {
     nodes {
       id
     }
   }
 }
  `
  const rdfsClassPagesQuery = await graphql(query)

  if (!rdfsClassPagesQuery.data) {
    console.error('The queryfile is empty, may the data not processed ? Do a gatsby clean.')
    return
  }

  let rdfsEntities = rdfsClassPagesQuery.data[graphqlAllKey].nodes
  // for dev : reduce the number of pages
  if (isTest) {
    console.warn('!!!! Test mode activated')
    rdfsEntities = rdfsEntities.slice(0, 5)
  }

  // B/ process the page for each entity

  // 4/ get the page's fragments dependency
  const fragments = pageFragmentsParser(entityTemplatePath)

  // @TODO: a check on same name for multiple fragments and raise an error
  const fragmentsDefs = fragments.map(f => f[1]).join('\n\n')
  // add fragmentKey in PageQuery if the fragment is `on Query`
  const fragmentsKeys = fragments.map(f => f[2] ? `...${f[0]}` : null).filter(f => f).join('\n  ')
  const pageQuery = `
${fragmentsDefs}

query PageQuery($id: String) {
  ${fragmentsKeys}
}
  `
    
  // 7/ create page for each entity
  const concurrencyConf = { concurrency: 50 }

  const mapper = async (conceptNode) => {
    const { id: conceptId } = conceptNode
    // query the related data
    const response = await graphql(pageQuery, conceptNode)
    const pageData = response.data
    if (!pageData) {
      const message = `No page data for conceptid = ${conceptId} . Problem during loading, try to restart`
      console.warn('====', message)
      console.log('-> content of response:',response)
      // throw new Error(message)
    }

    const conceptPath = getSlug(conceptId)

    createPage({
      path: `/${conceptPath}`,
      component: slash(entityTemplatePath),
      context: {
        id: conceptId,
        pageData
      },
    })
  }

  const pagePromises = rdfsEntities.map(mapper)
  await Promise.all(pagePromises)

  // @TODO: use other option under when p-map battle in ESM_WAR is solved
  // await pMap(rdfsEntities, mapper, concurrencyConf)

}

export async function onCreateNode({ node, actions, loadNodeContent }, pluginOptions) {

  const { createNode } = actions

  // ***** 3 ****** Generation from json-LD
  // 3.2 & 3.4/ see here
  // 3.5 / see create pages function
  // console.log('@TODO : restart here')

  if (!(
    node.sourceInstanceName === `jsonLD`
    && node.internal.type === 'File'
    && node.internal.mediaType === 'application/ld+json'
  )) {
    return
  }
  // don't process .md files
  if (node.extension === 'md') return

  //console.log(node)
  console.log('--> start processing', node.name)

  // 1/ transform content of json
  const ldDoc = JSON.parse(await loadNodeContent(node))

  if (!node) console.warn('No node ?', node)
  let preNodes = getGatsbyNodes(node.name, ldDoc)

  // ******************************
  // @TODO: migrate processing of this 2 olds 'std' to the new .graph std
  // ******************************
  
  
  if (node.name.startsWith('nodefr')) {
    console.warn(chalk.bgRedBright('@TODO: migrate & restaure: nodefr'))
    // return 
    const doc = await compactRdfs(ldDoc, 'fr')
    preNodes = await prepare4GatsbyNodes(doc)

    // add "rdfe:menu_publish" inscheme for publish
    addInScheme(preNodes, 'rdfe:menu_publish')

    console.log('@TODO: ?? add here a workaround that create used "external objects" (like schema:Text) ')
    // see file: gatsby/data/schemaorg-current-https.jsonld
    //   this should not be imported "fully". Moreover this should be "redirect" type nodes to generate good pages / redirections
  }

  if (node.name.startsWith('hrrdf')) {
    console.warn(chalk.bgRedBright('@TODO: migrate & restaure: hrrdf'))
    // return 
    const doc = await compactRdfs(ldDoc, 'en')
    preNodes = await prepare4GatsbyNodes(doc)

    // add "rdfe:menu_publish" inscheme for publish
    addInScheme(preNodes, 'rdfe:menu_publish')
  }

  // ******************************
  // END @TODO: migrate processing of this 2 olds 'std' to the new .graph std
  // ******************************

  if (!preNodes) {
    console.log(chalk.red('This file is no pre-processed:', node.name))
    return
  }

  // old processing scheme based on .result
  if (preNodes.results) {
    // pre-processing for graphql properties with @link that can be null on certain node.
    //   theses nodes need to be forced to []
    forceLinkedProperties(preNodes)

    let test = false
    if (test) {
      console.warn('!!!!!!!!!! Test mode activated only some nodes processed')
      const tl = preNodes.results.length
      // keep some classes and some props
      preNodes.results = [preNodes.results.slice(0, 3), preNodes.results.slice(tl - 4, tl - 1)].flat()
      // preNodes.results.splice(4)
    }

    // 1.1 : generate node's content
    //@TODO : save the context somewhere and reference it in nodes

    let count = 0
    let totalLength = preNodes.results.length
    preNodes.results.forEach(n => {
      createNode(n)
      count += 1
      if (count % 10000 === 0) console.log('--> Created Nodes :', count, 'on', totalLength)
    })
    return
  }

  //*********************** */
  // else case : new processing based on graph. preNodes is only entities

  console.warn('@TODO: integrate the getGatsbySources/rdfSourcesConfig processing here to get specific processings')

  // pre-processing for graphql properties with @link that can be null on certain node.
  //   theses nodes need to be forced to []
  forceLinkedProperties_2(preNodes)

  // 1.1 : generate node's content
  let count = 0
  let totalLength = preNodes.length
  preNodes.forEach(n => {
    createNode(n)
    count += 1
    if (count % 10000 === 0) console.log('--> Created Nodes :', count, 'on', totalLength)
  })

  //@TODO ? : save the context in graphQL and reference it in nodes

  
}

function addInScheme(ld, inScheme) {
  ld.results.forEach(n => n.inScheme ? n.inScheme.push(inScheme) : n.inScheme = [inScheme])
  //@TODO: add the inScheme type=ConceptScheme to the ld
}

function addInScheme_2(entities, inScheme) {
  entities.forEach(n => n.inScheme ? n.inScheme.push(inScheme) : n.inScheme = [inScheme])
}

function forceLinkedProperties(ld) {
  linkPropsToForce.forEach(conf => {
    const nodes = ld.results.filter(n => n.types.includes(conf.type))
    nodes.forEach(sourceNode => {
      conf.properties.forEach(prop => {
        if (!sourceNode[prop]) {
          sourceNode[prop] = []
        }
      })
    })

  })
}

function forceLinkedProperties_2(entities) {
  linkPropsToForce.forEach(conf => {
    const nodes = entities.filter(n => n.types.includes(conf.type))
    nodes.forEach(sourceNode => {
      conf.properties.forEach(prop => {
        if (!sourceNode[prop]) {
          sourceNode[prop] = []
        }
      })
    })

  })
}
