import nodefrContext from '../../src/stdContext/nodefrContext.js'
import openBagesContext from '../../src/stdContext/openBagesContext.js'
import rdfsContext from '../../src/stdContext/rdfsContext.js'
import schemaorgContext from '../../src/stdContext/schemaorgContext.js'
import skosContext from '../../src/stdContext/skosContext.js'


// define the ORI context but also an override for "description" that is an alias in many ontologies
// the override defined is `rdfx:description`
const oriLocalContext = {

  // NS
  ori : 'https://ori.competencies.be/ori/',
  rdfx: 'https://rdfx.competencies.be/lts/',

  // Aliases
  experienceStructure: {
    '@id': 'ori:experienceStructure',
    '@type': '@id',
    '@container': '@set'
  },

  experienceType: {
    '@id': 'ori:experienceType',
    '@type': '@id',
    '@container': '@set'
  },

  public: {
    '@id': 'ori:public',
    '@type': '@id',
    '@container': '@set'
  },

  role: {
    '@id': 'ori:role',
    '@type': '@id',
    '@container': '@set'
  },

  tache: {
    '@id': 'ori:tache',
    '@type': '@id',
    '@container': '@set'
  },

  hobby: {
    '@id': 'ori:hobby',
    '@type': '@id',
    '@container': '@set'
  },

  bloom: {
    '@id': 'ori:bloom',
    '@type': '@id',
    '@container': '@set'
  },

  description: {
    '@id': 'rdfx:description',
    '@container': '@set'
  },

  suggestionType: {
    '@id': 'ori:suggestionType',
    '@type': '@id',
    '@container': '@set'
  },

  suggestionChoice: {
    '@id': 'ori:suggestionChoice',
    '@type': '@id',
    '@container': '@set'
  }


}


// @TODO : extract to a more generic function,
//       : but be careful, this is "raw script" : the last win
const oriContext = Object.assign(
  {},
  rdfsContext, schemaorgContext, skosContext,
  openBagesContext,
  nodefrContext,
  oriLocalContext)

export default oriContext
