import getHash from './utils/getHash.js'

const asPayload = (json_value) => ({
  payload: JSON.stringify(json_value),
  messages: [],
  preprocSkip: false,
})

export default async function preProc(vfile) {
  const source = JSON.parse(vfile.value)
  if (Array.isArray(source)) {
    if(!source[0].fields){
      return asPayload(inokufuPreproc(source))
    }
  }
  
  if (Array.isArray(source)
    && source[0].fields
  ) {
    return asPayload(jobreadyPreproc(source))
  }

  if (source.badges) {
    return asPayload(orientoiPreproc(source))
  }

  throw new Error('No preprocessing identified')
}

function inokufuPreproc(source) {

  return source.map(s => {
    if (s.id) return s
    if (!s.url) {
      console.warn('There is no url in this object', s)
      return s
    }
    if (s.url) {
      s.id = getHash(s.url)
      return s
    }
  })
}

function jobreadyPreproc(source) {
  source[0].fields = source[0].fields.map(s => {
    if (s.id) return s
    if (!s.skill.name) {
      console.warn('There is no skill name', s)
      return s
    }
    s.id = getHash(s.skill.name)
    return s
  })

  return source
}

function orientoiPreproc(source) {
  if (Array.isArray(source.badges)) {
    console.warn('@TODO: case to implement, this is not orientoi badges')
    return source
  }
  // badges is an object so process it 
  const entries = Object.entries(source.badges)
  const badges_array = entries.map(([k, v]) => {
    return {
      id: getHash(k),
      label: k,
      number: v,
    }
  })
  source.badges = badges_array
  return source

}