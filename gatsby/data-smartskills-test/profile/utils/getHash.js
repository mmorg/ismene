import { createHash } from 'node:crypto'

/**
 * Returns a SHA256 hash using SHA-3 for the given `content`.
 *
 * @see https://en.wikipedia.org/wiki/SHA-3
 *
 * @param {String} content
 *
 * @returns {String}
 */
export default function getHash(content) {  
  return createHash('sha3-256').update(content).digest('hex')
}