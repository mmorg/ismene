import smartSkillsProdProfile from './smartSkillsProdProfile.js'
import orientoi_1 from '../sources/orientoi_1.json'
import { read } from 'to-vfile'


import yarrrml2rdf from '@mmorg/rml-processor/src/exec/yarrrml2rdf.js'


// docker compose exec gatsby sh -c 'cd jest-esm && npm run testESM -- --watch smartSkillsProdProfile.test'

describe('pre & post processings tests', () => {

  it('Add Ids to inokufu file', async () => {
    const vFile = await read(`${__dirname}/../sources/inokufu_1.json`, 'utf8')
    const r = await smartSkillsProdProfile.sourcePreproc(vFile)
    expect(JSON.parse(r.payload)).toMatchSnapshot()
  })

  it('Change to key/value object in Orientoi badges', async () => {
    const vFile = await read(`${__dirname}/../sources/orientoi_1.json`, 'utf8')
    const r = await smartSkillsProdProfile.sourcePreproc(vFile)
    const content = JSON.parse(r.payload)
    expect(content.badges.length).toBe(6)
    expect(content).toMatchSnapshot()
  })

})

describe('full processing tests', () => {

  it.todo('Verify config and implement the test')
  it.skip('Run for inokufu transform', async () => {
    const params = {
      sources: '**/data-smarskills-test/sources/inokufu_1*',
      mappings: '**/data-smarskills-test/rml/inokufu.yaml',
      targetDirectory: ['oriane', 'test-interop-api'],
      profile: 'defaultProfile',
    }
    const r = await yarrrml2rdf(params)
    console.log(r)
  })
  

})
