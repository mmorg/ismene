// import sameNameCouples from '../rml-profiles-utils/sameNameCouples.js.js'
import oriContext from '../stdContext/oriContext.js'
import preProc from './preProc.js'
import postProc from './postProc.js'
// This is the profile for http-rml transformation 

export default {
  targetContext: oriContext,
  sourcePreproc: preProc,
  postProc,
}


