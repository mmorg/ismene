
// @TODO: review this function as many things in it: 
// - check nonStd ld properties
// - Fix yarrml bug on ~iri
// - change uri id with something like `textUri2camelCase` (can be renamed as `generateSkosIdHashes` ?) 
export default async function postProc(ld, options) {
  // start : check non-stdld properties
  // @TODO: extract and make more usefull during debug
  //    -> 1: check if the property is already declared in json-ld context
  //    -> 2: check if "literals declarations" in context have a @language tag
  //    -> 3: (more difficult) identify the mapping that generated this => need to implement the check for each mapping
  const nonStd = ld.graph.reduce((acc, entity) => {
    const badKeys = Object.keys(entity).filter(k => k.includes(':'))
    if (badKeys.length) {
      acc.badKeys.push(...badKeys)
      acc.entities.push(entity)
    }
    return acc
  }, { badKeys: [], entities: [] })

  const uniques = new Set(nonStd.badKeys)
  if (uniques.size) {
    console.log('Propriétées non formatées en stdld :')
    console.log(`
    ==> 1/ verify ~iri in mapping files (data/rml)
    ==> 2/ Update the context in "gatsby/src/stdContext/oriContext.js"
    `)
    console.log('Non std properties:', uniques, '\n')
    console.log(nonStd.length, 'Affected entities:', nonStd.entities)
  }


  // hack for fixing the ~iri yarrrml/rml bug
  ld.graph.forEach(e => {
    if (e.id.includes('~iri')) e.id = e.id.replace('~iri', '')
  })

  for (const entity of ld.graph) {
    const entries = Object.entries(entity)
    for (const entry of entries) {
      let [key, value] = entry

      const isIdOrArray = key === 'id' || Array.isArray(value)
      if (!isIdOrArray) {
        console.log('To check, property not stdld for key', key, 'full entity', entity)
        continue
      }

      const isArrayOfString = typeof value[0] === 'string'
      if (!isArrayOfString) continue // silent, this is langString or Datatype

      // update and change the value in entity
      // value = key === 'id' ? textUri2camelCase(value) : value.map(textUri2camelCase)
      entity[key] = value
    }
  }

  // check if entity without type
  const withoutType = ld.graph.filter(e => !e.type)
  if (withoutType.length) {
    console.log(withoutType.length, 'entities without type \n', withoutType)
    throw new Error('See previous logs')
  }
  
  return ld
}

// function textUri2camelCase(textURI) {
//   // @TODO: extract this to a const utility ?
//   const splitAndKeep = (splitChar, toSplit) => {
//     const splitted = toSplit.map(t => {
//       const tSecond = t.split(splitChar)
//       return tSecond.map((s, i) => i + 1 >= tSecond.length ? s : [s, splitChar])
//     }).flat(2)

//     return splitted
//   }

//   const cleanAndCamel = (text) => {
//     // really specific cases with bad '/' encoding in yarrml (or in source file)
//     text = text.replace('et%2Fou', 'et ou')

//     // Remove accents and special chars see :
//     //   https://stackoverflow.com/a/37511463
//     //   https://ricardometring.com/javascript-replace-special-characters
//     text = text.normalize('NFKD').replace(/(\p{Diacritic}|[^0-9a-zA-Z\s])/gu, '')
//     return camelcase(text)
//   }

//   if (!textURI.includes('%')) return textURI

//   let toSplit = splitAndKeep('#', [textURI])
//   toSplit = splitAndKeep('/', toSplit)

//   // decode and camelCase the parts with %
//   let decoded = toSplit.map(t => t.includes('%') ? cleanAndCamel(decodeURI(t)) : t).join('')

//   // @TODO : remove this remplacement after tests okay on visualisation part
//   decoded = decoded.replace('#', '/')
//   return decoded
// }