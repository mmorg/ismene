
* Note : 
Many of this processes include a "preprocessing step" that modify the source json. The result of the preprocessing step is saved in `debug` folder with the switch `--savePreProc`

* Example commands:
- inokufu 
```
docker compose exec gatsby npm run toRdf --   -s '**/data-smartskills-test/sources/inokufu*'   -m '**/data-smartskills-test/rml/inokufu.yaml'   -t 'sources' 'results'    -p '**/data-smartskills-test/profile/smartSkillsProdProfile.js'
```

- jobready with save of preprocessed file:
```
docker compose exec gatsby npm run toRdf --   -s '**/data-smartskills-test/sources/jobready_1*'   -m '**/data-smartskills-test/rml/jobready.yaml'   -t 'sources' 'results'    -p '**/data-smartskills-test/profile/smartSkillsProdProfile.js' --savePreProc
```

- orientoi
```
docker compose exec gatsby npm run toRdf --   -s '**/data-smartskills-test/sources/orientoi_1*'   -m '**/data-smartskills-test/rml/orientoi.yaml'   -t 'sources' 'results'    -p '**/data-smartskills-test/profile/smartSkillsProdProfile.js'
```


* To test the transformations: 
- Jest tests don't works because of a strange bug in the esm war, see `gatsby/data-smartskills-test/profile/smartSkillsProdProfile.test.js`
- A manual test for all the tests is available here: http://localhost:1111/api/rml 
