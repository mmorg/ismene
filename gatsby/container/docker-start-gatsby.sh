#!/bin/sh

cd /apps/gatsby
# un-comment to acces others gatsby sites: example:
# cd /apps/min-repro

cresult=$(npm ls -parseable 2>&1)

# source : https://stackoverflow.com/a/24753942
case "$cresult" in
*ERR\!\ missing*) 
	echo "docker-start-gatsby.sh : Fresh start or a least one lib is missing. 'npm install' will run to init the environment"
    # note : `--legacy-peer-deps` for temp fix for @nivo, remove when updated. Actual version: 0.73.0  : https://github.com/plouc/nivo/issues/1256
    # npm install --legacy-peer-deps 
    npm install 
    echo "docker-start-gatsby.sh : End of initial install"

	;;
esac

# Note: nodemon is started from : /usr/local/bin/nodemon
# Use: for debugging, cli options : `--dump -V`
# See: for CLI options https://github.com/remy/nodemon/blob/main/doc/cli/options.txt
# Note: this nodemon is configured to restart gatsby on changes on nodes_modules via nodemon.js
# See : https://github.com/remy/nodemon/blob/master/faq.md#overriding-the-underlying-default-ignore-rules
# Add -L for docker over a mount: https://github.com/remy/nodemon#application-isnt-restarting
nodemon -L --config /apps/gatsby/container/nodemon.json --exec 'npm run clean && npm run start'
