#!/bin/sh

cresult=$(npm ls -parseable 2>&1)

# source : https://stackoverflow.com/a/24753942
case "$cresult" in
*ERR\!\ missing*)
	echo "docker-start-gatsby.sh : Fresh start or a least one lib is missing. 'npm install' will run to init the environment"
    # note : `--legacy-peer-deps` for temp fix for @nivo, remove when updated. Actual version: 0.73.0  : https://github.com/plouc/nivo/issues/1256
    npm install --legacy-peer-deps
    echo "docker-start-gatsby.sh : End of initial install"

	;;
esac

echo 'open browser on http://localhost:2222 to see the served content'

# Note: nodemon is started from : /usr/local/bin/nodemon
# Use: for debugging, cli options : `--dump -V`
# See: for CLI options https://github.com/remy/nodemon/blob/main/doc/cli/options.txt
# Note: this nodemon is configured to restart gatsby on changes on nodes_modules via nodemon.js
# See : https://github.com/remy/nodemon/blob/master/faq.md#overriding-the-underlying-default-ignore-rules
nodemon --config ./container/nodemon-min.json --exec 'npm run cb && npm run serve'


