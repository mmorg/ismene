#!/bin/sh

echo 'Start Server Script'

# use a minimal nodemon watch config, that only restart on a specific (nonexistant) file
nodemon --config ./container/nodemon-min.json --exec 'npm run serve'


