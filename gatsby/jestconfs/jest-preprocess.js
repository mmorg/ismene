const babelOptions = {
  presets: [
    'babel-preset-gatsby', 
    '@babel/preset-typescript'
  ],
  // changes for the import.meta.url battlefied in the ESM War
  // see: https://github.com/facebook/jest/issues/12183#issuecomment-1004320665
  plugins: [
    'babel-plugin-transform-import-meta',
  ],
}

// ESM WAR: 1/ without type:module in package.json
module.exports = require("babel-jest").default.createTransformer(babelOptions)


// ESM WAR: 2/ with type:module in package.json
// import pkg from 'babel-jest'
// // it depend if there is "type:module" in package.json or not && if it run by testESM or not
// const { createTransformer } = pkg.default ? pkg.default : pkg
// export default createTransformer(babelOptions)

