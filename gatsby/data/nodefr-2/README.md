
@TODO: generation process documentation 
@TODO: in generation process generate shacl statements for thesaurus like 
```js 
const classId = class.id
const className = noPrefix(class.id)
const propertyId = property.id 
const propertyName = noPrefix(property.name)
{
    "id": `nfr:shapes/node/${className}`,
    "type": [ "sh:NodeShape"],
    "targetClass": [ classId ],
    "property": [ `nfr:shapes/property/${propertyName}`],
},
{
    "id": `nfr:shapes/property/${propertyName}`,
    "type": [ "sh:PropertyShape"],
    "path": [propertyId],
    "isInCollectionOrScheme": [ targetCollOrSchId ]
}
```
:: source version : 
```js 
{
    "id": "ori:shapes/BloomValuesExampleShape",
    "type": [
        "sh:NodeShape"
    ],
    "property": [
        "ori:shapes/_propertyShape_bloom"
    ],
    "targetClass": [
        "ori:LearningObject"
    ]
},
{
    "id": "ori:shapes/_propertyShape_bloom",
    "type": [
        "sh:PropertyShape"
    ],
    "path": [
        "ori:bloom"
    ],
    "isInCollectionOrScheme": [
        "ori:Taxonomy/Bloom"
    ]
}
```
