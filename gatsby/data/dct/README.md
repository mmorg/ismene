
1/ transformation from .rdf to .jsonld
```
docker-compose exec gatsby node src/bin/ttl2ld ttld -f 'data/dct/dublin_core_terms-2020.01.20.ttl'
```

@TODO: others parts are to process:

2/ indexation of the ontology ==> Layerize the ontology 
```
docker-compose exec gatsby node src/bin/integrate mu -f '**/skos/*.jsonld' -g 'genericGenerator.js' -u
```
- files are saved in `__layers__`

3/ Exports of the layerized graphs 
```
docker-compose exec gatsby node src/bin/compactLayers export -o skos -u
```
- files are saved in `__exports__`

==> RESTART here to apply on all the ontologies
