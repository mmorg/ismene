
```
cd gatsby
```

1/ transformation from .rdf to .jsonld
```
node src/bin/ttl2ld xmld -f 'data/skos/skos-1.2.0.rdf'
```

2/ indexation of the ontology ==> Layerize the ontology 
```
node src/bin/integrate mu -f '**/skos/*.jsonld' -g 'data/skos/skosGenerator.js' -u
```
- files are saved in `__layers__`

3/ Exports of the layerized graphs 
```
node src/bin/compactLayers export -p 'data/skos' -u
```
- files are saved in `__exports__`

4/ Copy to rdfx-graphql API generator
```
cp gatsby/data/skos/__exports__/skos_X.X.X_gl-realm-compacted\:gen_v1.jsonld rdfx-graphql/data/skos-1.3.0-gen_v1/
```

=> @TODO : 
==> RESTART here to apply on all the ontologies
==> A/ Import new version of gen_terms
==> B/ import de gen_terms dans le graphql
```
# ancienne version de la commande sur api-pf
docker-compose exec gatsby node src/bin/importEntity imp
```
==> tenter un nouvel import sur une base "propre" sans modifications via l'interface.

