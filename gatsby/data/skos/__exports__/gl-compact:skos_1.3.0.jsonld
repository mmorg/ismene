{
	"@context": {
		"id": "@id",
		"graph": {
			"@id": "@graph",
			"@container": "@set"
		},
		"type": {
			"@id": "@type",
			"@container": "@set"
		},
		"rdf": "http://www.w3.org/1999/02/22-rdf-syntax-ns#",
		"rdfs": "http://www.w3.org/2000/01/rdf-schema#",
		"domain": {
			"@id": "rdfs:domain",
			"@type": "@id",
			"@container": "@set"
		},
		"range": {
			"@id": "rdfs:range",
			"@type": "@id",
			"@container": "@set"
		},
		"label": {
			"@id": "rdfs:label",
			"@container": "@set"
		},
		"comment": {
			"@id": "rdfs:comment",
			"@container": "@set"
		},
		"subClassOf": {
			"@id": "rdfs:subClassOf",
			"@type": "@id",
			"@container": "@set"
		},
		"subPropertyOf": {
			"@id": "rdfs:subPropertyOf",
			"@type": "@id",
			"@container": "@set"
		},
		"isDefinedBy": {
			"@id": "rdfs:isDefinedBy",
			"@container": "@set"
		},
		"seeAlso": {
			"@id": "rdfs:seeAlso",
			"@container": "@set"
		},
		"owl": "http://www.w3.org/2002/07/owl#",
		"inverseOf": {
			"@id": "owl:inverseOf",
			"@type": "@id",
			"@container": "@set"
		},
		"disjointWith": {
			"@id": "owl:disjointWith",
			"@type": "@id",
			"@container": "@set"
		},
		"dct": "http://purl.org/dc/terms/",
		"contributor": {
			"@id": "dct:contributor",
			"@container": "@set"
		},
		"creator": {
			"@id": "dct:creator",
			"@container": "@set"
		},
		"title_dct": {
			"@id": "dct:title",
			"@container": "@set"
		},
		"skos": "http://www.w3.org/2004/02/skos/core#",
		"inScheme": {
			"@id": "skos:inScheme",
			"@type": "@id",
			"@container": "@set"
		},
		"definition": {
			"@id": "skos:definition",
			"@container": "@set"
		},
		"scopeNote": {
			"@id": "skos:scopeNote",
			"@container": "@set"
		},
		"example": {
			"@id": "skos:example",
			"@container": "@set"
		},
		"sch": "http://schema.org/",
		"description": {
			"@id": "sch:description",
			"@container": "@set"
		},
		"xsd": "http://www.w3.org/2001/XMLSchema#"
	},
	"graph": [
		{
			"id": "skos:Ontology",
			"type": [
				"owl:Ontology"
			],
			"contributor": [
				"Dave Beckett",
				"Nikki Rogers",
				"Participants in W3C's Semantic Web Deployment Working Group."
			],
			"creator": [
				"Alistair Miles",
				"Sean Bechhofer"
			],
			"dct:description": {
				"@language": "en",
				"@value": "An RDF vocabulary for describing the basic structure and content of concept schemes such as thesauri, classification schemes, subject heading lists, taxonomies, 'folksonomies', other types of controlled vocabulary, and also concept schemes embedded in glossaries and terminologies."
			},
			"title_dct": [
				{
					"@language": "en",
					"@value": "SKOS Vocabulary"
				}
			],
			"seeAlso": [
				{
					"id": "http://www.w3.org/TR/skos-reference/"
				}
			]
		},
		{
			"id": "skos:Collection",
			"type": [
				"owl:Class",
				"rdfs:Class"
			],
			"isDefinedBy": [
				{
					"id": "http://www.w3.org/2004/02/skos/core"
				}
			],
			"label": [
				{
					"@language": "en",
					"@value": "Collection"
				}
			],
			"disjointWith": [
				"skos:Concept",
				"skos:ConceptScheme"
			],
			"definition": [
				{
					"@language": "en",
					"@value": "A meaningful collection of concepts."
				}
			],
			"inScheme": [
				"skos:scheme_for_rdfs:Class_generated"
			],
			"scopeNote": [
				{
					"@language": "en",
					"@value": "Labelled collections can be used where you would like a set of concepts to be displayed under a 'node label' in the hierarchy."
				}
			]
		},
		{
			"id": "skos:Concept",
			"type": [
				"owl:Class",
				"rdfs:Class"
			],
			"isDefinedBy": [
				{
					"id": "http://www.w3.org/2004/02/skos/core"
				}
			],
			"label": [
				{
					"@language": "en",
					"@value": "Concept"
				}
			],
			"definition": [
				{
					"@language": "en",
					"@value": "An idea or notion; a unit of thought."
				}
			],
			"inScheme": [
				"skos:scheme_for_rdfs:Class_generated"
			]
		},
		{
			"id": "skos:ConceptScheme",
			"type": [
				"owl:Class",
				"rdfs:Class"
			],
			"isDefinedBy": [
				{
					"id": "http://www.w3.org/2004/02/skos/core"
				}
			],
			"label": [
				{
					"@language": "en",
					"@value": "Concept Scheme"
				}
			],
			"disjointWith": [
				"skos:Concept"
			],
			"definition": [
				{
					"@language": "en",
					"@value": "A set of concepts, optionally including statements about semantic relationships between those concepts."
				}
			],
			"example": [
				{
					"@language": "en",
					"@value": "Thesauri, classification schemes, subject heading lists, taxonomies, 'folksonomies', and other types of controlled vocabulary are all examples of concept schemes. Concept schemes are also embedded in glossaries and terminologies."
				}
			],
			"inScheme": [
				"skos:scheme_for_rdfs:Class_generated"
			],
			"scopeNote": [
				{
					"@language": "en",
					"@value": "A concept scheme may be defined to include concepts from different sources."
				}
			]
		},
		{
			"id": "skos:OrderedCollection",
			"type": [
				"owl:Class",
				"rdfs:Class"
			],
			"isDefinedBy": [
				{
					"id": "http://www.w3.org/2004/02/skos/core"
				}
			],
			"label": [
				{
					"@language": "en",
					"@value": "Ordered Collection"
				}
			],
			"subClassOf": [
				"skos:Collection"
			],
			"definition": [
				{
					"@language": "en",
					"@value": "An ordered collection of concepts, where both the grouping and the ordering are meaningful."
				}
			],
			"inScheme": [
				"skos:scheme_for_rdfs:Class_generated"
			],
			"scopeNote": [
				{
					"@language": "en",
					"@value": "Ordered collections can be used where you would like a set of concepts to be displayed in a specific order, and optionally under a 'node label'."
				}
			]
		},
		{
			"id": "skos:altLabel",
			"type": [
				"rdf:Property",
				"owl:AnnotationProperty"
			],
			"comment": [
				{
					"@language": "en",
					"@value": "The range of skos:altLabel is the class of RDF plain literals."
				},
				{
					"@language": "en",
					"@value": "skos:prefLabel, skos:altLabel and skos:hiddenLabel are pairwise disjoint properties."
				}
			],
			"isDefinedBy": [
				{
					"id": "http://www.w3.org/2004/02/skos/core"
				}
			],
			"label": [
				{
					"@language": "en",
					"@value": "alternative label"
				}
			],
			"subPropertyOf": [
				"rdfs:label"
			],
			"definition": [
				{
					"@language": "en",
					"@value": "An alternative lexical label for a resource."
				}
			],
			"example": [
				{
					"@language": "en",
					"@value": "Acronyms, abbreviations, spelling variants, and irregular plural/singular forms may be included among the alternative labels for a concept. Mis-spelled terms are normally included as hidden labels (see skos:hiddenLabel)."
				}
			],
			"inScheme": [
				"skos:scheme_for_rdf:Property_generated"
			],
			"domain": [
				"rdfs:Resource",
				"skos:Collection",
				"skos:Concept",
				"skos:ConceptScheme",
				"skos:OrderedCollection"
			],
			"range": [
				"rdfs:Literal"
			]
		},
		{
			"id": "skos:broadMatch",
			"type": [
				"rdf:Property",
				"owl:ObjectProperty"
			],
			"isDefinedBy": [
				{
					"id": "http://www.w3.org/2004/02/skos/core"
				}
			],
			"label": [
				{
					"@language": "en",
					"@value": "has broader match"
				}
			],
			"subPropertyOf": [
				"skos:broader",
				"skos:mappingRelation",
				"skos:broaderTransitive",
				"skos:semanticRelation"
			],
			"inverseOf": [
				"skos:narrowMatch"
			],
			"definition": [
				{
					"@language": "en",
					"@value": "skos:broadMatch is used to state a hierarchical mapping link between two conceptual resources in different concept schemes."
				}
			],
			"inScheme": [
				"skos:scheme_for_rdf:Property_generated"
			],
			"domain": [
				"skos:Concept"
			],
			"range": [
				"skos:Concept"
			]
		},
		{
			"id": "skos:broader",
			"type": [
				"rdf:Property",
				"owl:ObjectProperty"
			],
			"comment": [
				{
					"@language": "en",
					"@value": "Broader concepts are typically rendered as parents in a concept hierarchy (tree)."
				}
			],
			"isDefinedBy": [
				{
					"id": "http://www.w3.org/2004/02/skos/core"
				}
			],
			"label": [
				{
					"@language": "en",
					"@value": "has broader"
				}
			],
			"subPropertyOf": [
				"skos:broaderTransitive",
				"skos:semanticRelation"
			],
			"inverseOf": [
				"skos:narrower"
			],
			"definition": [
				{
					"@language": "en",
					"@value": "Relates a concept to a concept that is more general in meaning."
				}
			],
			"inScheme": [
				"skos:scheme_for_rdf:Property_generated"
			],
			"scopeNote": [
				{
					"@language": "en",
					"@value": "By convention, skos:broader is only used to assert an immediate (i.e. direct) hierarchical link between two conceptual resources."
				}
			],
			"domain": [
				"skos:Concept"
			],
			"range": [
				"skos:Concept"
			]
		},
		{
			"id": "skos:broaderTransitive",
			"type": [
				"rdf:Property",
				"owl:ObjectProperty",
				"owl:TransitiveProperty"
			],
			"isDefinedBy": [
				{
					"id": "http://www.w3.org/2004/02/skos/core"
				}
			],
			"label": [
				{
					"@language": "en",
					"@value": "has broader transitive"
				}
			],
			"subPropertyOf": [
				"skos:semanticRelation"
			],
			"inverseOf": [
				"skos:narrowerTransitive"
			],
			"definition": [
				{
					"@language": "en",
					"@value": "skos:broaderTransitive is a transitive superproperty of skos:broader."
				}
			],
			"inScheme": [
				"skos:scheme_for_rdf:Property_generated"
			],
			"scopeNote": [
				{
					"@language": "en",
					"@value": "By convention, skos:broaderTransitive is not used to make assertions. Rather, the properties can be used to draw inferences about the transitive closure of the hierarchical relation, which is useful e.g. when implementing a simple query expansion algorithm in a search application."
				}
			],
			"domain": [
				"skos:Concept"
			],
			"range": [
				"skos:Concept"
			]
		},
		{
			"id": "skos:changeNote",
			"type": [
				"rdf:Property",
				"owl:AnnotationProperty"
			],
			"isDefinedBy": [
				{
					"id": "http://www.w3.org/2004/02/skos/core"
				}
			],
			"label": [
				{
					"@language": "en",
					"@value": "change note"
				}
			],
			"subPropertyOf": [
				"skos:note"
			],
			"definition": [
				{
					"@language": "en",
					"@value": "A note about a modification to a concept."
				}
			],
			"inScheme": [
				"skos:scheme_for_rdf:Property_generated"
			],
			"domain": [
				"skos:Concept",
				"rdfs:Resource",
				"owl:Thing",
				"skos:Collection",
				"skos:ConceptScheme",
				"skos:OrderedCollection"
			],
			"range": [
				"xsd:string",
				"rdf:langString",
				"rdf:HTML",
				"rdf:XMLLiteral"
			]
		},
		{
			"id": "skos:closeMatch",
			"type": [
				"rdf:Property",
				"owl:ObjectProperty",
				"owl:SymmetricProperty"
			],
			"isDefinedBy": [
				{
					"id": "http://www.w3.org/2004/02/skos/core"
				}
			],
			"label": [
				{
					"@language": "en",
					"@value": "has close match"
				}
			],
			"subPropertyOf": [
				"skos:mappingRelation",
				"skos:semanticRelation"
			],
			"definition": [
				{
					"@language": "en",
					"@value": "skos:closeMatch is used to link two concepts that are sufficiently similar that they can be used interchangeably in some information retrieval applications. In order to avoid the possibility of \"compound errors\" when combining mappings across more than two concept schemes, skos:closeMatch is not declared to be a transitive property."
				}
			],
			"inScheme": [
				"skos:scheme_for_rdf:Property_generated"
			],
			"domain": [
				"skos:Concept"
			],
			"range": [
				"skos:Concept"
			]
		},
		{
			"id": "skos:definition",
			"type": [
				"rdf:Property",
				"owl:AnnotationProperty"
			],
			"isDefinedBy": [
				{
					"id": "http://www.w3.org/2004/02/skos/core"
				}
			],
			"label": [
				{
					"@language": "en",
					"@value": "definition"
				}
			],
			"subPropertyOf": [
				"skos:note"
			],
			"definition": [
				{
					"@language": "en",
					"@value": "A statement or formal explanation of the meaning of a concept."
				}
			],
			"inScheme": [
				"skos:scheme_for_rdf:Property_generated"
			],
			"domain": [
				"skos:Concept",
				"rdfs:Resource",
				"owl:Thing",
				"skos:Collection",
				"skos:ConceptScheme",
				"skos:OrderedCollection"
			],
			"range": [
				"xsd:string",
				"rdf:langString",
				"rdf:HTML",
				"rdf:XMLLiteral"
			]
		},
		{
			"id": "skos:editorialNote",
			"type": [
				"rdf:Property",
				"owl:AnnotationProperty"
			],
			"isDefinedBy": [
				{
					"id": "http://www.w3.org/2004/02/skos/core"
				}
			],
			"label": [
				{
					"@language": "en",
					"@value": "editorial note"
				}
			],
			"subPropertyOf": [
				"skos:note"
			],
			"definition": [
				{
					"@language": "en",
					"@value": "A note for an editor, translator or maintainer of the vocabulary."
				}
			],
			"inScheme": [
				"skos:scheme_for_rdf:Property_generated"
			],
			"domain": [
				"skos:Concept",
				"rdfs:Resource",
				"owl:Thing",
				"skos:Collection",
				"skos:ConceptScheme",
				"skos:OrderedCollection"
			],
			"range": [
				"xsd:string",
				"rdf:langString",
				"rdf:HTML",
				"rdf:XMLLiteral"
			]
		},
		{
			"id": "skos:exactMatch",
			"type": [
				"rdf:Property",
				"owl:ObjectProperty",
				"owl:SymmetricProperty",
				"owl:TransitiveProperty"
			],
			"comment": [
				{
					"@language": "en",
					"@value": "skos:exactMatch is disjoint with each of the properties skos:broadMatch and skos:relatedMatch."
				}
			],
			"isDefinedBy": [
				{
					"id": "http://www.w3.org/2004/02/skos/core"
				}
			],
			"label": [
				{
					"@language": "en",
					"@value": "has exact match"
				}
			],
			"subPropertyOf": [
				"skos:closeMatch",
				"skos:mappingRelation",
				"skos:semanticRelation"
			],
			"definition": [
				{
					"@language": "en",
					"@value": "skos:exactMatch is used to link two concepts, indicating a high degree of confidence that the concepts can be used interchangeably across a wide range of information retrieval applications. skos:exactMatch is a transitive property, and is a sub-property of skos:closeMatch."
				}
			],
			"inScheme": [
				"skos:scheme_for_rdf:Property_generated"
			],
			"domain": [
				"skos:Concept"
			],
			"range": [
				"skos:Concept"
			]
		},
		{
			"id": "skos:example",
			"type": [
				"rdf:Property",
				"owl:AnnotationProperty"
			],
			"isDefinedBy": [
				{
					"id": "http://www.w3.org/2004/02/skos/core"
				}
			],
			"label": [
				{
					"@language": "en",
					"@value": "example"
				}
			],
			"subPropertyOf": [
				"skos:note"
			],
			"definition": [
				{
					"@language": "en",
					"@value": "An example of the use of a concept."
				}
			],
			"inScheme": [
				"skos:scheme_for_rdf:Property_generated"
			],
			"domain": [
				"skos:Concept",
				"rdfs:Resource",
				"owl:Thing",
				"skos:Collection",
				"skos:ConceptScheme",
				"skos:OrderedCollection"
			],
			"range": [
				"xsd:string",
				"rdf:langString",
				"rdf:HTML",
				"rdf:XMLLiteral"
			]
		},
		{
			"id": "skos:hasTopConcept",
			"type": [
				"rdf:Property",
				"owl:ObjectProperty"
			],
			"domain": [
				"skos:ConceptScheme"
			],
			"isDefinedBy": [
				{
					"id": "http://www.w3.org/2004/02/skos/core"
				}
			],
			"label": [
				{
					"@language": "en",
					"@value": "has top concept"
				}
			],
			"range": [
				"skos:Concept"
			],
			"inverseOf": [
				"skos:topConceptOf"
			],
			"definition": [
				{
					"@language": "en",
					"@value": "Relates, by convention, a concept scheme to a concept which is topmost in the broader/narrower concept hierarchies for that scheme, providing an entry point to these hierarchies."
				}
			],
			"inScheme": [
				"skos:scheme_for_rdf:Property_generated"
			]
		},
		{
			"id": "skos:hiddenLabel",
			"type": [
				"rdf:Property",
				"owl:AnnotationProperty"
			],
			"comment": [
				{
					"@language": "en",
					"@value": "The range of skos:hiddenLabel is the class of RDF plain literals."
				},
				{
					"@language": "en",
					"@value": "skos:prefLabel, skos:altLabel and skos:hiddenLabel are pairwise disjoint properties."
				}
			],
			"isDefinedBy": [
				{
					"id": "http://www.w3.org/2004/02/skos/core"
				}
			],
			"label": [
				{
					"@language": "en",
					"@value": "hidden label"
				}
			],
			"subPropertyOf": [
				"rdfs:label"
			],
			"definition": [
				{
					"@language": "en",
					"@value": "A lexical label for a resource that should be hidden when generating visual displays of the resource, but should still be accessible to free text search operations."
				}
			],
			"inScheme": [
				"skos:scheme_for_rdf:Property_generated"
			],
			"domain": [
				"rdfs:Resource",
				"skos:Collection",
				"skos:Concept",
				"skos:ConceptScheme",
				"skos:OrderedCollection"
			],
			"range": [
				"rdfs:Literal"
			]
		},
		{
			"id": "skos:historyNote",
			"type": [
				"rdf:Property",
				"owl:AnnotationProperty"
			],
			"isDefinedBy": [
				{
					"id": "http://www.w3.org/2004/02/skos/core"
				}
			],
			"label": [
				{
					"@language": "en",
					"@value": "history note"
				}
			],
			"subPropertyOf": [
				"skos:note"
			],
			"definition": [
				{
					"@language": "en",
					"@value": "A note about the past state/use/meaning of a concept."
				}
			],
			"inScheme": [
				"skos:scheme_for_rdf:Property_generated"
			],
			"domain": [
				"skos:Concept",
				"rdfs:Resource",
				"owl:Thing",
				"skos:Collection",
				"skos:ConceptScheme",
				"skos:OrderedCollection"
			],
			"range": [
				"xsd:string",
				"rdf:langString",
				"rdf:HTML",
				"rdf:XMLLiteral"
			]
		},
		{
			"id": "skos:inScheme",
			"type": [
				"rdf:Property",
				"owl:ObjectProperty"
			],
			"isDefinedBy": [
				{
					"id": "http://www.w3.org/2004/02/skos/core"
				}
			],
			"label": [
				{
					"@language": "en",
					"@value": "is in scheme"
				}
			],
			"range": [
				"skos:ConceptScheme"
			],
			"definition": [
				{
					"@language": "en",
					"@value": "Relates a resource (for example a concept) to a concept scheme in which it is included."
				}
			],
			"inScheme": [
				"skos:scheme_for_rdf:Property_generated"
			],
			"scopeNote": [
				{
					"@language": "en",
					"@value": "A concept may be a member of more than one concept scheme."
				}
			],
			"domain": [
				"skos:ConceptScheme",
				"rdfs:Resource",
				"owl:Thing",
				"skos:Collection",
				"skos:Concept",
				"skos:OrderedCollection"
			]
		},
		{
			"id": "skos:mappingRelation",
			"type": [
				"rdf:Property",
				"owl:ObjectProperty"
			],
			"comment": [
				{
					"@language": "en",
					"@value": "These concept mapping relations mirror semantic relations, and the data model defined below is similar (with the exception of skos:exactMatch) to the data model defined for semantic relations. A distinct vocabulary is provided for concept mapping relations, to provide a convenient way to differentiate links within a concept scheme from links between concept schemes. However, this pattern of usage is not a formal requirement of the SKOS data model, and relies on informal definitions of best practice."
				}
			],
			"isDefinedBy": [
				{
					"id": "http://www.w3.org/2004/02/skos/core"
				}
			],
			"label": [
				{
					"@language": "en",
					"@value": "is in mapping relation with"
				}
			],
			"subPropertyOf": [
				"skos:semanticRelation"
			],
			"definition": [
				{
					"@language": "en",
					"@value": "Relates two concepts coming, by convention, from different schemes, and that have comparable meanings"
				}
			],
			"inScheme": [
				"skos:scheme_for_rdf:Property_generated"
			],
			"domain": [
				"skos:Concept"
			],
			"range": [
				"skos:Concept"
			]
		},
		{
			"id": "skos:member",
			"type": [
				"rdf:Property",
				"owl:ObjectProperty"
			],
			"domain": [
				"skos:Collection"
			],
			"isDefinedBy": [
				{
					"id": "http://www.w3.org/2004/02/skos/core"
				}
			],
			"label": [
				{
					"@language": "en",
					"@value": "has member"
				}
			],
			"range": [
				"skos:Collection",
				"skos:Concept"
			],
			"definition": [
				{
					"@language": "en",
					"@value": "Relates a collection to one of its members."
				}
			],
			"inScheme": [
				"skos:scheme_for_rdf:Property_generated"
			]
		},
		{
			"id": "skos:memberList",
			"type": [
				"rdf:Property",
				"owl:FunctionalProperty",
				"owl:ObjectProperty"
			],
			"comment": [
				{
					"@language": "en",
					"@value": "For any resource, every item in the list given as the value of the\n      skos:memberList property is also a value of the skos:member property."
				}
			],
			"domain": [
				"skos:OrderedCollection"
			],
			"isDefinedBy": [
				{
					"id": "http://www.w3.org/2004/02/skos/core"
				}
			],
			"label": [
				{
					"@language": "en",
					"@value": "has member list"
				}
			],
			"range": [
				"rdf:List"
			],
			"definition": [
				{
					"@language": "en",
					"@value": "Relates an ordered collection to the RDF list containing its members."
				}
			],
			"inScheme": [
				"skos:scheme_for_rdf:Property_generated"
			]
		},
		{
			"id": "skos:narrowMatch",
			"type": [
				"rdf:Property",
				"owl:ObjectProperty"
			],
			"isDefinedBy": [
				{
					"id": "http://www.w3.org/2004/02/skos/core"
				}
			],
			"label": [
				{
					"@language": "en",
					"@value": "has narrower match"
				}
			],
			"subPropertyOf": [
				"skos:mappingRelation",
				"skos:narrower",
				"skos:semanticRelation",
				"skos:narrowerTransitive"
			],
			"inverseOf": [
				"skos:broadMatch"
			],
			"definition": [
				{
					"@language": "en",
					"@value": "skos:narrowMatch is used to state a hierarchical mapping link between two conceptual resources in different concept schemes."
				}
			],
			"inScheme": [
				"skos:scheme_for_rdf:Property_generated"
			],
			"domain": [
				"skos:Concept"
			],
			"range": [
				"skos:Concept"
			]
		},
		{
			"id": "skos:narrower",
			"type": [
				"rdf:Property",
				"owl:ObjectProperty"
			],
			"comment": [
				{
					"@language": "en",
					"@value": "Narrower concepts are typically rendered as children in a concept hierarchy (tree)."
				}
			],
			"isDefinedBy": [
				{
					"id": "http://www.w3.org/2004/02/skos/core"
				}
			],
			"label": [
				{
					"@language": "en",
					"@value": "has narrower"
				}
			],
			"subPropertyOf": [
				"skos:narrowerTransitive",
				"skos:semanticRelation"
			],
			"inverseOf": [
				"skos:broader"
			],
			"definition": [
				{
					"@language": "en",
					"@value": "Relates a concept to a concept that is more specific in meaning."
				}
			],
			"inScheme": [
				"skos:scheme_for_rdf:Property_generated"
			],
			"scopeNote": [
				{
					"@language": "en",
					"@value": "By convention, skos:broader is only used to assert an immediate (i.e. direct) hierarchical link between two conceptual resources."
				}
			],
			"domain": [
				"skos:Concept"
			],
			"range": [
				"skos:Concept"
			]
		},
		{
			"id": "skos:narrowerTransitive",
			"type": [
				"rdf:Property",
				"owl:ObjectProperty",
				"owl:TransitiveProperty"
			],
			"isDefinedBy": [
				{
					"id": "http://www.w3.org/2004/02/skos/core"
				}
			],
			"label": [
				{
					"@language": "en",
					"@value": "has narrower transitive"
				}
			],
			"subPropertyOf": [
				"skos:semanticRelation"
			],
			"inverseOf": [
				"skos:broaderTransitive"
			],
			"definition": [
				{
					"@language": "en",
					"@value": "skos:narrowerTransitive is a transitive superproperty of skos:narrower."
				}
			],
			"inScheme": [
				"skos:scheme_for_rdf:Property_generated"
			],
			"scopeNote": [
				{
					"@language": "en",
					"@value": "By convention, skos:narrowerTransitive is not used to make assertions. Rather, the properties can be used to draw inferences about the transitive closure of the hierarchical relation, which is useful e.g. when implementing a simple query expansion algorithm in a search application."
				}
			],
			"domain": [
				"skos:Concept"
			],
			"range": [
				"skos:Concept"
			]
		},
		{
			"id": "skos:notation",
			"type": [
				"rdf:Property",
				"owl:DatatypeProperty"
			],
			"isDefinedBy": [
				{
					"id": "http://www.w3.org/2004/02/skos/core"
				}
			],
			"label": [
				{
					"@language": "en",
					"@value": "notation"
				}
			],
			"definition": [
				{
					"@language": "en",
					"@value": "A notation, also known as classification code, is a string of characters such as \"T58.5\" or \"303.4833\" used to uniquely identify a concept within the scope of a given concept scheme."
				}
			],
			"inScheme": [
				"skos:scheme_for_rdf:Property_generated"
			],
			"scopeNote": [
				{
					"@language": "en",
					"@value": "By convention, skos:notation is used with a typed literal in the object position of the triple."
				}
			],
			"domain": [
				"skos:Concept"
			],
			"range": [
				"rdfs:Datatype"
			]
		},
		{
			"id": "skos:note",
			"type": [
				"rdf:Property",
				"owl:AnnotationProperty"
			],
			"isDefinedBy": [
				{
					"id": "http://www.w3.org/2004/02/skos/core"
				}
			],
			"label": [
				{
					"@language": "en",
					"@value": "note"
				}
			],
			"definition": [
				{
					"@language": "en",
					"@value": "A general note, for any purpose."
				}
			],
			"inScheme": [
				"skos:scheme_for_rdf:Property_generated"
			],
			"scopeNote": [
				{
					"@language": "en",
					"@value": "This property may be used directly, or as a super-property for more specific note types."
				}
			],
			"domain": [
				"skos:Concept",
				"rdfs:Resource",
				"owl:Thing",
				"skos:Collection",
				"skos:ConceptScheme",
				"skos:OrderedCollection"
			],
			"range": [
				"xsd:string",
				"rdf:langString",
				"rdf:HTML",
				"rdf:XMLLiteral"
			]
		},
		{
			"id": "skos:prefLabel",
			"type": [
				"rdf:Property",
				"owl:AnnotationProperty"
			],
			"comment": [
				{
					"@language": "en",
					"@value": "A resource has no more than one value of skos:prefLabel per language tag, and no more than one value of skos:prefLabel without language tag."
				},
				{
					"@language": "en",
					"@value": "The range of skos:prefLabel is the class of RDF plain literals."
				},
				{
					"@language": "en",
					"@value": "skos:prefLabel, skos:altLabel and skos:hiddenLabel are pairwise\n      disjoint properties."
				}
			],
			"isDefinedBy": [
				{
					"id": "http://www.w3.org/2004/02/skos/core"
				}
			],
			"label": [
				{
					"@language": "en",
					"@value": "preferred label"
				}
			],
			"subPropertyOf": [
				"rdfs:label"
			],
			"definition": [
				{
					"@language": "en",
					"@value": "The preferred lexical label for a resource, in a given language."
				}
			],
			"inScheme": [
				"skos:scheme_for_rdf:Property_generated"
			],
			"domain": [
				"rdfs:Resource",
				"skos:Collection",
				"skos:Concept",
				"skos:ConceptScheme",
				"skos:OrderedCollection"
			],
			"range": [
				"rdfs:Literal"
			]
		},
		{
			"id": "skos:related",
			"type": [
				"rdf:Property",
				"owl:ObjectProperty",
				"owl:SymmetricProperty"
			],
			"comment": [
				{
					"@language": "en",
					"@value": "skos:related is disjoint with skos:broaderTransitive"
				}
			],
			"isDefinedBy": [
				{
					"id": "http://www.w3.org/2004/02/skos/core"
				}
			],
			"label": [
				{
					"@language": "en",
					"@value": "has related"
				}
			],
			"subPropertyOf": [
				"skos:semanticRelation"
			],
			"definition": [
				{
					"@language": "en",
					"@value": "Relates a concept to a concept with which there is an associative semantic relationship."
				}
			],
			"inScheme": [
				"skos:scheme_for_rdf:Property_generated"
			],
			"domain": [
				"skos:Concept"
			],
			"range": [
				"skos:Concept"
			]
		},
		{
			"id": "skos:relatedMatch",
			"type": [
				"rdf:Property",
				"owl:ObjectProperty",
				"owl:SymmetricProperty"
			],
			"isDefinedBy": [
				{
					"id": "http://www.w3.org/2004/02/skos/core"
				}
			],
			"label": [
				{
					"@language": "en",
					"@value": "has related match"
				}
			],
			"subPropertyOf": [
				"skos:mappingRelation",
				"skos:related",
				"skos:semanticRelation"
			],
			"definition": [
				{
					"@language": "en",
					"@value": "skos:relatedMatch is used to state an associative mapping link between two conceptual resources in different concept schemes."
				}
			],
			"inScheme": [
				"skos:scheme_for_rdf:Property_generated"
			],
			"domain": [
				"skos:Concept"
			],
			"range": [
				"skos:Concept"
			]
		},
		{
			"id": "skos:scopeNote",
			"type": [
				"rdf:Property",
				"owl:AnnotationProperty"
			],
			"isDefinedBy": [
				{
					"id": "http://www.w3.org/2004/02/skos/core"
				}
			],
			"label": [
				{
					"@language": "en",
					"@value": "scope note"
				}
			],
			"subPropertyOf": [
				"skos:note"
			],
			"definition": [
				{
					"@language": "en",
					"@value": "A note that helps to clarify the meaning and/or the use of a concept."
				}
			],
			"inScheme": [
				"skos:scheme_for_rdf:Property_generated"
			],
			"domain": [
				"skos:Concept",
				"rdfs:Resource",
				"owl:Thing",
				"skos:Collection",
				"skos:ConceptScheme",
				"skos:OrderedCollection"
			],
			"range": [
				"xsd:string",
				"rdf:langString",
				"rdf:HTML",
				"rdf:XMLLiteral"
			]
		},
		{
			"id": "skos:semanticRelation",
			"type": [
				"rdf:Property",
				"owl:ObjectProperty"
			],
			"domain": [
				"skos:Concept"
			],
			"isDefinedBy": [
				{
					"id": "http://www.w3.org/2004/02/skos/core"
				}
			],
			"label": [
				{
					"@language": "en",
					"@value": "is in semantic relation with"
				}
			],
			"range": [
				"skos:Concept"
			],
			"definition": [
				{
					"@language": "en",
					"@value": "Links a concept to a concept related by meaning."
				}
			],
			"inScheme": [
				"skos:scheme_for_rdf:Property_generated"
			],
			"scopeNote": [
				{
					"@language": "en",
					"@value": "This property should not be used directly, but as a super-property for all properties denoting a relationship of meaning between concepts."
				}
			]
		},
		{
			"id": "skos:topConceptOf",
			"type": [
				"rdf:Property",
				"owl:ObjectProperty"
			],
			"domain": [
				"skos:Concept"
			],
			"isDefinedBy": [
				{
					"id": "http://www.w3.org/2004/02/skos/core"
				}
			],
			"label": [
				{
					"@language": "en",
					"@value": "is top concept in scheme"
				}
			],
			"range": [
				"skos:ConceptScheme"
			],
			"subPropertyOf": [
				"skos:inScheme"
			],
			"inverseOf": [
				"skos:hasTopConcept"
			],
			"definition": [
				{
					"@language": "en",
					"@value": "Relates a concept to the concept scheme that it is a top level concept of."
				}
			],
			"inScheme": [
				"skos:scheme_for_rdf:Property_generated"
			]
		},
		{
			"id": "skos:scheme_for_rdfs:Class_generated",
			"type": [
				"skos:ConceptScheme",
				"om:defaultScheme"
			],
			"description": [
				{
					"@language": "en",
					"@value": "Default scheme for rdfs:Class - autogenerated"
				},
				{
					"@language": "fr",
					"@value": "Scheme par défaut pour les rdfs:Class - autogenéré"
				}
			]
		},
		{
			"id": "skos:scheme_for_rdf:Property_generated",
			"type": [
				"skos:ConceptScheme",
				"om:defaultScheme"
			],
			"description": [
				{
					"@language": "en",
					"@value": "Default scheme for rdf:Property - autogenerated"
				},
				{
					"@language": "fr",
					"@value": "Scheme par défaut pour les rdf:Property - autogenéré"
				}
			]
		},
		{
			"id": "skos:memberOf",
			"type": [
				"rdf:Property",
				"owl:ObjectProperty"
			],
			"domain": [
				"skos:Collection",
				"skos:Concept"
			],
			"range": [
				"skos:Collection"
			],
			"label": [
				{
					"@language": "en",
					"@value": "MemberOf"
				}
			],
			"definition": [
				{
					"@language": "en",
					"@value": "Relates members to collections"
				}
			]
		}
	]
}