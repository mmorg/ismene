import entityRemover from '../../src/graphLayerScripts/entityRemover.js'
import propertyDomainRangeGuess from '../../src/graphLayerScripts/propertyDomainRangeGuess.js'
import propertyExplicitInherit from '../../src/graphLayerScripts/propertyExplicitInherit.js'
import propertySpecializeDomain from '../../src/graphLayerScripts/propertySpecializeDomain.js'
import skosCollectionCleaner from '../../src/graphLayerScripts/skosCollectionCleaner.js'
import skosMemberOf from '../../src/graphLayerScripts/skosMemberOf.js'

// @TODO: this is still used ?
export const layerRealm = ['skos_GEN_generator']

const skos130 = 'skos_1.3.0'
const ismene = 'ismene' // @TODO: extraire "l'ajout forcé des `inscheme`" du code par défault, en faire un LayerProcessor
const gen_v1 = 'gen_v1'
const jas100 = 'jasOnto-1.0.0'

const gen_v1_entityToRemove = [
  // Class
  'skos:ConceptScheme', 'skos:OrderedCollection',
  // Property
  'skos:hasTopConcept', 'skos:inScheme', 'skos:memberList',


  'skos:broadMatch', 'skos:broaderTransitive', 'skos:changeNote', 'skos:closeMatch',
  'skos:editorialNote', 'skos:exactMatch', 'skos:example', 'skos:hiddenLabel',
  'skos:historyNote', 'skos:mappingRelation', 'skos:narrowMatch', 'skos:narrowerTransitive',
  'skos:notation', 'skos:note', 'skos:related', 'skos:relatedMatch', 'skos:scopeNote',
  'skos:semanticRelation', 'skos:topConceptOf',
]
const skosCollection_entityToRemove = [
  '_:b0_n0', '_:b0_n2', '_:b0_n3',
]

export default function skosGENGenerator() {

  // @TODO: ? skos1_3_0 have to be removed from propertyExplicitInherit & propertyDomainRangeGuess or not ?
  const processors = [
    {
      fn: propertyExplicitInherit,
      layerRealm: [skos130, ismene, gen_v1, jas100]
    },
    {
      fn: propertyDomainRangeGuess,
      layerRealm: [skos130, ismene, gen_v1, jas100]
    },
    {
      fn: propertySpecializeDomain,
      layerRealm: [skos130, ismene, gen_v1, jas100]
    },
    {
      fn: entityRemover,
      layerRealm: [gen_v1, jas100],
      options: { toRemove: gen_v1_entityToRemove, log: false }
    },
    {
      fn: skosCollectionCleaner,
      layerRealm: [skos130, ismene, gen_v1, jas100],
      options: { toRemove: skosCollection_entityToRemove },
    },
    { // add the inverse property of "member" : "memberOf"
      fn:skosMemberOf,
      layerRealm:[skos130, ismene, gen_v1, jas100],
    }

  ]

  return processors

}
