
* See project 5-minutes-profile for an up to date version. 


# design notes: 


# Documentation sur FOAF : 

* Les ontologies principales sont sur les web-archives car ils ont un problème de publication sur l'url principale
  - doc: https://web.archive.org/web/20220518003509/http://xmlns.com/foaf/spec/20140114.html
  - schema: https://web.archive.org/web/20220614105937if_/http://xmlns.com/foaf/spec/20140114.rdf
  - site principal mais ancienne version: http://xmlns.com/foaf/0.1/

* une version "minimaliste / essentielle": https://sparontologies.github.io/foaf/current/foaf.html

* des ajouts intéressants pour représenter des transmissions (de maladies): https://bmcmedinformdecismak.biomedcentral.com/articles/10.1186/s12911-020-01287-8


==> voir pour l'utilisation de schema.org plutôt ?? 

==> ou de hrrdf ? https://ismene.competencies.be/hrrdf/PersonType/



# les classes dans dashemploi V1: 
* 20230424-prod-addviseo-agent
* 20230424-prod-addviseo-email-account
* 20230424-prod-addviseo-user-account
* 20230424-prod-addviseo-user-group

celles liées aux compétences et autre : 
* 20230424-prod-addviseo-experience
* 20230424-prod-addviseo-occupation
* 20230424-prod-addviseo-skill
==> chercher si d'autres... 
