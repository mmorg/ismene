* pour l'indexation : 
```
docker-compose exec gatsby node src/bin/integrate mu -f '**/data/hrrdf/*.jsonld'
```

# Exemple for multiple serializations of the same "rdfs ontology". 
* 1 class in example : 
```jsonld
{
  "@context": {
		"id": "@id",
		"graph": {
			"@id": "@graph",
			"@container": "@set"
		},
		"type": {
			"@id": "@type",
			"@container": "@set"
		},
		"rdf": "http://www.w3.org/1999/02/22-rdf-syntax-ns#",
		"rdfs": "http://www.w3.org/2000/01/rdf-schema#",
    "comment": {
			"@id": "rdfs:comment",
			"@language": "en",
			"@container": "@set"
		},
    "label": {
			"@id": "rdfs:label",
			"@language": "en",
			"@container": "@set"
		},
    "subClassOf": {
			"@id": "rdfs:subClassOf",
			"@type": "@id",
			"@container": "@set"
		}
  },
  "graph": [
    {
      "id": "hrrdf:AssessmentSubjectType",
      "type": [
        "rdfs:Class"
      ],
      "comment": [
        "Person to be assessed."
      ],
      "label": [
        "AssessmentSubjectType"
      ],
      "subClassOf": [
        "hrrdf:PersonLegalType",
        "hrrdf:PersonPhysicalInclusion"
      ]
    }
  ]
}

```

```xml
<?xml version="1.0" encoding="utf-8" ?>
<rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
         xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#">

  <rdfs:Class rdf:about="hrrdf:AssessmentSubjectType">
    <rdfs:comment xml:lang="en">Person to be assessed.</rdfs:comment>
    <rdfs:label xml:lang="en">AssessmentSubjectType</rdfs:label>
    <rdfs:subClassOf rdf:resource="hrrdf:PersonLegalType"/>
    <rdfs:subClassOf rdf:resource="hrrdf:PersonPhysicalInclusion"/>
  </rdfs:Class>

</rdf:RDF>
```

```ttl 
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .

<hrrdf:AssessmentSubjectType>
  a rdfs:Class ;
  rdfs:comment "Person to be assessed."@en ;
  rdfs:label "AssessmentSubjectType"@en ;
  rdfs:subClassOf <hrrdf:PersonLegalType>, <hrrdf:PersonPhysicalInclusion> .
```


