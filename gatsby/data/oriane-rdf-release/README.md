* generation command : 
  * attention, a cause du "Esm War", il faut réactiver `type:modules` dans le `package.json` avant de faire tourner ce script.
```
docker-compose exec gatsby npm run toRdf -- -s '**/oriane/*.json' -m '**/yarrrml/*.yaml' -p 'orianeProdProfile.js' -t 'oriane' 'oriane-rdf-prod'

cp gatsby/data/oriane-rdf-prod/oriane-X.Y.Z.jsonld gatsby/data/oriane-rdf-release/oriane-[NEW_VERSION].jsonld
rm gatsby/data/oriane-rdf-release/oriane-[OLD_VERSION].jsonld
```

@@TODO: a faire en post-processing de orianeProdProfile: vérification : 
* 1/ des entitées doublées (ex: "ori:Tache")
* 2/ des associations de type chelou, exemple = [
				"rdfs:Class",
				"skos:Concept"
* 3/ utilisation de ".label" sur des skos:Concept (en remplacement des .prefLabel)
* 4/ {"id":"ori:PublicType#Camarades","type":["ori:PublicType"] ==> c'est en fait un skos:Concept

* Ontology indexing command (required for Class & Properties indexing) : 
```
docker-compose exec gatsby node src/bin/integrate mu -f '**/oriane-rdf-release/*.jsonld' -e 'oriGenerateContribution.js' -u
```
