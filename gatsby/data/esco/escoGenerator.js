import { readJson } from '@mmorg/fsutils'
import entityRemover from '../../src/graphLayerScripts/entityRemover.js'
import owlDeprecatedRemover from '../../src/graphLayerScripts/owlDeprecatedRemover.js'
import propertyDomainRangeGuess from '../../src/graphLayerScripts/propertyDomainRangeGuess.js'
import propertyExplicitInherit from '../../src/graphLayerScripts/propertyExplicitInherit.js'
import propertySpecializeDomain from '../../src/graphLayerScripts/propertySpecializeDomain.js'
import skosCollectionCleaner from '../../src/graphLayerScripts/skosCollectionCleaner.js'
import skosMemberOf from '../../src/graphLayerScripts/skosMemberOf.js'
import subClassOfAttachProperty from '../../src/graphLayerScripts/subClassOfAttachProperty.js'

const skos = 'data/skos/__exports__/gl-compact:skos_1.3.0.jsonld'
// @TODO: this is still used ?
export const layerRealm = ['skos_GEN_generator']

const esco121 = 'esco_1.2.1'
const jas100 = 'jasOnto-1.0.0'

const esco_entityToRemove = [
  // This is a deprecated, not dereferencable url
  'http://purl.org/iso25964/DataSet/Versioning#hasVersionRecord',

]
const skosCollection_entityToRemove = [
  '_:b0_n0', '_:b0_n2', '_:b0_n3',
]

export default function escoGenerator() {

  // @TODO: ? skos1_3_0 have to be removed from propertyExplicitInherit & propertyDomainRangeGuess or not ?
  const processors = [
    {
      fn: owlDeprecatedRemover,
      layerRealm: [esco121, jas100],
      // options: { toRemove: gen_v1_entityToRemove, log: false }
    },
    {
      fn: entityRemover,
      layerRealm: [esco121, jas100],
      options: { toRemove: esco_entityToRemove, log: false }
    },
    {
      fn: subClassOfAttachProperty,
      layerRealm: [jas100],
      options: { externalGraph: [readJson(skos)] }
    }
  ]

  return processors

}
