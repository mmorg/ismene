* sources: 
  - rdf version: https://ec.europa.eu/esco/lod/static/model.rdf
  - html version: https://ec.europa.eu/esco/lod/static/model.html

```
cd gatsby
```

1/ transformation from .rdf to .jsonld
```
node src/bin/ttl2ld xmld -f 'data/esco/esco-1.2.0.rdf'
```

2/ indexation of the ontology ==> Layerize the ontology 
```
node src/bin/integrate mu -f '**/esco/*.jsonld' -g 'data/esco/escoGenerator.js' -u
```
- files are saved in `__layers__`

3/ Exports of the layerized graphs 
```
node src/bin/compactLayers export -p 'data/esco/' -u
```
- files are saved in `__exports__`

4/ Copy to jobs-&-skills API
```
cp data/esco/__exports__/_X.X.X_gl-realm-compacted\:esco_1.2.1.jsonld ../services/jobs-and-skills-api/data/esco-1.2.1/v1.jsonld
```

=> @TODO : 
- Fix parsing of this kind of structure rdfs:domain/owl:Class/owl:unionOf
<!-- http://data.europa.eu/esco/model#relatedEssentialSkill -->

    <owl:ObjectProperty rdf:about="http://data.europa.eu/esco/model#relatedEssentialSkill">
        <rdfs:subPropertyOf rdf:resource="http://www.w3.org/2004/02/skos/core#related"/>
        <rdfs:domain>
            <owl:Class>
                <owl:unionOf rdf:parseType="Collection">
                    <rdf:Description rdf:about="http://data.europa.eu/esco/model#Occupation"/>
                    <rdf:Description rdf:about="http://data.europa.eu/esco/model#Skill"/>
                </owl:unionOf>
            </owl:Class>
        </rdfs:domain>
        <rdfs:range rdf:resource="http://data.europa.eu/esco/model#Skill"/>
        <terms:issued rdf:datatype="http://www.w3.org/2001/XMLSchema#date">2013-12-03</terms:issued>
        <terms:modified rdf:datatype="http://www.w3.org/2001/XMLSchema#date">2013-12-03</terms:modified>
        <rdfs:comment xml:lang="en">The ESCO skill or competence that is essential for the subject occupation or skill.</rdfs:comment>
        <rdfs:isDefinedBy rdf:resource="http://data.europa.eu/esco/model"/>
        <rdfs:label xml:lang="en">has essential skill</rdfs:label>
        <vs:term_status rdf:datatype="http://www.w3.org/2001/XMLSchema#string">proposed</vs:term_status>
    </owl:ObjectProperty>
- do change here: 
  - gatsby/src/bin/ttl2ld.js
  - .command('xmld'
  - after const data = await toJsonLD(store) 
  - create logic to translate generated blank nodes to direct "domain" assign 
