
/**
 * The target is to define theses status as entities 
 * `label` field have to be changed in a `display_label` : [langString]
 */

// source: https://gitlab.com/mindmatcher.org/alignement-rome-esco/-/blob/855228d7b856f48ad5fe3676163d5e72626e6511/align/src/suggestions/stateManagement/matchingState.js
const status = [
  {id : 'v', name : 'validated', label: 'oui', state: 'validated'},
  {id : 'vb', name : 'validatedBut', label: 'oui mais', state: 'validated'},
  {id : 'nt', name : 'noTarget', label: 'pas ESCO', state: 'validated'},
  {id : 'a' , name : 'validated-alternative', label:'alt esco', state: 'alternative'},
  {id : 'm', name: 'maybe', label:'peut être',state: 'maybe'},
  {id : 'r', name: 'rejected', label:'non', state:'rejected'},
  {id : 'rb', name: 'rejectedBut', label:'non mais', state:'rejected'},
  {id : 'p', name: 'proposed', label:'', state:'proposed'}
]
