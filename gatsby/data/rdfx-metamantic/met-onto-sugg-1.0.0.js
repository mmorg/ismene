
const ls = (text) => ({
  '@language': 'en',
  '@value': text,
})

const context = {} //@TODO: define the @context !

const graph = [
  {
    id: "sugg:Ontology",
    type: ['owl:Ontology'],
    title: [
      {
        '@language': 'en',
        '@value': 'The Suggestions Ontology of the Metamantic framework'
      }
    ],
  },

  // suggestion and his 2 properties
  {
    id: 'met:Suggestion',
    type: ['rdfs:Class'],
    label: [ls('Suggestion')],
    comment: [ls('A Metamantic class for suggestions')],
    subClassOf: ['rdf:Statement']
  },

  {
    id: 'met:state',
    type: ['rdf:Property'],
    label: [ls('state')],
    comment: [ls('Define the State(s) of this Suggestion after contributions analysis.')],
    domain: ['met:Suggestion'],
    range: ['met:State'],
  },
  {
    id: 'met:contribution',
    type: ['rdf:Property'],
    label: [ls('contribution')],
    comment: [ls('Link the Contribution(s) of this Suggestion.')],
    domain: ['met:Suggestion'],
    range: ['met:Contribution'],
  },


  // 2 generic properties for State and Contributions objects
  {
    id: 'met:source',
    type: ['rdf:Property'],
    label: [ls('source')],
    comment: [ls('Name or link to the person, algorithm, organisation that create this State or Contribution.')],
    domain: ['met:State', 'met:Suggestion'],
    range: ['xsd:string', 'rdfs:Resource'],
  },
  {
    id: 'met:score',
    type: ['rdf:Property'],
    label: [ls('score')],
    comment: [ls('The value associated with the contribution.')],
    domain: ['met:State', 'met:Suggestion'],
    range: ['xsd:float'],
  },

  // the state object :
  {
    id: 'met:State',
    type: ['rdfs:Class'],
    label: [ls('State')],
    comment: [ls('A suggestion\'s state description')],
    subClassOf: ['rdfs:Class'],
  },

  {
    id: 'met:status',
    type: ['rdf:Property'],
    label: [ls('status')],
    comment: [ls('Link status definition(s) to the state.')],
    domain: ['met:State'],
    range: ['skos:Concept'], // @TODO: restriction to met/term/status entities
  },


  // the contribution object 
  {
    id: 'met:Contribution',
    type: ['rdfs:Class'],
    label: [ls('Contribution')],
    comment: [ls('A suggestion\'s contribution from a "suggester" entity (a person, an algorithm,...)')],
    subClassOf: ['rdfs:Class'],
  },

  {
    id: 'met:runId', // optional
    type: ['rdf:Property'],
    label: [ls('runId')],
    comment: [ls('A key to to identify multiples contributions in time and create history of this contributions.')],
    domain: ['met:Contribution'],
    range: ['xsd:string'],
  },


]

const onto = {
  ['@context']: context,
  graph,
}

export default onto


// instance example: 
const inst = {
  id: 'my:123',
  type: ['met:Suggestion'],
  subject: ['rome:125248'],
  property: ['skos:exactMatch'],
  object: ['skill:6ff01827-907d-43d8-b6e4-7162c1cf0aca'],

  // @TODO: check if this property keeped
  pathHash: 'f69674ec825132ab0a7269454793b1c7', // @TODO: do we keep this property ? It's the id ? or any triple can be "defined in advance" ?
  //  ==> the other option of suggestion id can be my:suggs/__subject_id__/__property_id__/__object_id(or value)__/ ==> but can be long and strange for object ==> create a hash of each path

  state: [{
    id: 'my:123/status/1',
    source: 'mm:basicVoterCalculation',
    score: 224.3782637,
    status: 'met:term/noTarget',
  }],

  contribution: [
    {
      id: 'my:output/1234',
      source: 'mlt',
      score: 7.0155697,
      runId: 'anything'
    },
    {
      id: 'my:output/5678',
      source: 'mlt+restricted',
      score: 1.487992346373,
      runId: 'anything'
    },

  ]
}