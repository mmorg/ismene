# Metamantic (met)
Meta semantic 
An ontology for metadata on rdf triples. 
Use reification principles.
Should be extended with yours metadata verbs. 

# @TODO: 
* define a "met:Statement" or "met:TripleMetadata" that subClassOf "rdfs:Statement" ? This root class can then redefine the s,p,o properties.
* add the definition & management of met:term/status: https://gitlab.com/mindmatcher.org/alignement-rome-esco/-/blob/855228d7b856f48ad5fe3676163d5e72626e6511/align/src/suggestions/stateManagement/matchingState.js
* regarder la vidéo de MEP hr-open

# Extensions examples
## met-suggestions

* Preliminary implementations :
  - In js : Emit & apply :  https://gitlab.com/genscan/genscan/-/blob/main/rml/src/jobMatching/createMetInstances.js
  - In python: Emit : https://gitlab.com/genscan/genscan/-/blob/main/rml/src/pythonTagging/runTagging.py

* Code to migrate for suggestions management and examples: https://gitlab.com/mindmatcher.org/alignement-rome-esco/-/blob/ee8a0aff1c5d7388f6297baf006e554947411d47/align/src/suggestions/README-suggestions-format.md
	

# met-prov 

* A metamantic class that reuse the prov-o properties to create data history, lineage, provenance on any Triple in your database.

* id = hashoftriple, s,p,o


# Documentation 

## RDF-Star (RDF*)

* argument: at the serialization level, this seems to be only a convenient notation. The Sparql level may need to be studied. 
* docs: 
  - w3c homepage: https://w3c.github.io/rdf-star/
  - nice tuto: https://www.w3.org/community/rdf-dev/2022/01/26/provenance-in-rdf-star/
  - spec: https://www.w3.org/2021/12/rdf-star.html
  - good comparison of "Triple's metadata" in paragraph "Metadata about Relationships Come in Handy" here: https://www.ontotext.com/blog/rdf-star-metadata-complexity-simplified/
  - detail of the 4 approches: https://www.ontotext.com/knowledgehub/fundamentals/what-is-rdf-star/
  

