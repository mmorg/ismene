<http://w3id.org/gaia-x/core#> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#Ontology> .
<http://w3id.org/gaia-x/core#> <http://creativecommons.org/ns#license> <http://www.apache.org/licenses/LICENSE-2.0> .
<http://w3id.org/gaia-x/core#> <http://purl.org/dc/terms/abstract> "The Gaia-X core ontology contains the components needed to model an efficient, competitive, secure and trustworthy federation of data infrastructure and service providers.\n\nThe different ontology modules can be found here: \n\nCore: <a href=\"https://www.w3id.org/gaia-x/core\">https://www.w3id.org/gaia-x/core</a> \n\nResource: <a href=\"https://www.w3id.org/gaia-x/resource\">https://www.w3id.org/gaia-x/resource</a> \n\nParticipant: <a href=\"https://www.w3id.org/gaia-x/participant\">https://www.w3id.org/gaia-x/participant</a> \n\nService Offering: <a href=\"https://www.w3id.org/gaia-x/service\">https://www.w3id.org/gaia-x/service</a> \n\nCompliance: <a href=\"https://www.w3id.org/gaia-x/compliance\">https://www.w3id.org/gaia-x/compliance</a> \n\nAll underlying information for this documentation page can be found on the landing page of the Gaia-X Service Characteristics repository: <a href=\"https://gaia-x.gitlab.io/technical-committee/service-characteristics\">https://gaia-x.gitlab.io/technical-committee/service-characteristics</a>.\n\nAdditional information on the constraints that have to be fulfilled be the self descriptions can be found here: <a href=\"https://gaia-x.gitlab.io/technical-committee/service-characteristics/yaml2shacl\">https://gaia-x.gitlab.io/technical-committee/service-characteristics/yaml2shacl</a>." .
<http://w3id.org/gaia-x/core#> <http://purl.org/dc/terms/contributor> "Akyürek, Haydar" .
<http://w3id.org/gaia-x/core#> <http://purl.org/dc/terms/contributor> "Bader, Sebastian" .
<http://w3id.org/gaia-x/core#> <http://purl.org/dc/terms/contributor> "Baum, Hannes" .
<http://w3id.org/gaia-x/core#> <http://purl.org/dc/terms/contributor> "Blanch, Josep" .
<http://w3id.org/gaia-x/core#> <http://purl.org/dc/terms/contributor> "Frömberg, Jan" .
<http://w3id.org/gaia-x/core#> <http://purl.org/dc/terms/contributor> "Gronlier, Pierre" .
<http://w3id.org/gaia-x/core#> <http://purl.org/dc/terms/contributor> "Hermsen, Felix" .
<http://w3id.org/gaia-x/core#> <http://purl.org/dc/terms/contributor> "Lange, Christoph" .
<http://w3id.org/gaia-x/core#> <http://purl.org/dc/terms/contributor> "Langkau, Jörg" .
<http://w3id.org/gaia-x/core#> <http://purl.org/dc/terms/contributor> "Leberecht, Markus" .
<http://w3id.org/gaia-x/core#> <http://purl.org/dc/terms/contributor> "Meinke, Kai" .
<http://w3id.org/gaia-x/core#> <http://purl.org/dc/terms/contributor> "Moosmann, Paul" .
<http://w3id.org/gaia-x/core#> <http://purl.org/dc/terms/contributor> "Niessen, Thomas" .
<http://w3id.org/gaia-x/core#> <http://purl.org/dc/terms/contributor> "Ogel, Frederic" .
<http://w3id.org/gaia-x/core#> <http://purl.org/dc/terms/contributor> "Ottradovetz, Klaus" .
<http://w3id.org/gaia-x/core#> <http://purl.org/dc/terms/contributor> "Qin, Chang" .
<http://w3id.org/gaia-x/core#> <http://purl.org/dc/terms/contributor> "Rubina, Alina" .
<http://w3id.org/gaia-x/core#> <http://purl.org/dc/terms/contributor> "Staginus, Judith" .
<http://w3id.org/gaia-x/core#> <http://purl.org/dc/terms/contributor> "Strunk, Anja" .
<http://w3id.org/gaia-x/core#> <http://purl.org/dc/terms/contributor> "Theissen-Lipp, Johannes" .
<http://w3id.org/gaia-x/core#> <http://purl.org/dc/terms/created> "2021-10-18T12:00:00+01:00"^^<http://www.w3.org/2001/XMLSchema#dateTimeStamp> .
<http://w3id.org/gaia-x/core#> <http://purl.org/dc/terms/creator> "Working Group Service Characteristics" .
<http://w3id.org/gaia-x/core#> <http://purl.org/dc/terms/modified> "2022-11-24T18:57:22+01:00"^^<http://www.w3.org/2001/XMLSchema#dateTimeStamp> .
<http://w3id.org/gaia-x/core#> <http://purl.org/dc/terms/title> "Gaia-X Core Ontology"@en .
<http://w3id.org/gaia-x/core#> <http://purl.org/vocab/vann/preferredNamespacePrefix> "gax-core" .
<http://w3id.org/gaia-x/core#> <http://purl.org/vocab/vann/preferredNamespaceUri> "http://w3id.org/gaia-x/core#" .
<http://w3id.org/gaia-x/core#> <http://rdfs.org/ns/void#vocabulary> <http://purl.org/dc/terms/> .
<http://w3id.org/gaia-x/core#> <http://rdfs.org/ns/void#vocabulary> <http://purl.org/vocab/vann/> .
<http://w3id.org/gaia-x/core#> <http://rdfs.org/ns/void#vocabulary> <http://purl.org/vocommons/voaf#> .
<http://w3id.org/gaia-x/core#> <http://rdfs.org/ns/void#vocabulary> <http://rdfs.org/ns/void#> .
<http://w3id.org/gaia-x/core#> <http://www.w3.org/2000/01/rdf-schema#label> "Gaia-X Core Ontology"@en .
<http://w3id.org/gaia-x/core#> <http://www.w3.org/2002/07/owl#versionInfo> "22.04" .
# 
# 
# #################################################################
# #
# #    Annotation properties
# #
# #################################################################
# 
# 
# http://creativecommons.org/ns#license
<http://creativecommons.org/ns#license> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#AnnotationProperty> .
# 
# http://purl.org/dc/terms/abstract
<http://purl.org/dc/terms/abstract> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#AnnotationProperty> .
# 
# http://purl.org/dc/terms/contributor
<http://purl.org/dc/terms/contributor> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#AnnotationProperty> .
# 
# http://purl.org/dc/terms/created
<http://purl.org/dc/terms/created> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#AnnotationProperty> .
# 
# http://purl.org/dc/terms/creator
<http://purl.org/dc/terms/creator> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#AnnotationProperty> .
# 
# http://purl.org/dc/terms/modified
<http://purl.org/dc/terms/modified> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#AnnotationProperty> .
# 
# http://purl.org/dc/terms/title
<http://purl.org/dc/terms/title> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#AnnotationProperty> .
# 
# http://purl.org/vocab/vann/preferredNamespacePrefix
<http://purl.org/vocab/vann/preferredNamespacePrefix> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#AnnotationProperty> .
# 
# http://purl.org/vocab/vann/preferredNamespaceUri
<http://purl.org/vocab/vann/preferredNamespaceUri> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#AnnotationProperty> .
# 
# http://rdfs.org/ns/void#vocabulary
<http://rdfs.org/ns/void#vocabulary> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#AnnotationProperty> .
# 
# 
# 
# #################################################################
# #
# #    Object Properties
# #
# #################################################################
# 
# 
# http://w3id.org/gaia-x/core#aggregationOf
<http://w3id.org/gaia-x/core#aggregationOf> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#ObjectProperty> .
<http://w3id.org/gaia-x/core#aggregationOf> <http://www.w3.org/2000/01/rdf-schema#domain> <http://w3id.org/gaia-x/core#Resource> .
<http://w3id.org/gaia-x/core#aggregationOf> <http://www.w3.org/2000/01/rdf-schema#domain> <http://w3id.org/gaia-x/core#ServiceOffering> .
<http://w3id.org/gaia-x/core#aggregationOf> <http://www.w3.org/2000/01/rdf-schema#range> <http://w3id.org/gaia-x/core#Resource> .
<http://w3id.org/gaia-x/core#aggregationOf> <http://www.w3.org/2000/01/rdf-schema#comment> "DID of resource self-escription related to the service and that can exist independently of it." .
<http://w3id.org/gaia-x/core#aggregationOf> <http://www.w3.org/2000/01/rdf-schema#comment> "DID of resources self-description related to the resource and that can exist independently of it." .
<http://w3id.org/gaia-x/core#aggregationOf> <http://www.w3.org/2000/01/rdf-schema#label> "aggregation of"@en .
# 
# http://w3id.org/gaia-x/core#dependsOn
<http://w3id.org/gaia-x/core#dependsOn> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#ObjectProperty> .
<http://w3id.org/gaia-x/core#dependsOn> <http://www.w3.org/2000/01/rdf-schema#domain> <http://w3id.org/gaia-x/core#ServiceOffering> .
<http://w3id.org/gaia-x/core#dependsOn> <http://www.w3.org/2000/01/rdf-schema#range> <http://w3id.org/gaia-x/core#ServiceOffering> .
<http://w3id.org/gaia-x/core#dependsOn> <http://www.w3.org/2000/01/rdf-schema#comment> "DID of the service offering self-description related to the service and that can exist independently of it." .
<http://w3id.org/gaia-x/core#dependsOn> <http://www.w3.org/2000/01/rdf-schema#label> "depends on"@en .
# 
# http://w3id.org/gaia-x/core#offeredBy
<http://w3id.org/gaia-x/core#offeredBy> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#ObjectProperty> .
<http://w3id.org/gaia-x/core#offeredBy> <http://www.w3.org/2000/01/rdf-schema#domain> <http://w3id.org/gaia-x/core#ServiceOffering> .
<http://w3id.org/gaia-x/core#offeredBy> <http://www.w3.org/2000/01/rdf-schema#range> <http://w3id.org/gaia-x/core#Participant> .
<http://w3id.org/gaia-x/core#offeredBy> <http://www.w3.org/2000/01/rdf-schema#comment> "DID of participant self-descrription, who is offering this service offering." .
<http://w3id.org/gaia-x/core#offeredBy> <http://www.w3.org/2000/01/rdf-schema#label> "offered by"@en .
# 
# http://w3id.org/gaia-x/core#operatedBy
<http://w3id.org/gaia-x/core#operatedBy> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#ObjectProperty> .
<http://w3id.org/gaia-x/core#operatedBy> <http://www.w3.org/2000/01/rdf-schema#domain> <http://w3id.org/gaia-x/core#Resource> .
<http://w3id.org/gaia-x/core#operatedBy> <http://www.w3.org/2000/01/rdf-schema#range> <http://w3id.org/gaia-x/core#Participant> .
<http://w3id.org/gaia-x/core#operatedBy> <http://www.w3.org/2000/01/rdf-schema#comment> "DID of participant self-description related to the participant, who operates this resource." .
<http://w3id.org/gaia-x/core#operatedBy> <http://www.w3.org/2000/01/rdf-schema#label> "operated by"@en .
# 
# 
# 
# #################################################################
# #
# #    Classes
# #
# #################################################################
# 
# 
# http://purl.org/vocommons/voaf#Vocabulary
<http://purl.org/vocommons/voaf#Vocabulary> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#Class> .
# 
# http://w3id.org/gaia-x/core#Participant
<http://w3id.org/gaia-x/core#Participant> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#Class> .
<http://w3id.org/gaia-x/core#Participant> <http://www.w3.org/2000/01/rdf-schema#label> "Participant"@en .
# 
# http://w3id.org/gaia-x/core#Resource
<http://w3id.org/gaia-x/core#Resource> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#Class> .
<http://w3id.org/gaia-x/core#Resource> <http://www.w3.org/2000/01/rdf-schema#label> "Resource"@en .
# 
# http://w3id.org/gaia-x/core#ServiceOffering
<http://w3id.org/gaia-x/core#ServiceOffering> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#Class> .
<http://w3id.org/gaia-x/core#ServiceOffering> <http://www.w3.org/2000/01/rdf-schema#label> "Service Offering"@en .
# 
# 
# 
# #################################################################
# #
# #    Individuals
# #
# #################################################################
# 
# 
# http://w3id.org/gaia-x/core#
<http://w3id.org/gaia-x/core#> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#NamedIndividual> .
<http://w3id.org/gaia-x/core#> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://purl.org/vocommons/voaf#Vocabulary> .
# 
# Generated by the OWL API (version 5.1.14) https://github.com/owlcs/owlapi/
