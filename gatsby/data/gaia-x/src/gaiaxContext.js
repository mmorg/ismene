import ccContext from '../../../src/stdContext/ccContext.js'
import dctContext from '../../../src/stdContext/dctContext.js'
import foafContext from '../../../src/stdContext/foafContext.js'
import owlContext from '../../../src/stdContext/owlContext.js'
import rdfContext from '../../../src/stdContext/rdfContext.js'
import rdfsContext from '../../../src/stdContext/rdfsContext.js'
import vannContext from '../../../src/stdContext/vannContext.js'
import vcardContext from '../../../src/stdContext/vcardContext.js'
import vcContext from '../../../src/stdContext/vcContext.js'
import voafContext from '../../../src/stdContext/voafContext.js'
import voidContext from '../../../src/stdContext/voidContext.js'
import w3nsContext from '../../../src/stdContext/w3nsContext.js'

export default function gaiaxContext() {
  const globalCtx = [
    localContext(),
    dctContext,
    ccContext,
    vannContext,
    rdfContext,
    rdfsContext,
    owlContext,
    voidContext,
    vcContext,
    foafContext,
    vcardContext,
    w3nsContext,
    voafContext,
    
  ]
  return globalCtx
}

function localContext() {
  return {
    id: '@id',
    graph: {
      '@id': '@graph',
      '@container': '@set'
    },
    type: {
      '@id': '@type',
      '@container': '@set'
    },

    gxcore: 'http://w3id.org/gaia-x/core#',
    gxframework: 'http://w3id.org/gaia-x/gax-trust-framework#',

    // literals

    // datatypes


    // references

  }
}