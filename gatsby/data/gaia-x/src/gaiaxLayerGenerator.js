// import addDeviationType from './addDeviationType.js'
// import entityRemover from './entityRemover.js'
// import propertyDomainRangeGuess from './propertyDomainRangeGuess.js'
// import propertyExplicitInherit from './propertyExplicitInherit.js'
// import propertySpecializeDomain from './propertySpecializeDomain.js'
// import skosCollectionCleaner from './skosCollectionCleaner.js'
// import skosMemberOf from './skosMemberOf.js'

import propertyDomainRangeGuess from '../../../src/graphLayerScripts/propertyDomainRangeGuess.js'
import propertyExplicitInherit from '../../../src/graphLayerScripts/propertyExplicitInherit.js'
import propertySpecializeDomain from '../../../src/graphLayerScripts/propertySpecializeDomain.js'

// @TODO: this is still used ?
export const layerRealm = ['skos_GEN_generator']

const gx22_10 = 'gx_1.22.10'
const ismene = 'ismene'
const gen_v1 = 'gen_v1'
const gen_v1_entityToRemove = [
  // Class
  'skos:ConceptScheme', 'skos:OrderedCollection',
  // Property
  'skos:hasTopConcept', 'skos:inScheme', 'skos:memberList',


  'skos:broadMatch', 'skos:broaderTransitive', 'skos:changeNote', 'skos:closeMatch',
  'skos:editorialNote', 'skos:exactMatch', 'skos:example', 'skos:hiddenLabel',
  'skos:historyNote', 'skos:mappingRelation', 'skos:narrowMatch', 'skos:narrowerTransitive',
  'skos:notation', 'skos:note', 'skos:related', 'skos:relatedMatch', 'skos:scopeNote',
  'skos:semanticRelation', 'skos:topConceptOf',
]
const skosCollection_entityToRemove = [
  '_:b0_n0', '_:b0_n2', '_:b0_n3',
]

export default function skosGENGenerator() {

  // @TODO: ? skos1_3_0 have to be removed from propertyExplicitInherit & propertyDomainRangeGuess or not ?
  const processors = [
    {
      fn: propertyExplicitInherit,
      layerRealm: [gx22_10, ismene]
    },
    {
      fn: propertyDomainRangeGuess,
      layerRealm: [gx22_10, ismene]
    },
    {
      fn: propertySpecializeDomain,
      layerRealm: [gx22_10, ismene]
    },
    // {
    //   fn: entityRemover,
    //   layerRealm: [gen_v1],
    //   options: { toRemove: gen_v1_entityToRemove, log: false }
    // },
    // {
    //   fn: skosCollectionCleaner,
    //   layerRealm: [skos1_3_0, ismene, gen_v1],
    //   options: { toRemove: skosCollection_entityToRemove },
    // },
    // { // add the inverse property of "member" : "memberOf"
    //   fn:skosMemberOf,
    //   layerRealm:[skos1_3_0, ismene, gen_v1],
    // }

  ]

  return processors

}
