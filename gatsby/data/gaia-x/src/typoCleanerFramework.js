import { rejectedNS, typoCleaner } from './typoCleaner.js'



// add a gaiax-core ns to remove "imported by tool" entity
export default function defaultCleaner(ldDoc) {
  const localRejected = [...rejectedNS, 'gxcore']
  typoCleaner(ldDoc, localRejected)
}
