import remove from 'lodash.remove'

export const rejectedNS = [
  'cc',
  'vann',
  'voaf',
  'dct',
  'void',
  'xsd',
  'vcard',
  'w3ns',
  'foaf',
  'vc',
]

// remove "external typings for tool compatibilty"
export default function defaultCleaner(ldDoc) {
  typoCleaner(ldDoc, rejectedNS)
}

export function typoCleaner(ldDoc, rejectedNS){
  const removed = remove(ldDoc.graph, (n) => {
    const [ns, name] = n.id.split(':')
    return rejectedNS.includes(ns)
  })
  console.log(removed.length, 'entities removed from ontology')
}