
# 1/ Ontology Transformation script :
```
node gatsby/src/bin/ttl2ld ttld -f 'gatsby/data/gaia-x/ontology.22.10/gax-core/ontology.ttl' -c 'gatsby/data/gaia-x/src/gaiaxContext.js' -r 'gatsby/data/gaia-x/src/typoCleaner.js'

node gatsby/src/bin/ttl2ld ttld -f 'gatsby/data/gaia-x/ontology.22.10/gax-trust-framework/ontology.ttl' -c 'gatsby/data/gaia-x/src/gaiaxContext.js' -r 'gatsby/data/gaia-x/src/typoCleanerFramework.js'
```

# 2/ indexation of the ontology ==> Layerize the ontology 

- check that the symbolic links are still valids
```
node gatsby/src/bin/integrate mu -f '**/data/gaia-x/gx-core/*.jsonld' -g 'gatsby/data/gaia-x/src/gaiaxLayerGenerator.js' -u

node gatsby/src/bin/integrate mu -f '**/data/gaia-x/gx-framework/*.jsonld' -g 'gatsby/data/gaia-x/src/gaiaxLayerGenerator.js' -u
```
- files are saved in `__layers__`

# 3/ Exports of the layerized graphs 
```
node gatsby/src/bin/compactLayers export -p 'gatsby/data/gaia-x/gx-core' -u

node gatsby/src/bin/compactLayers export -p 'gatsby/data/gaia-x/gx-framework' -u
```
- files are saved in `__exports__`

# 4/ import this ontology into Gatsby: 

!!! manual copies and fixes have to be done
@TODO: improve this process 

* copy `export` file & `ontomodel` file to name-ismene 
  - @TODO: create symbolic links & automate 
  - @bugFix: ln -s can't be created because of `license` field)

* add configuration to : 
  - gatsby/src/configs/rdfSourcesConfig.js

* see `# Debug notes` pour les problèmes d'affichage dans le menu

