@prefix : <http://ontology.datasud.fr/openemploi/> .
@prefix dc: <http://purl.org/dc/elements/1.1/> .
@prefix oep: <http://openemploi.datasud.fr/ontology/> .
@prefix owl: <http://www.w3.org/2002/07/owl#> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix xml: <http://www.w3.org/XML/1998/namespace> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
@prefix prov: <http://www.w3.org/ns/prov#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix carto: <https://ontologies.mindmatcher.org/carto/> .
@prefix terms: <http://purl.org/dc/terms/> .
@base <http://ontology.datasud.fr/openemploi/> .

<http://ontology.datasud.fr/openemploi/> rdf:type owl:Ontology ;
                                          owl:versionIRI <http://ontology.datasud.fr/openemploi/1.0.0/> ;
                                          owl:imports <http://ns.mnemotix.com/ontologies/2019/8/generic-model> ,
                                                      carto: ;
                                          dc:title "Ontologie Open Emploi"@fr ;
                                          terms:contributor "Mathieu Rogelja (Mnemotix)"@fr ,
                                                            "Pierre-René Lhérisson (Mnemotix)"@fr, "Olivier Coreau (mindmatcher.org)"@fr ;
                                          terms:creator "Nicolas Delaforge (Mnemotix)"@fr, "Florent André (mindmatcher.org)"@fr ;
                                          terms:description "Ontologie pour la description des relations entre compétences et pour le lien entre données de l'emploi et données de la formation."@fr .

#################################################################
#    Object Properties
#################################################################

###  http://ontology.datasud.fr/openemploi/hasPrereq
:hasPrereq rdf:type owl:ObjectProperty ;
           rdfs:domain :Knowledge ;
           rdfs:range :Prerequisite .


###  http://ontology.datasud.fr/openemploi/hasRncp
:hasRncp rdf:type owl:ObjectProperty ;
         rdfs:subPropertyOf owl:topObjectProperty .


###  http://ontology.datasud.fr/openemploi/hasZoneEmploi
:hasZoneEmploi rdf:type owl:ObjectProperty .


###  http://ontology.datasud.fr/openemploi/isMadeOf
:isMadeOf rdf:type owl:ObjectProperty ;
          rdfs:domain :Prerequisite ;
          rdfs:range :Knowledge .


###  http://ontology.datasud.fr/openemploi/isProvidedBy
:isProvidedBy rdf:type owl:ObjectProperty ;
              rdfs:domain :Training ;
              rdfs:range <http://ns.mnemotix.com/ontologies/2019/8/generic-model/Organization> .


###  http://ontology.datasud.fr/openemploi/memberOf
:memberOf rdf:type owl:ObjectProperty ;
          rdfs:domain carto:Skill ;
          rdfs:range :SkillGroup .


###  https://ontologies.mindmatcher.org/carto/hasOccupation
carto:hasOccupation rdfs:domain :Training .


#################################################################
#    Data properties
#################################################################

###  http://ontology.datasud.fr/openemploi/classroomHour
:classroomHour rdf:type owl:DatatypeProperty ;
               rdfs:domain :Training ;
               rdfs:range xsd:integer .


###  http://ontology.datasud.fr/openemploi/companyHour
:companyHour rdf:type owl:DatatypeProperty ;
             rdfs:domain :Training ;
             rdfs:range xsd:integer .


###  http://ontology.datasud.fr/openemploi/coreSkill
:coreSkill rdf:type owl:DatatypeProperty ;
           rdfs:subPropertyOf owl:topDataProperty ;
           rdfs:domain carto:Skill ;
           rdfs:range xsd:boolean .


###  http://ontology.datasud.fr/openemploi/disableAccessibility
:disableAccessibility rdf:type owl:DatatypeProperty ;
                      rdfs:domain :Training ;
                      rdfs:range xsd:boolean .


###  http://ontology.datasud.fr/openemploi/expertSkill
:expertSkill rdf:type owl:DatatypeProperty ;
             rdfs:subPropertyOf owl:topDataProperty ;
             rdfs:domain carto:Skill ;
             rdfs:range xsd:boolean .


###  http://ontology.datasud.fr/openemploi/isCpfEligible
:isCpfEligible rdf:type owl:DatatypeProperty ;
               rdfs:domain :Training ;
               rdfs:range xsd:boolean .


###  http://ontology.datasud.fr/openemploi/isMandatory
:isMandatory rdf:type owl:DatatypeProperty ;
             rdfs:domain :Prerequisite ;
             rdfs:range xsd:boolean .


###  http://ontology.datasud.fr/openemploi/iscoCode
:iscoCode rdf:type owl:DatatypeProperty ;
          rdfs:subPropertyOf owl:topDataProperty ;
          rdfs:domain carto:Occupation ;
          rdfs:range xsd:string .


###  http://ontology.datasud.fr/openemploi/objective
:objective rdf:type owl:DatatypeProperty ;
           rdfs:domain :Training ;
           rdfs:range xsd:string .


###  http://ontology.datasud.fr/openemploi/softSkill
:softSkill rdf:type owl:DatatypeProperty ;
           rdfs:subPropertyOf owl:topDataProperty ;
           rdfs:domain carto:Skill ;
           rdfs:range xsd:boolean .


###  http://ontology.datasud.fr/openemploi/specificSkill
:specificSkill rdf:type owl:DatatypeProperty ;
               rdfs:subPropertyOf owl:topDataProperty ;
               rdfs:domain :SkillGroup ,
                           carto:Skill ;
               rdfs:range xsd:boolean .


###  http://ontology.datasud.fr/openemploi/totalHour
:totalHour rdf:type owl:DatatypeProperty ;
           rdfs:domain :Training ;
           rdfs:range xsd:integer .


#################################################################
#    Classes
#################################################################

###  http://ontology.datasud.fr/openemploi/Certification
:Certification rdf:type owl:Class ;
               rdfs:subClassOf <http://ns.mnemotix.com/ontologies/2019/8/generic-model/Entity> .


###  http://ontology.datasud.fr/openemploi/Certifinfo
:Certifinfo rdf:type owl:Class ;
            rdfs:subClassOf <http://www.w3.org/2004/02/skos/core#Concept> .


###  http://ontology.datasud.fr/openemploi/FormaCode
:FormaCode rdf:type owl:Class ;
           rdfs:subClassOf <http://www.w3.org/2004/02/skos/core#Concept> .


###  http://ontology.datasud.fr/openemploi/Knowledge
:Knowledge rdf:type owl:Class ;
           rdfs:subClassOf <http://www.w3.org/2004/02/skos/core#Concept> .


###  http://ontology.datasud.fr/openemploi/Nsf
:Nsf rdf:type owl:Class ;
     rdfs:subClassOf <http://www.w3.org/2004/02/skos/core#Concept> .


###  http://ontology.datasud.fr/openemploi/Prerequisite
:Prerequisite rdf:type owl:Class ;
              rdfs:subClassOf <http://ns.mnemotix.com/ontologies/2019/8/generic-model/Entity> ;
              rdfs:comment "Réification de la relation de pré-requis pour les parcours de formation"@fr .


###  http://ontology.datasud.fr/openemploi/SkillGroup
:SkillGroup rdf:type owl:Class ;
            rdfs:subClassOf :Knowledge ;
            rdfs:comment "Classe qui représente les groupes/blocs de compétences."@fr .


###  http://ontology.datasud.fr/openemploi/Training
:Training rdf:type owl:Class ;
          rdfs:subClassOf <http://ns.mnemotix.com/ontologies/2019/8/generic-model/Duration> ;
          rdfs:comment "Classe générique qui représente les contenus/modules de formation"@fr .


###  Generated by the OWL API (version 4.5.9.2019-02-01T07:24:44Z) https://github.com/owlcs/owlapi
