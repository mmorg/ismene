Ontology Transformation script :
```
docker-compose exec gatsby node src/bin/ttl2ld ttld -f 'data/openEmploi/oep-ontology-1.0.0.ttl' -r 'oepCleaner.js'
```

Ontology Indexation script: 
```
docker-compose exec gatsby node src/bin/integrate mu -f '**/openEmploi/*.jsonld' -u
```
