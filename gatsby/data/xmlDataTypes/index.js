import { dirname } from 'path'
import { fileURLToPath } from 'url'
const __dirname = dirname(fileURLToPath(import.meta.url))
import { readJson } from '@mmorg/fsutils'

export const ontologyFileName = 'dataTypeDefinitions.jsonld'
export const ontology = readJson(`${__dirname}/${ontologyFileName}`)

export default ontology 
