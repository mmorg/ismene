
* Apparently there is no RDFS definition of the xsd datatypes
* The DataTypeDefinitions.xsd source file is extracted from [this recommandation](https://www.w3.org/TR/xmlschema-2/#schema)
  * This document precise the definition of datatypes used in the main [xml schema description](https://www.w3.org/2001/XMLSchema)

* A manual version rdfs version of this xsd is done for nodefr-2

## RML transformation of dataTypeDefinitions.xsd

* for this processing : 
  * attention, a cause du "Esm War", il faut réactiver `type:modules` dans le `package.json` avant de faire tourner ce script.
```
docker-compose exec gatsby npm run toRdf -- -s '**/xmlDataTypes/dataTypeDefinitions.xsd' -m '**/yarrml-xsd/Classes.yaml' -p 'xmlDatatypesProfile.js' -t 'xmlDataTypes' 'xmlDataTypes'
```

* after remove hrrdf.* created files while no parameters in command
