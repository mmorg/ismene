
version 1.3 is from 2017-07-20 and retrived from : https://www.w3.org/ns/shacl

version 1.4 is from and retrived from : https://github.com/w3c/data-shapes/blob/gh-pages/shacl/shacl.ttl


Ontology Transformation script :
```
docker-compose exec gatsby node src/bin/ttl2ld ttld -f 'data/shacl/shacl-1.4.0.ttl'
```

Ontology Indexation script: 
```
docker-compose exec gatsby node src/bin/integrate mu -f '**/shacl/*.jsonld' -u
```
