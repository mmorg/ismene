
* pour une traduction en fr des entitées du schema, importer : https://www.w3.org/2000/01/combined-ns-translation.rdf.fr
  * cette traduction est disponible à partir du fichier "see also" suivant : https://www.w3.org/2000/01/rdf-schema-more

* une version 1.2 de la specification de rdf:schema, en cours de draft: https://www.w3.org/TR/rdf12-schema/



Ontology Transformation script :
```
docker-compose exec gatsby node src/bin/ttl2ld ttld -f 'data/rdfs/rdf-schema-1.1.0.ttl'
```

Ontology Indexation script: 
```
docker-compose exec gatsby node src/bin/integrate mu -f '**/rdfs/*.jsonld' -u
```
