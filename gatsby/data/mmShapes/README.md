
* la version 0.0.1 est une exploration en cours à partir des ttl récupérés à partir des specs 
  * l'objectif est de définir une 1.0.0 qui soit directement en json-ld



Ontology Transformation script :
```
docker-compose exec gatsby node src/bin/ttl2ld ttld -f 'data/mmShapes/mmshape-with-example-0.0.1.ttl'
```

Ontology Indexation script: 
```
docker-compose exec gatsby node src/bin/integrate mu -f '**/mmShapes/*.jsonld' -u
```
