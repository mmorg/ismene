
* source for downloaded ttl file : https://gitlab.com/mnemotix/mnx-models/-/blob/master/generic-model/

* Remarks: 
  * on `mnx-skos.ttl` this is mostly addenum to skos:. For now this entities are removed. Check the accuracy of this extensions.
  * `mnx.ttl` is a more recent extension of `mnx-project.ttl`. This is the owl:Ontology & version number of `mnx.ttl` that will be keeped.


Ontology Transformation script :
```
docker-compose exec gatsby node src/bin/ttl2ld ttld -f 'data/mnxGenericModel/mnx-1.0.9.ttl' -t 'mnxMergeScript.js' -r 'mnxCleaner.js'
```

Ontology Indexation script: 
```
docker-compose exec gatsby node src/bin/integrate mu -f '**/mnxGenericModel/*.jsonld' -u
```
