
Transformation script :
```
docker-compose exec gatsby node src/bin/ttl2ld ttld -f 'data/dataCube/cube-0.2.0.ttl'
```

Ontology Indexation script: 
```
docker-compose exec gatsby node src/bin/integrate mu -f '**/datacube/*.jsonld' -u
```
