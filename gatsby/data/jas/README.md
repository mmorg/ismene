
* JAS is a composition of multiples ontology layers 

1/ Exports of the layerized graphs 
```
node src/bin/compactLayers export -p 'data/esco' 'data/skos' -t 'data/jas' -l 'jasOnto-1.0.0' -u
```
- files are saved in `__exports__`

2/ copy generated version to jas-api 
```
cp data/jas/__exports__/gl-compact\:jasOnto-1.0.0.jsonld ../services/jobs-and-skills-api/data/jasOnto-1.0.0/v1.1.jsonld 
```