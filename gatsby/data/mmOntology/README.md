Ontology Transformation script :
```
docker-compose exec gatsby node src/bin/ttl2ld ttld -f 'data/mmOntology/mm-ontology-1.0.0.ttl' -r 'mmoCleaner.js'
```

Ontology Indexation script: 
```
docker-compose exec gatsby node src/bin/integrate mu -f '**/mmOntology/*.jsonld' -u
```

# @TODO :

* the "inverseOf" property is only assign in one direction. This should be done for the other direction.
  - The inversed property is copied here for the first example only.
  - use bellow examples to create tests.

# hasAptitude / isAptitudeOf

```json
{
			"id": "mmo:hasAptitude",
			"type": [
				"owl:ObjectProperty",
				"rdf:Property"
			],
			"domain": [
				"mmo:Expectation"
			],
			"range": [
				"mmo:Aptitude"
			],
			"inverseOf": [
				"mmo:isAptitudeOf"
			],
			"inScheme": [
				"mmo:scheme_for_Property_generated"
			]
		},

{
			"id": "mmo:isAptitudeOf",
			"type": [
				"owl:ObjectProperty",
				"rdf:Property"
			],
			"domain": [
				"mmo:Aptitude"
			],
			"range": [
				"mmo:Expectation"
			],
			"inScheme": [
				"mmo:scheme_for_Property_generated"
			]
		},
```

# hasSkill / isSkillOf 

``` json
{
			"id": "mmo:hasSkill",
			"type": [
				"owl:ObjectProperty",
				"rdf:Property"
			],
			"domain": [
				"mmo:Aptitude"
			],
			"range": [
				"mmo:Skill"
			],
			"inverseOf": [
				"mmo:isSkillOf"
			],
			"inScheme": [
				"mmo:scheme_for_Property_generated"
			]
		},
```

# hasOccupation / isOccupationsOf

```json
{
			"id": "mmo:hasOccupation",
			"type": [
				"owl:ObjectProperty",
				"rdf:Property"
			],
			"range": [
				"mmo:Occupation"
			],
			"inverseOf": [
				"mmo:isOccupationOf"
			],
			"inScheme": [
				"mmo:scheme_for_Property_generated"
			]
		},
```

# expects / isExpectationOf

```json
[
  {
    "id": "mmo:expects",
    "type": ["owl:ObjectProperty", "rdf:Property"],
    "domain": ["mmo:Offer"],
    "range": ["mmo:Expectation"],
    "inverseOf": ["mmo:isExpectationOf"],
    "inScheme": ["mmo:scheme_for_Property_generated"]
  }
]
```

# hasOffer / isOfferOf 

```json
{
			"id": "mmo:hasOffer",
			"type": [
				"owl:ObjectProperty",
				"rdf:Property"
			],
			"domain": [
				"mmo:Apply"
			],
			"range": [
				"mmo:Offer"
			],
			"inverseOf": [
				"mmo:isOfferOf"
			],
			"inScheme": [
				"mmo:scheme_for_Property_generated"
			]
		},

```

## hasQualification / isQualificationOf 

```json
{
			"id": "mmo:hasQualification",
			"type": [
				"owl:ObjectProperty",
				"rdf:Property"
			],
			"domain": [
				"mmo:Award",
				"mmo:Experience"
			],
			"range": [
				"mmo:Qualification"
			],
			"inverseOf": [
				"mmo:isQualificationOf"
			],
			"inScheme": [
				"mmo:scheme_for_Property_generated"
			]
		},
```