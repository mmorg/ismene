
Ontology Transformation script :
```
docker-compose exec gatsby node src/bin/ttl2ld ttld -f 'data/rdf/22-rdf-syntax-ns-1.1.0.ttl'
```

Ontology Indexation script: 
```
docker-compose exec gatsby node src/bin/integrate mu -f '**/rdf/*.jsonld' -u
```
