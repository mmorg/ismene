import { dirname } from 'path'
import { fileURLToPath } from 'url'
const __dirname = dirname(fileURLToPath(import.meta.url))
import { readJson } from '@mmorg/fsutils'

export const ontologyFileName = '22-rdf-syntax-ns-1.1.0.jsonld'
export const ontology = readJson(`${__dirname}/${ontologyFileName}`)

export default ontology 
