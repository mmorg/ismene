#! /bin/sh 

docker compose build ismene
docker tag ismene europe-west1-docker.pkg.dev/ismene/prometheus-x-registry/ismene-test
docker push europe-west1-docker.pkg.dev/ismene/prometheus-x-registry/ismene-test
gcloud run deploy ismene --image=europe-west1-docker.pkg.dev/ismene/prometheus-x-registry/ismene-test --region=europe-west1
