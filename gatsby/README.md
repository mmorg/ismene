
# dev :

* [acces gatsby](http://localhost:1111/)
* [gatsby liste des pages](http://localhost:1111/dddd)
* [graphql](http://localhost:1111/___graphql)


## build & publish to prod: 

```
docker compose exec gatsby npm run cb
rm -rf toProd/public
cp -r gatsby/public/ toProd/.
git add toProd/
git commit -m "gen: next version"
git push
```

@TODO: documentation on `serv-gatsby`


# Theme et Gatsby SSR/Browser

* Le plugin `gatsby-theme-material-ui` est utilisé pour la configuration du thème MUI.
* Ce plugin surcharge les gatsby ssr et browser, via le composant: `gatsby/src/gatsby-theme-material-ui-top-layout/components/top-layout.js`

* L'ajout du DashLayout est donc fait dans ce composant de haut niveau. 

* l'ajout potentiel d'un store redux devra être fait dans ce composant


# restart here for adding new components:
* the last layout: gatsby/src/components/dashorm/DashTreeLayout6.js
* the new component type: gatsby/src/components/dashorm/cardComponents/ClassLiteralsComponent.js


# New ontology integration : 

see [/README.md] (paragraph : "Ontology integration")



# clean et restart pour la génération de toutes les pages 
```
docker-compose exec gatsby gatsby clean
```
* restart du container 

* si ces commandes ne permettent pas le parsing des données, faire une modification sans conséquence (exemple espace) sur ce fichier `gatsby/gatsby-node.js` 

# tests : 

```
docker compose exec gatsby npm test
```

## build : 

* build on local dev
```
docker compose exec gatsby npm run cb
```

* build on CI env
```
@todo: document
```

## lancement du serveur local `gatsby serve`

* Permet de faire tourner en paralelle la version dev et la version build. Pour (plus d'informations)[documentation/carnets_dev/gatby-serve_gatsbyDev_in_parallel.md]
* Commandes pour le up du container et le lancement d'une nouvelle génération
```
docker-compose up serv-gatsby
./gatsby/scripts/serv-restart-gatsby.sh
```
* url : http://localhost:9000/oep/Ontology/


## Tips and Tricks



# a extraire : 

* le plugin de création de nodes à partir de fichiers google : `gatsby/plugins/gatsby-source-google-spreadsheet2unist`

* le plugin de transformation des noeuds unist en noeuds gatsby


# @TODO for semantic dashboard : 

* idée générale : "la création d'un site sémantique par le menu"

* repartir de ce template : `gatsby/src/components/dashorm/DashTreeLayout.js`
* mieux séparer les parties dashboard autogen et sankey dans le fichier
* créer des menusTree puis les pagesTree dans le graphql
  * identifier la méthode de "reactification" pour le menu : a priori la même que pour le contentTree : tree -> hast -> react
* documenter le fonctionnement et l'usage du `pageContext`
* gestion de la position des card, notaemment via les sizeScale
* Notes : 
  * les menuTree et pageTree sont déréférencés dans le template pour disposer d'un aspect dynamique

