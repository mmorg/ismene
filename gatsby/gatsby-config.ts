import type { GatsbyConfig } from 'gatsby'
import { configVariables } from './src/configs/configVariables.js'
import getGatsbySources from './src/gatsbyUtils/getGatsbySources.js'

const {
  hrrdfFolder,
  nodefrFolder,
} = configVariables

const ontologyName = `Explorateur d'ontologies`

const sources = getGatsbySources()
// -----> @TODO: old `.results` json-ld, to update at source and remove
const oldSources = [// 1 - Generation for Hrrdf
  {
    resolve: `gatsby-source-filesystem`,
    options: {
      name: `jsonLD`,
      path: hrrdfFolder,
    },
  },

  // 3 - Generation for NodeFR
  {
    resolve: `gatsby-source-filesystem`,
    options: {
      name: `jsonLD`,
      path: nodefrFolder,
    },
  },
]
// console.log('Brut force changes to fasten interop-api build. @TODO: add an env var & a sourceConfig with only oriane model')
// const baseFolder = `${process.cwd()}`
// const dataFolder = `${baseFolder}/data`
// const sources = [{
//   resolve: `gatsby-source-filesystem`,
//   options: {
//     name: `jsonLD`,
//     path: `${dataFolder}/oriane-rdf-release`,
//   },
// }]
// const oldSources = []

const config: GatsbyConfig = {
  siteMetadata: { //@TODO: update and default use. This is not actually used
    title: `${ontologyName}`,
    description: `Navigation entre les classes et propriétés de ${ontologyName}.`,
    author: `@mindmatcher.org`,
    siteUrl: `https://ismene.competencies.be`, // @TODO: add a custom resolver `resolveSiteUrl` function to get "dynamic config"
  },
  // More easily incorporate content into your pages through automatic TypeScript type generation and better GraphQL IntelliSense.
  // If you use VSCode you can also use the GraphQL plugin
  // Learn more at: https://gatsby.dev/graphql-typegen
  graphqlTypegen: true,
  plugins: [
    'gatsby-plugin-sitemap',

    // @TODO: 20220908: can be removed no ?
    // {
    //   // for trailing slash management see : https://github.com/gatsbyjs/gatsby/discussions/27889
    //   resolve: `gatsby-plugin-force-trailing-slashes`,
    //   options: {
    //     excludedPaths: [`/404.html`],
    //   }
    // },

    // // 0 - global configurations
    // `gatsby-plugin-react-helmet`,
    // localhosted font files are not possible for now: https://github.com/hupe1980/gatsby-plugin-webfonts/issues/5
    `gatsby-theme-material-ui`, // Material-ui template management

    // images management (@TODO: check - comment before 20220908)
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },


    ...oldSources,

    // rdf-ld Generations
    ...sources,

  ],
}

export default config
