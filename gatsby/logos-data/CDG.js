import getOrganisationAndLogo from '../src/renovationScripts/getOrganisationAndLogo.js'

const CDG = getOrganisationAndLogo('CDG', 'fr', '.svg')
export default CDG
