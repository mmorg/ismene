import getOrganisationAndLogo from '../src/renovationScripts/getOrganisationAndLogo.js'

const academieDeVersailles = getOrganisationAndLogo('Academie de Versailles', 'fr', '.jpg')
export default academieDeVersailles
