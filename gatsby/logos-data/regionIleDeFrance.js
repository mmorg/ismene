import getOrganisationAndLogo from '../src/renovationScripts/getOrganisationAndLogo.js'

const regionIleDeFrance = getOrganisationAndLogo('regionIleDeFrance', 'fr', '.png')
export default regionIleDeFrance

