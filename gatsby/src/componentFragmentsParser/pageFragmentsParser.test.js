import pageFragmentsParser from './pageFragmentsParser.js'


// docker compose exec gatsby npm test -- --watch pageFragmentsParser.test

describe('dev test', () => {
    it('Extract GraphQL fragment from a page that declare "components" const', () => {
        const testTemplatePath = `${__dirname}/__fixtures__/DashTreeLayout5.js`
        const fragmentsFile = pageFragmentsParser(testTemplatePath)
        const fragment1 = fragmentsFile[0]
        expect(fragment1[0]).toBe('ClassLiteralsComponent')
        expect(fragment1[1].startsWith('fragment ClassLiteralsComponent')).toBe(true)
    })

    it.todo('implement the "dashComponentProvider"')
    // it('Extract GraphQL fragment from a page that import "defaultDashComponents" const', () => {
    //     expect('imp').toBe(true)
    // })

})
