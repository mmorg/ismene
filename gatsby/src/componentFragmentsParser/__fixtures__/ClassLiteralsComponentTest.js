import React from 'react'
import { graphql } from "gatsby"
import LiteralsAsText from '../baseComponents/LiteralsAsText'

// @TODO: create a generic LiteralsComponent based on
//      - a graphql interface hierarchy : https://www.gatsbyjs.com/docs/reference/graphql-data-layer/schema-customization/#custom-interfaces-and-unions
//      - a merge of : gatsby/src/components/dashorm/cardComponents/ConceptSchemeLiteralsComponent.js
//      - and : gatsby/src/components/dashorm/cardComponents/ClassLiteralsComponent.js
// --> issue to create

// this is to override the Gatsby's 'graphql' stringTemplate and not get compilation errors
function graphqlTest (strings){
  return strings.join('')
}


export const query = graphqlTest`
fragment ClassLiteralsComponent on Query{
  rdfsClass(id: {eq: $id}) {
    id
    label {
      _language
      _value
    }
    comment {
      _language
      _value
    }
    example {
      _language
      _value
    }
    status
  }
}
`

export default function ClassLiteralsComponent(props) {

  const { rdfsClass: entity } = props.data

  const literals = ['comment', 'example', 'status']

  return (
    <LiteralsAsText entity={entity} literalsConfig={literals}/>
  )
}
