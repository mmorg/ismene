import React, { useState } from 'react'
// import clsx from 'clsx'
// import { makeStyles } from '@material-ui/core/styles'
import DashBase from '../../dashibiden/DashBase'
import hastTree2ReactCompiler from './hastTree2ReactCompiler'
import dashTree2HastTree from './dashTree2HastTree'
import ClassLiteralsComponent from './ClassLiteralsComponentTest'


const components = {
  'ClassLiteralsComponent': ClassLiteralsComponent
}


export default function DashTreeLayout5(props) {

  // Manage menu from data
  // @TODO : in this function fix the "concept root" special case for isco in a generic way (use of concept scheme ?)
  const menuData = props.data.menu ? props.data.menu : props.data.menuOccupation
  const menuTree = getClassesMenu(menuData, 'Properties')

  const { entity } = props.data
  // console.log('here comes data for DashTreeLayout3 (esco)', entity)

  const dashTree = initDashTree()

  // Les différentes cards pour l'affichage d'une description esco
  // entityInfoCard = title = preflabel, data = description, position = 0,0, size = med
  const entityInfoCard = getClassInfoCard(entity)
  addCardNode(dashTree, entityInfoCard, 0, 0)

  // @TODO : see dashtreeLayout2 : for other components and cards.

  // const hastTree = dashTree2HastTree(contentTree) // previous transformations
  const hastTree = dashTree2HastTree(dashTree) // previous transformations
  const hastTree2React = hastTree2ReactCompiler(components)

  return (
    <>
      <DashBase menuTree={menuTree}>
        {hastTree2React(hastTree)}
      </DashBase>
    </>
  );
}


function initDashTree() {
  const dash = getDashboard()
  return {
    type: 'root',
    children: [dash]
  }
}

function addCardNode(dashTree, card, lineIndex, columnIndex) {
  const { children: [dash] } = dashTree

  // is the line exist ?
  let line = dash.children[lineIndex]
  if (!line) {
    line = getLine()
    dash.children.splice(lineIndex, 0, line)
  }

  // add the card in the line
  line.children.splice(columnIndex, 0, card)
}

function getDashboard(name) {
  if (!name) name = 'Default Dashboard Name'
  return {
    type: 'dashboard',
    name,
    children: []
  }
}

function getLine(name) {
  if (!name) name = 'Default Line Name'
  return {
    type: 'dashLine',
    name,
    children: []
  }
}

// @TODO : remane as entityInfoCard and extract this function
function getClassInfoCard(entity) {
  const clasInfoComp = {
    type: 'cardComponent',
    compName: 'ClassInfoComponent',
    properties: {
      data: entity
    }
  }

  let name = '__infoCard, name to fix__'
  if (entity.label) name = entity.label[0]

  return {
    type: 'cardNode',
    properties: {
      name,
      columnSize: 8,
    },
    children: [clasInfoComp]
  }

}


// const useStyles = makeStyles(theme => ({
//   paper: {
//     padding: theme.spacing(2),
//     display: 'flex',
//     overflow: 'auto',
//     flexDirection: 'column',
//   },
//   /*fixedHeight: {
//     height: 240,
//   },*/
// }))


