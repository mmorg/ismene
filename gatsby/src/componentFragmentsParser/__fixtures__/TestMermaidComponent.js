import React from 'react'
// import { graphql } from "gatsby"
import Mermaid from "react-mermaid2"
import classDiagramBuilder from '../../../mermaidUtils/classDiagramBuilder.js'

// this is to override the Gatsby's 'graphql' stringTemplate and not get compilation errors
function graphqlTest (strings){
  return strings.join('')
}

export const query = graphqlTest`
fragment testClassAndInheritance on Query {
  rdfsClass(id: {eq: $id}) {
    subClassOf {
      id
      label {
        _language
        _value
      }
      isAbstract
       # @TODO: see how to use "hasProperties"
      ### This is a "Properties" prop in the object
    }
    isAbstract
    id
  }
}

fragment testClassAndProperties on Query {
  classAndProperties: allRdfProperty(filter: {domain: {elemMatch: {id: {eq: $id}}}}) {
    nodes {
      id
      label {
        _language
        _value
      }
      type
      status
      domain {
        id
        label {
          _language
          _value
        }
      }
      range{
				id
      }
    }
  }
}

## @TODO: see to use "isRangedBy" property
fragment testPropertiesThatRange on Query {
  propertiesThatRange: allRdfProperty(
    filter: {range: {elemMatch: {id: {eq: $id}}}}
  ) {
    nodes {
      id
      domain {
        id
        label {
          _language
          _value
        }
      }
      label {
        _language
        _value
      }
      range {
        id
      }
    }
  }
}
`

// @TODO: rename it MermaidClassesComponent
export default function MermaidComponent(props) {

  // default for testing chart
  const chart = classDiagramBuilder(props.data)

  return (
    <Mermaid chart={chart}
    config = {{
      theme: "default"
    }}
    />
  )
}
