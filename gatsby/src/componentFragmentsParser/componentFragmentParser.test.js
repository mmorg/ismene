import componentFragmentParser from "./componentFragmentParser"

// docker-compose exec gatsby npm test -- --watch componentFragmentParser.test

const srcBase = `${__dirname}/../../..`

it.todo('RESTAURE THE OTHERS TESTS')
describe('dev test', () => {

  it('Extract fragment when there is one to extract', () => {
    // one fragment to extract
    const withFragment = `${srcBase}/components/dashorm/cardComponents/ClassLiteralsComponent.js`
    const fragmentTarget =
`fragment ClassLiteralsComponent on Query {
  rdfsClass(id: {eq: $id}) {
    id
    label {
      _language
      _value
    }
    comment {
      _language
      _value
    }
    example {
      _language
      _value
    }
    status
  }
}`
    const fragmentsFile = componentFragmentParser(withFragment)
    expect(fragmentsFile[0]).toStrictEqual(['ClassLiteralsComponent', fragmentTarget, true])

    // no fragment to extract
    const withoutFragment = `${srcBase}/components/dashorm/cardComponents/ListLinksComponent.js`
    const noFragmentFile = componentFragmentParser(withoutFragment)
    expect(noFragmentFile).toStrictEqual([])
  })

  it.only('Extract multiple fragments', () => {
    // 2 fragments to extract
    const file = `${__dirname}/__fixtures__/TestMermaidComponent.js`
    const threeFragments = componentFragmentParser(file)
    expect(threeFragments.length).toBe(3)
  })

  it('Check if the fragment is `on Query` or not', () => {

    const file = `${srcBase}/components/dashorm/cardComponents/ObjectPropertiesComponent.js`
    const fragments = componentFragmentParser(file)
    expect(fragments.length).toBe(3)
    // The 2 first fragments are not `on Query`
    expect(fragments[0][2]).toBe(false)
    expect(fragments[1][2]).toBe(false)
    // The last one is `on Query`
    expect(fragments[2][2]).toBe(true)

  })

})
