import { readData } from '@mmorg/fsutils'
import { parseToAst } from 'documentation/src/parsers/parse_to_ast'
import { parse, print } from 'graphql'

export default function componentFragmentParser(scriptPath) {
    const sourceCode = readData(scriptPath)

    // parse the Js AST with documentationjs parser (using babel under the wood)
    const sourceAst = parseToAst(sourceCode, scriptPath)

    const queryVariable = sourceAst.program.body.find(node => {
        if (!node.type === 'ExportNamedDeclaration') return false
        if (!node.declaration || !node.declaration.declarations) {
            // console.warn('No declaration(s) in component:', scriptPath)
            return false
        }

        return node.declaration.declarations[0].id.name === 'query'
    })

    if (!queryVariable) return [] // the export is not declared

    // get the template literal declaration 
    const varDeclarator = queryVariable.declaration.declarations[0]
    // @Question : what is the diffrence between value.raw and value.cooked ?
    const graphqlQuery = varDeclarator.init.quasi.quasis[0].value.cooked
    const graphqlAST = parse(graphqlQuery)
    // get the Fragments names
    const fragmentDefinitions = graphqlAST.definitions.filter(node => node.kind === 'FragmentDefinition')

    // structure: [fragmentName, fragmentcontent, isOnQuery]
    return fragmentDefinitions.map(node => [node.name.value, print(node), node.typeCondition.name.value === 'Query'])
}