import path from 'path'
import { readData } from '@mmorg/fsutils'
import { parseToAst } from 'documentation/src/parsers/parse_to_ast.js'
import componentFragmentParser from './componentFragmentParser.js'


export default function pageFragmentsParser(pageTemplatePath) {
    
    const pagePath = path.resolve(pageTemplatePath)

    // 1/ get the AST of the file
    const sourceCode = readData(pagePath)
    // parse the Js AST with documentationjs parser (using babel under the wood)
    const sourceAst = parseToAst(sourceCode, pagePath)
    
    const sourceProgramBody = sourceAst.program.body

    // copy-paste the source to get a an AST template and change the body
    const targetAst = Object.assign({}, sourceAst)
    targetAst.program.body = sourceAst.program.body.slice(0, 10)


    // 2/ retrieve the components at page level

    // 2.1/ get the imports paths 
    const importsDeclarations = sourceProgramBody.filter(node => node.type === 'ImportDeclaration')

    // 2.2/ get the components declaration
    // @TODO : make a more generic test 
    const variableDeclarations = sourceProgramBody.filter(node => node.type === 'VariableDeclaration')
    const componentsExport = variableDeclarations.find(node => node?.declarations && node.declarations[0].id.name === 'components')
    const componentsValues = componentsExport.declarations[0].init

    const componentsToRetrive = []
    for (const property of componentsValues.properties) {
        if (property.type === 'SpreadElement') {
            // @TODO : special case to manage, continue for now
            continue
        }

        if (property.type != 'ObjectProperty') console.warn('Unknow type for parsing')

        // component value name to retrive in component
        componentsToRetrive.push(property.value.name)

    }

    const componentsPaths = []
    for (const cToRetrieve of componentsToRetrive) {

        // 1/ search in imports 
        // console.log(importsDeclarations)
        const found = importsDeclarations.find(imp => imp.specifiers[0].local.name === cToRetrieve)
        let componentRelativePath = found.source.value
        // @TODO : make it better to guess the type of the file (.ts, .tsx,...)
        componentRelativePath = componentRelativePath.endsWith('.js') ? componentRelativePath : componentRelativePath + '.js'
        // build absolute path 
        const basePath = path.dirname(pagePath)
        componentsPaths.push(path.resolve(basePath, componentRelativePath))

    }

    const fragments = componentsPaths.map(path => componentFragmentParser(path)).flat(1)
    
    return fragments.filter( f => f.length) // filter empty arrays

}