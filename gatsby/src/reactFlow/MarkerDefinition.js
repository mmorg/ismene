import React from 'react'

// source: https://github.com/wbkd/react-flow/issues/915
// and then : https://gist.github.com/ambroseus/2f1ea898f39e8460e49cd80c9b5e9f5c


const Marker = ({ id, className, children }) => (
  <marker
    className={className || 'react-flow__arrowhead'}
    id={id}
    markerWidth="15"
    markerHeight="15"
    viewBox="-10 -10 20 20"
    orient="auto"
    markerUnits="userSpaceOnUse"
    refX="0"
    refY="0"
  >
    {children}
  </marker>
)


export default function MarkerDefinition({ color, id }) {
  return (
    <svg>
      <defs>
        <Marker id={id}>
          <polyline
            stroke={color}
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeWidth="1"
            fill={color}
            points="-12,-6 0,0 -12,6 -12,-6"
          />
        </Marker>
      </defs>
    </svg>
  )
}
