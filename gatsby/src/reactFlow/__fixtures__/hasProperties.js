const val = [
    {
      id: 'hrrdf:accountBasedProductCode',
      type: 'entityNode',
      data: { label: 'accountBasedProductCode' },
      style: { border: '4px solid', borderColor: '#9370DB', padding: 10 },
      position: { x: 1400, y: -400 }
    },
    {
      id: 'hrrdf:effectivePeriod',
      type: 'entityNode',
      data: { label: 'effectivePeriod' },
      style: { border: '4px solid', borderColor: '#9370DB', padding: 10 },
      position: { x: 1400, y: -300 }
    },
    {
      id: 'hrrdf:electionType',
      type: 'entityNode',
      data: { label: 'electionType' },
      style: { border: '4px solid', borderColor: '#9370DB', padding: 10 },
      position: { x: 1400, y: -200 }
    },
    {
      id: 'hrrdf:carrierOrganization',
      type: 'entityNode',
      data: { label: 'carrierOrganization' },
      style: { border: '4px solid', borderColor: '#9370DB', padding: 10 },
      position: { x: 1400, y: -100 }
    },
    {
      id: 'hrrdf:deductionInstructions',
      type: 'entityNode',
      data: { label: 'deductionInstructions' },
      style: { border: '4px solid', borderColor: '#9370DB', padding: 10 },
      position: { x: 1400, y: 0 }
    },
    {
      id: 'hrrdf:electedPlanId',
      type: 'entityNode',
      data: { label: 'electedPlanId' },
      style: { border: '4px solid', borderColor: '#9370DB', padding: 10 },
      position: { x: 1400, y: 100 }
    },
    {
      id: 'hrrdf:groupNumberId',
      type: 'entityNode',
      data: { label: 'groupNumberId' },
      style: { border: '4px solid', borderColor: '#9370DB', padding: 10 },
      position: { x: 1400, y: 200 }
    },
    {
      id: 'hrrdf:id',
      type: 'entityNode',
      data: { label: 'id' },
      style: { border: '4px solid', borderColor: '#9370DB', padding: 10 },
      position: { x: 1400, y: 300 }
    }
  ]

export default val