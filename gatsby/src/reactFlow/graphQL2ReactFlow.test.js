import { readJson } from '@mmorg/fsutils'
import graphQL2ReactFlow from './graphQL2ReactFlow.js'
import hasPropertiesJs from './__fixtures__/hasProperties.js'

// docker compose exec gatsby npm test -- --watch graphQL2ReactFlow.test

const fixturesF = `${__dirname}/__fixtures__`


describe('Tranformation of Rdfs model to React-Flow instructions', () => {

  let rfInstructions = null
  beforeAll(() => {
    const model = readJson(`${fixturesF}/isSubClassOf.json`).data
    rfInstructions = graphQL2ReactFlow(model.rdfsClass)
  })

  it('Generate good subClassOf relation', () => {
    const subject = 'hrrdf:AccountBasedCoverageType'
    const ob = 'hrrdf:CoverageType'
    const diagTriples = getDiagTriple(subject, ob, rfInstructions)
    expect(diagTriples).toMatchSnapshot()
  })

  it('Generate good isRangedBy relation', () => {
    const subject = 'hrrdf:EnrollmentType'
    const ob = 'hrrdf:AccountBasedCoverageType'
    const diagTriples = getDiagTriple(subject, ob, rfInstructions)
    expect(diagTriples).toMatchSnapshot()
  })

  it('Generate good domain properties', () => {
    const sourceSubject = 'hrrdf:AccountBasedCoverageType'
    // test on direct property 1
    const targetObject = 'hrrdf:accountBasedProductCode'
    const diagTriples = getDiagTriple(sourceSubject, targetObject, rfInstructions)
    expect(diagTriples).toMatchSnapshot()

    // test on direct property 2
    const targetObject2 = 'hrrdf:effectivePeriod'
    const diagTriples2 = getDiagTriple(sourceSubject, targetObject2, rfInstructions)
    expect(diagTriples2).toMatchSnapshot()

    // @TODO : test on herited property 1
  })

  it('Generate good domain types', () => {
    const sourceSubject = 'hrrdf:AccountBasedCoverageType'
    // test on direct property 1
    const rangeObject = 'hrrdf:EffectiveTimePeriodType'
    const diagTriples = getDiagTriple(sourceSubject, rangeObject, rfInstructions)
    expect(diagTriples).toMatchSnapshot()

    // test on direct property 2
    const rangeObject2 = 'hrrdf:OpenEndPeriodType'
    const diagTriples2 = getDiagTriple(sourceSubject, rangeObject2, rfInstructions)
    expect(diagTriples2).toMatchSnapshot()

    // @TODO : test on herited property 1
  })

  it('Generate good reduce number of Properties for one Type', () => {
    // see: const diagDomainPropertiesReduce
    const targetObject = 'hrrdf:IdentifierType'

    const sourceSubject = 'hrrdf:electedPlanId'
    const diagTriples = getDiagTriple(sourceSubject, targetObject, rfInstructions)
    expect(diagTriples).toMatchSnapshot()

    const sourceSubject2 = 'hrrdf:groupNumberId'
    const diagTriples2 = getDiagTriple(sourceSubject2, targetObject, rfInstructions)
    expect(diagTriples2).toMatchSnapshot()

    const sourceSubject3 = 'hrrdf:id'
    const diagTriples3 = getDiagTriple(sourceSubject3, targetObject, rfInstructions)
    expect(diagTriples3).toMatchSnapshot()

  })

  it('Generate good domain properties to types relation', () => {
    // see: const allPropertiesDuplicates
    const sourceSubject = 'hrrdf:effectivePeriod'
    // test on direct property 1
    const targetObject = 'hrrdf:EffectiveTimePeriodType'
    const diagTriples = getDiagTriple(sourceSubject, targetObject, rfInstructions)
    expect(diagTriples).toMatchSnapshot()

    // test on direct property 2
    const targetObject2 = 'hrrdf:OpenEndPeriodType'
    const diagTriples2 = getDiagTriple(sourceSubject, targetObject2, rfInstructions)
    expect(diagTriples2).toMatchSnapshot()

    // @TODO : test on herited property 1
  })

  it('Generate full diagram for a model', () => {
    expect(rfInstructions).toMatchSnapshot()
  })

})

it.todo('Create test for the others 3 files in __fixtures__')

describe('Manage case with mutiple "rangedBy\'s entities" on one property ', () => {

  let rfInstructions = null
  beforeAll(() => {
    const model = readJson(`${fixturesF}/multipleIsRangedBy.json`).data
    rfInstructions = graphQL2ReactFlow(model.rdfsClass)
  })


  it('Generate good links for the multiples rangedBy under `hrrdf:attachments`', () => {
    const diagThatRange = getDiagTripleByTarget('hrrdf:AttachmentType', rfInstructions)
    expect(diagThatRange.p.length).toBe(2 + 7)
  })


})

describe('Tests of some specifics cases', () => {

  let rfInstructions = null
  it('Not throw error when some properties are null', () => {
    const model = readJson(`${fixturesF}/nullRangeForProperty.json`).data
    const exec = () => graphQL2ReactFlow(model.rdfsClass)
    expect(exec).not.toThrow()
  })
})


function getDiagTripleByTarget(object, rfInstructions) {
  // s: @TODO: to be build it on the results of p-o selections
  const p = rfInstructions.edges.filter(i => i.target === object)
  const o = rfInstructions.nodes.filter(i => i.id === object)
  return { p, o }
}
function getDiagTriple(subject, obj, rfInstructions) {
  const s = rfInstructions.nodes.filter(i => i.id === subject)
  const p = rfInstructions.edges.filter(i => i.source === subject && i.target === obj)
  const o = rfInstructions.nodes.filter(i => i.id === obj)
  return { s, p, o }
}
