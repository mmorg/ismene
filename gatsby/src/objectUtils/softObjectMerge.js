
// this is a mutable function
export default function softObjectMerge(sourceObj, toMergeObj) {

  for (const [k, v] of Object.entries(toMergeObj)) {

    if (!sourceObj[k]) {
      sourceObj[k] = v
      continue
    }

    // A new value is to add. Is the sourceObject value already an array ? 
    if (!Array.isArray(sourceObj[k])) sourceObj[k] = [sourceObj[k]]

    // the value to merge is already an array ? 
    const toMergeValues = Array.isArray(toMergeObj[k]) ? toMergeObj[k] : [toMergeObj[k]]
    // @TODO: manage deduplication of string or entities or dataTypes objects
    sourceObj[k].push(...toMergeValues)

  }

}