
export default function expectJsonSnapshot(obj){
    const toJson = (obj)=>{
        return JSON.stringify(obj,null,2)
    }
    expect(toJson(obj)).toMatchSnapshot()
}