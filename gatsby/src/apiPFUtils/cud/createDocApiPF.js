import cudUtils from './cudUtils.js'
import fetch from 'node-fetch'

export default async function createDocApiPF(instructions, url) {

  // 1st Pass: create all the IDS 
  for (const inst of instructions) {
    const conf = {
      endpointBuilder: () => `${url}/${inst.endpoint}`,
      method: 'POST',
      contentType: 'application/ld+json',
    }
    await apiPFWriter(conf, inst.ids)
    // console.warn('@TODO: RESTAURE ME !!!')
  }

  // /2nd pass: update the entites 
  for (const inst of instructions) {
    const conf = {
      endpointBuilder: (e) => `${url}/${inst.endpoint}/${e.id}`,
      method: 'PATCH',
      contentType: 'application/merge-patch+json',
    }
    await apiPFWriter(conf, inst.entities)
  }


  console.log('** Import in AppSearch okay')

}

async function apiPFWriter(conf, documents) {
  const {
    endpointBuilder,
    method,
    contentType,
  } = conf

  return await cudUtils(async (doc) => {

    const endpoint = endpointBuilder(doc)
    const query = {
      headers: {
        Accept: 'application/ld+json',
        'Content-Type': contentType,
      },
      method,
      body: JSON.stringify(doc),
    }

    const r = await fetch(
      endpoint,
      query
    )

    const data = await r.json()

    // console.log('IN CUD UTILS', data)
    if (data['@type'] === 'hydra:Error') {
      console.log('\n Hydra error:')
      const {
        '@context': context,
        '@type': type,
        'hydra:title': title,
        'hydra:description': description,
      } = data
      const errorParse = { context, type, title, description }
      console.log(errorParse)
      console.log('\n On request: \n',
        endpoint,
        query,
        '\n'
      )
      return []
    }


    // return all as array 
    return Array.isArray(data) ? data : [data]
  }, documents)


}
