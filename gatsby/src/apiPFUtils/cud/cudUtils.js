import progress from 'cli-progress'
// import pMap from 'p-map'
import chunkArray from '../../arrayUtils/chunkArray.js'
import pThrottle from 'p-throttle'
import chalk from 'chalk'

// @TODO : improve with p-map and bar display like in datalab-s/rml project

// @TODO: remplace the p-map defaultConfig by the p-throttle ones
const defaultConfig = {
  limit: 2, // 2/1000 is okay, but long
  interval: 2000, // 1000
  docsCount: 10, // 80 not ok / doc per request / default limits = max 100 documents per request but also 235.7mb size max, so go down per request
  chunkDocs: true,
}


export default async function cudUtils(appSearchFn, documents, config = {}) {
  const { limit, interval, docsCount, chunkDocs } = Object.assign({}, defaultConfig, config)

  const throttle = pThrottle({ limit, interval })
  console.log('Processing configured with a docsCount =', docsCount, 'a limit =', limit, 'and an interval =', interval)

  // configure progress-bar
  const bar1 = new progress.SingleBar({}, progress.Presets.shades_classic)

  console.log('Start processing documents. Total documents length', documents.length)

  // create an array split
  const documentsLength = documents.length
  const documentChunks = chunkDocs ? documents : chunkArray(documents, docsCount)
  console.log(documentsLength, 'documents to be processed in', documentChunks.length, 'chunks')
  bar1.start(documentChunks.length, 0)


  const throttled = throttle(async documents => cudExec(documents, appSearchFn, bar1))
  const tts = documentChunks.map(async documents => await throttled(documents))
  const results = await Promise.all(tts)


  bar1.stop()
  return results
}

async function cudExec(documents, appSearchFn, bar1) {
  let response
  try {
    response = await waitAndRetry(0, 5, documents, appSearchFn)
  } catch (e) {
    console.log(chalk.bgRed('\n\nError during processing:'))
    throw e
  }

  // console.log(bar1)
  // errors management
  const errors = response.reduce((acc, resp) => {
    if (resp.errors) acc.push(...resp.errors)
    return acc
  }, [])
  if (errors.length) {
    console.log('\n')
    console.log('@TODO: manage this errors :', errors)
  }

  bar1.increment()
  return response
}

async function waitAndRetry(current, max, documents, appSearchFn) {
  // console.log(current, '===========', max)
  if (current > max) throw new Error(`\n\n\n ${max} EAI append for this request. Stop process here`)
  let response
  try {
    response = await appSearchFn(documents)
  } catch (e) {
    if (e.code === 'EAI_AGAIN') {
      console.log('\n==> EAI_AGAIN case')
      await new Promise(resolve => setTimeout(resolve, 1000))
      return await waitAndRetry(current + 1, max, documents, appSearchFn)
    }
    if (e.code === 'ECONNRESET') {
      console.log('\n==> Connexion reset case')
      await new Promise(resolve => setTimeout(resolve, 1000))
      return await waitAndRetry(current + 1, max, documents, appSearchFn)
    }
    if(e.code === 'ECONNREFUSED'){
      console.log('\n==> ECONNREFUSED')
      await new Promise(resolve => setTimeout(resolve, 10000))
      return await waitAndRetry(current + 1, max, documents, appSearchFn)
    }

    // last case: there is another error
    console.log(chalk.red('\n==> This is another case to manage. See error: \n'))
    console.log(e)
    
    // wait longer
    await new Promise(resolve => setTimeout(resolve, 10000))
    return await waitAndRetry(current + 1, max, documents, appSearchFn)
  }

  if (!response) {
    console.log(chalk.bgRed(`After ${max} retries, there is still an error`))
    throw new Error('Don\'t achieve the request.')
  }

  return response
}

