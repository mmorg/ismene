import elasticConf from '../../configurations/elasticConf.js'
import getAppSearchClient from '../getAppSearchClient.js'
import cudUtils from './cudUtils.js'

export default async function deleteDocAppSearch(documentIds, conf) {
  const { engineName } = conf
  const client = getAppSearchClient(conf)

  await cudUtils(async (docIds) => {
    return await client.destroyDocuments(engineName, docIds)
  }, documentIds)

  console.log('** Deletion is okay')
}



