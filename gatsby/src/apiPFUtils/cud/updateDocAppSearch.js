import getAppSearchClient from '../getAppSearchClient.js'
import cudUtils from './cudUtils.js'


export default async function updateDocAppSearch(documents, conf, loadConfig) {
  const { engineName } = conf
  const client = getAppSearchClient(conf)

  await cudUtils(async (docs) => {
    return await client.updateDocuments(engineName, docs)
  }, documents, loadConfig)

}
