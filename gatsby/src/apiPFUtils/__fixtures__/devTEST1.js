import exampleBigContext from './exampleBigContext.js'

const source = {
  '@context': exampleBigContext['@context'],
  graph:[
  {
    "id": "gendj:82d61d3ddd5d9c9d1219c778e164610a",
    "type": [
      "skos:Concept",
      "gendj:Job"
    ],
    "narrower": [
      "gendj:fdf99806745b150ef06daf80fe09167c"
    ],
    "prefLabel": [
      {
        "@language": "fr",
        "@value": "e-commerce"
      }
    ]
  },
  {
    "id": "gendj:fdf99806745b150ef06daf80fe09167c",
    "type": [
      "skos:Concept",
      "gendj:Position"
    ],
    "broader": [
      "gendj:82d61d3ddd5d9c9d1219c778e164610a"
    ],
    "prefLabel": [
      {
        "@language": "fr",
        "@value": "Responsable e-commerce"
      }
    ]
  }
]}

const target = [
  {
    endpoint: 'concepts',
    ids: [
      { id: 'gendj:82d61d3ddd5d9c9d1219c778e164610a' },
      { id: 'gendj:fdf99806745b150ef06daf80fe09167c' },
    ],
    entities: [
      {
        id: 'gendj:82d61d3ddd5d9c9d1219c778e164610a',
        prefLabel:'e-commerce',
        narrower: [
          'concepts/gendj:fdf99806745b150ef06daf80fe09167c'
        ],
      },
      {
        id: 'gendj:fdf99806745b150ef06daf80fe09167c',
        prefLabel: 'Responsable e-commerce',
        broader: [ 'concepts/gendj:82d61d3ddd5d9c9d1219c778e164610a'],
      }
    ]
  }
]

export {source, target}