
export default function stringifyLangStringGraph(newGraph, contextTree) {

  for (const entity of newGraph.graph) {

    const entityKeys = Object.keys(entity)

    for (const ek of entityKeys) {

      if (contextTree.datatypesKeys.has(ek)) {
        // check if this is a langString 
        const existingValues = Array.isArray(entity[ek]) ? entity[ek] : [entity[ek]]

        const newValues = existingValues.map(value => {
          if (isLangString(value)) return value['@value']
          console.log('Check this case in stringifyLangStringGraph:', value)
          return value
        })

        // assign the new value(s)
        if (newValues.length > 1){
          console.warn('@implement: There is 2 possibles values for a literal property in:', newValues)
          // only keep the first one until multi-value for literal is ok in apiPF
        }
        entity[ek] = newValues[0]
      }
    }

  }
}

function isLangString(datatype) {
  return datatype['@language'] && datatype['@value']
}