
export default function updateIdRefsOnInstructions(instructions, contextTree){
  
  // a) 2 passes : 1 pour créer le map des anciens / nouveaux uri 
  const uriMap = instructions.reduce((acc,instruction) => {
    for(const e of instruction.entities){
      acc.set(e.id, `${instruction.endpoint}/${e.id}`)
    }
    return acc
  }, new Map())

  
  // b) 1 pour mettre a jour les properties qui sont en idRefsKeys dans le contextTree 
  const iEntities = instructions.map(i => i.entities).flat()

  for(const e of iEntities){

    for(const k of Object.keys(e)){

      if(!contextTree.idRefsKeys.has(k))continue

      e[k] = e[k].map( uri => {
        const newUri = uriMap.get(uri)
        if(!newUri){
          console.log('This uri is not mapped in existing instructions entities:', uri)
          return uri
        }
        return newUri
      })

    }

  }

}