import entity2APFInstructions from './entity2APFInstructions.js'
import {source as s1, target as t1} from './__fixtures__/devTEST1.js'
import {readJson} from '@mmorg/fsutils'

// docker compose exec gatsby npm run testESM -- --watch entity2APFInstructions

it.todo('Remove this implementation when new infra validated')
describe.skip('dev scratchpad', () => {
  
  it('Generate target instructions', async () => {
    const apiOntology = readJson('./data/skos/__exports__/skos_X.X.X_gl-realm-compacted:generator_apiGenerator.jsonld')
    const instructions = entity2APFInstructions(s1, apiOntology)
    expect(instructions).toStrictEqual(t1)
  })

})
