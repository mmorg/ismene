import { snakeCase } from 'snake-case'
import pluralize from './pluralize.js'


const getEndpoint = (entity) => {

  const typeName = entity.type[0].split(':')[1]

  const endPoint = snakeCase(typeName)
  return pluralize(endPoint)

}

const newEndpoint = (endPointName) => ({
  endpoint: endPointName,
  ids: [],
  entities: [],
})

export default function graph2Instructions(ld){

  const endPointIndex = new Map()

  for(const e of ld.graph){

    // 1/ get Endpoint 
    const endPointName = getEndpoint(e)

    let endpoint = endPointIndex.get(endPointName)
    if(!endpoint){
      endpoint = newEndpoint(endPointName)
      endPointIndex.set(endPointName, endpoint)
    }

    // 2/ extract id and push
    endpoint.ids.push({id: e.id})
    // 3/ push to entity
    endpoint.entities.push(e)

  }


  return [...endPointIndex.values()]


}