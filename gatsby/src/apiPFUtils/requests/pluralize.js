
export default function pluralize(str){

  if(str.endsWith('s')) return str
  return `${str}s`

}