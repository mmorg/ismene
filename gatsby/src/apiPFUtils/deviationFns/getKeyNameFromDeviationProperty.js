
// this is an utility function
export default function getKeyNameFromDeviationProperty(deviationProperty){
  return deviationProperty.id.split(':')[1]
}