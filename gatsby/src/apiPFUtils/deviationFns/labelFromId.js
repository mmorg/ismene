import getKeyNameFromDeviationProperty from './getKeyNameFromDeviationProperty.js'

export default function labelFromId(valueToDeviate, deviationProperty, ontologyLd) {
  const deviationResult = {}

  // @TODO: instead of this, get the rdfs:Label from the rdfs:Class type definition
  const label = valueToDeviate.split(':')[1]

  const propertyNameForDeviation = getKeyNameFromDeviationProperty(deviationProperty)
  deviationResult[propertyNameForDeviation] = label


  return deviationResult
}