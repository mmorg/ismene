import getKeyNameFromDeviationProperty from './getKeyNameFromDeviationProperty.js'

export default function idFromId(valueToDeviate, deviationProperty, ontologyLd){
  const deviationResult = {}
  // 1/ generate the new id
  let value = valueToDeviate
  if(value.includes(':')) value = value.replace(':','-_-')
  deviationResult.id = value

  // 2/ create the property that keep the source Id
  const propertyNameForDeviation = getKeyNameFromDeviationProperty(deviationProperty)
  deviationResult[propertyNameForDeviation] = valueToDeviate
  
  // console.log(deviationResult)
  // throw new Error()
  return deviationResult
}