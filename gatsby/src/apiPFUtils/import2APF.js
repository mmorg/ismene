import createDocApiPF from './cud/createDocApiPF.js'
import entity2APFInstructions from './entity2APFInstructions.js'
import chalk from 'chalk'

export default async function import2APF(graph, targetOntology, url) {

  const instructions = entity2APFInstructions(graph, targetOntology)

  console.warn(chalk.bgRed('@TODO: remove this hacks'))
  const entities = instructions.map(i => i.entities).flat()
  entities.forEach( e => {
    // utils this fixed: remove `inScheme` for Gatsby from export (create layer)
    delete e.inScheme
    // until this fixed in main doc: Entitée externe avec Références Multiples 
    delete e.deviationType
  })

  // for debuging :
  // saveJson(instructions, `/apps/gatsby/src/apiPFUtils/DEBUG_SEND.json`)

  await createDocApiPF(instructions,url)

}
