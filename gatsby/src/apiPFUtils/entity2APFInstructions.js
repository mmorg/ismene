import filterByDomain from '../jsonldUtils/filterByDomain.js'
import filterByType from '../jsonldUtils/filterByType.js'
import findById from '../jsonldUtils/findById.js'
import graph2Instructions from './requests/graph2Instructions.js'
import idFromId from './deviationFns/idFromId.js'
import labelFromId from './deviationFns/labelFromId.js'
import softObjectMerge from '../objectUtils/softObjectMerge.js'
import mergeDedupObject from '../arrayUtils/mergeDedupObject.js'
import stringifyLangStringGraph from './stringifyLangStringGraph.js'
import updateIdRefsOnInstructions from './updateIdRefsOnInstructions.js'
import contextParser from '../jsonldUtils/contextUtil/contextParser.js'

export default function entity2APFInstructions(graph, targetOntology) {

  // 0/ create a contextTree from the merge of the 2 contexts
  const aggregatedContext = {
    '@context': Object.assign({}, graph['@context'], targetOntology['@context'])
  }
  const contextTree = contextParser(aggregatedContext)

  // étapes :
  // - identification des déviation et update des entitées existantes ou génération de nouvelles entites pour les déviations et mise à jour des références
  const deviations = extractDeviations(targetOntology)
  const newGraph = applyDeviations(graph, deviations)

  // - identification des langString (via contextParser) non "déviés" et transformations en string
  // ==> @TODO: faire par un ajout de nouvelles déviations ? 
  stringifyLangStringGraph(newGraph, contextTree)

  // - classification des entitées par leur endpoint cible (basé sur `.type`)
  const instructions = graph2Instructions(newGraph)

  // - identification des références non déviées et mise à jour de l'uri
  updateIdRefsOnInstructions(instructions, contextTree)

  // - post-processing for each entity on "sensible for apiPF field `type`"
  const clean_instruction = instructions.map(i => i.entities).flat()
  clean_instruction.forEach(e => delete e.type)
  return instructions
}


// A/ Identification and parsing Configuration
function extractDeviations(targetOntology) {

  // 1/ identifier les propriétés déviées, y associer les classes et les transformations
  // "mmd:DeviationProperty",
  const source_dProperty = filterByType(targetOntology, 'mmd:DeviationProperty')
  // create a deep copy of the properties
  const dProperty = JSON.parse(JSON.stringify(source_dProperty))

  for (const prop of dProperty) {
    if (prop.range.length > 1) throw new Error('@TODO: implement this case: multiple ranges')
    const rangeId = prop.range[0]

    const deviationRange = findById(targetOntology, rangeId)
    if (!deviationRange) throw new Error('The range id is not defined in the targetOntology')
    prop.deviationRange = deviationRange
    // console.log(deviationRange.type)

    const deviationProperty = filterByDomain(deviationRange.id, targetOntology)
    if (!deviationProperty.length) throw new Error('No deviation properties defined in the targetOntology for this ObjectDeviation' + deviationRange.id)
    prop.deviationProperty = deviationProperty

  }
  return dProperty
}

function applyDeviations(ld, deviations) {

  // 1/ Create an index of deviated properties
  const index = deviations.reduce((acc, dev) => {
    // @TODO: create a cleaner property extractor based on the graph context
    if (dev.deviationFor.length > 1) throw new Error('@TODO: implement multiple deviationFor')
    const i = dev.deviationFor[0].split(':')[1]
    acc.set(i, dev)
    return acc
  }, new Map())

  // @tODO: process each graph items an do changes
  let graphResult = []

  // for each entity
  for (const e of ld.graph) {

    // for each key of this 
    // 1/ ---- calculate the new deviatedEntities
    const deviatedEntries = [
      // array of [deviationProperty, [entities]]
    ]
    for (const k of Object.keys(e)) {
      if (!index.has(k)) continue
      const dev = index.get(k)

      if (dev.domain.length > 1) throw new Error('@TODO: implement')
      const isInDomain = e.type.includes(dev.domain[0])
      if (!isInDomain) continue

      // test had passed, do the tranformations
      const entityValue = e[k]
      const deviatedEntities = []
      // for each value on this key
      for (const valueToDeviate of entityValue) {
        const dEntity = { type: dev.range }

        // for each rule to apply
        for (const deviationProp of dev.deviationProperty) {
          const deviationFn = getDeviationRuleFn(deviationProp)
          const deviatedValue = deviationFn(valueToDeviate, deviationProp)
          if(deviatedValue) softObjectMerge(dEntity, deviatedValue)
        }

        deviatedEntities.push(dEntity)
      }

      if (deviatedEntities.length) {
        deviatedEntries.push([dev, deviatedEntities])
      }

    }

    // In any case: copy the source entity 
    const duplicatedSource = JSON.parse(JSON.stringify(e))
    graphResult.push(duplicatedSource)

    // there is a deviation for a property
    if (deviatedEntries.length) {
      for (const [dev, deviatedEntities] of deviatedEntries) {

        // 2/ ---- create the new deviation property 
        const deviationName = dev.id.split(':')[1]
        const deviationValues = deviatedEntities.map(e => e.id)
        const deviation = {}
        deviation[deviationName] = deviationValues
        softObjectMerge(duplicatedSource, deviation)

        // 3/ ---- remove the "to deviate" property
        if (dev.deviationFor.length > 1) throw new Error('@TODO: implement this case: multiple RemplacedProperties')
        const toDeviateProperty = dev.deviationFor[0].split(':')[1]
        // type is a special case for the rest of the process, so do not remove it
        if (toDeviateProperty !== 'type') delete duplicatedSource[toDeviateProperty]

        // 4/ ---- merge the generated deviatedEntities with the previous ones
        graphResult = mergeDedupObject(graphResult, deviatedEntities)

      }
    }
  }

  return {
    '@context': ld['@context'],
    graph: graphResult,
  }
}

function getDeviationRuleFn(deviatedProperty) {

  const devRule = deviatedProperty.deviationRule
  if (!devRule) return () => { }
  if (devRule.length > 1) throw new Error('@Implement: multiple dev rules')

  return getIndexed(devRule[0])


}

function getIndexed(ruleId) {
  // @TODO: clean this
  const ruleName = ruleId.split(':')[1]

  // console.info('@TODO: create a better function to retrive fn. For example: from a folder')

  const index = {
    idFromId,
    labelFromId,
  }


  const fn = index[ruleName]
  if (!fn) {
    console.warn('@TODO: you should define a function for this rule:', ruleName, '/', ruleId,
      'this is in file: gatsby/src/apiPFUtils/entity2APFInstructions.js'
    )
    return () => { }
  }

  return fn

}