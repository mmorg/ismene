import { readJson } from '@mmorg/fsutils'
import classDiagramBuilder from './classDiagramBuilder.js'


// docker-compose exec gatsby npm test -- --watch classDiagramBuilder.test

describe('Tranformation of Rdfs model to Mermaid classDiagram', () => {
  // @TODO: update the fixture with a more complete model
  const model = readJson(`${__dirname}/__fixtures__/classCertificationTest.json`).data

  it('Generate diagram for a model', () => {
    const diag = classDiagramBuilder(model)
    expect(diag).toMatchSnapshot()
  })

  it('Works for entity with no subclass', () =>{
    const model = readJson(`${__dirname}/__fixtures__/classXsd_string.json`).data
    const diag = classDiagramBuilder(model)
    expect(diag).toMatchSnapshot()
  })
})

describe.skip('Exploration on plantuml', () =>{

  it('Generate Mermaid diag to test in plantuml', () =>{
    const mermaidContent = readJson(`${__dirname}/__fixtures__/plantumlTest.json`).data
    const mermaidText = classDiagramBuilder(mermaidContent)
    console.log(mermaidText)
  })

})
