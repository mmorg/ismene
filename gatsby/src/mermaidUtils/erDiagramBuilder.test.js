import { readJson } from '@mmorg/fsutils'
import erDiagramBuilder from './erDiagramBuilder.js'


// docker-compose exec gatsby npm test -- --watch erDiagramBuilder.test

describe('Tranformation of Rdfs property to Mermaid erDiagram', () => {
  // @TODO: update the fixture with a more complete model
  const model = readJson(`${__dirname}/__fixtures__/erTest1.json`).data

  // it('Make indirect test for source data-structure', () => {
  //   expect(model.propertiesThatRange.nodes.length > 0).toBe(true)
  // })

  it('Generate er diagram for a property', () => {
    const diag = erDiagramBuilder(model)
    expect(diag).toMatchSnapshot()
  })

  // it('Works for entity with no subclass', () =>{
  //   const model = readJson(`${__dirname}/__fixtures__/xsd_string.json`).data
  //   const diag = erDiagramBuilder(model)
  //   expect(diag).toMatchSnapshot()
  // })

})