import yarrrml2rdf from '@mmorg/rml-processor/src/exec/yarrrml2rdf.js'
import { read } from 'to-vfile'
import { globby } from 'globby'
import { readData, readJson } from '@mmorg/fsutils'
import parsingConfs from '../configs/parsingConfs.js'

// to run this endpoint: 
// http://localhost:1111/api/rml

export default async function translator(req, res) {
  console.log('Start test of rml integration')

  // related command: 
  // docker compose exec gatsby npm run toRdf -- -s '**/data-smarskills-test/sources/inokufu*' -m '**/data-smarskills-test/rml/inokufu.yaml' -t 'sources' 'results'  -p 'defaultProfile'

  // - commande en cours de test: 

  const testCommand = `
  docker compose exec gatsby npm run toRdf -- \
  -s '**/data-smartskills-test/sources/inokufu*' \
  -m '**/data-smartskills-test/rml/inokufu.yaml' \
  -t 'sources' 'results'  \
  -p '**/data-smartskills-test/profile/smartSkillsProdProfile.js'
  `
  // const profile = readData('/apps/gatsby/data-smartskills-test/profile/smartSkillsProdProfile.js')
  // console.log('=============', profile)
  // const glob = ['**/data-smarskills-test/sources/inokufu*']
  // const testVfiles = await fromGlobby(glob) //(await globby(glob)).map( p => readJson(p))

  const testMessage = {}

  const testInokufu = (ld) => {
    if (!ld.graph.length === 16) return false
    return true
  }
  // @TODO: add source transformation to vfile in runAndTestfn
  const inokufuLd = await runAndTest('inokufu', testInokufu, testMessage)
  // console.log('====', inokufuLd)

  const testJobReady = (ld) => {
    console.warn('DO TEST')
    return true
  }
  const jobreadyLd = await runAndTest('jobready', testJobReady, testMessage)
  // console.log('====', jobreadyLd, '====')


  const testOrientoi = (ld) => {
    console.warn('@TODO: test for orientoi')
    return true
  }
  const orientoiLd = await runAndTest('orientoi', testOrientoi, testMessage)
  // console.log('=====', orientoiLd, '====')



  res.status(200).json({ testMessage })
}

async function fromGlobby(globs) {
  const paths = await globby(globs)
  return Promise.all(paths.map(p => read(p, 'utf8')))
}

async function runAndTest(configName, test, message) {

  const ld = await yarrrml2rdf(parsingConfs[configName])
  const test_result = test(ld)
  message[configName] = { pass: test_result }
  return ld
}
