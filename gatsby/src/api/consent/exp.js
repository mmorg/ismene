import consentExport from '../../visionsTrust/consentExport.js';

export default async function exp(req,res){

  if(req.method === 'POST'){
    console.log('in the post')
    await consentExport(req,res)
  }else {
    res.status(200).json({ info: `please use the POST verb` })
  }

}
