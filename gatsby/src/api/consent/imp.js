import consentImport from '../../visionsTrust/consentImport.js'

export default async function imp(req,res){

  if(req.method === 'POST'){
    await consentImport(req,res)
       
  }else {
    res.status(200).json({ info: `please use the POST verb` })
  }
}
