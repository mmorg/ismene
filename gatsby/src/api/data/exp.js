import dataExport from '../../visionsTrust/dataExport.js'

export default async function exp(req,res){

  if(req.method === 'POST'){
    await dataExport(req,res)
  }else {
    res.status(200).json({ info: `please use the POST verb` })
  }
}
