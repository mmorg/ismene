import dataImport from '../../visionsTrust/dataImport.js'

export default async function imp(req,res){

  if(req.method === 'POST'){
    await dataImport(req,res)
  }else {
    res.status(200).json({ info: `please use the POST verb` })
  }
}
