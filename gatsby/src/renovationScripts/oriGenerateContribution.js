import { dirname } from 'path'
import { saveData } from '@mmorg/fsutils'
import chalk from 'chalk'
import { globby } from 'globby'
import loadScript from '../importUtils/loadScript.js'
import oriDescriptionHTML from './oriDescriptionHTML.js'

const configFilePath = `${process.cwd()}/src/configs/orianeLogosConfig.js`

const defaultOptions = {
  updateFiles: false,
}
// This in integrated in post script (before std)
export default async function oriGenerateContribution(options) {
  const { updateFiles } = Object.assign({}, defaultOptions, options)

  // 1/ load js files in folder and make them absolute
  // constraint on Globby instruction : all file have to be in the same folder (because of dirname calculation)
  const filePaths = (await globby(['**/logos-data/*.js'])).map(p => `${process.cwd()}/${p}`)
  const fileResults = (await loadScript(filePaths)).map(s => s.default)
  let _dir = dirname(filePaths[0])
  _dir = _dir.replace('/apps/gatsby/','../../')

  // 2/ generate the config2image js file :
  const configTuples = fileResults.map(({ identifier, fileName }) =>
    ({ identifier, fileName, absolutePath: `${_dir}/${fileName}` }))
  const importLines = configTuples.map(({ identifier, absolutePath }) => `import ${identifier} from '${absolutePath}'`)
  const exportLines = configTuples.map(({ identifier }) => `  ${identifier},`)
  const configFileContent = getConfigFile(importLines, exportLines)

  if (!updateFiles) {
    // silent warn...
    // console.warn('Config file not saved. Content = \n', configFileContent)
  } else {
    console.log(chalk.green('Save js2imagePayload config file in'), configFilePath)
    saveData(configFileContent, configFilePath)
  }
  // 3/ extract the results
  const generatedEntities = fileResults.map(r => r.entities).flat()
  // @TODO:? do a merge strategy / warning here: there is one at the upper function

  // 4/ emit a sch_contributor triple to make the link with organisations.
  const ontologyId = 'ori:Ontologie'
  const triple = {
    id: ontologyId,
    sch_contributor: generatedEntities.filter(n => n.type.includes('sch:Organisation')).map(n => n.id)
  }


  // 5/ emit an html description of the ontology
  const descriptionTriple = {
    id: ontologyId,
    // example found here: https://json-ld.org/spec/latest/json-ld/
    // this is not the lastest version and don't taking care of "@language" here
    description: {
      ...oriDescriptionHTML
    }
  }


  generatedEntities.push(triple, descriptionTriple)

  // 5/ return the entites
  return generatedEntities

}

function getConfigFile(importLines, exportLines) {
  return `
// Auto-generated file from \`integrate\` script execution with \`-e\` & \`-u\`.
// See the integrate command in this example : gatsby/data/oriane-rdf-release/README.md
${importLines.join('\n')}

const orianeLogosConfig = {
${exportLines.join('\n')}
}

export default orianeLogosConfig
`
}
