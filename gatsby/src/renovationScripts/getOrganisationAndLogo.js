// import { dirname } from 'path'
// import { fileURLToPath } from 'url'
// const __dirname = dirname(fileURLToPath(import.meta.url))
import camelcase from 'camelcase'
import chalk from 'chalk'
import fs from 'fs'

// @TODO: see how to pass this parameter. for example in a generator function (fn that return fn) ?
const orianeImageFolder = `./logos-data`

export default function getOrganisationAndLogo(name, language, fileType) {

  const identifier = camelcase(name) //@TODO: generate it from name

  const id = `ori:${identifier}`
  const logoId = `ori:logo/${identifier}`

  const entities = [{
    id,
    type: ['sch:Organisation'],
    name: [
      {
        '@language': language,
        '@value': name,
      }
    ],
    logo: [logoId]
  },
  {
    id: logoId,
    type: ['sch:URL'],
    identifier: [`localLogo://${identifier}`]
  }]

  const fileName = `${identifier}${fileType}`
  const localPath = `${orianeImageFolder}/${fileName}`
  if(!fs.existsSync(localPath)){
    console.warn(chalk.red('Please rename. Payload image not found on path:'), localPath)
  }

  return {
    name,
    identifier,
    fileName,
    entities,
  }

}



