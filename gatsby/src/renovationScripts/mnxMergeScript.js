import { dirname } from 'path'
import multiOntologiesContext from '../configs/multiOntologiesContext.js'
import toStdLD from '../jsonldUtils/stdldUtils/toStdLD.js'
import turtle2jsonld from '../jsonldUtils/turtle2jsonld.js'

const sourceFiles = [
  'mnx-1.0.9.ttl', // default owl:Ontology to keep
  'mnx-project-1.0.2.ttl',
  'mnx-skos-1.0.0.ttl',
]
export default async function mnxMergeScript(path) {
  const folderName = dirname(path)

  let targetLd = null
  for await (const path of sourceFiles) {
    // 1/ read and std
    const ttlld = await turtle2jsonld(`${folderName}/${path}`)
    const ldDoc = await toStdLD(ttlld, multiOntologiesContext)

    // 2/ remove owl:ontology if not first
    if (path !== sourceFiles[0]) removeOntologyEntity(ldDoc)

    // 3/ merge to main one
    if (path === sourceFiles[0]) {
      targetLd = ldDoc
    } else {
      targetLd.graph.push(...ldDoc.graph)
    }

  }

  // run a last std to get merged entities
  return await toStdLD(targetLd, multiOntologiesContext)
}

function removeOntologyEntity(ldDoc) {
  const i = ldDoc.graph.findIndex(n => n.type && n.type.includes('owl:Ontology'))
  ldDoc.graph.splice(i, 1)
}
