import remove from 'lodash.remove'

// remove "external typings for tool compatibilty" : all non "mmo:" entities that don't have to reside in the ontology
export default function mmoCleaner(ldDoc){
  const rejectedNS = ['mnx', 'oep', 'dct']
  const removed = remove(ldDoc.graph, (n) => {
    const [ns,name] = n.id.split(':')
    return rejectedNS.includes(ns)
  })
  console.log(removed.length, 'entities removed from ontology')
}
