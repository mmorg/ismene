
const oriDescriptionHTML = {
  '@type': 'rdf:HTML',
  '@value':`
  <div>
  Cette ontologie a été lancée dans le cadre du projet <a href="https://visionspol.eu/2022/03/30/retour-sur-levenement-online-lorientation-2-0/" target="_blank">Smart Orientation</a>, financé par le Conseil Régional d'Ile de France, et remporté par Visions avec CY Cergy Paris Université, Académie de Versailles, le réseau LyLi, le SPRO du bassin est/ouest du 95, Charles de Gaulles Alliance, Mindmatcher, Jobready, Inokufu et Orientoi. <br/>
  Smart Orientation vise à construire un réseau de partage de données entre les acteurs publics et privés de l'orientation afin de permettre au bénéficiaire de faire circuler un profil complet tout au long de sa vie et avoir ainsi accès à des services d'orientation personnalisés. <br/>
  Les participants ont pu mettre en œuvre une première version de ce réseau et le tester avec plus de 1500 bénéficiaires. La présente ontologie constitue un premier socle permettant l'interopérabilité entre les acteurs.
  </div>
  <div>
  Elle est à faire évoluer via l'écosystème régional, national et européen sur le sujet. En ce sens, elle est apportée en tant que commun numérique à <a href="http://prometheus-x.org" target="_blank">Prometheus-X</a> pour favoriser l'émergence du Data Space for Education and Skills.
  </div>
  `,

}

export default oriDescriptionHTML
