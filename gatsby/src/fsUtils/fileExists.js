import fs from 'fs'

// '@TODO: export me to fsUtils Librairy'

export default async function fileExists(file) {
  return fs.promises.access(file, fs.constants.F_OK)
    .then(() => true)
    .catch(() => false)
}
