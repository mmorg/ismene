import React from 'react'

// test integration from source example: 
import { Refine } from "@pankod/refine-core";
import {
  notificationProvider,
  Layout,
  ErrorComponent,
} from "@pankod/refine-antd";
import dataProvider, { GraphQLClient } from "@pankod/refine-strapi-graphql";
import routerProvider from "@pankod/refine-react-router-v6";

import "@pankod/refine-antd/dist/reset.css";

// import { Login } from "pages/login";
import { PostList, PostCreate, PostEdit, PostShow } from '../components/dashorm/cardComponents/refine/pages/posts'

// import on top for Start test on "pure" graphql 
import { GraphQLClient as CustomGraphQLClient, default as customGraphqlDataProvider } from "@pankod/refine-graphql";
import { ConceptEdit, ConceptList } from '../components/dashorm/cardComponents/refine/pages/concept'

// use of loadable in order to workaround SSR incompatibility: 
//  https://www.gatsbyjs.com/docs/using-client-side-only-packages/#workaround-4-load-client-side-dependent-components-with-loadable-components
// import Loadable from "@loadable/component"

const API_URL = "https://api.strapi.refine.dev/graphql";

const client = new GraphQLClient(API_URL);
const gqlDataProvider = dataProvider(client);

// define a subdirectory for the refine editor: https://refine.dev/docs/api-reference/core/providers/router-provider/#serving-the-application-from-a-subdirectory
const { RouterComponent } = routerProvider;
const CustomRouterComponent = () => <RouterComponent basename="/editor" />;

// Start test on "pure" graphql 
const customAPI_URL = 'http://localhost:5000'
const customClient = new CustomGraphQLClient(customAPI_URL)
const customGqlDataProvider = customGraphqlDataProvider(customClient)

const multipleDataProvider = {
  // default: gqlDataProvider,
  default: customGqlDataProvider,
  rdfx: customGqlDataProvider,
}



// @TODO: retrieve `ressources` config from a gatsby query ? 
export default function LoadableRefineApp(props) {

  return (
    <>
      <Refine
        // dataProvider={gqlDataProvider}
        dataProvider={multipleDataProvider}
        // routerProvider={routerProvider}
        routerProvider={{
          ...routerProvider,
          RouterComponent: CustomRouterComponent,
        }}

        options={{ syncWithLocation: true }}

        // authProvider={authProvider}
        // LoginPage={Login}
        resources={[
          {
            name: "posts",
            list: PostList,
            create: PostCreate,
            edit: PostEdit,
            show: PostShow,
            canDelete: true,
          },
          {
            name: 'allConcept',
            options: {
              dataProviderName: 'rdfx'
            },
            list: ConceptList,
            edit: ConceptEdit, //(props) => <ConceptEdit operationType='update' {...props} />,
            create: ConceptEdit, // (props) => <ConceptEdit operationType='create' {...props} />,

          }
        ]}
        notificationProvider={notificationProvider}
        Layout={Layout}
        catchAll={<ErrorComponent />}
      />
    </>

  )
}
