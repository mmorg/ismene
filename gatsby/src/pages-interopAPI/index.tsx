import * as React from "react"
import type { HeadFC, PageProps } from "gatsby"
import { Link } from "gatsby-theme-material-ui"

export const Head: HeadFC = () => <title>Test InteropAPI - Home Page</title>

// const EmptyLayout = ({ children }) => <> {children} </>

const IndexPage = ({ pageContext } : PageProps) => (
  <> 
    <h1>Page de test dans le cadre du déploiement de l'InteropAPI</h1>
  </>
)

export default IndexPage


