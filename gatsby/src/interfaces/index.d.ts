export interface ICategory {
    id: string;
    title: string;
}

export interface IPost {
    id: string;
    title: string;
    // title: {value: string};
    content: string;
    category?: ICategory;
    // specific adds
    narrower?:  Array<IEntity>  // | Array<string>
    __formItemMultiple_narrower: Array<string>
}

export interface IEntity {
    id: string
    // @TODO: see how to add 'any properties' with String, Object or Array value
    // ==> revoir cette documentation: https://www.typescriptlang.org/docs/handbook/2/mapped-types.html

}