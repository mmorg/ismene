import IconDashboard from '@mui/icons-material/Dashboard'
import IconShoppingCart from '@mui/icons-material/ShoppingCart'
import IconPeople from '@mui/icons-material/People'
import IconBarChart from '@mui/icons-material/BarChart'
import IconLibraryBooks from '@mui/icons-material/LibraryBooks'

//@TODO : create another test with the 2 types : menuitem & subheader
const menu = {
  type: 'root',
  children: [
    {
      type: 'menuItem',
      name: 'Dashboard',
      link: '/',
      Icon: IconDashboard,
    },
    {
      type: 'menuDivider'
    },
    {
      type: 'menuItem',
      name: 'Def. op. menuItem',
      //link: '/generated/europass/AchievementListType/',
      Icon: IconShoppingCart,
      open: true,
      children: [
        {
          type: 'menuSubHeader',
          name: 'My sub Header'
        },
        {
          type: 'menuItem',
          name: 'Customers',
          link: '/customers',
          Icon: IconPeople,
        },
        {
          type: 'menuItem',
          name: 'Reports',
          link: '/reports',
          Icon: IconBarChart,
        },
        {
          type: 'menuDivider'
        }

      ]
    },
    {
      type: 'menuItem',
      name: '2 levels sub menu',
      Icon: IconLibraryBooks,
      children: [
        {
          type: 'menuItem',
          name: 'Level 2 with link',
          link: '/thelink'
        },
        {
          type: 'menuItem',
          name: 'Level 2',
          children: [
            {
              type: 'menuItem',
              name: 'Level 3',
            },
            {
              type: 'menuItem',
              name: 'Level 3',
            },
          ],
        },
      ],
    },
  ]
}

export default menu
