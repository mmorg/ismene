/**
 * @jest-environment jsdom
 */
import '@testing-library/jest-dom'
import { render, fireEvent, screen, prettyDOM } from '@testing-library/react'
import React from 'react'
import simpleMenuTree from "./__fixtures__/simpleMenuTree"
import AppMenu from './AppMenu'

// docker-compose exec gatsby npm test -- --watch AppMenu.test

// integration test folder : ontologies/gatsby/src/pages/devTests
// example of local test url : http://localhost:1111/devTests/simpleAppMenuTest/

it.todo('solve test in CI and activate') // 

describe.skip('App menu rendering', () => {
  
  it('render the component', () => {

    render(<AppMenu tree={simpleMenuTree} />)
    const link = screen.getByTestId('listItem-Customers')
    // for this specifics matchers : https://github.com/testing-library/jest-dom
    expect(link).toHaveAttribute('href', '/customers')

  })

})
