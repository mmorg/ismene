import React from 'react'
import hastTree2ReactCompiler from '../dashorm/hastTree2ReactCompiler'
import dashTree2HastTree from '../dashorm/dashTree2HastTree'

// this is the "heart of the site" : the Dashboard generation from instructions
export default function DashHeart(props) {
  const { dashTree, components } = props
  const hastTree = dashTree2HastTree(dashTree) // previous transformations
  const hastTree2React = hastTree2ReactCompiler(components)

  return (
    <>
      {hastTree2React(hastTree)}
    </>
  )
}
