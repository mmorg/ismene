
// configuration of default slug
export default function getSlug(id, absolute = false){
  if (!id) return null
  const [schema, uid] = id.split(':')
  // Add last slash for [gatsby force trailing slash](https://github.com/gatsbyjs/gatsby/discussions/27889)
  const url = `${schema}/${uid}/`
  return absolute ? `/${url}` : url
}
