import React from 'react'
import { Link } from 'gatsby'
import getSlug from './getSlug.js'
import getEntityLabel from '../../EntityUtils/getEntityLabel.js'

export default function createLink(entity) {
  const el = getEntityLabel(entity)
  //@TODO: a switch for external entities ?
  return (<Link to={getSlug(entity.id, true)}>{el}</Link>)
}
