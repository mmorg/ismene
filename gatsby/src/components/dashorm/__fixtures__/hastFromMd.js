import u from 'unist-builder'

const hastFromMd = getTree()
export default hastFromMd


function getTree (){
    return u('root', {
        position: {
          start: {
            line: 1,
            column: 1,
            offset: 0
          },
          end: {
            line: 5,
            column: 10,
            offset: 40
          }
        }
      }, [
        u('element', {
          tagName: 'h1',
          properties: { id: 'hello' },
          position: {
            start: {
              line: 1,
              column: 1,
              offset: 0
            },
            end: {
              line: 1,
              column: 8,
              offset: 7
            }
          }
        }, [u('text', {
            position: {
              start: {
                line: 1,
                column: 3,
                offset: 2
              },
              end: {
                line: 1,
                column: 8,
                offset: 7
              }
            }
          }, 'Hello')]),
        u('text', '\n'),
        u('element', {
          tagName: 'h2',
          properties: { id: 'table-of-contents' },
          position: {
            start: {
              line: 3,
              column: 1,
              offset: 9
            },
            end: {
              line: 3,
              column: 21,
              offset: 29
            }
          }
        }, [u('text', {
            position: {
              start: {
                line: 3,
                column: 4,
                offset: 12
              },
              end: {
                line: 3,
                column: 21,
                offset: 29
              }
            }
          }, 'Table of Contents')]),
        u('text', '\n'),
        u('element', {
          tagName: 'ul',
          properties: {}
        }, [
          u('text', '\n'),
          u('element', {
            tagName: 'li',
            properties: {}
          }, [u('element', {
              tagName: 'a',
              properties: { href: '#rhysd' }
            }, [u('element', {
                tagName: 'strong',
                properties: {}
              }, [u('text', '@rhysd')])])]),
          u('text', '\n')
        ]),
        u('text', '\n'),
        u('element', {
          tagName: 'h2',
          properties: { id: 'rhysd' },
          position: {
            start: {
              line: 5,
              column: 1,
              offset: 31
            },
            end: {
              line: 5,
              column: 10,
              offset: 40
            }
          }
        }, [u('element', {
            tagName: 'a',
            properties: { href: 'https://github.com/rhysd' },
            position: {
              start: {
                line: 5,
                column: 4,
                offset: 34
              },
              end: {
                line: 5,
                column: 10,
                offset: 40
              }
            }
          }, [u('element', {
              tagName: 'strong',
              properties: {}
            }, [u('text', {
                position: {
                  start: {
                    line: 5,
                    column: 5,
                    offset: 35
                  },
                  end: {
                    line: 5,
                    column: 11,
                    offset: 41
                  }
                }
              }, '@rhysd')])])])
      ])
  
}

