import { u } from 'unist-builder'

const hastFromMd = getTree()
export default hastFromMd

function getTree() {
  return u('root', {
    position: {
      start: {
        line: 1,
        column: 1,
        offset: 0
      },
      end: {
        line: 5,
        column: 10,
        offset: 40
      }
    }
  },
    [
      u('element',
        {
          tagName: 'a',
          properties: { href: '#rhysd' }
        },
        [
          u('text', 'first classic html link')
        ]),
      u('text', '\n'),


      u('element',
        {
          tagName: 'my',
          properties: { href: '#rhysd' }
        },
        [
          u('text', 'not overrided comp')
        ]
      ),
      u('text', '\n'),


      u('element',
        {
          tagName: 'MyComp',
          properties: { href: '#rhysd' }
        },
        [
          u('text', 'overrided comp')
        ]
      ),
      u('text', '\n'),
      u('text', '////////////////////'),

    ]
  )

}

