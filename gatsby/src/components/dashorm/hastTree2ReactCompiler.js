import React from 'react'
import rehype2react from 'rehype-react'


export default function hastTree2ReactCompiler(components = {}){
    const hastTree2React = new rehype2react({
        createElement: React.createElement,
        components,
      })
    return hastTree2React.Compiler
}


