import inspect from 'unist-util-inspect'
import dashTree2HastTree from "./dashTree2HastTree"
import dashTreeExploration from './__fixtures__/dashTreeExploration.json'


// docker-compose exec gatsby npm test -- --watch dashTree2HastTree.test

describe('Basic transformations', () => {

  it.todo('other tests on the following one')
  it('Run with no config', () => {
    // console.log(dashTreeExploration)
    const htree = dashTree2HastTree(dashTreeExploration)
    // console.log(inspect(htree))
    expect(htree.children.length).toBe(1)
    const { children: [dashBoard] } = htree
    expect(dashBoard.children.length).toBe(1)

    const { children: [dashLine] } = dashBoard

    const { children: [cardNode] } = dashLine

    const { children: [cardComponent] } = cardNode
    expect(cardComponent).toBeTruthy()

    expect(htree).toMatchSnapshot()
  })

  it('Not throw error on the test tree', () => {
    const t = () => {
      dashTree2HastTree(dashTreeExploration)
    }
    expect(t).not.toThrow()
  })
})