import styled from '@emotion/styled'
import Paper from '@mui/material/Paper'

// @TODO: deprecated, to remove ?

const PaperStyled = styled(Paper)(({ theme }) => ({
  ...theme.typography.body2,
  padding: theme.spacing(2),
  display: 'flex',
  overflow: 'auto',
  flexDirection: 'column',
}))

export default PaperStyled
