import React from 'react'
import getStringValues from '../../../jsonldUtils/getStringValues.js'


// @TODO: review theses logics and merge :
//      : gatsby/src/components/dashorm/baseComponents/LiteralsAsText.js
//      :
export default function LiteralsAsText(props) {

  const { entity, literalsConfig } = props
  if (!entity) return DefaultReturn()
  const informations = literalsConfig.reduce((acc, v) => {
    //@TODO : define this for human readable label of the key
    //      :  here it only take the property and uppercase the first letter
    const displayLabel = v.charAt(0).toUpperCase() + v.slice(1)

    const propertyValues = entity[v]
    if(!propertyValues) return acc

    // Before values extract, check type of value
    let datatypes = null
    let isDatatype = false
    let values = null

    // A/ Is this a Datatype ?
    //    @WARN: for now only test on first element
    const firstPv = propertyValues[0]
    if (firstPv.type) {
      isDatatype = true
      if (firstPv.type === 'rdf:HTML') {
        datatypes = propertyValues.map( (pv, i) => <div key={`${entity.id}-datatype-${i}`} dangerouslySetInnerHTML={{ __html: pv._value }}></div>)
      } else {
        console.warn('This Datatype processing do not exist')
        values = ['__datatype_not_processed__'] // default escape, just a string
      }
    }

    // B/ Is this an array of string or langString ? (not a dataType)
    // @TODO: make it an array of language objects : for i18n
    if (!isDatatype) {
      values = getStringValues(propertyValues)
      // don't update the acc to not get empty property
      if (!values) return acc
      if (values[0] === null) return acc
    }


    const descriptor = {
      key: {
        id: v,
        displayLabel
      },
      values,
      datatypes,
    }
    acc.push(descriptor)
    return acc
  }, [])

  return (
    <React.Fragment>
      {informations.map(descriptor =>
        <div key={descriptor.key.id}>
          <b>{descriptor.key.displayLabel}: </b>
          {descriptor.values ?
            // this is a string or langString array
            descriptor.values.length === 1 ?
              descriptor.values[0]
              : descriptor.values.map((v, i) => (<p key={i}>- {v}</p>))
            : // this is a datatype component array
            descriptor.datatypes
            }
        </div>
      )}
    </React.Fragment>
  );

}

function DefaultReturn() {
  return (
    <React.Fragment>
      <p>Entity variable is null. Please check your config</p>
      {/* // @TODO: faire un case pour ce problème de config ?
      <p>No literal in this entity. Check your config.</p> */}
    </React.Fragment>
  )
}
