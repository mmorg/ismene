/**
 * @jest-environment jsdom
 */
import '@testing-library/jest-dom'
import { render, screen } from '@testing-library/react'
import React from 'react'
import LiteralsAsText from './LiteralsAsText.js'

// docker-compose exec gatsby npm test -- --watch LiteralsAsText.test

describe('LiteralAsText component tests', () => {

  it('render when entity is null', () => {
    const testProps = { entity: null, literalsConfig: [] }
    render(<LiteralsAsText  {...testProps} />)

    screen.getByText('Entity variable is null. Please check your config')
  })

  it('render when entity is a rdfs:Class', () => {
    const testEntity = {
      "id": "hrrdf:AssessmentSubjectType",
      "label": [
        "AssessmentSubjectType"
      ],
      "example": null,
      "status": null,
      "comment": [
        "Person to be assessed."
      ]
    }
    const literalsConfig = [
      "comment",
      "example",
      "status"
    ]

    const testProps = { entity: testEntity, literalsConfig }
    render(<LiteralsAsText  {...testProps} />)

    screen.getByText('Person to be assessed.')
  })


  it('render when entity is a skos:ConceptScheme', () => {

    const entity = {
      id: "hrrdf:assessments",
      prefLabel: [
        {
          _language: "en",
          _value: "Assessments scheme"
        }
      ]
    }

    const literalsConfig = [
      'comment',
      'prefLabel',
    ]

    const testProps = { entity, literalsConfig }
    render(<LiteralsAsText  {...testProps} />)

    screen.getByText('Assessments scheme')
  })


})

describe('Compoent display Datatypes', () => {
  it('works for rdf:HTML', () => {
    const testEntity = {
      id: "ori:Ontologie",
      description: [
        {
          "_language": null,
          "_value": "<p>Some <b>description</b></p>",
          "type": "rdf:HTML"
        }
      ]
    }
    const literalsConfig = ['description']
    const testProps = { entity: testEntity, literalsConfig }
    render(<LiteralsAsText  {...testProps} />)
    screen.getByText('description')
  })
})
