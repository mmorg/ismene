import React, { useEffect, useState } from 'react'
import { useStaticQuery, graphql } from 'gatsby'
import DashMenu from './DashMenu.js'
import getRdfsMenu from './menuBuilderUtils/getRdfsMenu.js'
import BubbleChartIcon from '@mui/icons-material/BubbleChart'
import DeviceHubIcon from '@mui/icons-material/DeviceHub'
import AccountTreeIcon from '@mui/icons-material/AccountTree'

export default function DefaultMenu({ description, lang, meta, title }) {
  const [menuTree, setMenuTree] = useState([])
  const { classMenu, propertyMenu, termsMenu } = useStaticQuery(
    graphql`
    query menuQuery {
      classMenu: allSkosConceptScheme(filter: {types: {in: "cbe_sk:ClassScheme"}}) {
        nodes {
          id
          title {
            _value
            _language
          }
          types
          hasInScheme {
            __typename
            id
            ... on rdfs_Class {
              label {
                _language
                _value
              }
              hasSubClasses {
                id
                label {
                  _language
                  _value
                }
                hasSubClasses {
                  id
                  label {
                    _language
                    _value
                  }
                }
              }
            }
          }
        }
      }
      propertyMenu: allRdfProperty {
        nodes {
          id
          label {
            _language
            _value
          }
          type
        }
      }
      termsMenu: allSkosConceptScheme(filter: {types: {in: "cbe_sk:TermScheme"}}) {
        nodes {
          id
          title {
            _value
            _language
          }
          types
          hasInScheme {
            __typename
            id
            ... on skos_Concept {
              prefLabel {
                _value
                _language
              }
            }
          }
        }
      }
    }
    `
  )

  useEffect(() => {
    async function getMenuTree() {
      const clName = 'Les classes'
      const clFallback = 'hrrdf:AssessmentSubjectType' //'nfr:Certification'

      const prName = 'Les propriétés'
      const prFallback = 'hrrdf:accomodationTypeCodes' //'nfr:estLocalise'

      const termsName = 'Les référentiels'
      const termsFallBack = 'hrrdf-terms:AccountBasedProductCodeList/DependentCareFSA'

      const branches = [
        { nodes: classMenu.nodes, title: clName, icon: BubbleChartIcon, fallbackPage: clFallback },
        { nodes: propertyMenu.nodes, title: prName, icon: DeviceHubIcon, fallbackPage: prFallback },
        { nodes: termsMenu.nodes, title: termsName, icon: AccountTreeIcon, fallbackPage: termsFallBack },
      ]

      const menuTree = await getRdfsMenu(branches)

      setMenuTree(menuTree)
    }

    getMenuTree()
    // do not really understand this for now.
    //   documentation : https://bobbyhadz.com/blog/react-hook-useeffect-has-missing-dependency
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])



  return (
    <DashMenu menuTree={menuTree} />
  )
}
