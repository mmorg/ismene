import multiOntologyMenu_part1 from './__fixtures__/multiOntologyMenu_part1.json'
import multiOntologyMenu_part2 from './__fixtures__/multiOntologyMenu_part2.json'
import getMultiOntoMenu from './getMultiOntoMenu.js'

// docker-compose exec gatsby npm test -- --watch getMultiOntoMenu.test

const fc = (name, parent) => parent.children.find(n => n.name === name)

describe('Multi ontologies transformation to menuTree', () => {

  it('Transform the first structuration test', async () => {

    const { allOwlOntology: { nodes: ontologies } } = multiOntologyMenu_part1.data

    const multiMenuTree = await getMultiOntoMenu(ontologies)
    // console.log(multiMenuTree)

    const menuOntology1 = fc('Oriane', multiMenuTree)
    // console.log(menuOntology1)

    expect(menuOntology1).toMatchSnapshot()

    const ontology1Classes = fc('Les classes', menuOntology1)
    // console.log(ontology1Classes.children)
    expect(ontology1Classes.children.length).toBe(7)
    // console.log(ontology1Classes)

  })

  it('solve the unamed properties on example 2', async () => {
    const { allOwlOntology: { nodes: ontologies } } = multiOntologyMenu_part2.data

    const multiMenuTree = await getMultiOntoMenu(ontologies)
    // console.log(multiMenuTree)

    const menuOntology1 = fc('Oriane', multiMenuTree)
    const ontology1Properties = fc('Les propriétés', menuOntology1)
    expect(ontology1Properties.children[0].name).toBe('Type Bloom')

  })

})

