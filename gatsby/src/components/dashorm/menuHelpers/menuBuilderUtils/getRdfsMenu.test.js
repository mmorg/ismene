import getRdfsMenu from './getRdfsMenu.js'
import megaMenuOneLevel from '../../../dashibiden/dynamicMenu/__fixtures__/megaMenuOneLevel.json'
import classTreeExample from '../../../dashibiden/dynamicMenu/__fixtures__/classTreeExample.json'
import multiTypesMenu from './__fixtures__/multiTypesMenu.json'
import listOfConcepts from './__fixtures__/listOfConcepts.json'

// docker-compose exec gatsby npm test -- --watch getRdfsMenu.test

const DefaultIcon = () => { }

describe('Multi ontology transformation to menuTree', () => {


  it('Transform a complex menu with differents types', async () => {
    const { classMenu, propertyMenu, termsMenu } = multiTypesMenu.data

    const clName = 'Les classes'
    const clFallback = 'hrrdf:AssessmentSubjectType'

    const prName = 'Les propriétés'
    const prFallback = 'hrrdf:accomodationTypeCodes'

    const termsName = 'Les référentiels'
    const termsFallBack = 'hrrdf-terms:AccountBasedProductCodeList/DependentCareFSA'

    const branches = [
      { nodes: classMenu.nodes, title: clName, icon: DefaultIcon, fallbackPage: clFallback },
      { nodes: propertyMenu.nodes, title: prName, icon: DefaultIcon, fallbackPage: prFallback },
      { nodes: termsMenu.nodes, title: termsName, icon: DefaultIcon, fallbackPage: termsFallBack },
    ]

    const menuTree = await getRdfsMenu(branches)

    const f = (name) => menuTree.children.find(c => c.name === name)
    const cl = f(clName)
    expect(cl.children[0].name).toBe('Assessments')

    const pf = f(prName)
    expect(pf.children[0].name).toBe('abilities')

    const te = f(termsName)
    expect(te.children[0].name).toBe('AccountBasedProductCodeList')

  })
})

describe('Data to menuTree transformation', () => {

  const prFallback = 'hrrdf:accomodationTypeCodes'

  it('Transform flat mega properties data to propertiesMenuTree', async () => {

    const items = megaMenuOneLevel.data.allRdfProperty.nodes
    // items.splice(10, items.length)
    const branches= [
      { nodes: items, title: 'Properties', icon: DefaultIcon, fallbackPage: prFallback }
    ]

    const menuTree = await getRdfsMenu(branches)
    // console.log(menuTree.children[3])
    // expect('imp').toBe(true)
    expect(menuTree).toMatchSnapshot()
  })

  it.todo('Review this with menu ordering from skos:concept')
  it('Transform multi-level class data to classesTree', async () => {
    const items = classTreeExample.data.allRdfsClass.nodes
    const menuName = 'Les classes'
    const branches= [
      { nodes: items, title: menuName, icon: DefaultIcon, fallbackPage: prFallback }
    ]
    const menuTree = await getRdfsMenu(branches)

    const classesMenuItem = menuTree.children.find(n => n.name === menuName)
    const [entityOfWorld, institution] = classesMenuItem.children

    // test 2 levels menu
    expect(entityOfWorld.link).toBe('/nfr/EntiteDuMondeDeLaFormation/')
    expect(entityOfWorld.children).toBeTruthy()
    expect(entityOfWorld.children.length).toBe(16)

    // test 3 levels menu
    const siteClass = entityOfWorld.children[15]
    expect(siteClass.link).toBe('/nfr/Site/')
    expect(siteClass.children).toBeTruthy()
  })

  it('Transform list of concepts to a concepts Tree', async () => {
    const concepts = listOfConcepts
    const menuName = 'Les référentiels'
    const branches = [
      {nodes: concepts, title: menuName, icon: DefaultIcon, fallbackPage: prFallback}
    ]

    const conceptTree = await getRdfsMenu(branches)

    const termsMenuItem = conceptTree.children.find(n => n.name === menuName)
    // only 1 level by default (the export is only a partial one, no removal of default level)
    expect(termsMenuItem.children.length).toBe(1)
    const defaultLevel = termsMenuItem.children[0]

    expect(defaultLevel.children.length).toBe(7)

    // get the first familly : sécurité, réseau, cloud
    const familly = defaultLevel.children[0]
    expect(familly.children.length).toBe(6)

    const job = familly.children[0] // job = cloud with 3 positions
    expect(job.children.length).toBe(3)
    expect(job.children).toMatchSnapshot()

  })

})
