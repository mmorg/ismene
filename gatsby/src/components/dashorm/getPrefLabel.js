
console.warn('@TODO: deprecated, use test-ts-gatsby/src/EntityUtils/getEntityLabel.js')
export default function getPrefLabel(entity){

   const lang = 'fr'
   let finded = entity.prefLabel.find( p => p._language === lang)
   if(!finded){ // return the first one 
       finded = entity.prefLabel[0]
   }

   // do a split while preflabel with 2 version on occupation
   const [male] = finded._value.split('/')

   return male
//    const { prefLabel: [ { _value: prefLabelValue }] } = entity
//    return prefLabelValue
}