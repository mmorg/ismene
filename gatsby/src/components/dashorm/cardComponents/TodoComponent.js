import React from 'react'


export default function TodoComponent(props) {
  const { contentText } = props.data
  return (
    <p>{contentText}</p>
  )
}
