import React from 'react'
import { Link } from 'gatsby'
import getSlug from '../../componentUtils/getSlug'
import getPrefLabel from '../getPrefLabel'

export default function ListLinksComponent(props){
    const {data : {entities}} = props
    return (
        <React.Fragment>
            {entities.map( entity => 
                <div key={entity.id}>
                    <Link to={getSlug(entity.id)}>{getPrefLabel(entity)}</Link>
                </div>)
             }
        </React.Fragment>
      );
}