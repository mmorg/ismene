import React from 'react'
import { graphql } from "gatsby"
import LiteralsAsText from '../baseComponents/LiteralsAsText'

// @TODO: create a generic LiteralsComponent based on
//      - a graphql interface hierarchy : https://www.gatsbyjs.com/docs/reference/graphql-data-layer/schema-customization/#custom-interfaces-and-unions
//      - a merge of : gatsby/src/components/dashorm/cardComponents/ConceptSchemeLiteralsComponent.js
//      - and : gatsby/src/components/dashorm/cardComponents/ClassLiteralsComponent.js
// --> issue to create


export const query = graphql`
fragment ConceptLiteralsComponent on Query{
  entity: skosConcept(id: {eq: $id}) {
    id
    prefLabel {
      _language
      _value
    }
  }
}
`

export default function ConceptLiteralsComponent(props) {

  const { entity } = props.data
  const literals = ['id', 'prefLabel', 'altLabel']

  return (
    <LiteralsAsText entity={entity} literalsConfig={literals}/>
  )
}
