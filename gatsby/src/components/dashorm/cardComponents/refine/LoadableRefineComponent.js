import React from 'react'

// test integration from source example: 
import { Refine } from "@pankod/refine-core";
import "@pankod/refine-antd/dist/reset.css";
import {
  notificationProvider,
  ErrorComponent,
} from "@pankod/refine-antd";

// use memoryRouter for independance
import routerProvider, { MemoryRouterComponent } from "@pankod/refine-react-router-v6";

// Graphql endpoint:
import graphqlDataProvider, { GraphQLClient } from "@pankod/refine-graphql";

import { ConceptEdit, ConceptList } from './pages/concept'

const API_URL = 'http://localhost:5000'
const customClient = new GraphQLClient(API_URL)
const dataProvider = graphqlDataProvider(customClient)

// @TODO: move it to : `gatsby/src/refine`
export default function LoadableRefineComponent(props) {

  const entityLocation = `/allConcept/edit/${props.data.entity.id.replace(':', '%3A')}`

  return (
    <>
      <Refine
        dataProvider={dataProvider}
        routerProvider={{
          ...routerProvider,
          RouterComponent: MemoryRouterComponent.bind({
            initialRoute: entityLocation,
          }),
        }}
        resources={[
          {
            name: 'allConcept',
            list: ConceptList,
            edit: ConceptEdit,
            // create: ConceptEdit,
          }
        ]}
        notificationProvider={notificationProvider}
        catchAll={<ErrorComponent />}
      />
    </>


  )
}
