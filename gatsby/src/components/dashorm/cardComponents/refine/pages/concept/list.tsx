import React from 'react'
import { IResourceComponentsProps, useExport } from "@pankod/refine-core";

import {
    List,
    Table,
    useTable,
    Space,
    EditButton,
    ShowButton,
    DeleteButton,
    getDefaultSortOrder,
    FilterDropdown,
    Select,
    useSelect,
    ExportButton,
    ImportButton,
    CreateButton,
    useImport,
    TextField,
    TagField,
} from "@pankod/refine-antd";

import { IPost, ICategory } from '../../../../../../interfaces'

export const ConceptList: React.FC<IResourceComponentsProps> = () => {
    const { tableProps, sorter } = useTable<IPost>({
        initialSorter: [
            // {
            //   field: "id",
            //   order: "asc",
            // },
        ],
        metaData: {
            operation: 'allConcept',
            fields: [
                'id',
                { prefLabel: ['value'] },
                { narrower: [{ prefLabel: ['value'] }] },
                { broader: [{ prefLabel: ['value'] }] },
            ],
        },
    });



    return (
        <List
            headerProps={{
                extra: (
                    <Space>
                        {/* //@TODO: add import & export buttons */}
                        <CreateButton />
                    </Space>
                ),
            }}
        >
            <Table {...tableProps} rowKey="id">
                <Table.Column
                    key="id"
                    // dataIndex="id"
                    title="ID"
                    sorter={{ multiple: 2 }}
                    defaultSortOrder={getDefaultSortOrder("id", sorter)}
                    render={(value) => {
                        const idValue = value.id ? value.id : '___No ID___'
                        const textValue = `${idValue.slice(0, 9)}...`
                        return (<TextField value={textValue} />)
                    }}
                />
                <Table.Column
                    key="title"
                    title="PrelLabel"
                    sorter={{ multiple: 1 }}
                    render={(value) => {
                        const textValue = value.prefLabel[0].value ? value.prefLabel[0].value : 'No prefLabel'
                        return (<TextField value={textValue} />)
                    }}
                />

                {/* @TODO: auto-generate theses columns with `predicateToIRI` 
        like in: rdfx-graphql/src/appSearchUtils/normDenorm/normalisation/normForAppSearchWithDepth.js */}
                <Table.Column
                    key='broader'
                    title="Broaders"
                    sorter={{ multiple: 1 }}
                    render={(value) => {
                        // @ts-ignore
                        const textValues = value?.broader?.map(e => e.prefLabel[0].value)
                        // @ts-ignore
                        return textValues ? textValues.map((t,i) => <TagField key={`broader-${i}`} value={t} />) : ''
                    }}
                />
                <Table.Column
                    key={'narrower'}
                    title="Narrowers"
                    sorter={{ multiple: 1 }}
                    render={(value) => {
                        // @ts-ignore
                        const textValues = value?.narrower?.map(e => e.prefLabel[0].value)
                        // @ts-ignore
                        return textValues ? textValues.map((t,i) => <TagField key={`narrower-${i}`} value={t} />) : ''
                    }}
                />

                <Table.Column<IPost>
                    title="Actions"
                    dataIndex="actions"
                    render={(_, record) => (
                        <Space>
                            <EditButton
                                hideText
                                size="small"
                                recordItemId={record.id}
                            />
                            <ShowButton
                                hideText
                                size="small"
                                recordItemId={record.id}
                            />
                            <DeleteButton
                                hideText
                                size="small"
                                recordItemId={record.id}
                            />
                        </Space>
                    )}
                />
            </Table>
        </List>
    );
};
