import React from "react"
// @ts-ignore
import splitId from '../../../../../../jsonldUtils/splitId'
// @ts-ignore
import getStringValues from '../../../../../../jsonldUtils/getStringValues'
import { HttpError, IResourceComponentsProps } from "@pankod/refine-core"

import {
    Edit,
    Form,
    Input,
    ListButton,
    RefreshButton,
    Select,
    Space,
    useForm,
    useSelect,
} from "@pankod/refine-antd";

import MDEditor from "@uiw/react-md-editor";

import { IPost, ICategory, IEntity } from '../../../../../../interfaces'


// hook to create multiple Form.Item component. 
// Option 1 : source : https://stackoverflow.com/a/53906907
//  maybe a little bit too hackish ? 
// Option 2 : try this ? : https://usehooks-ts.com/react-hook/use-map

// @ts-ignore
// @TODO: add specific filters in configuration, make it more generic
const useMultipleSelectConf = multipleSelectConf => multipleSelectConf.map((conf, i) => {

    const {
        predicateLabel,
        predicateTerm,
        multipleItemName,
    } = conf

    const { selectProps } = useSelect<ICategory>({
        // resource: 'AllConcept',
        resource: 'Concept',
        optionLabel: 'prefLabel[0].value', // () => { return 'test'} , // => @TODO: a contribution to allow fn more than just "lodash Objectpath" : https://refine.dev/docs/api-reference/core/hooks/useSelect/#optionlabel-and-optionvalue
        optionValue: 'id',
        metaData: {
            operation: 'allConcept',
            fields: [
                'id',
                { prefLabel: ['value'] },
            ],
        },
        pagination: {
            current: 1,
            pageSize: 500,
        },
        // @TODO: restaure this when connexion is ok on graphql: this will allow live notifications.
        liveMode: 'off',
    })
    // return selectProps
    return (
        <Form.Item
            label={predicateLabel}
            name={multipleItemName}
        // rules={[
        //   {
        //     required: true,
        //   },
        // ]}
        >
            <Select
                {...selectProps}
                mode='multiple'
            />
        </Form.Item>
    )

})

/**
 * 
 * @returns React.FC<IResourceComponentsProps>
 */
// @ts-ignore
export const ConceptEdit = (props) => {
    const entityType = 'Concept'
    // @TODO: this is a workaround until : 
    //  A/ Refine configuration parameter (issue to-do) for the 'InputType' Name. 
            // the "variable" name can be passed but the input.type name is fixed by : https://github.com/refinedev/refine/blob/master/packages/graphql/src/dataProvider/index.ts#L136
    //  B/ GraphQl InputType can be "oneOf" (https://github.com/graphql/graphql-spec/pull/825) or "Struct" (https://github.com/graphql/graphql-wg/blob/main/rfcs/Struct.md#composite-type-capable-of-input-polymorphism)
    // source: Actual Input Type is not configurable in the Refine's Metadata object (https://github.com/refinedev/refine/blob/master/packages/graphql/src/dataProvider/index.ts#L136)
    // ==> issue in refine to check : https://github.com/refinedev/refine/issues/3409
    //      ==> the other option will be to hack the lib and build it
    console.warn('!!!!!!!!!!!!! Re-check here the issue !!!')
    const { operationType } = props
    const operation = operationType ? `${entityType}_${operationType}` : entityType
    // console.log(props, '---', flavour ,'))))', others, children)
    const { form, formProps, saveButtonProps, queryResult } = useForm<
        IPost,
        HttpError,
        IPost
    >({
        metaData: {
            operation, //: 'Concept_create',
            fields: [
                'id',
                { prefLabel: ['value'] },
                { narrower: ['id', { prefLabel: ['value'] }] },
                { broader: ['id', { prefLabel: ['value'] }] },
            ],
        },
    });

    const conceptData = queryResult?.data?.data

    // ==> Start multi-select "hack" 
    // load initial values for multi-select
    // Test and try with "Refine.useSelect" hook do not work. Maybe a bug report should be done. 
    //  the workaround found is in this example: https://github.com/ant-design/ant-design/issues/24196
    // This workaround may also be required by typescript. I don't find how to map on IEntity or String
    const predicateToIRI_ref = [
        {
            id: 'skos:broader',
            label: [
                {
                    '@language': 'en',
                    '@value': 'has broader'
                }
            ],
        },
        {
            id: 'skos:narrower',
            label: [
                {
                    '@language': 'en',
                    '@value': 'has narrower'
                }
            ],
        },
    ]
    // @TODO: define the predicateToDatatype_ref = [] // ??need a more specialized predicateToLangString ? 

    const multipleSelectConf = predicateToIRI_ref.map(e => {
        const { term: predicateTerm } = splitId(e.id)
        const _label = e.label ? getStringValues(e.label) : null
        const predicateLabel = _label.length ? _label[0] : predicateTerm

        let predicateValues = []
        // @ts-ignore
        if (conceptData && conceptData[predicateTerm]) {
            // @ts-ignore
            predicateValues = conceptData[predicateTerm]
        }

        // @TODO: check if really used... in "multiple case". Maybe for init the values in the form ? 
        let multipleInitialValues = []
        if (conceptData) {
            // @ts-ignore
            multipleInitialValues = conceptData[predicateTerm]?.map(e => e.id)
        }

        const multipleConf = {
            predicateLabel,
            predicateTerm,
            predicateValues,
            multipleItemName: `__formItemMultiple_${predicateTerm}`,
            multipleInitialValues,
            // selectProps,
        }

        return multipleConf
    })

    const fieldValueObject = multipleSelectConf.reduce((acc, conf) => {
        const {
            predicateTerm, predicateValues,
            multipleItemName, multipleInitialValues
        } = conf
        const predicateFieldValue = {
            [predicateTerm]: predicateValues,
            [multipleItemName]: multipleInitialValues,
        }
        Object.assign(acc, predicateFieldValue)
        return acc
    }, {})

    // populate predicateToIri field values
    form.setFieldsValue(fieldValueObject)


    // generate the Form.Item Components
    const predicateToIRI_formItem = useMultipleSelectConf(multipleSelectConf)

    // IndexMap for Refine's multiple structure to source object config
    const multipleSelectConfMap = multipleSelectConf.reduce((acc, conf) => {
        acc.set(conf.multipleItemName, conf)
        return acc
    }, new Map())

    return (
        <Edit
            saveButtonProps={saveButtonProps}
            headerProps={{
                extra: (
                    <Space>
                        <ListButton />
                        <RefreshButton onClick={() => queryResult?.refetch()} />
                    </Space>
                ),
            }}
        >
            <Form
                {...formProps}
                layout='vertical'
                onFinish={(values) => {
                    console.warn('Start calling the onFinish', values, '@TODO: to restaure !!!')
                    // transform "name hack" to legit property 
                    const entity = Object.fromEntries(
                        Object.entries(values).map(([key, value]) => {
                            if (multipleSelectConfMap.has(key)) {
                                const legitKey = multipleSelectConfMap.get(key).predicateTerm
                                return [legitKey, value]
                            }
                            // if (key === narrower_multipleItemName) return ['narrower', value]
                            return [key, value]
                        })
                    )
                    console.log(entity)
                    return formProps.onFinish?.(entity as any)
                }}
            >
                <Form.Item
                    label='ID (without Prefix automatically added)'
                    name={['id']}
                    rules={[
                        {
                            required: true,
                        },
                    ]}
                >
                    <Input />
                </Form.Item>
                <Form.Item
                    label='Title'
                    name={['prefLabel', 0, 'value']}
                    rules={[
                        {
                            required: true,
                        },
                    ]}
                >
                    <Input />
                </Form.Item>

                {/* predicatesToIRI display */}
                {predicateToIRI_formItem}

                @TODO: restaure the "md content editor" for descriptions !
                {/* <Form.Item
          label="Content"
          name="content"
          rules={[
            {
              required: true,
            },
          ]}
        >
          <MDEditor data-color-mode="light" />
        </Form.Item> */}
            </Form>
        </Edit>
    );
};
