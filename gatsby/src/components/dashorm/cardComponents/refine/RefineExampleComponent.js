import React from 'react'

// import and create a Refine component in a "Lazy / Client only" way
const ClientSideOnlyLazy = React.lazy(() =>
  import('./LoadableRefineComponent')
)

export default function RefineExampleComponent(props) {

  const isSSR = typeof window === "undefined"

  return (
    <>
      {!isSSR && (
        <React.Suspense fallback={<div />}>
          <ClientSideOnlyLazy {...props} />
        </React.Suspense>
      )}
    </>
  )
}
