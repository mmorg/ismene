import React from 'react';

export default function OccupationInfoComponent(props){
    const {data : {description}} = props
    
    const fr = description.find( d => d.language === 'fr')
    const en = description.find( d => d.language === 'en')
    return (
        <React.Fragment>
            { fr && 
                <>
                    <div><b>Français :</b></div>
                    <div>{fr.nodeLiteral._value}</div>
                </>
                
            }
            { en && 
                <>
                    <div><b>English :</b></div>
                    <div>{en.nodeLiteral._value}</div>
                </>
            }
        </React.Fragment>
      );
}