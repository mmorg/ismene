import React from 'react';

export default function EntityInfoComponent(props){
  console.warn('@TODO: update this component to be "entity ready"')
    const {data} = props
    let content = 'No description for this item'
    if(data) content = JSON.stringify(data)
    return (
        <React.Fragment>
            <p>{content}</p>
        </React.Fragment>
      );
}
