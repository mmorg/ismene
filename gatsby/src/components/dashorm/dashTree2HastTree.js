import { map as unistUtilMap } from 'unist-util-map'

export default function dashTree2HastTree(dashTree) {
    const dashDomNodes = ['dashboard', 'dashLine', 'cardNode', 'cardComponent']

    return unistUtilMap(dashTree, (n, i, p) => {
        if (dashDomNodes.includes(n.type)) {
            return getHastNode(n)
        }
        return n
    })

}

function getHastNode(node) {
    const overide = {
        type: 'element',
        tagName: node.type
    }

    const clone = Object.assign({}, node, overide)

    // overide tagName from `node.compName` prop
    // remove the `clone.compName`
    if (node.compName) {
        clone.tagName = node.compName
        delete clone.compName
    }

    return clone
}