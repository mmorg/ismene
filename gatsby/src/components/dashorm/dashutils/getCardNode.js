
export default function getCardNode(data, componentName, options) {
  const defaultValues = {
    cardName: '',
    // sizeScale: 'medium', // little / medium / large //@Deprecated
    colunmSize: 8,
    showContribution: false,
  }

  const _options = Object.assign({}, defaultValues, options)
  return {
    type: 'cardNode',
    properties: {
      name: _options.cardName, // @TODO: remove this compatibility field, change name to cardName... or to "title"
      ..._options,
      data,
    },
    children: [
      {
        type: 'cardComponent',
        compName: componentName,
        properties: {
          data,
        }
      }
    ]
  }
}
