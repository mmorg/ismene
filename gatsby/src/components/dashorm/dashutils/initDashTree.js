import getDashboard from './getDashboard.js'

export default function initDashTree() {
    const dash = getDashboard()
    return {
        type: 'root',
        children: [dash]
    }
}
