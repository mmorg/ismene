
export default function getDashboard(name) {
    if (!name) name = 'Default Dashboard Name'
    return {
        type: 'dashboard',
        name,
        children: []
    }
}