import getLine from './getLine.js'

export default function addCardNode(dashTree, card, lineIndex, columnIndex) {
    const { children: [dash] } = dashTree

    // is the line exist ? 
    let line = dash.children[lineIndex]
    if (!line) {
        line = getLine()
        dash.children.splice(lineIndex, 0, line)
    }

    // add the card in the line
    line.children.splice(columnIndex, 0, card)
}
