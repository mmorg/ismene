
export default function getLine(name) {
    if (!name) name = 'Default Line Name'
    return {
        type: 'dashLine',
        name,
        children: []
    }
}