import React from 'react'
import Grid from '@mui/material/Grid'
import Title from '../componentUtils/Title.js'
import { Card, CardContent, CardHeader, IconButton } from '@mui/material'
import styled from '@emotion/styled'
import ForumIcon from '@mui/icons-material/Forum'
import HandymanIcon from '@mui/icons-material/Handyman'
import RefinePopUpAction from './cardComponents/refine/RefinePopUpAction.js'

const dashboard = (props) => {
  const { children } = props
  return (
    <>
      <Grid container spacing={3} justifyContent='space-between'>
        {children}
      </Grid>
    </>
  )
}

const dashLine = (props) => {
  const { children } = props
  return (
    <Grid container item spacing={1}>
      {children}
    </Grid>
  )
}

const CardNode = (props) => {
  const { children,
    data, name, sizeScale, columnSize,
    showContribution, showEdit
  } = props
  const entityId = data.entity.id

  const cardHeaderProperties = {}

  // add sub-header
  // @TODO: use a "copiable chip" instead of just string
  cardHeaderProperties.subheader = entityId

  const actions = []

  if (showContribution) {
    const extLink = `https://gitlab.com/mmorg/ismene/-/issues/?sort=created_asc&state=opened&label_name%5B%5D=${entityId}&first_page_size=20`
    actions.push(
      <IconButton aria-label="contribution" href={extLink} target="_blank">
        <ForumIcon />
      </IconButton>
    )
  }

  if(showEdit){
    actions.push(<RefinePopUpAction {...props}/>)
    // const extLink = `https://test.com`
    // actions.push(
    //   <IconButton aria-label="contribution" href={extLink} target="_blank">
    //     <HandymanIcon />
    //   </IconButton>
    // )
  }

  // merge the actions into the property
  if(actions.length){
    cardHeaderProperties.action = (<>{actions}</>)
  }

  let md = 8
  if (columnSize) md = columnSize
  let cardSize = { xs: 12, md }
  if (sizeScale && !columnSize) { // @TODO: remove we migrated
    //default to sizeScale = 'medium', // 'little'/ medium / large
    cardSize = { xs: 12, md: 8, lg: 8 }
    if (sizeScale === 'little') cardSize = { xs: 12, md: 8, lg: 4 }
    if (sizeScale === 'large') cardSize = { xs: 12, md: 8, lg: 12 }
    console.warn('@Deprecated, use columnSize instead')
  }


  return (
    <Grid item {...cardSize}>
      <Card>
        <CardHeader
          title={
            name && <Title>{name}</Title>
          }
          {...cardHeaderProperties}
          sx={{ pb: 0 }}
        />
        <CardContentStyled sx={{ pt: 0.25 }}>
          {children}
        </CardContentStyled>
      </Card>
    </Grid>

  )
}

const CardContentStyled = styled(CardContent)(({ theme }) => ({
  ...theme.typography.body2,
  padding: theme.spacing(2),
  display: 'flex',
  overflow: 'auto',
  flexDirection: 'column',
}))

export { dashboard, dashLine, CardNode as cardNode }

