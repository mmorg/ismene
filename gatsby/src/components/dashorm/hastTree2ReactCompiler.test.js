import React from 'react'
import unified from 'unified'
import markdown from 'remark-parse'
import slug from 'remark-slug'
import toc from 'remark-toc'
import remark2rehype from 'remark-rehype'
import highlight from 'rehype-highlight'
import rehype2react from 'rehype-react'
import inspect from 'unist-util-inspect'
import hastSimple from './__fixtures__/hastSimple'
import hastTree2ReactCompiler from './hastTree2ReactCompiler'


// docker-compose exec gatsby npm test -- --watch hastTree2ReactCompiler.test

describe('Get the compiler', () => {
  it.todo('find a good way to test this. Maybe using react components tests ? ')

  it.todo('move this test to good hasttree2reactComponent lib file')
  it('Run with no config', () => {
    const hastTree2React = hastTree2ReactCompiler()
    const compiled = hastTree2React(hastSimple)
    expect(compiled).toMatchSnapshot()
  })

  it('run with a component override', () => {
    // compiler conf
    const MyComp = () => {
      return (<p> This is the comp override</p>)
    }
    const hastTree2React = hastTree2ReactCompiler({
      'MyComp': MyComp,
    })

    const compiled = hastTree2React(hastSimple)
    expect(compiled.props.children).toMatchSnapshot()
  })
})

describe('Explorations around dashTree to React stuff', () => {

  it.skip('show hast tree from md for saving and reuse', () => {
    // from md for now
    var processor = unified()
      .use(markdown)
      .use(slug)
      .use(toc)
      // .use(github, { repository: 'rehypejs/rehype-react' })
      .use(remark2rehype)
      // .use(getUNotation)
      .use(inspectTree)
      .use(highlight)
      .use(rehype2react, { createElement: React.createElement })

    const text = '# Hello\n\n## Table of Contents\n\n## @rhysd\n\n<dash>Contents</dash>'

    const processed = processor.processSync(text)
    console.log(processed)
    console.log(processed.result)

    expect(true).toBe(false)
  })
})

// @TODO : put it in utils
// source : https://github.com/unifiedjs/unified#example-10
function inspectTree(options) {

  return transformer

  function transformer(tree) {
    console.log(inspect(tree))
  }
}
