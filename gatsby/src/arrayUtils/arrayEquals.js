
// @TODO : update this file to "main repos" = competenciesMarketModel

// Source  : https://stackoverflow.com/a/13523757
//@TODO : a test file for it (add exemples in post)

export default function arraysEquals(arr1, arr2){

    return ( (arr1.length === arr2.length)
        && (arr1.every(function(u, i) {
            // Use "is" instead of "===" to manage NaN
            return is(u, arr2[i]);
        }))
    )
}

/* eslint-disable */
// see this doc and use NaN :: https://eslint.org/docs/rules/no-self-compare
function is(a, b) {
    return ((a === b) && (a !== 0 || 1 / a === 1 / b)) // false for +0 vs -0
        || ((a !== a) && (b !== b)); // true for NaN vs NaN
}
/* eslint-disable */
