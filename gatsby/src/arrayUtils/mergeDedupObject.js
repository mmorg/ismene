
const defaultIdentityFn = e => e.id
export default function mergeDedupObject(source, toMerge, identityFn = defaultIdentityFn){


  const mergedIndex = source.reduce((acc,v)=> {
    const sourceIndex = identityFn(v)
    if(!sourceIndex){
      console.warn('The identifyFn shoud not return null. In this case, all objects with identity=`null` will be equals')
      console.warn('Source value is', v)
    }
    if(acc.has(sourceIndex)) console.warn('Two objects have the same identity in the source array. Last one will win')
    acc.set(sourceIndex, v)
    return acc
  }, new Map())

  for(const newObject of toMerge){
    const newIdentity = identityFn(newObject)
    if(!mergedIndex.has(newIdentity)) mergedIndex.set(newIdentity, newObject)
    // else case: this is duplicate, so we discard
  }

  return [...mergedIndex.values()]

}