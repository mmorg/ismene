
// for array of `datatypes` for now
// @TODO: add a indirection for array of objects ? Find others scripts.
export default function mergeDedup(arr1, arr2){
  return [...(new Set([...arr1,...arr2]))]
}
