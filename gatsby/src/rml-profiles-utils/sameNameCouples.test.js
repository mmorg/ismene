import sameNameCouples from './sameNameCouples.js'

// docker-compose exec rml npm test -- --watch sameNameCouples.test


describe('Test of sameNameCouples', () => {

  it('Associate for each source a corresponding yaml file', async () => {

    const sourcePaths = [
      '/test/file1.csv',
      '/test/fileX.xml',
    ]
    const targetPaths = [
      '/test/mappings/file1.yaml',
      '/test/mappings/fileX.yaml'
    ]

    const sourceGlobTest =  'glob-test'
    const targetGlobTest = 'glob-test'

    const processingCouples1 = await sameNameCouples(sourcePaths, targetPaths, sourceGlobTest, targetGlobTest)
    expect(processingCouples1.length).toBe(2)
  })

  it('works for files with a "." in the name', async () => {

    const sourcePaths = [
      '/test/file.with .dot.csv',
      '/test/file2.csv'
    ]
    const targetPaths = [
      '/test/mappings/file.with .dot.yaml',
      '/test/file2.yaml'
    ]

    const sourceGlobTest =  'glob-test'
    const targetGlobTest = 'glob-test'

    const processingCouples1 = await sameNameCouples(sourcePaths, targetPaths, sourceGlobTest, targetGlobTest)
    expect(processingCouples1.length).toBe(2)
  })

})

