import chalk from 'chalk'

export default async function sameNameCouples(sourcePaths, mappingPaths, sourceGlob, mappingGlob) {

  if (!sourcePaths.length) {
    throw new Error(`Your Source glob instructions don't return any file. Check your Source glob: ${JSON.stringify(sourceGlob)}`)
  }
  if (!mappingPaths.length) {
    throw new Error(`Your Mapping glob instructions don't return any file. Check your Mapping glob: ${JSON.stringify(mappingGlob)}`)
  }

  const processingCouples = sourcePaths.reduce((acc, sp) => {
    const mappingBase = sourcePath2mappingBase(sp)
    const mappingId = `${mappingBase}.yaml`
    const mappingPath = mappingPaths.find(p => p.endsWith(mappingId))


    if (!mappingPath) {
      console.log(chalk.red('No mapping file found with this name'), mappingId,
        chalk.red('for this source file'), sp)
      return acc
    }

    acc.push([sp, mappingPath])
    return acc
  }, [])

  // console.log(processingCouples)

  return processingCouples

}


function sourcePath2mappingBase(source) {
  let mappingId = source.split('/').pop()
  mappingId = mappingId.split('.')
  mappingId = mappingId[mappingId.length - 2]
  return mappingId
}
