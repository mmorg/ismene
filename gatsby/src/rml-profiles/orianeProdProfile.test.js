import orianeProdProfile from './orianeProdProfile.js'
import sourceLD1 from './__fixtures__/source-ld.json'

// docker-compose exec gatsby npm test -- --watch orianeProdProfile.test

describe('Text to id transformation', () => {

  it('Manage full text to id transformations', async () => {
    const r = await orianeProdProfile.postProc(sourceLD1, {saveOntoFile:false})
    // console.log(r.graph.slice(10,15))
    expect(r).toMatchSnapshot()
  })

})
