import sameNameCouples from '../rml-profiles-utils/sameNameCouples.js'
import oriContext from '../stdContext/oriContext.js'
import { saveJson } from '@mmorg/fsutils'
import chalk from 'chalk'
import camelcase from 'camelcase'

// This is the profile for generating the ontology. 

export default {
  targetContext: oriContext,
  getProcessingCouples: sameNameCouples,
  postProc
}

const defaultOptions = {
  saveOntoFile: true,
}

// @TODO: extract to a specific config folder like: `gatsby/data-smartskills-test`

// @TODO: review this function as many things in it: 
// - check nonStd ld properties
// - Fix yarrml bug on ~iri
// - change uri id with something like `textUri2camelCase` (can be renamed as `generateSkosIdHashes` ?) 
// - extract Ontology entities
// - save resulting file (should be remove with the new saveFull options)
async function postProc(ld, options) {
  const { saveOntoFile } = Object.assign({}, defaultOptions, options)

  // start : check non-stdld properties
  // @TODO: extract and make more usefull during debug
  //    -> 1: check if the property is already declared in json-ld context
  //    -> 2: check if "literals declarations" in context have a @language tag
  //    -> 3: (more difficult) identify the mapping that generated this => need to implement the check for each mapping
  const nonStd = ld.graph.reduce((acc, entity) => {
    const badKeys = Object.keys(entity).filter(k => k.includes(':'))
    if (badKeys.length) {
      acc.badKeys.push(...badKeys)
      acc.entities.push(entity)
    }
    return acc
  }, { badKeys: [], entities: [] })

  const uniques = new Set(nonStd.badKeys)
  if (uniques.size) {
    console.log(chalk.red('Propriétées non formatées en stdld :'))
    console.log(`
    ==> 1/ verify ~iri in mapping files (data/rml)
    ==> 2/ Update the context in "gatsby/src/stdContext/oriContext.js"
    `)
    console.log('Non std properties:', uniques, '\n')
    console.log(nonStd.length, 'Affected entities:', nonStd.entities)
  }


  // hack for fixing the ~iri yarrrml/rml bug
  ld.graph.forEach(e => {
    if (e.id.includes('~iri')) e.id = e.id.replace('~iri', '')
  })

  for (const entity of ld.graph) {
    const entries = Object.entries(entity)
    for (const entry of entries) {
      let [key, value] = entry

      const isIdOrArray = key === 'id' || Array.isArray(value)
      if (!isIdOrArray) {
        console.log('To check, property not stdld for key', key, 'full entity', entity)
        continue
      }

      const isArrayOfString = typeof value[0] === 'string'
      if (!isArrayOfString) continue // silent, this is langString or Datatype

      // update and change the value in entity
      value = key === 'id' ? textUri2camelCase(value) : value.map(textUri2camelCase)
      entity[key] = value
    }
  }

  // check if entity without type
  const withoutType = ld.graph.filter(e => !e.type)
  if (withoutType.length) {
    console.log(withoutType.length, 'entities without type \n', withoutType)
    throw new Error('See previous logs')
  }

  // @TODO : extract and make it more generic in library, as a "fileSave" step ?
  // extract classes and properties from main graph
  const types = ['rdf:', 'rdfs:', 'skos:']
  const ontologyEntities = extractEntities(types, ld)
  const ontologyGraph = {
    "@context": Object.assign({}, ld['@context']),
    graph: ontologyEntities,
  }

  console.log(chalk.redBright('Filtered output for Oriane : only classes and properties :'))
  const folderPath = `/apps/gatsby/data/oriane-rdf-prod`
  const fileName = `${folderPath}/oriane-X.Y.Z.jsonld`
  if (saveOntoFile) saveJson(ontologyGraph, fileName)


  console.log('Number of "ontology entities" generated :', ontologyGraph.graph.length, 'Number of source entities:', ld.graph.length)
  return ld
}

function textUri2camelCase(textURI) {
  // @TODO: extract this const utilities ?
  const splitAndKeep = (splitChar, toSplit) => {
    const splitted = toSplit.map(t => {
      const tSecond = t.split(splitChar)
      return tSecond.map((s, i) => i + 1 >= tSecond.length ? s : [s, splitChar])
    }).flat(2)

    return splitted
  }

  const cleanAndCamel = (text) => {
    // really specific cases with bad '/' encoding in yarrml (or in source file)
    text = text.replace('et%2Fou', 'et ou')

    // Remove accents and special chars see :
    //   https://stackoverflow.com/a/37511463
    //   https://ricardometring.com/javascript-replace-special-characters
    text = text.normalize('NFKD').replace(/(\p{Diacritic}|[^0-9a-zA-Z\s])/gu, '')
    return camelcase(text)
  }

  if (!textURI.includes('%')) return textURI

  let toSplit = splitAndKeep('#', [textURI])
  toSplit = splitAndKeep('/', toSplit)

  // decode and camelCase the parts with %
  let decoded = toSplit.map(t => t.includes('%') ? cleanAndCamel(decodeURI(t)) : t).join('')

  // @TODO : remove this remplacement after tests okay on visualisation part
  decoded = decoded.replace('#', '/')
  return decoded
}


function extractEntities(types, ld) {
  const extractionArray = []
  for (const e of ld.graph) {
    for (const t of e.type) {
      for (const f of types) {
        if (t.startsWith(f)) extractionArray.push(e) // @TODO: add a continue to the top of "for"
      }
    }
  }
  return extractionArray

}
