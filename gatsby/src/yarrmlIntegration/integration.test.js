import findById from '../jsonldUtils/findById.js'

it.todo('RESTAURE THIS after the "esm war"')
// import yarrrml2rdf from '@datalab-s/rml-js-ontology-elevator'

// docker-compose exec gatsby npm test -- --watch integration.test.js

const getConfMultiSource = (sources) => ({
  sources,
  mappings: '**/rml/yarrrml/hrJson.yaml',
  targetDirectory: ['HR-XML', 'hrjson-test'],
  concatFileName: 'from-jest',
  profile: 'hrrdfProfile',
  setLength: 0,
  rmlSave: false,
  saveRawLd: false,
  stdOut: false,
  saveMapping: false,
  saveIndividuals: false,
  saveFull: false,
  printLogs: false,
})

const getConfOneSource = (fileGlobby) => getConfMultiSource([fileGlobby])

it.todo('Fix the integration of functions. Bug is caused by the java-nodejs-bridge calling of nodejs command. This is due to the "not pure esm" gatsby actual version')
describe.skip('Primitive datatypes related transformations', () => {

  describe('Primitives datatypes equivalents', () => {
    jest.setTimeout(10000)

    let mInclRdf = null
    beforeAll(async () => {
      const sources = [
        // définitions
        '**/HR-XML-test/**/DateType.json',
      ]

      const conf = getConfMultiSource(sources)
      mInclRdf = await yarrrml2rdf(conf)
    })

    it('Transform string primitive', () => {
      const DateType = findById(mInclRdf, 'hrrdf:DateType')
      const target = {
        id: 'hrrdf:DateType',
        type: ['rdfs:Class'],
        comment: ['Date representation according ISO 8601 standard.'],
        label: ['DateType'],
        subClassOf: ['xsd:string'],
        inScheme: ['hrrdf:data']
      }
      expect(DateType).toStrictEqual(target)
    })

  })

})
