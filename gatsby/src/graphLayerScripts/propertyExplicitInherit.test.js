import { readJson } from '@mmorg/fsutils'
import findById from '../jsonldUtils/findById.js'
import propertyExplicitInherit from './propertyExplicitInherit.js'
// npm run test -- --watch propertyExplicitInherit.test

const fixtureFolder = `${__dirname}/__fixtures__`

describe('Test ExplicitInherit layer on Skos Properties', () => {

  let inheritLayer

  const f = `${fixtureFolder}/skos:Ontology-1.2.0-gl:layer=Extract_narrower_prefLabel_exactMatch.jsonld`
  const ld = readJson(f)
  beforeAll(async () => {
    inheritLayer = await propertyExplicitInherit(ld)
  })

  it('Manage heritance on skos:narrower', async () => {
    const target = {
      id: 'skos:narrower',
      subPropertyOf: ['skos:narrowerTransitive', 'skos:semanticRelation'],
    }
    const entityPatch = findById(inheritLayer.ld, target.id)
    expect(entityPatch).toStrictEqual(target)
  })

  it('Manage heritance on skos:prefLabel', async () => {
    const target = {
      id: 'skos:prefLabel',
      subPropertyOf: ['rdfs:label'],
    }
    const entityPatch = findById(inheritLayer.ld, target.id)
    // console.log(entityPatch)
    expect(entityPatch).toStrictEqual(target)
  })

  it('Manage heritance on skos:exactMatch', () => {
    const target = {
      id: 'skos:exactMatch',
      subPropertyOf: ['skos:closeMatch', 'skos:mappingRelation', 'skos:semanticRelation'],
    }

    // console.log(findById(inheritLayer.ld, 'skos:closeMatch')) // parent 1
    // console.log(findById(inheritLayer.ld, 'skos:mappingRelation')) // parent 2
    // console.log(findById(inheritLayer.ld, 'skos:semanticRelation')) // parent 3 with a range and a domain.

    const entityPatch = findById(inheritLayer.ld, target.id)
    // console.log(entityPatch)
    expect(entityPatch).toStrictEqual(target)
  })

})
