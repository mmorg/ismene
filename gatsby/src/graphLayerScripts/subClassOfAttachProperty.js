import filterByType from '../jsonldUtils/filterByType.js'
import hasProperties from '../jsonldUtils/hasProperties.js'

const layerGraph = []

const inheritLayer = {
  name: 'from subClassOf attaching properties',
  layerRealm: ['subClassOfAttachProperty'],
  ld: {
    graph: layerGraph,
  }
}

const defaultOptions = {
  externalGraph: [],
  log: false
}

export default async function subClassOfAttachProperty(graph, options) {

  const { externalGraph } = Object.assign(defaultOptions, options)

  // 1/ Search for Classes with SubClass in source graph
  let classesSource = filterByType(graph, 'rdfs:Class').filter(e => e.subClassOf)
  classesSource = removeDeprecated(classesSource)
  // console.log(classesSource)

  const world = externalGraph.reduce((acc, ld) => {
    // brute force add, do not check for duplicates
    acc.graph.push(...ld.graph)
    return acc
  }, { graph: [...classesSource] })
  const worldMap = getMap([world])

  // build entity & parents' properties structure
  const propertyAttached = []
  for (const cl of classesSource) {
    // recursive search to get all the parent-hood 
    const parents = recurseGetParent(cl.subClassOf, worldMap)

    const properties = parents.map(pe => hasProperties(pe, world)).flat()

    propertyAttached.push({
      entity : cl,
      parentProperties : properties
    })
  }

  // 3/ "domain" the properties to the current class (explicit graph)
  for(const pa of propertyAttached){
    const {entity, parentProperties}= pa 
    parentProperties.forEach( prop => prop.domain.push(entity.id))
  }

  console.log('@TODO: manage the `inverseOf` for herited properties')

  const patches = propertyAttached.reduce((acc,{parentProperties}) => {acc.push(...parentProperties) ; return acc ;}, [] )

  layerGraph.push(...patches)
  return inheritLayer
}

function getMap(lds) {
  const graphMap = new Map()
  for (const l of lds) {
    for (const e of l.graph) {
      if (graphMap.has(e.id)) console.log('Warn: there is an override:', e, 'previous entity', graphMap.get(e.id))
      graphMap.set(e.id, e)
    }
  }
  return graphMap

}

function recurseGetParent(ids, graphMap) {
  const parents = []
  // hard filter on blank nodes
  const _ids = ids.filter(i => !i.startsWith('_:b'))
  for (const i of _ids) {
    const p1 = graphMap.get(i)
    if (!p1) {
      console.log('Warn: this entity is not found in current and externals graphs:', i)
      continue
    }
    parents.push(p1)
    if (p1.subClassOf && p1.subClassOf.length) {
      parents.push(...recurseGetParent(p1.subClassOf, graphMap))
    }
  }

  return parents
}

function removeDeprecated(entities) {
  return entities.reduce((acc, e) => {
    if (!e?.deprecated?.length) acc.push(e)
    return acc
  }, [])
}