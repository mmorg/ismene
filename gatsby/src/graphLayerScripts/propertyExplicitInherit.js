import filterByType from '../jsonldUtils/filterByType.js'
import getPathSteps from '../jsonldUtils/graphUtils/getPathSteps.js'
import emitPatchFromPath from '../jsonldUtils/graphUtils/emitPatchFromPath.js'

const layerGraph = []

const inheritLayer = {
  name: 'from explicitInherit function',
  layerRealm: ['explicitInherit'],
  ld: {
    graph: layerGraph,
  }
}

export default async function propertyExplicitInherit(graph) {

  // 2/ heritance on Properties
  const properties = filterByType(graph, 'rdf:Property')
  const withSubPropertyOf = properties.filter(e => e.subPropertyOf && e.subPropertyOf.length)

  // try to guess the domain from the "subPropertyOf follow"
  const pathToFollow = 'subPropertyOf'
  const hasSubPropertyUnrefMatrix = withSubPropertyOf.map(e => getPathSteps(e, pathToFollow))

  const patches = emitPatchFromPath(withSubPropertyOf, hasSubPropertyUnrefMatrix, pathToFollow)

  layerGraph.push(...patches)
  return inheritLayer
}
