import updateStrategies from './updateStrategies.js';

const getLayer = (graph) => ({
  name: 'from skosMemberOf function',
  layerRealm: ['skosMemberOf'],
  updateStrategy: updateStrategies.add,
  ld: {
    graph,
  }
})

export default function skosMemberOf() {
  const graph = [
    {
      id: 'skos:memberOf',
      type: [
        'rdf:Property',
        'owl:ObjectProperty'
      ],
      domain: [
        'skos:Collection', 'skos:Concept',
      ],
      range: [
        'skos:Collection'
      ],
      label: [
        {
          '@language': 'en',
          '@value': 'MemberOf'
        }
      ],
      definition: [
        {
          '@language': 'en',
          '@value': 'Relates members to collections'
        }
      ],
      // @TODO: add a 'since: [v1.3.0]' ontology property
    }
  ]
  return getLayer(graph)
}