// import { dirname } from 'path'
// import { fileURLToPath } from 'url'
// const __dirname = dirname(fileURLToPath(import.meta.url))
import findById from '../jsonldUtils/findById.js'
import { readJson } from '@mmorg/fsutils'

const deviationLayer = {
  name: 'from addDeviationType function',
  layerRealm: ['deviationType', 'apiGenerator'],
  ld: {
    '@context': '__to_define__',
    graph: [],
  }
}
const entityLayer = {
  name: 'Created Entities for the DeviationType layer',
  layerRealm: ['deviationType', 'gendj:ClassTerms'],
  ld: {
    '@context': '__to_define__',
    graph: [],
  }
}

const deviationGraph = readJson('./data/mmDeviation/mm-deviation-1.0.0.jsonld')

const searchInSourceGraph_Init = (sourceGraph) => {
  return (id) => findById(sourceGraph, id)
}

let searchInSourceGraph = null

// layerGenerator for Deviation=Type : aims to create a Target Class with instances 
// @TODO: pass layerRealm value to deviation
// @TODO: how to pass an entities layer realm ? 
export default function addDeviationType(graph, nsPathIndexOverride = {}) {

  const localDeviationTemplate = JSON.parse(JSON.stringify(findById(deviationGraph, 'mmd:deviationType')))

  // pour chaque skos:Concept
  // add deviation with target of : genTypes
  // with entitiesValues : ['skos:Concept', 'gendj:Job', "gendj:Position","gendj:Sector"]
  // local ns : ["gendj": "https://gen.competencies.be/terms/digitalJobs/",]


  const conf = {
    instancesOfType: ['skos:Concept'],
    deviationTarget: 'gendj:genTermTypes', // @TODO: create a class
    deviationValues: [
      { id: 'skos:Concept', label: 'Concept' },
      { id: 'gendj:Sector', label: 'Famille' },
      { id: 'gendj:Position', label: 'Métier' },
      { id: 'gendj:Job', label: 'Emploi' },
    ],
    localNs: { gendj: 'https://gen.competencies.be/terms/digitalJobs/', }
  }

  // throw new Error('Restart Here With Implementation')

  searchInSourceGraph = searchInSourceGraph_Init(graph)

  // 1/ search instances and Check
  // console.log('@TODO: make it generic for multiples instancesOfType')
  const skosConcept = searchInSourceGraph(conf.instancesOfType[0])
  if(!skosConcept){
    console.warn('!!!! No instance of this type in the graph', conf.instancesOfType[0])
    return []
  }

  // A/ Define target Class for deviation skos:ClassTerm an his "label" property
  const rangeClassTerm = {
    id: conf.deviationTarget,
    type: ['rdfs:Class'],
  }

  // rct = rangeClassTerm
  const rctLabelId = `${conf.deviationTarget}-property-label`
  const rctPropertyLabel = {
    id: rctLabelId,
    type: ['rdf:Property'],
    label: 'label',
    domain: [conf.deviationTarget],
    range: ['xsd:string'],
  }

  const rctSourceId = `${conf.deviationTarget}-property-sourceId`
  const rctPropertySourceId = {
    id: rctSourceId,
    type: ['rdf:Property'],
    label: 'source rdfx id',
    domain: [conf.deviationTarget],
    range: ['xsd:string'],
  }
  // add them to the deviation Layer
  deviationLayer.ld.graph.push(rangeClassTerm, rctPropertyLabel, rctPropertySourceId)

  // B/ override domain & range of the localDeviationTemplate
  localDeviationTemplate.domain = conf.instancesOfType
  localDeviationTemplate.range = [rangeClassTerm.id]
  // add it to the deviation layer
  deviationLayer.ld.graph.push(localDeviationTemplate)

  // C/ define entities for the target Class
  const entities = conf.deviationValues.map( dv => ({
    id: `${dv.id}_deviation`,
    type: [conf.deviationTarget],
    deviationLabel: [dv.label],
    deviationSourceId: [dv.id]
  }))
  // add them to the entity layer
  entityLayer.ld.graph.push(...entities)
  
  // add specific context to entityLayer
  const getAlias = (id) => ({
    '@id':  id,
    '@type': "@id",
    '@container': '@set'
  })
  entityLayer.ld['@context'] = {
    deviationLabel: getAlias(rctLabelId),
    deviationSourceId: getAlias(rctLabelId)
  }

  return [deviationLayer,entityLayer]
}
