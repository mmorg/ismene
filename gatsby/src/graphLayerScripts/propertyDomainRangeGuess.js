// import { dirname } from 'path'
// import { fileURLToPath } from 'url'
// const __dirname = dirname(fileURLToPath(import.meta.url))
import filterByType from '../jsonldUtils/filterByType.js'
import mergeLayers from '../jsonldUtils/graphLayersChef/mergeLayers.js'
import forcePatchOnDirection from './forcePatchOnDirection.js'
import findById from '../jsonldUtils/findById.js'
import loader from '../jsonldUtils/graphUtils/loader.js'

let layerGraph = []

const inheritLayer = {
  name: 'from domainRangeGuess function',
  layerRealm: ['domainRangeGuess'],
  ld: {
    graph: [],
  }
}

const searchInSourceGraph_Init = (sourceGraph) => {
  return (id) => findById(sourceGraph, id)
}

let searchInSourceGraph = null

// layerGenerator for rdf:Property, base it's guess on explicit inheritance of property
export default function propertyDomainRangeGuess(graph, nsPathIndexOverride = {}) {

  searchInSourceGraph = searchInSourceGraph_Init(graph)

  const properties = filterByType(graph, 'rdf:Property')
  const _localProperties = JSON.parse(JSON.stringify(properties))

  // 1/ no Domain
  const domainPatches = createPatchFromHeritance(_localProperties, 'domain', nsPathIndexOverride)

  // 2/ no Range
  const rangePatches = createPatchFromHeritance(_localProperties, 'range', nsPathIndexOverride)

  // do the merge of the patches to get uniques ones
  layerGraph = mergeLayers({ graph: domainPatches }, { graph: rangePatches }).graph

  inheritLayer.ld.graph = layerGraph

  return inheritLayer
}

function createPatchFromHeritance(_localProperties, propertyDirection, nsPathIndexOverride) {
  const noDirection = e => !(e[propertyDirection] && e[propertyDirection].length)
  const getDirection = e => e[propertyDirection]

  const { unref } = loader(nsPathIndexOverride)

  // 1/ get the entity without value for the propertyDirection
  const withoutDirectionValue = _localProperties.filter(noDirection)

  // 2/ check if there is a parent wood, and create patch for the ones with parent
  const {
    patches,
    noHeritance
  } = withoutDirectionValue.reduce((acc, e) => {

    if (!e.subPropertyOf?.length) {
      acc.noHeritance.push(e)
      return acc
    }

    // aggreate the Direction results on this parenthood
    const parentsDirection = e.subPropertyOf.reduce((acc2, p) => {
      const addDirectionsValues = (entity) => {
        const directionValues = getDirection(entity)
        if (directionValues?.length) directionValues.forEach(v => acc2.add(v))
      }
      // 1/ search in current graph
      // @TODO: see to dyamically define nsPathIndexOverride to directly use the search with the "last graph" included
      const parent = searchInSourceGraph(p)
      if (parent) {
        addDirectionsValues(parent)
      }
      if (!parent) {
        // search for description in others ontologies:
        const worldParent = unref(p)
        addDirectionsValues(worldParent)
        return acc2
      }
      return acc2
    }, new Set())

    // console.log('!!!!!!!!!!!!!!', parentsDirection.size)
    if (!parentsDirection.size) {
      acc.noHeritance.push(e)
      return acc
    }

    // finally, create patch for the ones "directions from parenthood" we find
    const patch = { id: e.id }
    patch[propertyDirection] = [...parentsDirection]
    acc.patches.push(patch)

    return acc

  }, { patches: [], noHeritance: [] })


  // ForceEmit on entity still without domain or range
  const manualPatches = forceEmitPatch(noHeritance, propertyDirection)

  // c) finally merge all the layers
  const finalPatches = mergeLayers(
    { graph: patches }, { graph: manualPatches }
  ).graph

  return finalPatches // patches
}


function forceEmitPatch(entities, propertyDirection) {
  const patches = entities.reduce((acc, e) => {
    const mapping = forcePatchOnDirection[propertyDirection] ? forcePatchOnDirection[propertyDirection][e.id] : null
    if (!mapping) {
      console.warn(`
      No manual mapping on direction __${propertyDirection}__ for entity: "${e.id}"
      Add manual mapping into ${__dirname}/forcePatchOnDirection.js`
      )
      return acc
    }

    const p = { id: e.id }
    p[propertyDirection] = mapping

    acc.push(p)
    return acc
  }, [])

  return patches
}
