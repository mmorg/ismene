import updateStrategies from './updateStrategies.js'

const getLayer = (graph) => ({
  name: 'from entityRemover function',
  layerRealm: ['entityRemover'],
  updateStrategy: updateStrategies.remove,
  ld: {
    graph,
  }
})

const defaultOptions = { toRemove: [], log: false }
// this function also remove properties associated to classes
export default function entityRemover(ld, options) {
  const graph = []
  const { toRemove, log } = Object.assign({}, defaultOptions, options)

  const { keepEntity, removeEntity } = ld.graph.reduce((acc, e) => {

    // 1/ is entity a Class to Remove ? 
    const isClassToRemove = toRemove.includes(e?.id)
    if (isClassToRemove) {
      acc.removeEntity.push(e)
      return acc
    }

    // last, we keep the entity 
    acc.keepEntity.push(e)
    return acc

  }, { keepEntity: [], removeEntity: [] })

  // informations
  const remainingClass = keepEntity.map(e => e.id)
  if (log) console.log('Remaining entities after removal:', remainingClass)

  graph.push(...removeEntity)

  return getLayer(graph)
}