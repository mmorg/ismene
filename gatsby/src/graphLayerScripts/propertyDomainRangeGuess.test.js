import { readJson } from '@mmorg/fsutils'
import findById from '../jsonldUtils/findById.js'
import mergeLayers from '../jsonldUtils/graphLayersChef/mergeLayers.js'
import propertyDomainRangeGuess from './propertyDomainRangeGuess.js'
import propertyExplicitInherit from './propertyExplicitInherit.js'

// docker-compose exec gatsby npm run testESM -- --watch propertyDomainRangeGuess.test

const fixtureFolder = `${__dirname}/__fixtures__`

describe('Test Domain & Range Layer Guesser on Skos', () => {

  let layerGraph

  const f = `${fixtureFolder}/skos:Ontology-1.2.0-gl:layer=Extract_narrower_prefLabel_exactMatch.jsonld`
  const ld = readJson(f)
  beforeAll(async () => {
    // 1/ apply explicitInherit before domainRangeGuess
    const inheritLayer = await propertyExplicitInherit(ld)
    const concatGraph = mergeLayers(ld, inheritLayer.ld)
    const nsPathIndexOverride = { skos: concatGraph }
    layerGraph = propertyDomainRangeGuess(concatGraph, nsPathIndexOverride).ld
  })


  it('Force undefined domain to be set by human', async () => {
    const target = {
      id: 'skos:inScheme',
      domain: ['skos:ConceptScheme', 'rdfs:Resource', 'owl:Thing']
    }
    const entityPatch = findById(layerGraph, target.id)
    expect(entityPatch).toStrictEqual(target)
  })

  it('Add good domains for skos:narrower & skos:prefLabel', async () => {
    const targetNarrower = {
      id: 'skos:narrower',
      domain: ['skos:Concept'],
      range: ['skos:Concept'],
    }
    const entityPatchNarrower = findById(layerGraph, targetNarrower.id)
    expect(entityPatchNarrower).toStrictEqual(targetNarrower)

    const targetPrefLabel = {
      id: 'skos:prefLabel',
      domain: ['rdfs:Resource'],
      range: ['rdfs:Literal'],
    }
    const entityPatchPrefLabel = findById(layerGraph, targetPrefLabel.id)
    expect(entityPatchPrefLabel).toStrictEqual(targetPrefLabel)
  })

})
