import updateStrategies from './updateStrategies.js'

const getLayer = (graph) => ({
  name: 'from owl:deprecated remover',
  layerRealm: ['owlDeprecatedRemover'],
  updateStrategy: updateStrategies.remove,
  ld: {
    graph,
  }
})

const defaultOptions = { log: false }
// this function also remove properties associated to classes
export default function owlDeprecatedRemover(ld, options) {
  const graphToRemove = []
  const { log } = Object.assign({}, defaultOptions, options)

  for(const entity of ld.graph){
    if(!entity?.deprecated?.length) continue
    // console.log(entity?.deprecated?.length)
    const dataType = entity.deprecated[0]
    if(dataType['@value']) graphToRemove.push(entity)
  }

  // const { keepEntity, removeEntity } = ld.graph.reduce((acc, e) => {

  //   // 1/ is entity a Class to Remove ? 
  //   const isClassToRemove = toRemove.includes(e?.id)
  //   if (isClassToRemove) {
  //     acc.removeEntity.push(e)
  //     return acc
  //   }

  //   // last, we keep the entity 
  //   acc.keepEntity.push(e)
  //   return acc

  // }, { keepEntity: [], removeEntity: [] })

  // // informations
  // const remainingClass = keepEntity.map(e => e.id)
  // if (log) console.log('Remaining entities after removal:', remainingClass)

  // graphToRemove.push(...removeEntity)

  return getLayer(graphToRemove)
}