import { readJson } from '@mmorg/fsutils'
import findById from '../jsonldUtils/findById.js'
import addDeviationType from './addDeviationType.js'

// docker compose exec gatsby npm run testESM -- --watch addDeviationType.test

const fixtureFolder = `${__dirname}/__fixtures__`

describe('Test deviation on Type application on gendj: case', () => {

  let deviationGraph
  let entityGraph
  const f = `${fixtureFolder}/skos:Ontology-1.2.0-gl:layer=Extract_narrower_prefLabel_exactMatch.jsonld`
  const ld = readJson(f)
  beforeAll(async () => {
    const [deviationLayer, entityLayer] = addDeviationType(ld)
    deviationGraph = deviationLayer.ld
    entityGraph = entityLayer.ld
  })


  it('Add configured deviation', async () => {
    // A/ on deviationGraph
    // console.log(deviationGraph.graph)
    expect(deviationGraph.graph).toMatchSnapshot()
    // 1/ skos:Concept as a deviationType property with range='gendj:genTypes'
    const target = {
      id: 'mmd:deviationType',
      type: ['rdf:Property'],
    }
    const deviationTypeProp = findById(deviationGraph, target.id)
    expect(deviationTypeProp.type).toStrictEqual(target.type)


    // 2/ The class 'gendj:genTermTypes' exist 
    const gttTarget = { id: 'gendj:genTermTypes', type: ['rdfs:Class'] }
    const genTermTypes = findById(deviationGraph, gttTarget.id)
    expect(genTermTypes.type).toStrictEqual(gttTarget.type)

    // B/ on entityGraph 
    // console.log(entityGraph)
    // 1/ There 4 instances of gendj:genTypes
    expect(entityGraph.graph.length).toBe(4)

    // 2/ There is a specific 'context' to alias the property

    expect(entityGraph['@context']).toMatchSnapshot()
  })

})
