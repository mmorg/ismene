import entityRemover from './entityRemover.js'
import updateStrategies from './updateStrategies.js'


// This file export 2 lauers until the rdfx-layer can include rdfx-metadata entities

/**
 * Theses remover & replacer are to fix a "strange notation" in the skos source xml, to define multiple possible range. 
 * This notation lead to a json-ld blank node with a "unionOf" that is not needed
 * Source notation with typo:
```xml 
<rdf:Description rdf:about="#member">
  <!-- S32 -->
  <rdfs:range>
    <owl:Class>
      <owl:unionOf rdf:parseType="Collection">
        <owl:Class rdf:about="#Concept"/>
        <owl:Class rdf:about="#Collection"/>
      </owl:unionOf>
    </owl:Class>
  </rdfs:range>
</rdf:Description>
```
*/
export default function skosCollectionCleaner(ld, options) {
  return [
    skosCollectionRemover(ld, options),
    skosCollectionReplacer(ld, options)
  ]
}

const getLayer_remover = (graph) => ({
  name: 'from skosCollectionCleaner function - remove layer',
  layerRealm: ['collectionCleaner'],
  updateStrategy: updateStrategies.remove,
  ld: {
    graph,
  }
})
function skosCollectionRemover(ld, options) {
  const { ld: { graph } } = entityRemover(ld, options)
  return getLayer_remover(graph)
}


const getLayer_replacer = (graph) => ({
  name: 'from skosCollectionCleaner function - replace layer',
  layerRealm: ['collectionCleaner'],
  updateStrategy: updateStrategies.replace,
  ld: {
    graph,
  }
})
function skosCollectionReplacer() {
  const graph = [
    {
      id: 'skos:member',
      range: [
        'skos:Collection', 'skos:Concept',
      ],
    }
  ]
  return getLayer_replacer(graph)
}