
const genericParenthood = ['rdfs:Resource', 'owl:Thing']
const textTypeRange = ['xsd:string', 'rdf:langString', 'rdf:HTML', 'rdf:XMLLiteral']
const skosNoteTypeDomain = ['skos:Concept', ...genericParenthood]
// The structure of this configuration is :
/**
 * {
 *  propertyDirection : {
 *   entityNSId_1: [forced_patches]
 *   entityNSId_2: [forced_patches]
 *  }
 * }
 */
const forcePatchOnDirection = {

  domain: {
    'skos:inScheme': ['skos:ConceptScheme', ...genericParenthood],

    'skos:notation': ['skos:Concept'],

    // note childwood case
    'skos:note': skosNoteTypeDomain, // @TODO: how to manage "auto-heritance" ? (skos:note is parent of all others skos:~=Note)
    'skos:scopeNote': skosNoteTypeDomain,
    'skos:historyNote': skosNoteTypeDomain,
    'skos:example': skosNoteTypeDomain,
    'skos:editorialNote': skosNoteTypeDomain,
    'skos:definition': skosNoteTypeDomain,
    'skos:changeNote': skosNoteTypeDomain,
  },

  range: {
    'skos:notation': ['rdfs:Datatype'],

    // note childwood case
    'skos:note': textTypeRange, // @TODO: how to manage "auto-heritance" ? (skos:note is parent of all others skos:~=Note)
    'skos:scopeNote': textTypeRange,
    'skos:historyNote': textTypeRange,
    'skos:example': textTypeRange,
    'skos:editorialNote': textTypeRange,
    'skos:definition': textTypeRange,
    'skos:changeNote': textTypeRange,
  },

}

export default forcePatchOnDirection
