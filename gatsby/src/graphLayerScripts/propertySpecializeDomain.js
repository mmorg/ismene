import filterByDomain from '../jsonldUtils/filterByDomain.js'
import filterByType from '../jsonldUtils/filterByType.js'
import updateStrategies from './updateStrategies.js'

const getLayer = (graph) => ({
  name: 'from skosMemberOf function',
  layerRealm: ['skosMemberOf'],
  updateStrategy: updateStrategies.add,
  ld: {
    graph,
  }
})

// add explicit `domain` to generic `rdfs:Resource` Items
export default function propertySpecializeDomain(ontology){

  // 1/ search for all properties with domain 'rdfs:Resource` 
  const property = filterByDomain('rdfs:Resource', ontology)

  // 2/ get ontology's Classes
  const classId = filterByType(ontology, 'rdfs:Class')
    .map( e => e.id)  
    .filter( id => id !== '_:b0_n0') // special 'typo' case in source to clean upstream
  
  // 3/ Create the new graph with the patches 
  const graph = property.map( e => {
    const patch = {
      id : e.id, 
      domain: classId
    }
    return patch
  })
  
  return getLayer(graph)
}