import { Gitlab } from '@gitbeaker/node'
import { readJson, saveJson } from '@mmorg/fsutils'
import gitlabConfig from '../configs/gitlabConfig.js'
import pThrottle from 'p-throttle'
import pAll from 'p-all'
import chalk from 'chalk'


export default async function pushId2Labels(labelsToCreate, labelStoreFile) {
  const { token, projectId } = gitlabConfig
  const api = new Gitlab({
    token,
  })

  // 1/ get a  default color for the issues
  // @TODO : get a color scheme for variations. See comments after the function
  const cs = '#f47421'

  // 2/ load the already created labels :
  const exists = new Set(readJson(labelStoreFile))

  const alreadyExistingLabels = []
  const labelsToPush = labelsToCreate.reduce((acc, v) => {
    exists.has(v) ? alreadyExistingLabels.push(v) : acc.push(v)
    return acc
  }, [])

  console.log('Number of already existing labels:', alreadyExistingLabels.length)


  // limit seems to be 2,000 requests per minute
  //   https://docs.gitlab.com/ee/user/gitlab_com/#gitlabcom-specific-rate-limits
  const throttle = pThrottle({
    limit: 2000,
    interval: 60000
  })

  const createApi = throttle(async labelId => {
    try {
      return await api.Labels.create(projectId, labelId, cs)
    } catch (e) {
      console.log(chalk.red('!!!!! this label is not filtered by the store, but throws this error on api:'),e.description, ':', labelId)
      return { error: e }
    }
  })

  console.log('==> Start processing add labels to API')
  const promises = labelsToPush.map(l => () => createApi(l))
  let queries = await pAll(promises) //, { stopOnError: false })

  // get "not on error" and add the names to the exist set
  const notOnError = queries.filter(n => !n.error)
  notOnError.forEach(n => exists.add(n.name))

  console.log(notOnError.length, chalk.green('labels added'))
  return exists
}
