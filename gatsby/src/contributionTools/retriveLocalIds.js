import { readJson } from '@mmorg/fsutils'
import chalk from 'chalk'
import { globby } from 'globby'


const cleanerBag = {
  unprefixedBlankNodes: [],
  excludedPrefix: [],
  doNotStartWithPrefix: [],
  startsWithHttp: [],
}

export default async function retriveLocalIds(globbyInstruction, prefixExclude) {

  const localIds = new Set()
  const files = await globby([globbyInstruction])
  // console.log(files)
  const graphs = files.map(f => readJson(f))
  for (const g of graphs) {
    if (!g.graph) {
      console.log(chalk.red('This graph is not in the stdld format')) //, 'do it have a .results', g.results)//, g)
      console.log('Source file keys:', Object.keys(g))
      console.log('Extract of first entities:')
      if (g['@graph']) {
        console.log(g['@graph'].slice(0, 2))
        continue
      }
      continue
    }

    for (const e of g.graph) {
      if (!isIdToKeep(e.id, prefixExclude)) continue
      localIds.add(e.id)
    }
  }

  // console.log(localIds, localIds.size)
  const cleanerBagSize = Object.fromEntries(Object.entries(cleanerBag).map(([key, val]) => [key, val.length]))
  console.log('\nCount of Entities retained from label creation by the cleaner:', cleanerBagSize) // cleanerBag
  return localIds
}

function isIdToKeep(id, prefixExclude) {
  // 1/ get prefix / value
  const [prefix, value] = id.split(':')
  if (!prefix || !value) {
    cleanerBag.doNotStartWithPrefix.push(id)
    return false
  }

  if (prefixExclude.includes(prefix)) {
    cleanerBag.excludedPrefix.push(id)
    return false
  }

  if (prefix.startsWith('http')) {
    cleanerBag.startsWithHttp.push(id)
    return false
  }

  if (prefix === '_') {
    cleanerBag.unprefixedBlankNodes.push(id)
    return false
  }

  return true

}
