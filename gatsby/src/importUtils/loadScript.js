import chalk from 'chalk'
import fileExists from '../fsUtils/fileExists.js'

export default async function loadScript(paths) {
  const checkAndImport = async path => {
    if (!await fileExists(path)) {
      console.log(chalk.red(`This js file don't exist:`),path)
      throw new Error()
    }

    let fn = []
    try{
      fn = await import(path)
    }catch (e){
      console.log(chalk.bgRed('There is an error in the loaded file:'), path)
      throw e
    }

    return fn
  }

  const promises = paths.map(checkAndImport)
  return Promise.all(promises)
}
