
export default {
  id: '@id',
  graph: {
    '@id': '@graph',
    '@container': '@set'
  },
  type: {
    '@id': '@type',
    '@container': '@set'
  },

  // generic NS
  mnx: 'http://ns.mnemotix.com/ontologies/2019/8/generic-model/',


  hasAccessPrivilege: {
    '@id': 'mnx:hasAccessPrivilege',
    '@type': '@id',
    '@container': '@set'
  },

  hasScope: {
    '@id': 'mnx:hasScope',
    '@type': '@id',
    '@container': '@set'
  },


}
