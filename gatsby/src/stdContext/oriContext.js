import nodefrContext from './nodefrContext.js'
import openBagesContext from './openBagesContext.js'
import rdfsContext from './rdfsContext.js'
import schemaorgContext from './schemaorgContext.js'
import skosContext from './skosContext.js'


const oriLocalContext = {

  // NS
  ori : 'https://ori.competencies.be/ori/',
  // Aliases
  experienceStructure: {
    '@id': 'ori:experienceStructure',
    '@type': '@id',
    '@container': '@set'
  },

  experienceType: {
    '@id': 'ori:experienceType',
    '@type': '@id',
    '@container': '@set'
  },

  public: {
    '@id': 'ori:public',
    '@type': '@id',
    '@container': '@set'
  },

  role: {
    '@id': 'ori:role',
    '@type': '@id',
    '@container': '@set'
  },

  tache: {
    '@id': 'ori:tache',
    '@type': '@id',
    '@container': '@set'
  },

  hobby: {
    '@id': 'ori:hobby',
    '@type': '@id',
    '@container': '@set'
  },

  bloom: {
    '@id': 'ori:bloom',
    '@type': '@id',
    '@container': '@set'
  },



}


// @TODO : extract to a more generic function,
//       : but be careful, this is "raw script" : the last win
const oriContext = Object.assign(
  {},
  rdfsContext, schemaorgContext, skosContext,
  openBagesContext,
  nodefrContext,
  oriLocalContext)

export default oriContext
