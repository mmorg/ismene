export default {
  id: '@id',
  graph: {
    '@id': '@graph',
    '@container': '@set'
  },
  type: {
    '@id': '@type',
    '@container': '@set'
  },

  // generic NS
  skos: 'http://www.w3.org/2004/02/skos/core#',

  // specific NS
  // @TODO: remove it to specific stdContext
  gendj: 'https://gen.competencies.be/terms/digitalJobs/',

  // skos related alias
  inScheme: {
    '@id': 'skos:inScheme',
    '@type': '@id',
    '@container': '@set'
  },

  prefLabel: {
    '@id': 'skos:prefLabel',
    '@container': '@set'
  },

  altLabel: {
    '@id': 'skos:altLabel',
    '@container': '@set'
  },

  narrower: {
    '@id': 'skos:narrower',
    '@type': '@id',
    '@container': '@set'
  },

  broader: {
    '@id': 'skos:broader',
    '@type': '@id',
    '@container': '@set'
  },

  member: {
    '@id': 'skos:member',
    '@type': '@id',
    '@container': '@set'
  },

  // literals :
  definition: {
    '@id': 'skos:definition',
    '@container': '@set'
  },

  scopeNote: {
    '@id': 'skos:scopeNote',
    '@container': '@set'
  },

  example: {
    '@id': 'skos:example',
    '@container': '@set'
  },

  historyNote: {
    '@id': 'skos:historyNote',
    '@container': '@set'
  },


}
