
export default {
  id: '@id',
  graph: {
    '@id': '@graph',
    '@container': '@set'
  },
  type: {
    '@id': '@type',
    '@container': '@set'
  },

  // generic NS
  owl: 'http://www.w3.org/2002/07/owl#',

  
  // literals

  // datatypes
  deprecated: {
    '@id': 'owl:deprecated',
    '@container': '@set'
  },
  
  // references


  // @TODO: sort all the following to the categories (literals, datatypes, references)
  inverseOf: {
    '@id': 'owl:inverseOf',
    '@type': '@id',
    '@container': '@set'
  },

  disjointWith: {
    '@id': 'owl:disjointWith',
    '@type': '@id',
    '@container': '@set'
  },

  propertyDisjointWith: {
    '@id': 'owl:propertyDisjointWith',
    '@type': '@id',
    '@container': '@set'
  },

  unionOf: {
    '@id': 'owl:unionOf',
    '@type': '@id',
    '@container': '@set'
  },

  // is this one of the rare exception of @container:@set ?
  versionInfo: {
    '@id': 'owl:versionInfo',
    '@container': '@set'
  },

  priorVersion: {
    '@id': 'owl:priorVersion',
    '@container': '@set'
  },


  // @TODO: see how to manage theses cases:
  imports: {
    '@id': 'owl:imports',
    // '@type': '@id',
    '@container': '@set'
  },

  versionIRI: {
    '@id': 'owl:versionIRI',
    // '@type': '@id',
    '@container': '@set'
  },

  maxCardinality: {
    '@id': 'owl:maxCardinality',
    '@container': '@set'
  },




}
