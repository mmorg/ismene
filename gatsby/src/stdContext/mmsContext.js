// @TODO: import RDFS context and add only specifics (like in oriContext)
export default {
  id: '@id',
  graph: {
    '@id': '@graph',
    '@container': '@set'
  },
  type: {
    '@id': '@type',
    '@container': '@set'
  },

  // generic NS
  mms: 'https://ismene.competencies.be/mms/',

  // references
  isInCollectionOrScheme: {
    '@id': 'mms:isInCollectionOrScheme',
    '@type': '@id',
    '@container': '@set'
  },


}
