
export default {
  id: '@id',
  graph: {
    '@id': '@graph',
    '@container': '@set'
  },
  type: {
    '@id': '@type',
    '@container': '@set'
  },

  void: 'http://rdfs.org/ns/void#',

  
  // literals
  
  // datatypes
  

  // references
  vocabulary: {
    '@id': 'void:vocabulary',
    '@container': '@set',
    '@type': '@id',
  },

  

}
