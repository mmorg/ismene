
export default {
  id: '@id',
  graph: {
    '@id': '@graph',
    '@container': '@set'
  },
  type: {
    '@id': '@type',
    '@container': '@set'
  },

  vc: 'https://www.w3.org/2018/credentials#',

  
  // literals
  
  // datatypes
  

  // references
  

  

}
