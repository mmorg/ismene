
export default {
  id: '@id',
  graph: {
    '@id': '@graph',
    '@container': '@set'
  },
  type: {
    '@id': '@type',
    '@container': '@set'
  },

  // generic NS
  om: 'https://ontomodel.competencies.be/ontomodel/',

  index: {
    '@id': 'om:index',
    '@type': '@id',
    '@container': '@set'
  },

  records: {
    '@id': 'om:records',
    '@type': '@id',
    '@container': '@set'
  },


  // @TODO: move it to set
  icon: {
    '@id': 'om:icon',
    // '@container': '@set'
  },


}
