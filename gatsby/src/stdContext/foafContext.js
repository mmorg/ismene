// @TODO: import RDFS context and add only specifics (like in oriContext)
export default {
  id: '@id',
  graph: {
    '@id': '@graph',
    '@container': '@set'
  },
  type: {
    '@id': '@type',
    '@container': '@set'
  },

  // generic NS
  foaf: 'http://xmlns.com/foaf/0.1/',


  // domain: {
  //   '@id': 'rdfs:domain',
  //   '@type': '@id',
  //   '@container': '@set'
  // },

  // shacl related aliases
  // maxCount: {
  //   '@id': 'sh:maxCount',
  //   '@type': 'xsd:integer',
  // },

  // literals
  mbox: {
    '@id': 'foaf:mbox',
    '@container': '@set'
  },


}
