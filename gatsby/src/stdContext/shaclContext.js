
export default {
  id: '@id',
  graph: {
    '@id': '@graph',
    '@container': '@set'
  },
  type: {
    '@id': '@type',
    '@container': '@set'
  },

  // NS
  sh: 'http://www.w3.org/ns/shacl#',

  // references
  nodeKind: {
    '@id': 'sh:nodeKind',
    '@type': '@id',
    '@container': '@set'
  },
  path: {
    '@id': 'sh:path',
    '@type': '@id',
    '@container': '@set'
  },
  parameter: {
    '@id': 'sh:parameter',
    '@type': '@id',
    '@container': '@set'
  },
  datatype: {
    '@id': 'sh:datatype',
    '@type': '@id',
    '@container': '@set'
  },
  declare:{ // @TODO : this emit blank node.
    '@id': 'sh:declare',
    '@type': '@id',
    '@container': '@set'
  },
  propertyValidator:{
    '@id': 'sh:propertyValidator',
    '@type': '@id',
    '@container': '@set'
  },
  jsLibrary:{
    '@id': 'sh:jsLibrary',
    '@type': '@id',
    '@container': '@set'
  },
  property:{
    '@id': 'sh:property',
    '@type': '@id',
    '@container': '@set'
  },
  targetClass:{
    '@id': 'sh:targetClass',
    '@type': '@id',
    '@container': '@set'
  },



  suggestedShapesGraph:{
    '@id': 'sh:suggestedShapesGraph',
    // '@type': '@id', // @TODO should be activated ?
    '@container': '@set'
  },


  // literals
  jsFunctionName: {
    '@id': 'sh:jsFunctionName',
    '@container': '@set'
  },
  message: {
    '@id': 'sh:message',
    '@container': '@set'
  },
  sh_description: {
    '@id': 'sh:description',
    '@container': '@set'
  },
  sh_name: {
    '@id': 'sh:name',
    '@container': '@set'
  },



  // typed literals
  optional: {
    '@id': 'sh:optional',
    // @TODO: do we use datatype switch in stdld ?
    // '@type': 'xsd:boolean',
    '@container': '@set'
  },

  namespace: {
    '@id': 'sh:namespace',
    // @TODO: do we use datatype switch in stdld ?
    // '@type': 'xsd:anyURI',
    '@container': '@set'
  },

  jsLibraryURL: {
    '@id': 'sh:jsLibraryURL',
    // @TODO: do we use datatype switch in stdld ?
    // '@type': 'xsd:anyURI',
    '@container': '@set'
  },




  // datatypes


}
