export default {
  id: '@id',
  graph: {
    '@id': '@graph',
    '@container': '@set'
  },
  type: {
    '@id': '@type',
    '@container': '@set'
  },

  // generic NS
  rdf: 'http://www.w3.org/1999/02/22-rdf-syntax-ns#',

  first: {
    '@id': 'rdf:first',
    '@type': '@id',
    '@container': '@set'
  },

  // @TODO: for cube-0.2.0.jsonld => id = "n3-7" there is a problem
  // others rdf:rest properties a
  rest: {
    '@id': 'rdf:rest',
    '@type': '@id', // see construction of
    '@container': '@set'
  },
}
