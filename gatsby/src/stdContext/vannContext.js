
export default {
  id: '@id',
  graph: {
    '@id': '@graph',
    '@container': '@set'
  },
  type: {
    '@id': '@type',
    '@container': '@set'
  },

  vann: 'http://purl.org/vocab/vann/',

  
  // literals
  preferredNamespacePrefix: {
    '@id': 'vann:preferredNamespacePrefix',
    '@container': '@set',
  },

  preferredNamespaceUri: {
    '@id': 'vann:preferredNamespaceUri',
    '@container': '@set',
  },
  
  // datatypes
  

  // references
  
  

}
