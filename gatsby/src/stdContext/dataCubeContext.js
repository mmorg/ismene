// @TODO: import RDFS context and add only specifics (like in oriContext)
export default {
  id: '@id',
  graph: {
    '@id': '@graph',
    '@container': '@set'
  },
  type: {
    '@id': '@type',
    '@container': '@set'
  },

  // generic NS
  qb: 'http://purl.org/linked-data/cube#',


  // domain: {
  //   '@id': 'rdfs:domain',
  //   '@type': '@id',
  //   '@container': '@set'
  // },

  // shacl related aliases
  // maxCount: {
  //   '@id': 'sh:maxCount',
  //   '@type': 'xsd:integer',
  // },

  // // special cases
  // title: {
  //   '@id': 'dct:title',
  //   '@container': '@set'
  // },


}
