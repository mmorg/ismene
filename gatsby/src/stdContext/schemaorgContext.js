
export default {
  id: '@id',
  graph: {
    '@id': '@graph',
    '@container': '@set'
  },
  type: {
    '@id': '@type',
    '@container': '@set'
  },

  // NS
  sch: 'http://schema.org/', // @TODO : pass to https when api-plateform schema generator is ready

  // literals
  address: {
    '@id': 'sch:address',
    '@container': '@set'
  },

  stardDate: {
    '@id': 'sch:stardDate',
    '@container': '@set'
  },

  author: {
    '@id': 'sch:author',
    '@container': '@set'
  },

  image: {
    '@id': 'sch:image',
    '@container': '@set'
  },

  provider: {
    '@id': 'sch:provider',
    '@container': '@set'
  },

  publisher: {
    '@id': 'sch:publisher',
    '@container': '@set'
  },

  url: {
    '@id': 'sch:url',
    '@container': '@set'
  },

  name: {
    '@id': 'sch:name',
    '@container': '@set'
  },

  // ref
  logo: {
    '@id': 'sch:logo',
    '@container': '@set'
  },

  // references
  identifier: {
    '@id': 'sch:identifier',
    '@type': '@id',
    '@container': '@set'
  },

  sch_contributor: {
    '@id': 'sch:contributor',
    '@type': '@id',
    '@container': '@set'
  },

  description: {
    '@id': 'sch:description',
    '@container': '@set'
  },

  


}
