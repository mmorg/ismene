
export default {
  id: '@id',
  graph: {
    '@id': '@graph',
    '@container': '@set'
  },
  type: {
    '@id': '@type',
    '@container': '@set'
  },

  dct: 'http://purl.org/dc/terms/',

  // references

  // literals
  contributor: {
    '@id': 'dct:contributor',
    '@container': '@set'
  },

  creator: {
    '@id': 'dct:creator',
    '@container': '@set'
  },

  abstract: {
    '@id': 'dct:abstract',
    '@container': '@set'
  },

  title_dct: {
    '@id': 'dct:title',
    '@container': '@set'
  },

  // datatypes
  created: {
    '@id': 'dct:created',
    '@container': '@set'
  },

  modified: {
    '@id': 'dct:modified',
    '@container': '@set'
  },

  issued: {
    '@id': 'dct:issued',
    '@container': '@set'
  },

  // reference
  license:{
    '@id': 'dct:license',
    '@container': '@set'
  },


}
