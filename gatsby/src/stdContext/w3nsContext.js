
export default {
  id: '@id',
  graph: {
    '@id': '@graph',
    '@container': '@set'
  },
  type: {
    '@id': '@type',
    '@container': '@set'
  },

  w3ns: 'http://www.w3.org/ns/org#',

  
  // literals
  
  // datatypes
  

  // references
  

  

}
