console.log('@TODO: update with new NS file & import like in oriContext')
// @TODO: séparer le contexte pour afficher l'ontologie (Rdfs + Skos) du contexte d'utilisation en tant que stdLD : celui-ci
export default {
    id: '@id',
    graph: {
        '@id': '@graph',
        '@container': '@set'
    },
    type: {
        '@id': '@type',
        '@container': '@set'
    },

    // generic NS
    rdf: 'http://www.w3.org/1999/02/22-rdf-syntax-ns#',
    rdfs: 'http://www.w3.org/2000/01/rdf-schema#',
    xsd: 'http://www.w3.org/2001/XMLSchema#',
    sh: 'https://www.w3.org/ns/shacl#',
    dct: 'http://purl.org/dc/terms/',
    vs: 'https://www.w3.org/2003/06/sw-vocab-status/ns/',
    owl: 'http://www.w3.org/2002/07/owl#',
    sch : 'http://schema.org/', // @TODO : pass to https when api-plateform schema generator is ready

    // specific NS
    nfr: 'https://nodefr.competencies.be/nfr/',

    aPourNiveau: {
      '@id': 'nfr:aPourNiveau',
      '@container': '@set'
    },

    chargeDeTravail: {
      '@id': 'nfr:chargeDeTravail',
      '@container': '@set'
    },

    description: {
      '@id': 'nfr:description',
      '@container': '@set'
    },

    devise: {
      '@id': 'nfr:devise',
      '@container': '@set'
    },

    montant: {
      '@id': 'nfr:montant',
      '@container': '@set'
    },

    typeDActivite: {
      '@id': 'nfr:typeDActivite',
      '@container': '@set'
    },

    dateDeCreation:{
      '@id': 'nfr:dateDeCreation',
      '@container': '@set'
    }



}
