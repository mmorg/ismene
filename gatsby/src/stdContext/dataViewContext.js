
export default {
  id: '@id',
  graph: {
    '@id': '@graph',
    '@container': '@set'
  },
  type: {
    '@id': '@type',
    '@container': '@set'
  },

  // generic NS
  dv: 'http://www.w3.org/2003/g/data-view#',

  // @TODO: see how to manage theses cases:
  namespaceTransformation: {
    '@id': 'dv:namespaceTransformation',
    // '@type': '@id',
    '@container': '@set'
  },



}
