
export default {
  id: '@id',
  graph: {
    '@id': '@graph',
    '@container': '@set'
  },
  type: {
    '@id': '@type',
    '@container': '@set'
  },

  vs : 'http://www.w3.org/2003/06/sw-vocab-status/ns#',

  
  // literals
  term_status: {
    '@id': 'vs:term_status',
    '@container': '@set',
  }
  // datatypes
  

  // references
  

  

}
