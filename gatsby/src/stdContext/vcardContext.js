
export default {
  id: '@id',
  graph: {
    '@id': '@graph',
    '@container': '@set'
  },
  type: {
    '@id': '@type',
    '@container': '@set'
  },

  vcard: 'http://www.w3.org/2006/vcard/ns#',

  
  // literals
  
  // datatypes
  

  // references
  

  

}
