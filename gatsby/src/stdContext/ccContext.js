
export default {
  id: '@id',
  graph: {
    '@id': '@graph',
    '@container': '@set'
  },
  type: {
    '@id': '@type',
    '@container': '@set'
  },

  cc: 'http://creativecommons.org/ns#',

  
  // literals
  
  
  // datatypes
  

  // references
  license: {
    '@id': 'cc:license',
    '@container': '@set',
    '@type': '@id',
  },
  

}
