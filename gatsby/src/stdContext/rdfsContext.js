
export default {
  id: '@id',
  graph: {
    '@id': '@graph',
    '@container': '@set'
  },
  type: {
    '@id': '@type',
    '@container': '@set'
  },

  // NS
  rdfs: 'http://www.w3.org/2000/01/rdf-schema#',

  // rdfs related aliases
  domain: {
    '@id': 'rdfs:domain',
    '@type': '@id',
    '@container': '@set'
  },

  range: {
    '@id': 'rdfs:range',
    '@type': '@id',
    '@container': '@set'
  },

  label: {
    '@id': 'rdfs:label',
    '@container': '@set'
  },

  comment: {
    '@id': 'rdfs:comment',
    '@container': '@set'
  },

  subClassOf: {
    '@id': 'rdfs:subClassOf',
    '@type': '@id',
    '@container': '@set'
  },

  subPropertyOf: {
    '@id': 'rdfs:subPropertyOf',
    '@type': '@id',
    '@container': '@set'
  },

  isDefinedBy: {
    '@id': 'rdfs:isDefinedBy',
    '@container': '@set'
  },

  seeAlso: {
    '@id': 'rdfs:seeAlso',
    '@container': '@set'
  },

}
