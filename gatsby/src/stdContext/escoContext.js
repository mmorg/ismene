
export default {
  id: '@id',
  graph: {
    '@id': '@graph',
    '@container': '@set'
  },
  type: {
    '@id': '@type',
    '@container': '@set'
  },

  esco : 'http://data.europa.eu/esco/model#',

  
  // literals
  
  // datatypes
  

  // references
  

  

}
