// @TODO: import RDFS context and add only specifics (like in oriContext)
export default {
  id: '@id',
  graph: {
    '@id': '@graph',
    '@container': '@set'
  },
  type: {
    '@id': '@type',
    '@container': '@set'
  },

  // generic NS
  rdf: 'http://www.w3.org/1999/02/22-rdf-syntax-ns#',
  rdfs: 'http://www.w3.org/2000/01/rdf-schema#',
  xsd: 'http://www.w3.org/2001/XMLSchema#',
  sh: 'https://www.w3.org/ns/shacl#',
  skos: 'http://www.w3.org/2004/02/skos/core#',
  dct: 'http://purl.org/dc/terms/',
  // specific NS
  hrrdf: 'https://hrrdf.competencies.be/model#',
  'hrrdf-terms': 'https://hrrdf.competencies.be/terms/',
  cbe_sk: 'https://schemas.competencies.be/skos-extensions/',
  // Specific to Syntalent referentiel import
  synRef: 'https://datalab-s.competencies.be/referentiels/',

  // rdfs related aliases
  domain: {
    '@id': 'rdfs:domain',
    '@type': '@id',
    '@container': '@set'
  },

  range: {
    '@id': 'rdfs:range',
    '@type': '@id',
    '@container': '@set'
  },

  label: {
    '@id': 'rdfs:label',
    '@language': 'en',
    '@container': '@set'
  },

  comment: {
    '@id': 'rdfs:comment',
    '@language': 'en',
    '@container': '@set'
  },

  subClassOf: {
    '@id': 'rdfs:subClassOf',
    '@type': '@id',
    '@container': '@set'
  },

  // shacl related aliases
  maxCount: {
    '@id': 'sh:maxCount',
    '@type': 'xsd:integer',
  },

  // xsd:element/@minOccurs
  minCount: {
    '@id': 'sh:minCount',
    '@type': 'xsd:integer',
  },

  path: {
    '@id': 'sh:path',
    '@type': '@id',
    '@container': '@set'
  },

  // skos related alias
  inScheme: {
    '@id': 'skos:inScheme',
    '@type': '@id',
    '@container': '@set'
  },

  prefLabel: {
    '@id': 'skos:prefLabel',
    '@language': 'en',
    '@container': '@set'
  },

  altLabel: {
    '@id': 'skos:altLabel',
    '@language': 'en',
    '@container': '@set'
  },

  // dc terms related aliases
  title: {
    '@id': 'dct:title',
    '@container': '@set'
  },

  description: {
    '@id': 'dct:description',
    '@container': '@set'
  },

}
