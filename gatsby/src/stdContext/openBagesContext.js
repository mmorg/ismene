export default {
  id: '@id',
  graph: {
    '@id': '@graph',
    '@container': '@set'
  },
  type: {
    '@id': '@type',
    '@container': '@set'
  },

  // ontology NS
  openBadge: 'https://w3id.org/openbadges#',

  // references
  badge: {
    '@id': 'openBadge:badge',
    '@type': '@id',
    '@container': '@set'
  },

  issuer: {
    '@id': 'openBadge:issuer',
    '@type': '@id',
    '@container': '@set'
  },


  // literals
  issuedOn: {
    '@id': 'openBadge:issuedOn',
    '@container': '@set'
  },

  recipient: {
    '@id': 'openBadge:recipient',
    '@container': '@set'
  },

  // ==> !!!! same names in 2 ontologies case
  openBadge_image: {
    '@id': 'openBadge:image',
    '@container': '@set'
  },

  openBadge_name: {
    '@id': 'openBadge:name',
    '@container': '@set'
  },

  openBadge_url: {
    '@id': 'openBadge:url',
    '@container': '@set'
  },


  // end of same names case
  criteria: {
    '@id': 'openBadge:criteria',
    '@container': '@set'
  },


}
