
export default {
  id: '@id',
  graph: {
    '@id': '@graph',
    '@container': '@set'
  },
  type: {
    '@id': '@type',
    '@container': '@set'
  },

  voaf: 'http://purl.org/vocommons/voaf#',

  
  // literals
  
  // datatypes
  

  // references
  

  

}
