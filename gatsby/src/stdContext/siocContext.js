
export default {
  id: '@id',
  graph: {
    '@id': '@graph',
    '@container': '@set'
  },
  type: {
    '@id': '@type',
    '@container': '@set'
  },

  // generic NS
  sioc: 'http://rdfs.org/sioc/ns#',

}
