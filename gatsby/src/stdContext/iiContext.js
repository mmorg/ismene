
export default {
  id: '@id',
  graph: {
    '@id': '@graph',
    '@container': '@set'
  },
  type: {
    '@id': '@type',
    '@container': '@set'
  },

  // NS
  ii: 'https://mm.competencies.be/indexInstruction/',

  instruction: {
    '@id': 'ii:instruction',
    '@type': '@id',
    '@container': '@set'
  },

}
