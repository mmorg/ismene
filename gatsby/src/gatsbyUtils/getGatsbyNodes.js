import rdfSourcesConfig from '../configs/rdfSourcesConfig.js'

export default function getGatsbyNodes(nodeName, ldDoc) {

  const conf = rdfSourcesConfig.find(c => nodeName.startsWith(c.key))
  if (!conf) {
    console.log('____ No configuration file found for this node:', nodeName)
    return
  }

  // console.log('@TODO : a function that check the structure of the ld to get all properties as stdLd: check : gatsby/src/rml-profiles/orianeProdProfile.js')
  return conf.fn(nodeName, ldDoc, conf)
}


