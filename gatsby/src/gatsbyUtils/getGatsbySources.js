import rdfSourcesConfig from '../configs/rdfSourcesConfig.js'

export default function getGatsbySources() {
  return rdfSourcesConfig.map(sourceConf => {
    const { path, ignore } = sourceConf
    if (!path || path === '') {
      throw new Error('path parameter is required from config')
    }
    return {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `jsonLD`,
        path,
        ignore,
      },
    }
  })
}
