import chalk from 'chalk'
import yargs from 'yargs'
import { hideBin } from 'yargs/helpers'
import turtle2jsonld from '../jsonldUtils/turtle2jsonld.js'
import * as nodePath from 'path'
import { saveJson } from '@mmorg/fsutils'
import toJsonLD from '../jsonldUtils/xml2jsonLd/toJsonLD.js'
import getStoreFromFile from '../jsonldUtils/xml2jsonLd/getStoreFromFile.js'
import toStdLD from '../jsonldUtils/stdldUtils/toStdLD.js'
import filterByType from '../jsonldUtils/filterByType.js'
import multiOntologiesContext from '../configs/multiOntologiesContext.js'
import contextShaper from '../jsonldUtils/contextUtil/contextShaper.js'

// Multiple commands to transform to json-ld : see bellow :
const argv = yargs()
  // docker-compose exec gatsby node src/bin/ttl2ld ttld -f 'data/rdf/22-rdf-syntax-ns.ttl'
  .command('ttld', 'Transform ttl ontology model to a jsonld one',
    {
      sourceFile: {
        description: 'Relative path from working directory source ttl file to transform',
        alias: 'f',
        type: 'string',
        demandOption: true,
      },
      // @TODO: make this script path relative
      turtle2ldScript: {
        description: 'Hook for loading the LD: Run default function from the script in folder `src/renovationScripts` to provide the LD to process. SourceFile param will fix the name of resulting LD.',
        alias: 't',
        type: 'string',
      },
      renovationScript: {
        description: 'Hook before save file: Run default function of the relative path script.',
        alias: 'r',
        type: 'string',
      },

      contextLD : {
        description: 'The context to load for the processing',
        alias: 'c',
        type: 'string',
      }

    },
    // handler
    async (argv) => {
      const { sourceFile, renovationScript, turtle2ldScript, contextLD } = argv
      const path = `${process.cwd()}/${sourceFile}`

      let contextLocal = multiOntologiesContext
      if(contextLD){
        const contextPath = `${process.cwd()}/${contextLD}`
        const contextScript = (await import(contextPath)).default
        contextLocal = await contextScript()
      }

      let ldDoc = null
      if (turtle2ldScript) {
        const scriptsFolder = `${process.cwd()}/src/renovationScripts`
        const ldScript = (await import(`${scriptsFolder}/${turtle2ldScript}`)).default
        ldDoc = await ldScript(path)
      } else {
        ldDoc = await turtle2jsonld(path)
        ldDoc = await toStdLD(ldDoc, contextLocal)
      }


      // Start : renovation of schema
      renovateOntology(ldDoc) // no post std, so be sure to have previous context
      changeOntologyId(ldDoc)
      transformToLangString(
        ldDoc,
        ['definition', 'description', 'title', 'comment', 'label']
        , 'en')

      addRdfTypesToOwl(ldDoc)
      // end renovation of schema

      if (renovationScript) {
        const renovationFn = (await import(`${process.cwd()}/${renovationScript}`)).default
        renovationFn(ldDoc)
      }

      // do a ligth context from the data
      contextShaper(ldDoc)

      // define the target
      const baseName = nodePath.basename(path, '.ttl')
      const dir_name = nodePath.dirname(path)
      const targetFile = `${dir_name}/${baseName}.jsonld`
      saveJson(ldDoc, targetFile)

    }
  )

  // docker-compose exec gatsby node src/bin/ttl2ld xmld -f 'data/skos/skos-1.2.0.rdf'
  .command('xmld', 'Transform xml ontology model to a jsonld one',
    {
      sourceFile: {
        description: 'Relative path from working directory source ttl file to transform',
        alias: 'f',
        type: 'string',
        demandOption: true,
      },
    },
    // handler
    async (argv) => {
      const path = `${process.cwd()}/${argv.sourceFile}`

      // transform xml to json-ld
      const options = {
        contentType: 'application/rdf+xml',
      }
      console.warn(`
      @TODO: review this for :
      1/ create a unique function xml2jsonld
      2/ find a way to remove rdflib ?
      `)
      const store = getStoreFromFile(path, options)
      const data = await toJsonLD(store)

      const ldDoc = await toStdLD(data, multiOntologiesContext)

      changeOntologyId(ldDoc)

      // some descriptions are not language taggued
      transformToLangString(
        ldDoc,
        ['definition', 'description', 'title', 'comment', 'label']
        , 'en')

      addRdfsClassToOwlClass(ldDoc)

      // do a ligth context from the data
      contextShaper(ldDoc)

      // define the target
      if (!path.endsWith('.rdf')) {
        throw new Error('@TODO: make the next line more generic')
      }
      const baseName = nodePath.basename(path, '.rdf')
      const dir_name = nodePath.dirname(path)
      const targetFile = `${dir_name}/${baseName}.jsonld`
      saveJson(ldDoc, targetFile)

    }
  )

  .help()
  .alias('help', 'h')
  .parse(hideBin(process.argv))



function addRdfsClassToOwlClass(ldDoc) {
  console.warn('@TODO: deprecated, use this function instead: addRdfTypesToOwl')
  const classes = filterByType(ldDoc, 'owl:Class')
  classes.forEach(n => n.type.push('rdfs:Class'))
}

function addRdfTypesToOwl(ldDoc) {
  // 1/ filter and update Classes
  const rdfsClass = 'rdfs:Class'
  const classes = filterByType(ldDoc, 'owl:Class')
  classes.forEach(n => {
    if (!n.type.includes(rdfsClass)) n.type.unshift(rdfsClass)
  })

  // 2/ filter on properties and update
  const rdfProperty = 'rdf:Property'
  const properties = [
    ...filterByType(ldDoc, 'owl:DatatypeProperty'),
    ...filterByType(ldDoc, 'owl:ObjectProperty')
  ]
  properties.forEach(n => {
    if (!n.type.includes(rdfProperty)) n.type.unshift(rdfProperty)
  })


}

// transform old dc/elements url to the new dc/terms/
function renovateOntology(ldDoc) {
  // 1/ Renovate dc:
  // Detailled description :
  // hard replace of old dc namespace "http://purl.org/dc/elements/1.1/"
  // to the new one in the first item with the new terms ns
  // see this doc for explanation : [Section 1: Introduction and Definitions](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/)

  // renovate on .id value
  ldDoc.graph = ldDoc.graph.map( node => {
    // process the node entries
    const newEntries = Object.entries(node).map(([key, value]) => {

      const rempl = 'http://purl.org/dc/elements/1.1/'

       // renovate .id value
       if(key === 'id') value = value.replace(rempl, '')

       // renovate key value
      if (key.startsWith(rempl)) {
        const newKey = key.replace(rempl, '')
        if (typeof value === 'string') return [newKey, [value]]
        if (!Array.isArray(value)) return [newKey, [value]]
        return [newKey, value]
      }
      return [key, value]

    })

    return Object.fromEntries(newEntries)

  })
}

// this is root url that is used, but "in my sense" do not reference the "Ontology instance", but the file and all entities inside.
function changeOntologyId(ldDoc) {
  const ontoNodes = filterByType(ldDoc, 'owl:Ontology')
  if (ontoNodes.length > 1) console.warn('!!!!!!!!!!!!!!! special case to check')
  if (!ontoNodes.length) return
  const ontology = ontoNodes[0]

  const httpRe = /^https?:\/\//

  if (httpRe.test(ontology.id)) {
    let changed = false

    if (ontology.id === 'http://www.w3.org/1999/02/22-rdf-syntax-ns#') {
      ontology.id = 'rdf:Ontology'
      changed = true
    }

    if (ontology.id === 'http://www.w3.org/2004/02/skos/core') {
      ontology.id = 'skos:Ontology'
      changed = true
    }

    if (ontology.id === 'http://purl.org/linked-data/cube') {
      ontology.id = 'qb:Ontology'
      changed = true
    }

    if (ontology.id === 'http://www.w3.org/2002/07/owl') {
      ontology.id = 'owl:Ontology_entity' // special case to not have 2 owl:Ontology : the Class and an instance
      changed = true
    }

    if (ontology.id === 'http://www.w3.org/2000/01/rdf-schema#') {
      ontology.id = 'rdfs:Ontology'
      changed = true
    }

    if (ontology.id === 'https://ontologies.mindmatcher.org/carto/') {
      ontology.id = 'mmo:Ontology'
      changed = true
    }

    if (ontology.id === 'http://ontology.datasud.fr/openemploi/') {
      ontology.id = 'oep:Ontology'
      changed = true
    }

    // !!! Typo in source no trailling /
    if (ontology.id === 'http://ns.mnemotix.com/ontologies/2019/8/generic-model') {
      ontology.id = 'mnx:Ontology'
      changed = true
    }

    if (ontology.id === 'http://www.w3.org/ns/shacl#') {
      ontology.id = 'sh:Ontology'
      changed = true
    }

    if (ontology.id === 'http://w3id.org/gaia-x/core#') {
      ontology.id = 'gxcore:Ontology'
      changed = true
    }

    if (ontology.id === 'http://w3id.org/gaia-x/gax-trust-framework#') {
      ontology.id = 'gxframework:Ontology'
      changed = true
    }

    if (ontology.id === 'http://data.europa.eu/esco/model') {
      ontology.id = 'esco:Ontology'
      changed = true
    }




    if (!changed) {
      console.log('====> From `changeOntologyId` fn : New case to implement here:', ontology)
      console.warn(chalk.red('This ontology id is to renovate:', ontology.id))
    }
  }
}

const stringIsAValidUrl = (s) => {
  try {
    new URL(s);
    return true;
  } catch (err) {
    return false;
  }
};

function transformToLangString(ldDoc, properties, language) {

  for (const entity of ldDoc.graph) {
    for (const propertyKey of properties) {
      const valueToChange = entity[propertyKey]
      if (!valueToChange) continue
      entity[propertyKey] = valueToChange.map(s => {
        if (typeof s === 'string') return ({ '@language': language, '@value': s })
        return s
      })
    }
  }
}
