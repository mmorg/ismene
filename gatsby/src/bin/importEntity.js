import { readJson } from '@mmorg/fsutils'
import chalk from 'chalk'
import yargs from 'yargs'
import { hideBin } from 'yargs/helpers'
import import2APF from '../apiPFUtils/import2APF.js'
import loadScript from '../importUtils/loadScript.js'
// import ontoIndexer from '../ontoModel/ontoIndexer.js'


console.warn('!!!!!!!!!!!!!!!!!! Because of the "esm war" type:module have to be manually added to run this script')

// docker compose exec gatsby node src/bin/importEntity imp -c localImport.js
const argv = yargs()
  .command('imp', 'Import entites to APIpf',
    {
      conf: {
        description: 'Configuration file in folder: @TODO: ref the folder here',
        alias: 'c',
        type: 'string',
        demandOption: true,
      },
    },
    // handler
    async (argv) => {
      const confPath = argv.conf
      const scriptBase = `/apps/gatsby/src/configs/importAPF`
      const config = (await loadScript([`${scriptBase}/${confPath}`]))[0].default
      console.log(config)
      const {graph, ontology, url} = config
      // const genGraph = readJson('/apps/gatsby/data/genscan/genTerms-v1.1.0.jsonld')
      // const genOntology = readJson('/apps/gatsby/data/skos/__exports__/skos_X.X.X_gl-realm-compacted:generator_apiGenerator.jsonld')
      console.log('RESTART HERE', 'read the file', 'create the conf')
      await import2APF(graph,ontology,url)
      // await ontoIndexer(argv)
      if (!argv.u) {
        console.log(chalk.yellow('Changes not applied on files. Use -u to update'))
      } else {
        console.log(chalk.green('Indexing applied on files'))
      }
      return
    }
  )

  .help()
  .alias('help', 'h')
  .parse(hideBin(process.argv))
