import { saveJson } from '@mmorg/fsutils'
import chalk from 'chalk'
import yargs from 'yargs'
import { hideBin } from 'yargs/helpers'
import exportCompacted from '../jsonldUtils/graphLayersChef/exportCompacted.js'

// docker-compose exec gatsby node src/bin/integrate mu -f '**/oriane-rdf-release/*.jsonld'
const argv = yargs()
  .command('export', 'Exports a compacted version (single Graph) of the LayerRealm',
    {
      updateFiles: {
        description: 'Update the files (default yes)',
        alias: 'u',
        type: 'boolean',
      },
      // @Deprecated
      ontologyName: {
        description: 'Select the ontologyName',
        alias: 'o',
        type: 'string',
      },
      parentFolder: {
        description: 'Relative path for parent folder of `___layers___`',
        alias: 'p',
        type: 'array',
      },
      targetFolder: {
        description: 'Target parent folder for `__exports` when there is multiple parents Folder',
        alias: 't',
        type: 'string',
      },
      layerReaml: {
        description: '@TODO: select the realms you want for a file (@TODO: make param an array & implement)',
        alias: 'l',
        type: 'array',
      },
    },
    // handler
    async (argv) => {
      if(argv.o) throw new Error('This parameter is removed, use -p instead')
      const fileBag = await exportCompacted(argv)
      if (!argv.u) {
        console.log(chalk.yellow('Changes not applied on files. Use -u to update'))
      } else {
        console.log(chalk.green('Indexing applied on files'))
        fileBag.forEach(({ fileName, ldContent }) => saveJson(ldContent, fileName))
      }

    }
  )

  .help()
  .alias('help', 'h')
  .parse(hideBin(process.argv))
