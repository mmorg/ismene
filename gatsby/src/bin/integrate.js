import chalk from 'chalk'
import yargs from 'yargs'
import { hideBin } from 'yargs/helpers'
import ontoIndexer from '../ontoModel/ontoIndexer.js'

// @TODO: remove the locals `package.json` after esm war

// docker-compose exec gatsby node src/bin/integrate mu -f '**/oriane-rdf-release/*.jsonld'
const argv = yargs()
  .command('mu', 'Update the ontology model file',
    {
      ontoFiles: {
        description: 'Globby instructions for the ontology files',
        alias: 'f',
        type: 'string',
        demandOption: true,
      },
      updateFiles: {
        description: 'Update the files (default false)',
        alias: 'u',
        type: 'boolean',
      },
      // @TODO: remove base path
      externalEntities: {
        description: 'Execute the js default function. Base path for scripts: `gatsby/src/renovationScripts`',
        alias: 'e',
        type: 'string',
      },
      graphLayers: {
        description: 'Execute the js default function.',
        alias: 'g',
        type: 'string',
      }
    },
    // handler
    async (argv) => {
      await ontoIndexer(argv)
      if (!argv.u) {
        console.log(chalk.yellow('Changes not applied on files. Use -u to update'))
      } else {
        console.log(chalk.green('Indexing applied on files'))
      }

    }
  )

  .help()
  .alias('help', 'h')
  .parse(hideBin(process.argv))
