
export default function sortGraph(ld) {
  ld.graph.sort((a, b) => a.id.localeCompare(b.id))
}
