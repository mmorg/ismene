import jsonld from 'jsonld'

export default async function toStdLD(ldDoc, stdContexts, forceContext = false) {

  // @TODO : create a more "smart" that throw errors on overrides
  //       : but be careful, for now, this is "raw script" : the last win
  const localContext = Object.assign({}, ...stdContexts)

  if(forceContext) ldDoc['@context'] = localContext

  return await jsonld.compact(ldDoc, localContext)
}
