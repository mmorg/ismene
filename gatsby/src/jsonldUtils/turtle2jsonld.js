import n3 from 'n3'
import jsonld from 'jsonld'
import fs, { existsSync } from 'fs'

// inspired by : https://github.com/digitalbazaar/jsonld.js/issues/280
// be carefull as this code may cause problems in some cases. More infos the issue

// @TODO: move to subfolder /rdfUtils/



/**
 *
 * @param {string} filePath
 * @returns {jsonLD}
 */
export default async function turtle2jsonld(filePath) {
  if (!existsSync(filePath)) throw new Error('The source file do not exist: '+filePath)

  const rdfStream = fs.createReadStream(filePath)
  const parsedTurtle = await turtleParser(rdfStream)
  const jsonRep = await jsonld.fromRDF(parsedTurtle)
  return jsonRep
}

async function turtleParser(rdfStream) {
  const parser = new n3.Parser();
  let [quads, prefixes] = await new Promise((resolve, reject) => {
    let quads = []
    parser.parse(rdfStream, (error, quad, prefixes) => {
      if (error)
        reject(error)
      else if (quad)
        quads.push(quad)
      else
        resolve([quads, prefixes])
    })
  })
  return quads
}
