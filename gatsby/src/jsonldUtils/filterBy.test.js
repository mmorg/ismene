import filterBy from './filterBy.js'

// docker-compose exec rml npm test -- --watch filterBy.test

describe('Test of object structure options', () => {
  const testGraph = {
    '@context': {},
    graph: [
      {
        "id": "gendj:009746388c58ea81de3cdbcd866fcba0",
        "type": [
          "skos:Concept"
        ],
        "broader": [
          "gendj:857047f5b79ac18d9f74c981cd62831d"
        ],
        "narrower": [
          "gendj:9e40561ba4abaa64b11866366040762a"
        ],
        "prefLabel": [
          {
            "@language": "fr",
            "@value": "Growth hacking"
          }
        ]
      },
    ]
  }


  it('manage search for literal with language', async () => {
    // return result for match
    const text = 'Growth hacking'
    const entity = {
      '@language': 'fr',
      '@value': text,
    }
    const r = filterBy('prefLabel', entity, testGraph)
    expect(r.length).toBe(1)

    // return nothing for non existing value
    const notFoundEntity = {
      '@language': 'fr',
      '@value': 'test',
    }
    const notFound = filterBy('prefLabel', notFoundEntity, testGraph)
    expect(notFound.length).toBe(0)
  })

  it('manage search for "id object"', async () => {
    const entity = { id: "skos:Concept" }
    const r = filterBy('type', entity, testGraph)
    expect(r.length).toBe(1)
  })

  it('manage search for "id string"', async () => {
    const entity = "skos:Concept"
    const r = filterBy('type', entity, testGraph)
    expect(r.length).toBe(1)
  })

})

