

// inverse relation of domain
export default function hasProperties(entity, graphLD){
    return graphLD.graph.filter( e => e.domain ? e.domain.includes(entity.id) : null)
}
