import filterBy from './filterBy.js'

export default function filterByDomain(domainId, ld) {
  return filterBy('domain', domainId, ld)
}
