import getStoreFromFile from './getStoreFromFile'
import toJsonLD from './toJsonLD'

// docker compose exec gatsby npm test -- --watch toJsonLD.test

describe('JsonLD processing is okay', () => {

  it('works for .ttl file', async () => {
    const ttlFile = `${__dirname}/__fixtures__/7entities.ttl`
    const store = getStoreFromFile(ttlFile)

    // transform toJsonLD
    const data = await toJsonLD(store)
    expect(data).toMatchSnapshot()
  })

  it('works for .xml file', async () => {
    const xmlFile = `${__dirname}/__fixtures__/skos.rdf`
    const options = {
      contentType: 'application/rdf+xml',
    }
    const store = getStoreFromFile(xmlFile, options)

    // transform toJsonLD
    const data = await toJsonLD(store)
    expect(data).toMatchSnapshot()
  })


})
