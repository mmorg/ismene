import * as rdflib from 'rdflib'
import fs from 'fs'
import { rootQuadWhy } from './rdfLibConfigs.js'

const defaultOption = {
  contentType: 'text/turtle',
  rootQuadWhy,
  existingStore: null,
}

export default function getStoreFromFile(escoFile, options = {}) {
  let { contentType, rootQuadWhy, existingStore } = Object.assign({}, defaultOption, options)

  const data = fs.readFileSync(escoFile).toString()

  if (!existingStore) existingStore = rdflib.graph()

  // console.log('-- start importing')
  rdflib.parse(data, existingStore, rootQuadWhy.value, contentType)
  // console.log(('-- import is ok'))
  return existingStore
}
