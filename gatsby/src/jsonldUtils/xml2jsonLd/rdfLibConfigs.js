import * as rdflib from 'rdflib'
const rootQuadWhy = rdflib.sym('http://lib.competencies.be/rootQuadWhy')

/** RDF */
const rdfTypeProp = rdflib.sym('http://www.w3.org/1999/02/22-rdf-syntax-ns#type')

/** Skos */
const skosNS = 'http://www.w3.org/2004/02/skos/core#'
const prefLabelProp = rdflib.sym(`${skosNS}prefLabel`)
const altLabelProp = rdflib.sym(`${skosNS}altLabel`)
const hiddenLabelProp = rdflib.sym(`${skosNS}hiddenLabel`)
const broaderProp = rdflib.sym(`${skosNS}broader`)
const narrowerProp = rdflib.sym(`${skosNS}narrower`)
const inSchemeProp = rdflib.sym(`${skosNS}inScheme`)
const notationProp = rdflib.sym(`${skosNS}notation`)
/** ESCO NS */
const escoNS = 'http://data.europa.eu/esco/model#'
const skillClass = rdflib.sym(`${escoNS}Skill`)
const occupationClass = rdflib.sym(`${escoNS}Occupation`)
const relatedEssentialSkillProp = rdflib.sym(`${escoNS}relatedEssentialSkill`)
const relatedOptionalSkillProp = rdflib.sym(`${escoNS}relatedOptionalSkill`)
const skillReuseLevelProp = rdflib.sym(`${escoNS}skillReuseLevel`)
const skillTypeProp = rdflib.sym(`${escoNS}skillType`)

/** ESCO Schemes */
const escoScheme = 'http://data.europa.eu/esco/concept-scheme/'
const skillTransersalScheme = rdflib.sym(`${escoScheme}skill-transversal-groups`)
const skillScheme = rdflib.sym(`${escoScheme}skills`)
const iscoScheme = rdflib.sym(`${escoScheme}isco`)
/** ESCO model */
const escoModel = 'http://data.europa.eu/esco/model#'
const escoLanguage = rdflib.sym(`${escoModel}language`)
const escoNodeLiteral = rdflib.sym(`${escoModel}nodeLiteral`)

/** Skos-XL */
const xlNS = 'http://www.w3.org/2008/05/skos-xl#'
const xlLabelClass = rdflib.sym(`${xlNS}Label`)
const xlPrefLabelProp = rdflib.sym(`${xlNS}prefLabel`)
const xlHiddenLabelProp = rdflib.sym(`${xlNS}hiddenLabel`)
const xlAltLabelProp = rdflib.sym(`${xlNS}altLabel`)

/** DC Terms */
const dctNS = 'http://purl.org/dc/terms/'

const dctDescriptionProp = rdflib.sym(`${dctNS}description`)


export {
    rootQuadWhy,
    // RDF
    rdfTypeProp,
    // SKOS
    prefLabelProp, altLabelProp, hiddenLabelProp,
    broaderProp, narrowerProp, inSchemeProp,
    notationProp,
    // esco NS
    skillClass, occupationClass,
    relatedEssentialSkillProp, relatedOptionalSkillProp,
    skillReuseLevelProp, skillTypeProp,
    // esco Scheme
    skillTransersalScheme, skillScheme,
    iscoScheme, escoLanguage, escoNodeLiteral,
    // SKOS-XL
    xlLabelClass, xlPrefLabelProp,
    xlHiddenLabelProp, xlAltLabelProp,
    // DC Terms
    dctDescriptionProp
}
