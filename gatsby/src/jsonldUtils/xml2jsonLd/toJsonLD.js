import * as rdflib from 'rdflib'
import { rootQuadWhy } from './rdfLibConfigs.js'


const defaultOptions = {
  contentType: 'application/ld+json',
  rootQuadWhy,
}

export default async function toJsonLD(store, options) {
  let { contentType, rootQuadWhy } = Object.assign({}, defaultOptions, options)

  const exec = (resolve, reject) => {
    rdflib.serialize(rootQuadWhy, store, undefined, contentType, (v, d) => { resolve(JSON.parse(d)) })
  }

  return new Promise(exec)
}

