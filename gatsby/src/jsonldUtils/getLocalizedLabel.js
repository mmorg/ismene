
// @TODO: make a more "smart" function that manage the 2 standards cases

console.warn('@Deprecated: use getStringValues instead')

export default function getLocalizedLabel(entity, property = 'prefLabel', lang = 'en') {

  if (!entity[property]) return '__Empty property from getLocalizedLabel__'

  const langString = entity[property].find(l => l._language === lang)
  return langString._value
}

