import filterBy from './filterBy.js'

export default function Graph(jsonLd){

  const ld = jsonLd

  return {
    filterBy : (property, value) => filterBy(property,value,ld),

  }

}
