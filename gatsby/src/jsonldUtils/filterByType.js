

// @TODO: change to (type, ld) and use filterBy
export default function filterByType(ld, type){
  return ld.graph.filter( n => n.type && n.type.includes(type))
}
