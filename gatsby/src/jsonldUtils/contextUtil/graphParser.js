
export default function graphParser(ld) {

  const entityKeys = new Set()

  ld.graph.forEach(entity => {
    Object.keys(entity).forEach(k => entityKeys.add(k))
  })

  // remove specials 'id' & 'type' entries
  entityKeys.delete('id')
  entityKeys.delete('type')

  return { entityKeys }
}