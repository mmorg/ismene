import contextParser from './contextParser.js'
import graphParser from './graphParser.js'

/**
 * @deprecated use this package instead: packages/rdfx-layer/src/contextUtil/README.md
 * This function mutate the LD context
 * @param {*} ld 
 * @returns 
 */
export default function contextShaper(ld) {

  const ldContext = ld['@context']

  // AST is a little_big term here
  const contextAst = contextParser(ld)
  const graphAst = graphParser(ld)

  // Get all the context's keys 
  const keys = new Set([...contextAst.idRefsKeys, ...contextAst.datatypesKeys])

  // 1/ remove all the propertyAliases
  for (const k of keys) {
    if (graphAst.entityKeys.has(k)) continue
    delete ldContext[k]
  }

  // 2/ remove all the unused nsAliases
  const ligthContextAst = contextParser({ ['@context']: ldContext })

  const usedContextNs = getUsedNsFromContext(ligthContextAst)

  const usedGraphNs = getUsedNsFromGraph(ld.graph, ligthContextAst)

  // @TODO: create getUsedNsFromDataType()

  // apply removals on the context
  const usedNs = new Set([...usedContextNs, ...usedGraphNs])
  // b) remove unused from NS
  for (const contextNS of ligthContextAst.nsAliasKeys) {
    if (usedNs.has(contextNS)) continue
    delete ldContext[contextNS]
  }

  return ld
}

function getNs(id) {
  return id.split(':')[0]
}

function getUsedNsFromContext(ligthContextAst) {

  // a) identify used NS in the context 
  const idRefsValues = Object.values(ligthContextAst.idRefs)
  const dataTypesVAlues = Object.values(ligthContextAst.datatypes)

  const usedNs = [...idRefsValues, ...dataTypesVAlues].reduce((acc, payload) => {
    const ns = getNs(payload['@id'])
    acc.add(ns)
    return acc
  }, new Set())

  return usedNs
}

function getUsedNsFromGraph(entities, ligthContextAst) {
  const idProperty = ligthContextAst.idRefsKeys

  const usedNs = entities.reduce((acc, e) => {
    for (const propertyName of idProperty) {
      if (!e[propertyName]) continue
      // 1/ extract values
      const nsArray = e[propertyName].map(getNs)
      acc.add(...nsArray)
    }
    return acc
  }, new Set())

  return usedNs
}