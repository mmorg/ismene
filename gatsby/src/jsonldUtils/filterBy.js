
export default function filterBy(property, obj, ld) {


  let includesTest = null
  // case 1: object is a literal
  const { '@value': testLit, '@language': testLang } = obj
  if (testLit) {
    if (!testLang) {
      console.warn('Search will be independant from language')
    }

    includesTest = (propertyValues) => propertyValues.some(value => testSameLiteral(value, testLit, testLang))
    return ld.graph.filter(n => n[property] && includesTest(n[property]))
  }

  // case 2: object is an "id object"
  let stringValue = obj.id
  // case 3 : object is a string
  if (!stringValue) stringValue = obj

  // filter with include
  return ld.graph.filter(n => n[property] && n[property].includes(stringValue))
}

// @TODO : add parameter for dataTypes
function testSameLiteral(literal, value, language,) {
  if (!value && !language) return false
  let result = true
  if (value) result = result && literal['@value'] === value
  if (language) result = result && literal['@language'] === language
  return result
}
