import unRef from '../unRef.js'

// descendant tree traversal
export default function getRecursiveNarrower(entitySource, graph) {
  const narrower = getNarrower(entitySource, graph)

  if (!narrower.length) return []

  const aggregate = []
  for (const n of narrower) {
    const narrows = getRecursiveNarrower(n, graph).flat()
    aggregate.push(n, ...narrows)
  }

  return aggregate
}

function getNarrower(entitySource, graph) {
  return unRef('narrower', entitySource, graph)
}
