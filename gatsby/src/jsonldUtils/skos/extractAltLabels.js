
// @TODO: use a call to `getStringValues` inside this helper
export default function extractAltLabels(entities, lang = 'fr'){

  const literals = entities.map( e => e['altLabel'] ? e['altLabel'] : []).flat()

  const langLiteral = literals.filter( lit => lit['@language'] === lang)

  return langLiteral.map( lit => lit['@value'])

}
