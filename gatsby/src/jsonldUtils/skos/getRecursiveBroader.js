import unRef from '../unRef.js'

// ///up/// tree traversal
export default function getRecursiveBroader(entitySource, graph) {
  const broader = getBroader(entitySource, graph)

  if (!broader.length) return []
  const aggregate = []
  for (const n of broader) {
    const broads = getRecursiveBroader(n, graph)
    // @TODO: change to add here in reverse order
    aggregate.push(n, ...broads)
  }
  return aggregate
}

function getBroader(entitySource, graph) {
  return unRef('broader', entitySource, graph)
}
