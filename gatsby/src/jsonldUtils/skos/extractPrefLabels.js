
// @TODO: use a call to `getStringValues` inside this helper
export default function extractPrefLabels(entities, lang = 'fr'){

  const literals = entities.map( e => e['prefLabel'] ? e['prefLabel'] : []).flat()

  const langLiteral = literals.filter( lit => lit['@language'] === lang)

  return langLiteral.map( lit => lit['@value'])

}
