import { readJson } from '@mmorg/fsutils'
import getStringValues from './getStringValues.js'

// npm test -- --watch getStringValues.test

const fixturesFolder = `${__dirname}/__fixtures__`

describe('Localized extracts in `gatsby mode` (ex: _language)', () => {

  it('Extract String from langString', () => {
    const hasAptitude = readJson(`${fixturesFolder}/hasAptitude.json`)

    // test fr flocalised comment
    const commentString = getStringValues(hasAptitude.comment)
    expect(commentString).toStrictEqual(['Aptitudes dont dispose l\'utilisateur'])

    const nullValue = getStringValues(hasAptitude.example)
    expect(nullValue).toBe(null)

    const unlanguedLiteral = getStringValues(['a value'])
    expect(unlanguedLiteral).toStrictEqual(['a value'])

    const uniqueLiteralUnlangued = getStringValues('unique literal')
    expect(uniqueLiteralUnlangued).toStrictEqual(['unique literal'])

    // @TODO: faire un test sur des numbers
  })
  

  it('Extract String on the last fallback language', () => {
    const test = [
      {
        '_language': 'en',
        '_value': 'yearsKnown'
      }
    ]
    expect(getStringValues(test)).toStrictEqual(['yearsKnown'])

  })

})

describe('Localised extracts in `json-ld mode` (ex: @language)', () => {

  it('Extract String from LangString', () => {
    const test = [{
      '@language': 'en', 
      '@value': 'has narrower'
    }]
    expect(getStringValues(test)).toStrictEqual(['has narrower'])
  })
})

describe('Localized extracts in `rdfx-graphql mode` (ex: language)', () => {

  it('Extract String from LangString', () => {
    const test = [{
      'language': 'en', 
      'value': 'has narrower'
    }]
    expect(getStringValues(test)).toStrictEqual(['has narrower'])
  })
})