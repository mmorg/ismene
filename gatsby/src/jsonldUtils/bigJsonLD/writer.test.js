import { dirname } from 'path'
import { fileURLToPath } from 'url'
it.todo('Restaure after the "esm war"')
// const __dirname = dirname(fileURLToPath(import.meta.url))
import { promises as fs, constants } from 'fs'
import writer from './writer'
// docker-compose exec proc npm test -- --watch writer.test

const exist = async (path) => {
    try {
        await fs.access(path, constants.F_OK)
        return true
    } catch (err) {
        return false
    }
}
const deleteFile = async (path) => {
    if (await exist(path)) {
        await fs.unlink(path)
    } // else do nothing
}

const getJson = async (path) => {
    return JSON.parse((await fs.readFile(path)).toString())
}

describe('dev test for json-ld writer', () => {

    it.todo('test is default @graph key')
    it('Write json-ld with context and specific @graph key', async () => {

        // @TODO: move it to beforeAll
        const escoFile = `${__dirname}/__fixtures__/esco_v1.0.8-1K.jsonld`
        // load it in memory with classical read
        const ld = await getJson(escoFile)

        const targetFile = `${__dirname}/__fixtures__/test-writer-specific.jsonld`

        // delete the file to write if exist
        await deleteFile(targetFile)

        await writer(ld, targetFile, {logs : false})

        // test if the file exist
        expect(await exist(targetFile)).toBe(true)
        // console.log(ld)

        // load it add get "@context" & "results" properties
        const r = await getJson(targetFile)
        expect(r['@context']).toBeTruthy()

        expect(r.results).toBeTruthy()

        // delete the file after creation
        // comment this line to inspect the generated file
        await deleteFile(targetFile)
    })
})
