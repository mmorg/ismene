

const graphKey = '@graph'

export default function getGraphKey(context) {
    const found = Object.entries(context).find(([key, value]) => (value === graphKey) || (value['@id'] && value['@id'] === graphKey))
    if (found) {
        const [userDefinedKey] = found
        return userDefinedKey
    }
    return undefined
}