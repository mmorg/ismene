import getGraphKey from './getGraphKey.js'

// docker-compose exec rml npm test -- --watch getGraphKey.test

describe('dev test for graph key detection', () => {

    it('Detect `simple` and `object` @graph key mapping', async () => {

        // test with a simple key mapping
        const context1 = {
            id: '@id',
            myKey: '@graph'
        }

        expect(getGraphKey(context1)).toBe('myKey')

        // test with an container key mapping
        const context2 = {
            id: '@id',
            myKey: {
                '@id' : '@graph',
                '@container': '@set'
            }
        }
        expect(getGraphKey(context2)).toBe('myKey')
        
    })
})