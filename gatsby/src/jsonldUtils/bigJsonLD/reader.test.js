import { dirname } from 'path'
import { fileURLToPath } from 'url'
it.todo('Restaure after the "esm war"')
// const __dirname = dirname(fileURLToPath(import.meta.url))
import reader from './reader'

// docker-compose exec proc npm test -- --watch reader.test

describe('dev test for json-ld writer', () => {

    it.todo('test is default @graph key')
    it('Write json-ld with context and specific @graph key', async () => {

        // @TODO: move it to beforeAll
        const escoFile = `${__dirname}/__fixtures__/esco_v1.0.8-1K.jsonld`

        const object = await reader(escoFile, {logs : false})

        expect(object['@context']).toBeTruthy()

        expect(object.results.length > 1).toBe(true)
    })
})
