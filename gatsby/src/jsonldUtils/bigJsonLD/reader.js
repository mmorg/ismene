import fs from 'fs'

// @TODO: JSONStream is archived, use "stream-json" instead
//    => see example here: rml/src/jsonldUtils/bigJson/readArrayInFile.js
import JSONStream from 'JSONStream'

import getGraphKey from './utils/getGraphKey.js'
import chalk from 'chalk'

// load jsonld in memory
export default async function reader(path, options = { logs: true }) {
    // @TODO : find a way to test if only array and process accordingly

    const result = {}
    // part1 : read context
    const context = await streamContext(path)
    result['@context'] = context

    // detect key
    let graphKey = '@graph'
    const found = getGraphKey(context)
    if (found) graphKey = found

    // read stream key content
    const graphContent = await stream2array(path, `${graphKey}.*`, options)

    result[graphKey] = graphContent

    return result
}

function streamContext(path) {
    const stream = fs.createReadStream(path, { encoding: 'utf8' })
    const parser = JSONStream.parse('@context')
    stream.pipe(parser)

    return new Promise(resolve => {
        let context = null
        parser.on('data', (obj) => context = obj)
        parser.on('error', (error) => console.log(error))
        parser.on('end', () => resolve(context))

    })
}

function stream2array(path, graphKey, options) {
    const stream = fs.createReadStream(path, { encoding: 'utf8' })
    const parser = JSONStream.parse(`${graphKey}`)
    stream.pipe(parser)

    return new Promise(resolve => {
        const doc = []
        parser.on('data', function (obj) {
            if (options.logs && doc.length % 100000 === 0) console.log('processed :', doc.length)
            doc.push(obj)
        })

        parser.on('error', (error) => console.log(error))


        parser.on(
            'end',
            function handleFinish() {
                if (options.logs) console.log(chalk.green("Import complete!"))
                resolve(doc)
            }
        );

    })
}
