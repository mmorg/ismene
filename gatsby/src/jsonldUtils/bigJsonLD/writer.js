import fs from 'fs'
import JSONStream from 'JSONStream'
import chalk from 'chalk'
import getGraphKey from './utils/getGraphKey.js'

export default async function writer(docLD, path, options = { logs: true }) {
  // @TODO : test if only array. If yes directly use array writer and default stringify config
  // setup the transformStream in array mode
  let transformStream = JSONStream.stringify()
  let entities = docLD

  if (docLD['@context']) {
    let entitiesKey = '@graph'
    const context = docLD['@context']
    // find if there is a specific key for @graph
    const found = getGraphKey(context)
    if (found) {
      entitiesKey = found
    }

    const open = `{\n "@context": ${JSON.stringify(context)},\n "${entitiesKey}":[\n`
    const close = `\n]\n}`
    const sep = ',\n'

    // if yes, configure 'transformStream'
    transformStream = JSONStream.stringify(open, sep, close)

    // update entities if context
    entities = docLD[entitiesKey]
  }



  return new Promise(async (resolve, reject) => {
    // setup and configure the data file output stream
    const outputStream = fs.createWriteStream(path);
    // Once the JSONStream has flushed all data to the output stream, let's indicate done.
    outputStream.on(
      "finish",
      function handleFinish() {
        if (options.logs) console.log(chalk.green("File serialization complete!"))
        resolve()
      }
    )

    // pipe the serialized objects to output stream.
    transformStream.pipe(outputStream);

    // Iterate over the records and write EACH ONE to the TRANSFORM stream individually.
    let index = 0
    for (const e of entities) {
      if (options.logs && index % 100000 === 0 && index != 0) console.log('-> on', entities.length, 'processed items:', index)
      await write(transformStream, e)
      index += 1
    }

    // Once we've written each record in the record-set, we have to end the stream so that
    // the TRANSFORM stream knows to output the end of the array it is generating.
    transformStream.end();

  })


}

// prevents "backpressuring" when writing to stream
// source : https://gist.github.com/stevenkaspar/509f792cbf1194f9fb05e7d60a1fbc73
const write = (writer, data) => {
  return new Promise((resolve) => {
    if (!writer.write(data)) {
      writer.once('drain', resolve)
    }
    else {
      resolve()
    }
  })
}
