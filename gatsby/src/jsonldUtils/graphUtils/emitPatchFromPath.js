
export default function emitPatchFromPath(sourceEntities, pathMatrix, patchDirection) {

  const patches = sourceEntities.reduce((acc, e, i) => {
    const graphPath = pathMatrix[i]

    // 1/ there is values from the pathMatrix ?
    // get patchDirection ids and remove (possibles?) null ones
    const valuesFromPath = graphPath.reduce((acc2, v) => {
      if (v) acc2.push(v.id)
      return acc2
    }, [])

    if (!valuesFromPath.length) return acc

    // 2/ there is a patch that already exist
    // @TODO: check, this may be not usefull as each sourceEntity (e) should be unique
    let exist = acc.get(e.id)
    if (!exist) {
      exist = { id: e.id }
      exist[patchDirection] = []
      acc.set(e.id, exist)
    }

    // make an array of uniques values
    const uniques = [...new Set([...exist[patchDirection], ...valuesFromPath])]
    exist[patchDirection] = uniques

    return acc
  }, new Map())

  return [...patches.values()]
}
