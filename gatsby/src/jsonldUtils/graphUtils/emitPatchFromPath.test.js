import { readJson } from '@mmorg/fsutils'
import emitPatchFromPath from './emitPatchFromPath.js'

// docker-compose exec gatsby npm run testESM -- --watch emitPatchFromPath.test

const fixtureFolder = `${__dirname}/__fixtures__`

describe('Test emitPatch from an "on coding" example', () => {

  it('Output the same result as a simpler function', async () => {
    const {
      sourceEntities,
      pathMatrix,
      patchDirection,
      expectedResults
    } = readJson(`${fixtureFolder}/testCase_1.json`)

    const patches = emitPatchFromPath(sourceEntities, pathMatrix, patchDirection)
    expect(patches).toStrictEqual(expectedResults)

  })

})
