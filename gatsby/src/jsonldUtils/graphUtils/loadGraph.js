import { readJson } from '@mmorg/fsutils'
import getOntologyNsPathIndex from './getOntologyNsPathIndex.js'

const loadingCache = new Map()
export default function loadGraph(ontologyNS, nsPathIndexOverride) {
  const nsPathIndex = getOntologyNsPathIndex()
  let found = loadingCache.get(ontologyNS)
  if (!found) {
    const path = nsPathIndex[ontologyNS]

    if (!path && nsPathIndexOverride[ontologyNS]) {
      loadingCache.set(ontologyNS, nsPathIndexOverride[ontologyNS])
      return nsPathIndexOverride
    }

    if (!path) {
      const { ns } = splitId(ontologyNS)
      const errorTxt = `
      This ns don't have an indexed file: ${ns},
      before auto-loading, change the index in this file: ${__dirname}/getOntologyNsPathIndex.js
      `
      throw new Error(errorTxt)
    }

    found = readJson(`${process.cwd()}/${path}`)
    loadingCache.set(ontologyNS, found)
  }

  return found
}
