import splitId from '../splitId.js'
import findById from '../findById.js'
import loadGraph from './loadGraph.js'
import getOntologyNsPathIndex from './getOntologyNsPathIndex.js'

let _nsPathIndexOverride = {}
export default function loader(nsPathIndexOverride) {
  _nsPathIndexOverride = nsPathIndexOverride
  const nsPathIndex = getOntologyNsPathIndex()

  return {
    nsPathIndex,
    load : loadGraph, // @TODO:? remove ?
    unref,
  }
}

function unref(id) {
  const { ns } = splitId(id)
  const graph = loadGraph(ns, _nsPathIndexOverride)
  return findById(graph, id)
}
