import loader from './loader.js'

export default function getPathSteps(e, propertyName, nsPathIndexOverride = {}) {
  const linkedEntities = e[propertyName]

  if (!linkedEntities) return []

  // 1/ recursive unref of propertyName direction (example: subProperty)
  const unrefMatrix = recusiveUnref(linkedEntities, propertyName, nsPathIndexOverride)

  const unrefPathEntities = unrefMatrix.flat(1)
  return unrefPathEntities
}

function recusiveUnref(ids, property, nsPathIndexOverride) {

  const { unref } = loader(nsPathIndexOverride)
  const unrefMatrix = []
  const _unrefLine = ids.map(unref)

  // push the first line
  unrefMatrix.push(_unrefLine)

  // there is more to unref ?
  const toUnrefChildren = _unrefLine.reduce((acc, v) => {
    if (v[property] && v[property].length)
      return new Set([...acc, ...v[property]])
    return acc
  }, new Set())

  if (toUnrefChildren.size) {
    const _unrefMatrixChildren = recusiveUnref([...toUnrefChildren], property, nsPathIndexOverride)
    unrefMatrix.push(..._unrefMatrixChildren)
  }
  return unrefMatrix
}
