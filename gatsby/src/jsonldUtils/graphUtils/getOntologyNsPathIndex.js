

// @TODO: make this automatically generated with globby.
// @TODO:? and loading graph content & managing graphLayer merge ?
const nsIndex = {
  skos: 'data/skos/skos-1.2.0.jsonld',
  rdfs: 'data/rdfs/rdf-schema-1.1.0.jsonld',
}

let cache = null

export default function getOntologyNsPathIndex() {
  if (!cache) cache = nsIndex
  return cache
}
