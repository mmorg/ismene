
export default function findById(doc, id) {
    const filtered = doc.graph.filter(node => node.id === id)
    if (filtered.length > 1) console.warn('Multiple entities with this id in the document')
    return filtered[0]
}
