
console.warn('@Deprecated: replace with updateLayer / updateGraph / updateEntity chain')
// @TODO: manage the merge of the @contexts
// Create a new merged layer. This do not mutate the sourceLayer
/**
 * @deprecated
 * @param {*} sourceLayer 
 * @param  {...any} otherLayers 
 * @returns 
 */
export default function mergeLayers(sourceLayer, ...otherLayers) {

  // 1/ hard copy of the first layer to not mutate it
  const _sourceLayer = JSON.parse(JSON.stringify(sourceLayer))

  // 2/ flattening the otherLayers to manage generator that return multiple Layers
  const _otherLayers = otherLayers.flat()

  // 2/ create an indexMap of the _sourceLayer's entities
  const sourceMap = _sourceLayer.graph.reduce((acc, v) => {
    acc.set(v.id, v)
    return acc
  }, new Map())

  for (const layer of _otherLayers) {

    for (const patch of layer.graph) {

      const exist = sourceMap.get(patch.id)
      if (!exist) {
        sourceMap.set(patch.id, patch)
        continue
      }

      // @TODO: be able to define another strategy that `hardMerge`
      //     ==> use `gatsby/src/objectUtils/softObjectMerge.js` for the soft one and export hardMerge in a file
      // do the `hardMerge`
      Object.assign(exist, patch)
    }
  }

  // override the values of the local sourceLayer
  _sourceLayer.graph = [...sourceMap.values()]
  return _sourceLayer
}
