import hash from 'object-hash'
import updateStrategies from '../../graphLayerScripts/updateStrategies.js'

// This function mutate the sourceEntity with the patch
/**
 * @deprecated use this package instead: packages/rdfx-layer/package.json
 * @param {*} updateStrategy 
 * @param {*} sourceEntity 
 * @param  {...any} patchEntity 
 * @returns 
 */
export default function updateEntity(updateStrategy, sourceEntity, ...patchEntity) {

  const patches = patchEntity.flat()
  
  for (const patch of patches) {
    // 1/ The strategy is "add"
    if (updateStrategy === updateStrategies.add) {
      add(sourceEntity, patch)
      continue
    }

    // 3/ The strategy is "replace", it do the remplacement at property's value level
    if (updateStrategy === updateStrategies.replace) {
      Object.assign(sourceEntity, patch)
      continue
    }
  }

  return sourceEntity
}


function add(sourceEntity, targetEntity) {

  for (const [tKey, tValue] of Object.entries(targetEntity)) {
    if (tKey === 'id') continue

    const sourceValue = sourceEntity[tKey]
    if (!sourceValue) {
      sourceEntity[tKey] = tValue
      continue
    }
    // there is an existing value
    if (Array.isArray(sourceValue)) {
      if (Array.isArray(tValue)) {
        sourceEntity[tKey] = mergeArrayOfScalars(sourceValue, tValue)
      } else {
        sourceValue.push(tValue)
      }
    } else {
      if (Array.isArray(tValue)) {
        sourceEntity[tKey] = [sourceValue, ...tValue]
      } else {
        sourceEntity[tKey] = [sourceValue, tValue]
      }
    }
  }
}

function mergeArrayOfScalars(source, target) {
  // Scalar type is object ? 
  const firstSource = source[0] ? source[0] : null
  const firstTarget = target[0] ? target[0] : null

  if (!firstSource || !firstTarget) throw new Error('@TODO: implement')

  if (typeof firstSource === 'object' && typeof firstTarget === 'object') {
    return mergeArrayOfObjects(source, target) // [...source, ...target]
  }

  if (typeof firstSource === 'string' && typeof firstTarget === 'string') {
    const unique = new Set([...source, ...target])
    return [...unique]
  }

  throw new Error('@TODO: implement: this merge case is not managed')

}

function mergeArrayOfObjects(source, target) {
  // 1/ create the hash set of source
  const sourceHash = new Set(source.map(hash))
  for(const t of target){
    const tHash = hash(t)
    if(sourceHash.has(tHash)){
      continue // discard on existing
    }
    source.push(t)
    sourceHash.add(tHash)
  }

  return source
}
