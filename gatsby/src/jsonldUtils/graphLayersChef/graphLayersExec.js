import loadScript from '../../importUtils/loadScript.js'
import mergeLayers from './mergeLayers.js'
import contextShaper from '../contextUtil/contextShaper.js'
import updateStrategies from '../../graphLayerScripts/updateStrategies.js'


const defaultGL = async () => ''

export const sourceLayerRealm = 'sourceFromAuthors'
const sourceLayer = {
  name: 'Source Layer for the following generated Layers',
  layerRealm: [sourceLayerRealm],
  ld: {}
}

/**
 * GraphLayer v1 : layer.graph contains "entities to merge" & this is the layer.updateStrategy that define how to merge theses entities
 * GraphLayer v2 : @TODO: the layer.graph contains rdfx-metadata instructions (or diff). Each instruction define the way to merge. 
 * @param {string} scriptName 
 * @param {*} modelLD 
 * @returns 
 */
export default async function graphLayersExec(scriptName = null, modelLD) {

  // 0/ define the layers aggregator and create the first default layer that contains the source graph
  sourceLayer.ld = modelLD
  const layers = [sourceLayer]

  // 1/ Step one : load the scripts configuration
  let generationConfigs = defaultGL
  if (scriptName) {
    const glFolder = `${process.cwd()}/src/graphLayerScripts`
    const loadedScript = (await loadScript([`${process.cwd()}/${scriptName}`]))[0]
    generationConfigs = loadedScript.default
  }

  // 2/ Exec the Generation Layers functions and prepend the sourceLayer
  const execConfigs = await generationConfigs()

  if (!execConfigs?.length) return []

  // option 1: parallel exec : each function do calculations on the source graph
  /*
  const execLayers = execFunctions?.length ?
      await Promise.all(execFunctions.map(s => s(modelLD)))
     : []
  */

  // aggregate the generated realms to add them to the source layer realm
  const sourceAddenum = new Set()

  // option 2: sequential exec : each function do calculations on "aggregated with previous layer graph"
  const execLayers = []
  let aggegatedGraph = JSON.parse(JSON.stringify(modelLD))
  for (const conf of execConfigs) {
    const {fn, options = {}} = conf
    // a) Execute the layer generator function
    const execResult = await fn(aggegatedGraph, options)
    const scriptLayer = !Array.isArray(execResult) ? [execResult] : execResult

    // b) Add layerRealms from conf object 
    scriptLayer.forEach(layer => layer.layerRealm.push(...conf.layerRealm))

    conf.layerRealm.forEach( l => sourceAddenum.add(l))

    // c) Add the current layer(s) to the global layer aggregator 
    layers.push(...scriptLayer)

    // d) Merge or not the current layer(s) with the main processed one
    const toAggregate_layers = scriptLayer.filter( layer => layer?.updateStrategy !== updateStrategies.remove)
    const toAggregate_graphs = toAggregate_layers.map(l => l.ld)
    aggegatedGraph = mergeLayers(aggegatedGraph, ...toAggregate_graphs)
  }

  layers.push(...execLayers)

  // update the source layer realms definition 
  sourceLayer.layerRealm.push(...sourceAddenum.values())

  // create context on missing ones and make the contexts ligths
  const cumulativeSourceContext = {}
  for (const l of layers) {
    if(l.ld['@context']) Object.assign(cumulativeSourceContext, l.ld['@context']) // @TODO: use a "mergeContext" function
    l.ld['@context'] = JSON.parse(JSON.stringify(cumulativeSourceContext)) 
    contextShaper(l.ld)
  }

  return layers
}
