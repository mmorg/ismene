import { readJson } from '@mmorg/fsutils'
import { globby } from 'globby'
import fs from 'fs'
import fileExists from '../../fsUtils/fileExists.js'
import chefDefaultConf from './chefDefaultConf.js'
import { sourceLayerRealm } from './graphLayersExec.js'
import updateLayers from './updateLayers.js'

const { exportFolder, layerFolder } = chefDefaultConf

export default async function exportCompacted({ parentFolder, targetFolder, layerReaml }) {

  const forceIncludeSource = true //@TODO: make it configurable

  // 1/ define the export folder
  let exportFolderForGraph
  if (parentFolder.length > 1) {
    if (!targetFolder) {
      throw new Error('When multiple parents folders are defined, a target folder with the -t option must be defined')
    }
    exportFolderForGraph = `${process.cwd()}/${targetFolder}/${exportFolder}`

  } else {
    exportFolderForGraph = `${process.cwd()}/${parentFolder[0]}/${exportFolder}`
  }
  // export folder exist or create it
  !await fileExists(exportFolderForGraph) ? await fs.promises.mkdir(exportFolderForGraph, { recursive: true }) : ''

  // 2/ manage the layers folders and get files (layers)
  const _layerFolder = parentFolder.map(pf => `${process.cwd()}/${pf}/${layerFolder}`)

  const layers = await Promise.all(_layerFolder.map(async lf => globby([lf + '/'])))
  const layersContent = layers.flat().map(readJson)

  // create Map by layerName
  const realmMap = layersContent.reduce((acc, v) => {
    v.layerRealm.forEach(realm => {
      let realmLayers = acc.get(realm)
      if (!realmLayers) {
        realmLayers = []
        if (forceIncludeSource && realm != sourceLayerRealm) realmLayers.push(...acc.get(sourceLayerRealm))
        acc.set(realm, realmLayers)
      }
      realmLayers.push(v)
    })
    return acc
  }, new Map())

  const fileBag = []
  for (const [realmKey, layers] of realmMap.entries()) {
    if (layerReaml) {
      if (!layerReaml.includes(realmKey)) continue
    }

    const [source, ...others] = layers
    const resultLayer = updateLayers(source, ...others)
    // @TODO: emit the "rdfx-graph:id_layer" entity inside the graph. 
    //  This entity bring properties like : 'name', 'layerRealm', 'fileName', ...
    const ldContent = resultLayer.ld
    fileBag.push({
      fileName: `${exportFolderForGraph}/gl-compact:${realmKey}.jsonld`,
      ldContent,
    })
  }

  return fileBag
}
