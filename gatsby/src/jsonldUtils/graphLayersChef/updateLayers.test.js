import { readJson } from '@mmorg/fsutils'
import findById from '../findById.js'
import updateLayers from './updateLayers.js'

// npm test -- --watch updateLayers.test

const ff = `${__dirname}/__fixtures__`
const skosSource = readJson(`${ff}/skos-1.2.0_gl_0.jsonld`)
const skosRemover = readJson(`${ff}/skos-1.2.0_gl_3.jsonld`)
const skosReplacer = readJson(`${ff}/skos-1.2.0_gl_5.jsonld`)
describe('Test on remove & remplace', () => {

  it('By default, add values to properties', () => {
    // remove the strategy from the "replacer" 
    const {name, layerRealm, ld} = skosReplacer
    const result_layer = updateLayers(skosSource, {name,layerRealm,ld})

    const member = findById(result_layer.ld, 'skos:member')
    expect(member.range).toStrictEqual(['_:b0_n0','skos:Collection', 'skos:Concept'])
  })

  it('Remove entities', () => {
    const layer = updateLayers(skosSource, skosRemover)
    expect(skosSource.ld.graph.length).toBe(38)
    expect(layer.ld.graph.length).toBe(14)
  })

  it('Replace a property value with the target one(s)', () => {
    const layer = updateLayers(skosSource, skosReplacer)
    const member = findById(layer.ld, 'skos:member')
    expect(member.range).toStrictEqual(['skos:Collection', 'skos:Concept'])
  })


})

