import { readJson } from '@mmorg/fsutils'
import filterByType from '../filterByType.js'
import findById from '../findById.js'
import graphLayersExec from './graphLayersExec.js'

// npm test -- --watch graphLayersExec.test

describe('Test on existing layerGenerator configuration', () => {

  let { layers, 
    heritanceLayer, classRmLayer,
    collectionRmLayer, collectionReplaceLayer,
  } = {}
  
  beforeAll(async () => {
    const staticFile = 'data/skos/skos-1.2.0.jsonld'
    const graphLD = readJson(staticFile)
    layers = await graphLayersExec('skosGENGenerator.js', graphLD)

    // console.log(layers)
    heritanceLayer = layers[2]
    classRmLayer = layers[3]
    collectionRmLayer = layers[4]
    collectionReplaceLayer = layers[5]
  })

  it('Manage the merge of layerRealm', async () => {
    // @TODO: make this test check "at least"
    expect(heritanceLayer.layerRealm).toStrictEqual(['domainRangeGuess', 'skos_1.3.0','ismene', 'gen_v1'])
  })

  it('Create a ligth context for each layer', async () => {
    const resultContext = heritanceLayer.ld['@context']
    // the target context should have ns like 'skos', 'owl' as there are used as refValues
    expect(resultContext.skos).toBeTruthy()

    expect(Object.keys(resultContext).length ).toBe(8)
  })

  it('Create good domain/range deduction for `skos:relatedMath` in `heritanceLayer`', () => {
    const target = {
      id: 'skos:relatedMatch',
      domain: ['skos:Concept'],
      range: ['skos:Concept'],
    }
    const patch = findById(heritanceLayer.ld, target.id)
    expect(patch).toStrictEqual(target)
  })

  it('Create good domain/range deduction for `skos:inScheme` in `heritanceLayer`', () => {
    const target = {
      id: 'skos:inScheme',
      domain: ['skos:ConceptScheme', 'rdfs:Resource', 'owl:Thing']
    }
    const patch = findById(heritanceLayer.ld, target.id)
    // console.log(patch)
    expect(patch).toStrictEqual(target)
  })

  it('Create a good Class & Properties remover layer', () => {
    // A/ test on Class remover
    const classes = filterByType(classRmLayer.ld, 'rdfs:Class')
    const properties = filterByType(classRmLayer.ld, 'rdf:Property')

    expect(classes.length).toBe(2)
    expect(properties.length).toBe(22)
    expect(classRmLayer.ld.graph.length).toBe(24)
  })

  it('Create a good skosCollection remplacement', () => {
    expect(collectionRmLayer.ld.graph.length).toBe(3)

    const graph = collectionReplaceLayer.ld.graph
    expect(graph.length).toBe(1)
  })


})

