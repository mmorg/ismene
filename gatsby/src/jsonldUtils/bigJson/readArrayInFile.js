import StreamArray from 'stream-json/streamers/StreamArray.js'
import fs from 'fs'

// source:  https://ckhconsulting.com/parsing-large-json-with-nodejs/
// parse big file with stream
// add promises in the game

// example of usage :rml/src/experiment/prepareElasticFromLheo.js
export default async function readArrayInFile(filePath, onDataFn, onEndFn) {

  return new Promise((resolve, reject) => {

    const jsonStream = StreamArray.withParser()

    //internal Node readable stream option, pipe to stream-json to convert it for us
    fs.createReadStream(filePath).pipe(jsonStream.input)

    //You'll get json objects here
    //Key is the array-index here
    jsonStream.on('data', async (datakv) => {
      try {
        await onDataFn(datakv, resolve, reject)
      } catch (e) {
        throw e
      }

    })

    jsonStream.on('end', async (datakv) => {
      await onEndFn(datakv, resolve, reject)
    })

  })

}
