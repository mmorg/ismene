import findById from './findById.js'

export default function unRef(property, entitySource, graph) {
  if (!entitySource[property]) return []
  return entitySource[property].map(id => findById(graph, id))
}
