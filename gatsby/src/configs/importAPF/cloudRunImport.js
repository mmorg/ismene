import { readJson } from '@mmorg/fsutils';

export default {
  graph: readJson('/apps/gatsby/data/genscan/genTerms-v1.1.0.jsonld'),
  ontology: readJson('/apps/gatsby/data/skos/__exports__/skos_X.X.X_gl-realm-compacted:generator_apiGenerator.jsonld'),
  url: 'https://ismene-api-gen-gsodvw6jdq-ew.a.run.app', 
}