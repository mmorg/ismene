
// @TODO: remove this file and pass token throw env vars. See: https://github.com/jdalrymple/gitbeaker
const gitlabConfig = {
  token: 'your_token',
  projectId: 1, // project number to set
}

export default gitlabConfig
