import remove from 'lodash.remove'
import prepare4GatsbyNodes_graph from '../jsonld2Nodes/prepare4GatsbyNodes_graph.js'

const baseFolder = `${process.cwd()}`
const dataFolder = `${baseFolder}/data`

// special case of "ontology node in source file": this collide with the one in -ontoModel
// gatsby merge is the "last called win", and the source one don't have indexes informations
const removeSourceOntology = (ldDoc) => {
  if (!ldDoc.graph) {
    console.warn('@TODO: no ldDoc.graph of this doc:', ldDoc, '\n ====================')
    return
  }
  const owlIndex = ldDoc.graph.findIndex(n => n.type && n.type.includes('owl:Ontology'))
  ldDoc.graph.splice(owlIndex, 1)
}

const addInScheme_2 = (entities, inScheme) => {
  entities.forEach(n => n.inScheme ? n.inScheme.push(inScheme) : n.inScheme = [inScheme])
}

// @TODO: find a way to express and not remove theses blank_node : add a ".type" and a namespaced "id" ?
const removeBlankNode = (ldDoc) => {
  const removed = remove(ldDoc.graph, n => {
    return !n.type && !n.id.includes(':')
  })

  console.log(removed.length, 'blank nodes removed')
}

const weightedTypes = ['rdfs:Class', 'rdf:Property']

// model of config :
/*
{ //
    path: `${dataFolder}/`,
    key: '',
    fn: (nodeName, ldDoc, _this) => {

    }

  },
*/
export default [

  { // gaia-x core
    path: `${dataFolder}/gaia-x/gx-core-ismene`,
    key: 'gx-core',
    fn: (nodeName, ldDoc, _this) => {
      // Remove 'owl:Ontology' in the source file (not in the model)
      if (!nodeName.startsWith(`${_this.key}-ontoModel`)) {
        removeSourceOntology(ldDoc)
      }

      const preNodes = prepare4GatsbyNodes_graph(ldDoc)

      // @TODO: check if this property still valid
      // addInScheme_2(preNodes, 'rdfe:menu_publish')
      return preNodes
    }
  },

  { // gaia-x framework
    path: `${dataFolder}/gaia-x/gx-framework-ismene`,
    key: 'gx-framework',
    fn: (nodeName, ldDoc, _this) => {
      // Remove 'owl:Ontology' in the source file (not in the model)
      if (!nodeName.startsWith(`${_this.key}-ontoModel`)) {
        removeSourceOntology(ldDoc)
      }

      const preNodes = prepare4GatsbyNodes_graph(ldDoc)

      // @TODO: check if this property still valid
      addInScheme_2(preNodes, 'rdfe:menu_hidden')
      return preNodes
    }
  },

  /** all of theses needs to be here because of on fligth properties generation. */

  { // oriane
    path: `${dataFolder}/oriane-rdf-release`,
    key: 'oriane',
    fn: (nodeName, ldDoc, _this) => {
      const preNodes = prepare4GatsbyNodes_graph(ldDoc)
      // @TODO: check if this property still valid
      addInScheme_2(preNodes, 'rdfe:menu_publish')
      return preNodes
    }
  },
  { // genTerms
    path: `${dataFolder}/genscan`,
    key: 'genTerms',
    fn: (nodeName, ldDoc, _this) => {
      const preNodes = prepare4GatsbyNodes_graph(ldDoc)
      // @TODO: check if this property still valid
      addInScheme_2(preNodes, 'rdfe:menu_publish')
      return preNodes
    }
  },
  { // openEmploi
    path: `${dataFolder}/openEmploi`,
    key: 'oep-ontology',
    fn: (nodeName, ldDoc, _this) => {
      // Remove 'owl:Ontology' in the source file (not in the model)
      if (!nodeName.startsWith(`${_this.key}-ontoModel`)) {
        removeSourceOntology(ldDoc)
      }

      // console.log('@TODO : a function that check the structure of the ld to get all properties as stdLd: check : gatsby/src/rml-profiles/orianeProdProfile.js')
      const preNodes = prepare4GatsbyNodes_graph(ldDoc, weightedTypes)

      // add "rdfe:menu_hidden" inscheme to not display
      // @TODO: check if still valid
      addInScheme_2(preNodes, 'rdfe:menu_hidden')
      return preNodes
    }
  },
  { // mm-ontology
    path: `${dataFolder}/mmOntology`,
    key: 'mm-ontology',
    fn: (nodeName, ldDoc, _this) => {
      // Remove 'owl:Ontology' in the source file (not in the model)
      if (!nodeName.startsWith(`${_this.key}-ontoModel`)) {
        removeSourceOntology(ldDoc)
      }

      // console.log('@TODO : a function that check the structure of the ld to get all properties as stdLd: check : gatsby/src/rml-profiles/orianeProdProfile.js')
      const preNodes = prepare4GatsbyNodes_graph(ldDoc, weightedTypes)

      // add "rdfe:menu_hidden" inscheme to not display
      // @TODO: check if still valid
      addInScheme_2(preNodes, 'rdfe:menu_hidden')
      return preNodes
    }
  },
  { // mnx Generic Model
    path: `${dataFolder}/mnxGenericModel`,
    key: 'mnx',
    fn: (nodeName, ldDoc, _this) => {
      // Remove 'owl:Ontology' in the source file (not in the model)
      if (!nodeName.startsWith(`${_this.key}-ontoModel`)) {
        removeSourceOntology(ldDoc)
      }

      // console.log('@TODO : a function that check the structure of the ld to get all properties as stdLd: check : gatsby/src/rml-profiles/orianeProdProfile.js')
      const preNodes = prepare4GatsbyNodes_graph(ldDoc, weightedTypes)

      // add "rdfe:menu_hidden" inscheme to not display
      // @TODO: check if still valid
      addInScheme_2(preNodes, 'rdfe:menu_hidden')
      return preNodes
    }
  },
  { // shacl
    path: `${dataFolder}/shacl`,
    key: 'shacl',
    fn: (nodeName, ldDoc, _this) => {
      // Remove 'owl:Ontology' in the source file (not in the model)
      if (!nodeName.startsWith(`${_this.key}-ontoModel`)) {
        removeSourceOntology(ldDoc)

        // add remove of un-namespaced blank nodes(like in datacube)
        removeBlankNode(ldDoc)
      }

      // console.log('@TODO : a function that check the structure of the ld to get all properties as stdLd: check : gatsby/src/rml-profiles/orianeProdProfile.js')
      const preNodes = prepare4GatsbyNodes_graph(ldDoc, weightedTypes)

      // add "rdfe:menu_hidden" inscheme to not display
      // @TODO: check if still valid
      addInScheme_2(preNodes, 'rdfe:menu_hidden')
      return preNodes
    }
  },
  { // dataCube
    path: `${dataFolder}/dataCube`,
    key: 'cube',
    fn: (nodeName, ldDoc, _this) => {
      // Remove 'owl:Ontology' in the source file (not in the model)
      if (!nodeName.startsWith(`${_this.key}-ontoModel`)) {
        removeSourceOntology(ldDoc)

        // add remove of un-namespaced blank nodes(like in datacube)
        removeBlankNode(ldDoc)
      }

      const preNodes = prepare4GatsbyNodes_graph(ldDoc, weightedTypes)

      // // special case, remove the last entity because of the "strange rdf:rest"
      // const i = preNodes.findIndex(n => n.id === 'n3-7')
      // preNodes.splice(i, 1)

      // add "rdfe:menu_hidden" inscheme to not display
      // @TODO: check if still valid
      addInScheme_2(preNodes, 'rdfe:menu_hidden')
      return preNodes
    }
  },
  { // skos
    path: `${dataFolder}/skos`,
    key: 'skos',
    fn: (nodeName, ldDoc, _this) => {
      // Remove 'owl:Ontology' in the source file (not in the model)
      if (!nodeName.startsWith(`${_this.key}-ontoModel`)) {
        removeSourceOntology(ldDoc)
      }
      // console.log('@TODO : a function that check the structure of the ld to get all properties as stdLd: check : gatsby/src/rml-profiles/orianeProdProfile.js')
      const preNodes = prepare4GatsbyNodes_graph(ldDoc, weightedTypes)
      // add "rdfe:menu_hidden" inscheme to not display
      // @TODO: check if still valid
      // console.log(preNodes)
      addInScheme_2(preNodes, 'rdfe:menu_hidden')
      return preNodes
    },
    ignore: ['**/__exports__/**', '**/__layers__/**']
  },
  { // rdf Definitions
    path: `${dataFolder}/rdf`,
    key: '22-rdf-syntax-ns',
    fn: (nodeName, ldDoc, _this) => {
      // Remove 'owl:Ontology' in the source file (not in the model)
      if (!nodeName.startsWith(`${_this.key}-ontoModel`)) {
        removeSourceOntology(ldDoc)
      }

      // console.log('@TODO : a function that check the structure of the ld to get all properties as stdLd: check : gatsby/src/rml-profiles/orianeProdProfile.js')
      const preNodes = prepare4GatsbyNodes_graph(ldDoc)
      // add "rdfe:menu_hidden" inscheme to not display
      // @TODO: check if still valid
      addInScheme_2(preNodes, 'rdfe:menu_hidden')
      return preNodes
    }
  },
  { // rdfs
    path: `${dataFolder}/rdfs`,
    key: 'rdf-schema',
    fn: (nodeName, ldDoc, _this) => {
      // Remove 'owl:Ontology' in the source file (not in the model)
      if (!nodeName.startsWith(`${_this.key}-ontoModel`)) {
        removeSourceOntology(ldDoc)
      }

      // console.log('@TODO : a function that check the structure of the ld to get all properties as stdLd: check : gatsby/src/rml-profiles/orianeProdProfile.js')
      const preNodes = prepare4GatsbyNodes_graph(ldDoc)

      // add "rdfe:menu_hidden" inscheme to not display
      // @TODO: check if still valid
      addInScheme_2(preNodes, 'rdfe:menu_hidden')
      return preNodes
    }
  },
  { // xmlDataTypes
    path: `${dataFolder}/xmlDataTypes`,
    key: 'dataTypeDefinitions',
    fn: (nodeName, ldDoc, _this) => {
      // console.log('@TODO : a function that check the structure of the ld to get all properties as stdLd: check : gatsby/src/rml-profiles/orianeProdProfile.js')
      const preNodes = prepare4GatsbyNodes_graph(ldDoc)
      // add "rdfe:menu_hidden" inscheme to not display
      // @TODO: check if still valid
      addInScheme_2(preNodes, 'rdfe:menu_hidden')
      return preNodes
    }
  },
  { // owl
    path: `${dataFolder}/owl`,
    key: 'owl',
    fn: (nodeName, ldDoc, _this) => {
      // Remove 'owl:Ontology' in the source file (not in the model)
      if (!nodeName.startsWith(`${_this.key}-ontoModel`)) {
        removeSourceOntology(ldDoc)
      }

      // console.log('@TODO : a function that check the structure of the ld to get all properties as stdLd: check : gatsby/src/rml-profiles/orianeProdProfile.js')
      const preNodes = prepare4GatsbyNodes_graph(ldDoc)

      // add "rdfe:menu_hidden" inscheme to not display
      // @TODO: check if still valid
      addInScheme_2(preNodes, 'rdfe:menu_hidden')
      return preNodes
    }
  },

]
