
const parsingConfs = {
  inokufu : {
    sources: ['**/data-smartskills-test/sources/inokufu*'],
    mappings: '**/data-smartskills-test/rml/inokufu.yaml',
    targetDirectory: ['sources', 'results'],
    profile: '**/data-smartskills-test/profile/smartSkillsProdProfile.js',
  },

  jobready : {
    sources: ['**/data-smartskills-test/sources/jobready_1*'],
    mappings: '**/data-smartskills-test/rml/jobready.yaml',
    targetDirectory: ['sources', 'results'],
    profile: '**/data-smartskills-test/profile/smartSkillsProdProfile.js',
  },

  orientoi : {
    sources: ['**/data-smartskills-test/sources/orientoi_1*'],
    mappings: '**/data-smartskills-test/rml/orientoi.yaml',
    targetDirectory: ['sources', 'results'],
    profile: '**/data-smartskills-test/profile/smartSkillsProdProfile.js',
  },
}

export default parsingConfs