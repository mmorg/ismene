import rdfContext from '../stdContext/rdfContext.js'
import rdfsContext from '../stdContext/rdfsContext.js'
import owlContext from '../stdContext/owlContext.js'
import dctContext from '../stdContext/dctContext.js'
import skosContext from '../stdContext/skosContext.js'
import mmoContext from '../stdContext/mmoContext.js'
import mnxContext from '../stdContext/mnxContext.js'
import oepContext from '../stdContext/oepContext.js'
import foafContext from '../stdContext/foafContext.js'
import dataCubeContext from '../stdContext/dataCubeContext.js'
import dataViewContext from '../stdContext/dataViewContext.js'
import siocContext from '../stdContext/siocContext.js'
import geopContext from '../stdContext/geopContext.js'
import provContext from '../stdContext/provContext.js'
import shaclContext from '../stdContext/shaclContext.js'
import omContext from '../stdContext/omContext.js'
import iiContext from '../stdContext/iiContext.js'
import schemaorgContext from '../stdContext/schemaorgContext.js'
import mmsContext from '../stdContext/mmsContext.js'
import oriContext from '../stdContext/oriContext.js'
import vsContext from '../stdContext/vsContext.js'
import escoContext from '../stdContext/escoContext.js'

export default [
  rdfContext,
  rdfsContext,
  owlContext,
  dctContext,
  skosContext,
  foafContext,
  dataCubeContext,
  dataViewContext,
  mmoContext,
  mnxContext,
  oepContext,
  siocContext,
  provContext,
  geopContext,
  oriContext,
  shaclContext,
  omContext,
  iiContext,
  schemaorgContext,
  mmsContext,
  vsContext,
  escoContext,
  
]
