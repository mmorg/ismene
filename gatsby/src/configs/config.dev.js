
const baseFolder = `${__dirname}/../..`
const contentsFolder = `${baseFolder}/contents`
const dataFolder = `${baseFolder}/data`

export default {

  // @TODO: update and remove unused
  // test with minimal, hand made entities.
  // @TODO : reorganise this folder to be on the last composition
  // modelsFolder: `${__dirname}/contents/models/entities`,

  // extract some entities from the generated mdx content
  modelsFolder: `${contentsFolder}/models/devtest1`,
  // test with prod folder
  // modelsFolder: `${baseFolder}/contents/models/devtest2generate`
  googleServiceAccount: `${__dirname}/dashemploi-google-service-account.json`,

  docsFolder: `${contentsFolder}/userGenerated`,

  fapFolder: `${contentsFolder}/fapFiles/imported/`,
  iscoFolder: `${contentsFolder}/isco/prod/`,
  // limit the number of generated content
  iscoTest: true,
  escoTest: true,
  ///// end TODO

  // @TODO: remove with the rdfSourceConfig
  hrrdfFolder: `${dataFolder}/hrrdf/`,

  // orianeFolder: `${dataFolder}/oriane-rdf-release`,

  nodefrFolder: `${dataFolder}/nodefr-2/`,

  // genScanFolder: `${dataFolder}/genscan`,

  // xmlDataTypesFolder: `${dataFolder}/xmlDataTypes`,

  // rdfDefinitionsFolder: `${dataFolder}/rdf`,

  // skosFolder: `${dataFolder}/skos`,

  // dataCubeFolder: `${dataFolder}/dataCube`,

  // owlFolder: `${dataFolder}/owl`,

  // rdfsFolder: `${dataFolder}/rdfs`,

  // mmoFolder: `${dataFolder}/mmOntology`,

}

export { baseFolder }
