import configDev from './config.dev.js'
import configProd from './config.prod.js'

const _confDev = 'dev'
const _confProd = 'prod'
const configVariables = getConf()

export {configVariables}

function getConf(){
  const env = process.env.APP_ENV || _confDev
  if(env === _confProd){
    return Object.assign({}, configDev, configProd);
  }
  return configDev
}