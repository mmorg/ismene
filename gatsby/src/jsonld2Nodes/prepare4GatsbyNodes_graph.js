import hash from 'object-hash'

const defaultWeightedTypes = ['Occupation', 'Skill', 'Concept']

export default function prepare4GatsbyNodes_graph(docLD, weightedTypes = defaultWeightedTypes) {
  // start preprocessing before adding to gatsby
  if (!docLD.graph) {
    console.warn('.graph property is empty, be carefull, no processing done')
    return []
  }

  return docLD.graph.map(e => {
    manageType(e, weightedTypes)
    addInternalInfo(e)
    return e
  })

}

function addInternalInfo(entity) {
  entity.internal = { type: entity.type, contentDigest: hash(entity) }
}

function manageType(entity, weightedTypes) {
  if(!entity.type){
    entity.type = ['special_case___no_type_defined_in_source__']
  }
  // 1/ manage to get only one type (based on weights). Save the type list in `.types`
  entity.types = entity.type
  let finded = false
  // first finded win
  for (const t of weightedTypes) {
    if (entity.type.includes(t)) {
      entity.type = t
      finded = true
      break
    }
  }
  if (!finded) { // assign the first one
    entity.type = entity.type[0]
  }

  // 2/ change the NS separator to an allowed gatsby char
  entity.type = entity.type.replace(':', '_')
}
