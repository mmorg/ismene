import { readJson, saveJson, readData } from "../fsUtils"
import prepare4GatsbyNodes from "./prepare4GatsbyNodes"
import turtle2jsonld from "../jsonldUtils/turtle2jsonld"
import escoPostProcess from "./escoPostProcess"
import frameEsco from "./frameEsco"

// docker-compose exec gatsby node --max-old-space-size=8192 -r esm src/jsonld2Nodes/runPrepareEsco.js

run().then(() => console.log('runned'))

async function run() {
    const target = '/app/contents/isco/prod/exportEsco-V0.2-prepared.jsonld'

    const escoTypes = ['Occupation', 'Skill', 'NodeLiteral']
    // 1/ frame esco and filter and export 
    // source reading from ttl : framing from full json-ld seems infinite
    const escoFile = '/app/contents/isco/source/esco_v1.0.3.ttl'
    const escoDoc = readData(escoFile)
    const doc = await turtle2jsonld(escoDoc)
    let escoFull = await frameEsco(doc, escoTypes)
    console.log('Frame 1 : ok')


    // 2/ frame the translations 
    const translations = readJson('/app/contents/isco/source/translatedDefinitions.json')
    const t = await frameEsco(translations, escoTypes)
    console.log('Frame 2 : ok')

    // 3/ merge them in esco, and reframe it 
    escoFull.results.push(...t.results)
    escoFull = await frameEsco(escoFull, escoTypes)
    console.log('Frame 3 : ok')

    // 4/ post processing
    escoPostProcess(escoFull)
    console.log('processing okay')
    saveJson(escoFull, target)
    console.log('save okay')
}