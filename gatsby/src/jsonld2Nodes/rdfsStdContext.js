
console.warn('@TODO: use this instead : import rdfsContext from "../stdContext/rdfsContext.js"')

export default (lang = 'en') => ({
    //@TODO: change results to graph
    results: '@graph',
    id: '@id',
    type: {
        '@id': '@type',
        '@container': '@set',
    },
    rdf: 'http://www.w3.org/1999/02/22-rdf-syntax-ns#',
    rdfs: 'http://www.w3.org/2000/01/rdf-schema#',
    skos: 'http://www.w3.org/2004/02/skos/core#',
    skt: 'http://purl.org/iso25964/skos-thes#',
    dct: 'http://purl.org/dc/terms/',
    nfr: 'https://gitlab.com/mmorg/nodefr-2/',
    vs: 'https://www.w3.org/2003/06/sw-vocab-status/ns/',
    sch: 'http://schema.org/',
    sh: 'https://www.w3.org/ns/shacl#',
    xsd: 'http://www.w3.org/2001/XMLSchema#',
    rdfe: 'https://rdfexplorer.competencies.be/model#',
    hrrdf: 'https://hrrdf.competencies.be/model#',

    // rdfs
    domain: {
        '@id': 'rdfs:domain',
        '@type': '@id',
        '@container': '@set',
    },
    range: {
        '@id': 'rdfs:range',
        '@type': '@id',
        '@container': '@set',
    },
    label: {
        '@id': 'rdfs:label',
        // '@language': lang,
        '@container': '@set'
    },
    comment: {
        '@id': 'rdfs:comment',
        // '@language': lang,
        '@container': '@set'
    },
    subClassOf: {
        '@id': 'rdfs:subClassOf',
        '@type': '@id',
        '@container': '@set'
    },
    // skos
    inScheme: {
        '@id': 'skos:inScheme',
        '@type': '@id',
        '@container': '@set',
    },
    notation: {
        '@id': 'skos:notation',
        '@container': '@set'
    },
    example: {
        '@id': 'skos:example',
        // '@language': lang,
        '@container': '@set'
    },
    scopeNote: {
        '@id': 'skos:scopeNote',
        // '@language': lang,
        '@container': '@set'
    },
    note: {
        '@id': 'skos:note',
        // '@language': lang,
        '@container': '@set'
    },
    prefLabel: { //this is a multilang field config to replicate
        '@id': 'skos:prefLabel',
        '@container': '@set',
    },

    // isAbstract
    isAbstract: 'sh:isAbstract',

    // vs
    status: {
        '@id': 'vs:status',
        // '@language': lang,
        '@container': '@set'
    },

    // shacl
    maxCount: {
        '@id': 'sh:maxCount',
        '@type': 'xsd:integer'
    },
    minCount: {
        '@id': 'sh:minCount',
        '@type': 'xsd:integer'
    },
    path: {
        '@id': 'sh:path',
        '@type': '@id',
        '@container': '@set'
    },

})
