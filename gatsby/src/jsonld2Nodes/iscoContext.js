
export default {
    "@version": 1.1,
    // ns declaration : 
    isco: 'http://data.europa.eu/esco/isco/',
    occupation: 'http://data.europa.eu/esco/occupation/',
    rome: 'https://rome.competencies.be/code/',
    esco: 'http://data.europa.eu/esco/model#',
    escoLit: 'http://data.europa.eu/esco/node-literal/',
    skos: 'http://www.w3.org/2004/02/skos/core#',

    // LD structure : remove @ and urls 
    id: "@id",
    type: {
        '@id': "@type",
        '@container': '@set'
    },
    results: '@graph',

    // type mapping : 
    Concept: 'http://www.w3.org/2004/02/skos/core#Concept',
    Occupation: 'http://data.europa.eu/esco/model#Occupation',
    Skill: 'http://data.europa.eu/esco/model#Skill',
    NodeLiteral: 'esco:NodeLiteral',
    PlainLiteral : 'http://www.w3.org/1999/02/22-rdf-syntax-ns#PlainLiteral',
    HTML: 'http://www.w3.org/1999/02/22-rdf-syntax-ns#HTML',

    language: 'esco:language',
    nodeLiteral: 'esco:nodeLiteral',
    // property mapping 
    inScheme: {
        '@id': 'http://www.w3.org/2004/02/skos/core#inScheme',
        '@type': '@id',
        '@container': '@set'
    },
    broader___NODE: {
        '@id': 'skos:broader',
        '@type': '@id',
        '@container': '@set'
    },
    topConceptOf: {
        '@id': 'skos:topConceptOf',
        '@type': '@id',
        '@container': '@set'
    },
    broaderTransitive___NODE: {
        '@id': 'skos:broaderTransitive',
        '@type': '@id',
        '@container': '@set'
    },
    // define narrower as children
    children: {
        '@id': 'http://www.w3.org/2004/02/skos/core#narrower',
        '@type': '@id',
        '@container': '@set'
    },

    // advanced mappings : 
    // 1/ for language : index by lang
    prefLabel: {
        '@id': 'http://www.w3.org/2004/02/skos/core#prefLabel',
        // '@container': ['@language', '@set'],
        '@container': ['@set'],
    },
    altLabel: {
        '@id': 'http://www.w3.org/2004/02/skos/core#altLabel',
        // '@container': ['@language', '@set'],
        '@container': ['@set'],
    },

    // 2/ for notation : only display the code and no more the @type & @value
    notation: {
        '@id': 'http://www.w3.org/2004/02/skos/core#notation',
        '@type': 'http://data.europa.eu/esco/Notation/ISCO08'
    },

    // esco specific mappings
    // for occupations
    relatedEssentialSkill___NODE: {
        '@id': 'esco:relatedEssentialSkill',
        '@type': '@id',
        '@container': '@set'
    },
    relatedOptionalSkill___NODE: {
        '@id': 'esco:relatedOptionalSkill',
        '@type': '@id',
        '@container': '@set'
    },
    skill: 'http://data.europa.eu/esco/skill/',
    occupation: 'http://data.europa.eu/esco/occupation/',
    description___NODE: {
        '@id': 'http://purl.org/dc/terms/description',
        '@type': '@id',
        '@container': '@set'
    },
    // for skills 
    isEssentialSkillFor___NODE: {
        '@id': 'esco:isEssentialSkillFor',
        '@type': '@id',
        '@container': '@set'
    },
    isOptionalSkillFor___NODE: {
        '@id': 'esco:isOptionalSkillFor',
        '@type': '@id',
        '@container': '@set'
    },
    skillType: {
        '@id': 'esco:skillType',
        '@type': '@id',
        '@container': '@set'
    }

}
