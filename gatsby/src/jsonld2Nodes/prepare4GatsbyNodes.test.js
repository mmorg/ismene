import prepare4GatsbyNodes from './prepare4GatsbyNodes.js'
import { readJson } from '@mmorg/fsutils'
import escoPostProcess from './escoPostProcess.js'
import frameEsco from './frameEsco.js'

// docker-compose exec gatsby npm test -- --watch prepare4GatsbyNodes

const iscoFixturePath = `${__dirname}/__fixtures__`

describe('simple ld data preparation for adding to gatsby', () => {
    let simpleLD, C0, C0110, C1111, C16, C17

    beforeAll(async () => {
        // test data preparation
        const simpleDoc = readJson(`${iscoFixturePath}/2-isco-nodes.jsonld`)
        simpleLD = await frameEsco(simpleDoc, ['Concept'])
        C0 = simpleLD.results[0]
        C0110 = simpleLD.results[3] //children as rome & esco
        C1111 = simpleLD.results[13]
        C16 = simpleLD.results[16] // occupation link
        C17 = simpleLD.results[17] // isco:link
    });

    it.todo('Extract this test to a new "frameEsco" test')
    it('Prepare ld data', async () => {
        expect(C0.children[1]).toBe('isco:C02')

        expect(C16.children).toEqual(['occupation:e9cf8b24-6b8b-47cc-9dd3-b601aa135960'])

        expect(C17.children).toEqual(['isco:C1120'])

        // no direct broader but a gatsby ref property
        expect(C17.broader).toBeFalsy()
        expect(C17.broader___NODE).toEqual(["isco:C11"])

        expect(C0110.children[0]).toBe('occupation:0b1d8dcb-d941-4d79-99bf-fc10c1806c43')
    })
})

describe('advanced ld to gatsby node processing', () => {

    it('deals with multitype', async () => {
        const multiTDoc = readJson(`${iscoFixturePath}/multiType.jsonld`)
        const multiTypeLD = await prepare4GatsbyNodes(multiTDoc)

        const [node1, node2] = multiTypeLD.results

        // console.log(multiTypeLD['@context'], node1)
        expect(node1.type).toBe('esco_Occupation')
    })

    it.todo('A mettre à jour suite aux nouvelles fonctions')
    it.skip('Deals with Occupation & Skill', async () => {
        const multiTDoc = readJson(`${iscoFixturePath}/exportEsco-V0.2-DEV.jsonld`)
        const typeFilter = ['Occupation', 'Skill', 'NodeLiteral']
        let multiTypeLD = await frameEsco(multiTDoc, typeFilter)
        multiTypeLD = await prepare4GatsbyNodes(multiTDoc)
        console.log(multiTypeLD)
        expect(multiTypeLD.results.length).toBe(2)

        // do test on occupation
        const occupation = multiTypeLD.results.find(n => n.id === 'occupation:35e67944-231e-4bc9-8c69-560f203167ff')
        // console.log(occupation)
        // console.log(occupation.description)
        expect(occupation.type).toBe('Occupation')
        expect(occupation.relatedEssentialSkill___NODE[0]).toBe('skill:440f64e5-e66c-45ee-bc29-53c14a2ad720')

        expect(occupation.relatedOptionalSkill___NODE[0]).toBe('skill:2ca8f0db-2c2a-4d07-af6c-effb99fed08b')

        // do test on skill
        const skill = multiTypeLD.results.find(n => n.id === 'skill:440f64e5-e66c-45ee-bc29-53c14a2ad720')
        // console.log(skill)
        // do test on esco-Literal
        const escoLit = multiTypeLD.results.find(n => n.id === 'escoLit:bb2bb8af-3700-450c-b617-672dae5658dd')
        console.log(escoLit)
        expect(escoLit.language).toBeTruthy()
        expect(escoLit.nodeLiteral).toBeTruthy()



        escoPostProcess(multiTypeLD)



        const enUS = multiTypeLD.results.find(n => n.id === 'escoLit:197b6ea0-c5b2-4988-9f21-7da937a41951')
        expect(enUS.language).toBe('en-us')


        console.log(multiTypeLD.results)
        const enUS0 = multiTypeLD.results.find(n => n.id === 'escoLit:114a7c3d-d595-46a9-ac0c-d74ab3408f4b')

        expect(escoLit.nodeLiteral.type).toBe('PlainLiteral')
        expect(enUS0.nodeLiteral.type).toBe('HTML')




        // a rajouter pour tester le filtre ::
        // // {
        // 			"id": "escoLit:114a7c3d-d595-FOR-TESt",
        // 			"type": "esco:NodeLiteral",
        // 			"esco:language": "en",
        // 			"esco:nodeLiteral": {
        // 				"type": "http://www.w3.org/1999/02/22-rdf-syntax-ns#HTML",
        // 				"@value": "Includes people performing medical research, <a resource=\"http://data.europa.eu/esco/skill/61bef658-d7f9-4759-b1b0-0f9cb3bc581a\" typeOf=\"http://data.europa.eu/esco/model#Skill\" href=\"http://data.europa.eu/esco/skill/61bef658-d7f9-4759-b1b0-0f9cb3bc581a\"><span property=\"http://www.w3.org/2000/01/rdf-schema#label\" xml:lang=\"en\">biotechnology</span></a>, biochemistry, pharmaceutical research."
        // 			}
        // 		}

    })
})