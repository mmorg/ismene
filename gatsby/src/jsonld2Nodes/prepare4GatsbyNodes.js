import hash from 'object-hash'

export default async function prepare4GatsbyNodes(docLD) {
  // start preprocessing before adding to gatsby
  if (!docLD.results) {
    console.warn('Framing result is empty, be carefull')
  } else {
    manageType(docLD)
    addInternalInfo(docLD)
  }

  return docLD
}

// assumption : all nodes here are presented as flat
function addInternalInfo(ldDoc) {
  ldDoc.results.forEach(
    n => n.internal = { type: n.type, contentDigest: hash(n) }
  )
}

function manageType(ldDoc) {
  // @TODO : export this weightedTypes to config parameter
  const weightedTypes = ['Occupation', 'Skill', 'Concept']

  ldDoc.results.forEach(n => {
    // 1/ manage to get only one type (based on weights)
    n.types = n.type
    let finded = false
    // first finded win
    for (const t of weightedTypes) {
      if (n.type.includes(t)) {
        n.type = t
        finded = true
        break
      }
    }
    if (!finded) { // assign the first one
      n.type = n.type[0]
    }

    // 2/ change the NS separator to an allowed gatsby char
    n.type = n.type.replace(':', '_')

  })
}
