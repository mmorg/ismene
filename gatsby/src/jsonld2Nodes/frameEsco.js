import jsonld from 'jsonld'
import iscoContext from "./iscoContext";

console.warn('@Deprecated: use the new framing structure based on stdld')
export default async function frameEsco( docLD, typeFilter ){

    // console.log('@TODO : restaure Concept type for isco framing')
    // console.log('@TODO : see for language structure change')
    const frame = {
        '@context': iscoContext,
        // options
        '@explicit': true,
        '@omitDefault': true,

        // filter
        type: typeFilter, //['Occupation', 'Skill', 'NodeLiteral'],
        // type:[ 'Concept'],
        // configurations
        children: {
            "@embed": "@never",
            '@omitDefault': true,
        },
        broader___NODE: {
            "@embed": "@never",
            '@omitDefault': true,
        },
        relatedEssentialSkill___NODE: {
            "@embed": "@never",
            '@omitDefault': true,
        },
        relatedOptionalSkill___NODE: {
            "@embed": "@never",
            '@omitDefault': true,
        },
        description___NODE: {
            "@embed": "@never",
            '@omitDefault': true,
            // nodeLiteral: { '@omitDefault': true }
        },

        topConceptOf: { '@embed': '@never', '@omitDefault': true },
        prefLabel: { '@value': {}, '@language': ['fr', 'en'] },
        altLabel: { '@value': {}, '@language': ['fr', 'en'] },
        broaderTransitive___NODE: { '@embed': '@never', '@omitDefault': true, },
        // narrower: { '@embed': '@never', '@omitDefault': true, },
        //
        skillType: { '@embed': '@never', '@omitDefault': true },
        language: { '@omitDefault': true },
        nodeLiteral: { '@omitDefault': true }

    }
    // frame a document
    return await jsonld.frame(docLD, frame);

}
