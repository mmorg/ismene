import jsonld from 'jsonld'
import rdfsStdContext from './rdfsStdContext.js'

// Transform the LD document into a compacted form
// @TODO: make the context more generic or at least configurable via parameters
export default async function compactRdfs(ldDoc, lang){

    // 1/ generate the target @context
    const gatsbyRdfsContext = rdfsStdContext(lang)

    // override the existing context with the std one
    // @TODO ? : manage something more fine-grained to prevent errors ?
    const mergedContext = Object.assign({}, ldDoc['@context'], gatsbyRdfsContext)

    return await jsonld.compact(ldDoc, mergedContext)
}
