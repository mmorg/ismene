import { readJson } from '@mmorg/fsutils'
import compactRdfs from './compactRdfs'

// docker compose exec gatsby npm test -- --watch compactRdfs

describe('dev test and fixes', () => {

  it('Generate a "prepared for gatsby" datastructure', async () => {
    const sourceLd = readJson(`${__dirname}/__fixtures__/nodefr2-full-0.4.0.jsonld`)
    const targetLd = await compactRdfs(sourceLd)

    // test on the source file. There is a "subClassOf" property
    const withSubClassSource = sourceLd.graph.find(n => n.id === 'nfr:CertificatBadgeDiplome')
    expect(withSubClassSource.subClassOf).toBeTruthy()
    const withSubClass = targetLd.results.find(n => n.id === 'nfr:CertificatBadgeDiplome')
    expect(withSubClass.subClassOf).toBeTruthy()

    // search for class and properties of nfr:Certification
    const certifId = 'nfr:Certification'
    const certifClass = targetLd.results.filter(n => n.id === certifId)

    const certifProps = targetLd.results.filter(n => n.domain && n.domain.includes(certifId))
    expect(certifClass.length).toBe(1)
    expect(certifProps.length).toBe(2)

    // check for "range targets not defined in the source model"
    // like xsd:string, sch:....
    const prop = targetLd.results.find(n => n.id === 'nfr:dateDHabilitation')
    expect(prop.range.length).toBe(2)
  })

  it('Keep unknow source definitions and override the default standardized ones', async () => {
    const sourceLd = readJson(`${__dirname}/__fixtures__/hrrdf-with-terms.jsonld`)
    const targetLd = await compactRdfs(sourceLd)

    const namespacedId = targetLd.results.find(n => n.id === 'hrrdf-terms:AccountBasedProductCodeList')
    expect(namespacedId).toBeTruthy()
  })

})
