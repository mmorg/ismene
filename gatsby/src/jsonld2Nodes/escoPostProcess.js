
export default function escoPostProcess(multiTypeLD){
    // do some escoPost-processing to get all properties to the same format
    multiTypeLD.results.forEach(node => {
        // clean languages : 
        // en-us is with `language: {
        //   type: 'http://www.w3.org/2001/XMLSchema#language',
        //   '@value': 'en-us'
        // },` but the other are with "fr" / 'en' ...
        if (node.language && node.language.type) node.language = node.language['@value']

        // clean node litteral as it could be a string of a node with type 
        // put all on node with PlainLiteral type 
        if(node.nodeLiteral && !node.nodeLiteral.type){
            node.nodeLiteral = {
                type : 'PlainLiteral',
                '@value' : node.nodeLiteral
            }
        }
        
    })
}