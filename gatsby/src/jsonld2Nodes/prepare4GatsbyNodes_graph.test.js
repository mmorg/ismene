import { readJson } from '@mmorg/fsutils'
import prepare4GatsbyNodes_graph from './prepare4GatsbyNodes_graph.js'

// docker-compose exec gatsby npm test -- --watch prepare4GatsbyNodes_graph

const iscoFixturePath = `${__dirname}/__fixtures__`

describe('advanced ld to gatsby node processing', () => {

  it('deals with multitype', async () => {
    const multiTDoc = readJson(`${iscoFixturePath}/multiType_graph.jsonld`)
    const multiTypeLD = await prepare4GatsbyNodes_graph(multiTDoc)

    const [node1, node2] = multiTypeLD

    expect(node1.type).toBe('esco_Occupation')
  })

})
