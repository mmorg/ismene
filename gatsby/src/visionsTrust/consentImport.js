import fetch from 'node-fetch'
import getJwtToken from './utils/getJwtToken.js'
import gLogs from './utils/gLogs.js'

/**
 * Receives the signed consent from Visions and makes the Data Export request
 *
 * The request body received looks like the following :
 * - serviceExportUrl {string} The data export endpoint of the EXPORT SERVICE
 * - signedCOnsent {string} The signed consent to send along with the export request
 *
 * Copyright Visions 2021
 * Original Authors: Felix Bole
 */
// modified by Florent André @mindmatcher.org

export default async function consentImport(req, res) {
  const { serviceExportUrl, signedConsent, dataImportUrl } = req.body

  // Request body validation
  if (serviceExportUrl != undefined && signedConsent != undefined) {

    // Send request to the EXPORT SERVICE's. This service will callback the data import
    const bodyjs = {
      signedConsent: signedConsent,
      dataImportUrl: dataImportUrl,
    }
    const result = await fetch(serviceExportUrl, {
      method: 'POST',
      headers: {
        'content-type': 'application/json',
        authorization: `Bearer ${getJwtToken()}`,
      },
      body: JSON.stringify(bodyjs)
    })
    const serviceExportResp = await result.json()
    const logContent = {
      endpoint: 'ConsentImport',
      message: 'ConsentImport: stage 1 : Consent Loading',
      serviceExportResp
    }
    if(serviceExportResp.error){
      Object.assign(logContent, {
        severity: 'ERROR',
        fetchParams : {
          url: serviceExportUrl,
          body: bodyjs,
        }
      })
    }

    gLogs(logContent)

    return res.status(200).json({
      success: true,
      message: "Data export request successfully sent to the export service."
    });

  }

  // last case : return error
  gLogs({
    severity: 'ERROR',
    endpoint: 'ConsentImport',
    message: 'ConsentImport: stage 1 : Consent Loading',
  })
  res.status(400).json({ error: "missing-body-param-error", message: "Missing parameters from the request body." })
}
