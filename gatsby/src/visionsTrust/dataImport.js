import yarrrml2rdf from '@mmorg/rml-processor/src/exec/yarrrml2rdf.js';
import { globby } from 'globby';
import fetch from 'node-fetch'
import { read } from 'to-vfile';
import parsingConfs from '../configs/parsingConfs.js';
import getJwtToken from './utils/getJwtToken.js'
import gLogs from './utils/gLogs.js';

/**
 * Receives the data from the EXPORT SERVICE
 *
 * Copyright Visions 2021
 * Original Authors: Felix Bole
 */
// modified by Florent André @mindmatcher.org

export default async function dataImport(req, res) {
  try {

    const { data, user, signedConsent } = req.body;

    console.log('@TODO: make this var as env')
    // const visionUrl = 'https://visionstrust.com/v1/consents/exchange/interop/verify'
    const visionUrl = 'https://staging.visionstrust.com/v1/consents/exchange/interop/verify'
    const verificationR = await fetch(visionUrl,{
      method: 'POST',
      headers: {
        'content-type': 'application/json',
        authorization: `Bearer ${getJwtToken()}`,
      },

      body: JSON.stringify({
        signedConsent,
      }),
      
    });

    const verification = await verificationR.json()
    gLogs({
      endpoint: 'DataImport',
      message: 'DataImport: stage 1 : Get the verification token',
      fetchParams: verification,
    },)

    const { dataImportUrl } = verification;

    // PROCESS INTEROP DATA AS YOU PLEASE
    const interopData = await transformData(data, dataImportUrl)

    const dataImportBody = {
      data: interopData,
      user,
      signedConsent,
    }

    gLogs({
      endpoint: 'DataImport',
      message: 'DataImport: stage 2 : call to dataImport',
      fetchParams: dataImportBody,
    },)

    // Send data to import service with a payload format matching the one
    // you received
    const dataImportR = await fetch(dataImportUrl,{
      method: 'POST',
      headers: {
        'content-type': 'application/json',
        authorization: `Bearer ${getJwtToken()}`,
      },
      body: JSON.stringify(dataImportBody),
    });

    const dataImportResponse = await dataImportR.json()
    gLogs({
      endpoint: 'DataImport',
      message: 'DataImport: stage 2 : response of dataImport',
      fetchParams: dataImportResponse,
    })

    // Send ok Response to requester
    res.status(200).json({ message: 'Data received and processed' });

  } catch (error) {
    gLogs({
      severity: 'ERROR',
      endpoint: 'DataImport',
      message: 'DataImport: stage x : error during processing',
      errorContent: error.stack,
    })
    res.status(400).json({ error: 'data-import-error', message: 'No data imported' })
  }

}

  // See /rml endpoint to run tests
async function transformData(data, dataImportUrl){

  // @TODO: create a test from dataImportUrl and transform `data` in vFile

  // this is a random implementation on the returned data

  console.log('import from:',dataImportUrl)

  gLogs({
    endpoint: 'DataImport',
    message: `@TODO: update to dynamic import. This is random from ${dataImportUrl}`,
  },)

  const keys = Object.keys(parsingConfs)
  const position = Math.floor(Math.random() * keys.length)
  const randomConf = parsingConfs[keys[position]]

  // testing data as stream 
  const vfiles = await fromGlobby(randomConf.sources)
  randomConf.sources = vfiles

  const ld = await yarrrml2rdf(randomConf)

  return ld
}

async function fromGlobby(globs){
  const paths = await globby(globs)
  return Promise.all( paths.map( p => read(p, 'utf8')))
}