
const { Console } = console
const gConsole = new Console({ stdout: process.stdout, stderr: process.stderr })

// @TODO: review this doc to attach logs to the source request: 
//  https://cloud.google.com/run/docs/logging


export default function gLogs(logs, req, prio){

  const defaultConf = {
    severity: 'DEBUG',
  }

  const log = Object.assign(defaultConf, logs)

  gConsole.log(JSON.stringify(log))
  
}