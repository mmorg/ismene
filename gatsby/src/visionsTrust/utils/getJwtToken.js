import jwt from 'jsonwebtoken'
import config from '../config/config.js'


export default function getJwtToken() {
  // const d = new Date()
  // const now = d.getTime()
  
  const serviceKey = config.SERVICE_KEY
  const secretKey = config.SERVICE_SECRET_KEY
  let token = jwt.sign(
    {
      serviceKey,
      iat: (new Date().getTime())
    },
    secretKey,
    { expiresIn: 5 * 60 }
  )
  return token

}