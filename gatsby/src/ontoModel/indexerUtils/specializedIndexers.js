import entityOrSchemeIndexer from './entityOrSchemeIndexer.js'

export function classOrSchemeIndexer(sourceLd, ontologyPrefix) {
  const entityType = 'rdfs:Class'
  return entityOrSchemeIndexer(entityType, sourceLd, ontologyPrefix)
}

export function propertyOrSchemeIndexer(sourceLd, ontologyPrefix) {
  const entityType = 'rdf:Property'
  return entityOrSchemeIndexer(entityType, sourceLd, ontologyPrefix)
}

export function conceptOrSchemeIndexer(sourceLd, ontologyPrefix) {
  const entityType = ['skos:Concept', 'skos:Collection']
  return entityOrSchemeIndexer(entityType, sourceLd, ontologyPrefix)
}

export function datatypeOrSchemeIndexer(sourceLd, ontologyPrefix) {
  const entityType = 'rdfs:Datatype'
  return entityOrSchemeIndexer(entityType, sourceLd, ontologyPrefix)
}
