import filterBy from '../jsonldUtils/filterBy.js'
import filterByType from '../jsonldUtils/filterByType.js'
import findById from '../jsonldUtils/findById.js'
import ontoIndexer from './ontoIndexer.js'

// cause of ESM war : should be in type module

// docker-compose exec gatsby npm run testESM -- --watch ontoIndexer.test

describe('Test on Oriane with no schemes', () => {

  let ontology, ontoModel
  beforeAll(async () => {
    // expect('imp').toBe(true)
    const testFiles = '**/oriane-rdf-release/*.jsonld'
    const conf = {
      ontoFiles: testFiles
    }

    const index = await ontoIndexer(conf)
    ontology = index.ontology
    ontoModel = index.ontoModel
  })

  it('Create index for Class and Property', async () => {

    // get classes, they should be indexed now
    const classes = filterByType(ontology, 'rdfs:Class')
    expect(classes).toMatchSnapshot()

    // // get properties, they should be indexed
    const properties = filterByType(ontology, 'rdf:Property')
    expect(properties).toMatchSnapshot()

    // get Concepts, index should be on scheme or collection
    const concepts = filterByType(ontology, 'skos:Concept')
    expect(concepts.length).toBe(19)

    // check the global ontoModel
    expect(ontoModel).toMatchSnapshot()
  })

  it('Create index for skos:Collection', () => {
    const owlOntology = findById(ontoModel, 'ori:Ontologie')
    const omOntoIndex = findById(ontoModel, 'ori:defaultIndex_Term_2')
    const skosCollection = filterByType(ontology, 'skos:Collection')
    // console.log(owlOntology)
    // console.log(omOntoIndex)
    expect(omOntoIndex).toMatchSnapshot()
    // console.log(skosCollection)

    const defaultSchemeId = 'ori:scheme_for_skos:Concept_&_skos:Collection_generated'
    const defaultScheme = findById(ontology, defaultSchemeId)
    expect(defaultScheme).toMatchSnapshot()

    const areInDefaultScheme = filterBy('inScheme', defaultSchemeId, ontology)
    // console.log(areInDefaultScheme)
    expect(areInDefaultScheme.length).toBe(0)

  })

})

describe('Test on hrrdf files (with -shapes- and Classes schemas', () => {

  it('Parse the file', async () => {
    const testFiles = '**/data/hrrdf/*.jsonld'
    const conf = {
      ontoFiles: testFiles
    }

    const { ontology, ontoModel } = await ontoIndexer(conf)

    const classIndex = findById(ontoModel, 'hrrdf:defaultIndex_Class_0')
    expect(classIndex).toMatchSnapshot()

  })

})

describe('Test on files with an existing owl:Ontology', () => {

  it('Rdfs: Merge the source ontology node with the source one & test new Datatype index', async () => {
    const testFiles = '**/data/rdf/*.jsonld'
    const conf = {
      ontoFiles: testFiles,
    }

    const { ontology, ontoModel } = await ontoIndexer(conf)

    // console.log(ontoModel.graph[0])
    expect(ontoModel.graph[0]).toMatchSnapshot()

    const datatypeIndex = findById(ontoModel, 'rdf:defaultIndex_Datatype_3')
    // console.log(datatypeIndex)
    expect(datatypeIndex).toMatchSnapshot()

  })

  it('Shacl: Merge the source owl:Ontology and keep properties in ontoModel one', async () => {
    const testFiles = '**/data/shacl/*.jsonld'
    const conf = {
      ontoFiles: testFiles,
    }

    const { ontology, ontoModel } = await ontoIndexer(conf)

    // console.log(ontoModel.graph[0])
    expect(ontoModel.graph[0]).toMatchSnapshot()
  })

})

describe('Test available indirections in script', () => {
  it('Can add external entities after processing and before save with `merge level1 of objects` strategy ', async () => {
    const testFiles = '**/data/oriane-rdf-release/*.jsonld'
    const conf = {
      ontoFiles: testFiles,
      externalEntities: 'oriGenerateContribution.js'
    }

    const { ontology, ontoModel } = await ontoIndexer(conf)

    const ontoEntity = ontoModel.graph[0]
    // console.log(ontoEntity)
    expect(ontoEntity.sch_contributor).toMatchSnapshot()

    // console.log(ontoModel.graph)
    expect(ontoModel.graph.length).toBe(4 + 2 * 12) // 4 is minimum + 2 x nb_contributors
  })
})
