import { classOrSchemeIndexer, conceptOrSchemeIndexer, datatypeOrSchemeIndexer, propertyOrSchemeIndexer } from './indexerUtils/specializedIndexers.js'


// !!!!!!!!!!!!!!!! New icons have to be added to : gatsby/src/components/dashorm/menuHelpers/menuBuilderUtils/node2menuItem.js
const defaultIndexes = [
  {
    id: 'ii:defaultIndex_Class',
    type: ['om:OntoIndex'],
    title: [
      {
        '@language': 'fr',
        '@value': 'Les classes'
      },
      {
        '@language': 'en',
        '@value': 'Classes'
      }
    ],
    icon: "BubbleChart",
    indexer: classOrSchemeIndexer,
  },
  {
    id: 'ii:defaultIndex_Property',
    type: ['om:OntoIndex'],
    title: [
      {
        '@language': 'fr',
        '@value': 'Les propriétés'
      }
    ],
    icon : "DeviceHub",
    indexer: propertyOrSchemeIndexer,
  },
  {
    id: 'ii:defaultIndex_Term',
    type: ['om:OntoIndex'],
    title: [
      {
        '@language': 'fr',
        '@value': 'Les référentiels'
      }
    ],
    icon : "AccountTree",
    indexer: conceptOrSchemeIndexer,
  },
  {
    id: 'ii:defaultIndex_Datatype',
    type: ['om:OntoIndex'],
    title: [
      {
        '@language': 'fr',
        '@value': 'Les Datatypes'
      }
    ],
    icon : "DataObject",
    indexer: datatypeOrSchemeIndexer,
  }
]

export default defaultIndexes
