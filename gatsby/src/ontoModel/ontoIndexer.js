import fs from 'fs'
import { readJson, saveJson } from '@mmorg/fsutils'
import { globby } from 'globby'
import * as nodePath from 'path'
import fsPromises from 'fs/promises'
import defaultIndexes from './defaultIndexes.js'
import filterByType from '../jsonldUtils/filterByType.js'
import chalk from 'chalk'
import toStdLD from '../jsonldUtils/stdldUtils/toStdLD.js'
import remove from 'lodash.remove'
import multiOntologiesContext from '../configs/multiOntologiesContext.js'
import loadScript from '../importUtils/loadScript.js'
import graphLayersExec from '../jsonldUtils/graphLayersChef/graphLayersExec.js'
import fileExists from '../fsUtils/fileExists.js'
import chefDefaultConf from '../jsonldUtils/graphLayersChef/chefDefaultConf.js'

const ontoBagTemplate = {
  ontology: {
    version: null,
    baseName: null,
    dirName: null,
    path: null,
  },
  ontoModel: {
    version: null,
    baseName: null,
    dirname: null,
    path: null,
  }
}

//@TODO: extract all this "Bag management in the "graphUtils" folder
const isOntoModel = (baseName) => baseName.includes('ontoModel')
const isNotSourceFile = (baseName) => baseName.includes('shapes')
const getVersionAndOntoName = (baseName) => {
  const matches = /(.*)-(\d*\.\d*\.\d*)/g.exec(baseName)
  if (!matches) {
    console.log(chalk.red('No version number can be extracted from:'), baseName)
    throw new Error('No version number')
  }
  // matches[0] // this is the full name with version
  const ontoName = matches[1]
  const version = matches[2]
  // console.log(matches, '==========')
  // throw new Error()
  return { ontoName, version }
}

const initBag = (path) => {
  const ontoBag = JSON.parse(JSON.stringify(ontoBagTemplate))
  path.forEach(p => {
    const baseName = nodePath.basename(p, '.jsonld')
    const vAndo = getVersionAndOntoName(baseName)
    const dirName = nodePath.dirname(p)
    const path = p

    if (isOntoModel(baseName)) {
      ontoBag.ontoModel = { ...vAndo, baseName, dirName, path }
      return
    }
    if (isNotSourceFile(baseName)) {
      return
    }

    // else case : this is the main file
    ontoBag.ontology = { ...vAndo, baseName, dirName, path }

  })
  return ontoBag
}

export default async function ontoIndexer(argv) {
  const {
    ontoFiles,
    updateFiles = false,
    externalEntities = null,
    graphLayers = null,
  } = argv

  let ontologyFiles = await globby([ontoFiles])

  const ontoBag = initBag(ontologyFiles)

  if (!ontoBag.ontology.baseName) {
    console.log(chalk.red('File catched by globby:', ontologyFiles))
    throw new Error('Ontology file not detected')
  }
  if (!ontoBag.ontoModel.baseName) {
    console.log(chalk.red('File catched by globby:', ontologyFiles))
    throw new Error('Ontology model is not detected')
  }

  const { ontology, ontoModel } = ontoBag

  // basic check
  if (!ontology.path || !ontoModel.path) {
    throw new Error('ontology bag required info not found')
  }

  // if version mis-match
  if (ontology.version != ontoModel.version) {
    console.log('update the file version')
    const newPath = ontoModel.path.replace(ontoModel.version, ontology.version)
    await fsPromises.rename(ontoModel.path, newPath)
    ontoModel.path = newPath
  }

  const ontoLD = readJson(ontology.path)
  const modelLD = readJson(ontoModel.path)


  // 1/ get the ontology node : from ontoModel and source
  const ontologyType = 'owl:Ontology'
  const ontologyFromModel = filterByType(modelLD, ontologyType)
  const ontologyFromSource = filterByType(ontoLD, ontologyType)

  // check
  if (ontologyFromModel.length > 1 || ontologyFromSource.length > 1) {
    throw new Error('More than 1 ontology node in the model or source file')
  }

  let ontologyEntity = null
  if (ontologyFromModel.length && ontologyFromSource.length) {
    // merge the `source` one into the `model` one
    ontologyEntity = Object.assign(ontologyFromModel[0], ontologyFromSource[0])
  } else {
    if (!ontologyFromModel.length) {
      throw new Error('No ontology defined in model is to implement')
    } else {
      ontologyEntity = ontologyFromModel[0]
    }
  }

  ontologyModelIndexAndMerge(ontologyEntity, modelLD, ontoLD)
  await addExternalEntities(externalEntities, modelLD, argv)

  console.warn(chalk.red('@TODO: export this ontology info management as a "GraphLayer"'))

  // do a compaction get a clean Ontology graph
  const compactedOntoLd = await toStdLD(ontoLD, multiOntologiesContext, true)

  // GraphLayers generation from indirections
  const layers = await graphLayersExec(graphLayers, compactedOntoLd)

  if (updateFiles) {
    saveJson(ontoLD, ontology.path)
    saveJson(modelLD, ontoModel.path)
    await saveLayers(layers, ontology)
  }

  return {
    ontology: ontoLD,
    ontoModel: modelLD,
  }
}

async function saveLayers(layers, sourceOntoBag) {

  console.log('===> @TODO: manage update and override of layers')
  // 1/ load existing layers
  // 2/ create index of existing,
  // 3/ run diff on previous & next
  // 4/ reconciliate the diff ==> smartDiff ??
  // 5/ if diff :
  /*
    - message
    - save the previous value and save the new version
  */
  // ==> @TODO:END


  const layerFolderName = chefDefaultConf.layerFolder

  const layerFolderPath = `${sourceOntoBag.dirName}/${layerFolderName}`

  // folder exists or create it ?
  !await fileExists(layerFolderPath) ? await fs.promises.mkdir(layerFolderPath, { recursive: true }) : ''

  // save the layers as it comes :
  layers.forEach((l, i) => {
    const layerName = `${layerFolderPath}/${sourceOntoBag.ontoName}-${sourceOntoBag.version}_gl_${i}.jsonld`
    saveJson(l, layerName)
  })

}

function ontologyModelIndexAndMerge(ontologyEntity, modelLD, ontoLD) {
  const ontologyPrefix = ontologyEntity.id.split(':')[0]

  // 2/ init the ontologyModel in accordance to the instructions to add the "index" property
  const targetIndex = ontologyEntity.instruction.map((instructionId, i) => {
    const instructionIndex = getOntoIndex(instructionId)
    if (!instructionIndex) return null

    // use object.assign to keep the indexer function
    const newIndex = Object.assign({}, instructionIndex)
    // create a unique id
    const instructionPath = instructionId.split(':')[1]
    newIndex.id = `${ontologyPrefix}:${instructionPath}_${i}`
    // console.log(instructionIndex)
    return newIndex
  })

  // update the record of each ontoIndex with the indexer execution
  targetIndex.forEach(ontoIndex => {
    if (!ontoIndex.indexer) {
      console.warn('no indexer for this ontoIndex', ontoIndex.id)
      return
    }

    const records = ontoIndex.indexer(ontoLD, ontologyPrefix)

    ontoIndex.records = records

  })

  // update the modelLD with index properties and indexes entities
  const indexIds = targetIndex.map(i => i.id)
  ontologyEntity.index = indexIds

  // Build the target ontology model
  // --> remove owl:Ontology & om:OntoIndex from the source
  remove(modelLD.graph, (n) => n.type && (n.type.includes('owl:Ontology') || n.type.includes('om:OntoIndex')))

  // --> build the new graph
  modelLD.graph = [ontologyEntity, ...targetIndex, ...modelLD.graph]

}

const defaultOptions = {
  updateFiles: false
}

async function addExternalEntities(externalEntities, modelLD, options) {
  const _options = Object.assign({}, defaultOptions, options)
  const extEnt = []
  if (externalEntities) {
    const indirectionFolder = `${process.cwd()}/src/renovationScripts`
    const indirection = (await loadScript([`${indirectionFolder}/${externalEntities}`]))[0].default
    extEnt.push(...(await indirection(_options)))
  }

  // merge strategy = merge externals to existing when they actually exist
  for (const ext of extEnt) {
    const exist = modelLD.graph.find(n => n.id === ext.id)
    if (exist) {
      Object.assign(exist, ext)
      continue
    }
    modelLD.graph.push(ext)
  }
}


function getOntoIndex(instructionId) {
  const found = defaultIndexes.filter(e => e.id === instructionId)
  if (!found.length) {
    console.warn('No default instruction index with this id', instructionId)
    return null
  }
  if (found.length > 1) {
    console.warn('More than one instruction index find with this id', instructionId, '==> review the defaultIndexes definition')
  }

  return found[0]
}
