import type {Config} from 'jest'

const config: Config = {
  transform: {
    '^.+\\.[jt]sx?$': '<rootDir>/jestconfs/jest-preprocess.js',
  },
  moduleNameMapper: {
    ".+\\.(css|styl|less|sass|scss)$": `identity-obj-proxy`,
    ".+\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$": `<rootDir>/__mocks__/file-mock.js`,
  },
  watchPathIgnorePatterns: ['<rootDir>/node_modules','<rootDir>/public', '<rootDir>/\\.cache'],
  testPathIgnorePatterns: [`node_modules`, `\\.cache`, `<rootDir>.*/public`],
  transformIgnorePatterns: ['node_modules/(?!('
    + 'gatsby'
    + '|gatsby-script'
    + '|gatsby-link'
    + '|@mmorg/fsutils'
    + '|jsonld'
    + '|@digitalbazaar/http-client'
    + '|documentation'
    + '|unified'
    + '|remark-parse'
    + '|remark-slug'
    + '|remark-toc'
    + '|remark-rehype'
    + '|rehype-highlight'
    + '|unist-util-inspect'
    + '|bail'
    + '|is-plain-obj'
    + '|trough'
    + '|vfile'
    + '|vfile-message'
    + '|unist-util-inspect'
    + '|unist-util-map'
    + '|unist-builder'
    + '|unist-util-position'
    + '|unist-util-generated'
    + '|unist-util-visit'
    + '|unist-util-visit-parents'
    + '|unist-util-is'
    + '|unist-util-find-after'
    + '|unist-util-stringify-position'
    + '|micromark'
    + '|micromark-util-combine-extensions'
    + '|micromark-util-chunked'
    + '|micromark-factory-space'
    + '|micromark-util-character'
    + '|micromark-core-commonmark'
    + '|micromark-util-classify-character'
    + '|micromark-util-resolve-all'
    + '|decode-named-character-reference'
    + '|character-entities'
    + '|micromark-util-subtokenize'
    + '|micromark-factory-destination'
    + '|micromark-factory-label'
    + '|micromark-factory-title'
    + '|micromark-factory-whitespace'
    + '|micromark-util-normalize-identifier'
    + '|micromark-util-character'
    + '|micromark-util-html-tag-name'
    + '|micromark-util-decode-numeric-character-reference'
    + '|micromark-util-decode-string'
    + '|micromark-util-normalize-identifier'
    + '|micromark-util-sanitize-uri'
    + '|micromark-util-character'
    + '|micromark-util-encode'
    + '|mdast-util-to-string'
    + '|mdast-util-to-hast'
    + '|mdast-util-definitions'
    + '|mdast-util-from-markdown'
    + '|mdast-util-toc'
    + '|hast-util-whitespace'
    + '|hast-util-to-text'
    + '|hast-util-is-element'
    + '|hast-to-hyperscript'
    + '|remark-slug'
    + '|remark-github'
    + '|remark-toc'
    + '|remark-rehype'
    + '|remark-rehype'
    + '|remark-parse'
    + '|rehype-highlight'
    + '|rehype-react'
    + '|lowlight'
    + '|fault'
    + '|property-information'
    + '|space-separated-tokens'
    + '|comma-separated-tokens'
    + '|web-namespaces'
    + '|trim-lines'
    + '|globby'
    + '|slash'
    + '|to-vfile'
    // @mmorg lib and dependencies
    + '|@mmorg/rml-processor'
    + '|array-union'
    + '|p-map'
    + '|aggregate-error'
    + '|indent-string'
    + '|clean-stack'
    + '|escape-string-regexp'
    + '|rml-integrated'

    + ')/)'],
  globals: {
    __PATH_PREFIX__: ``,
  },
  // testEnvironmentOptions: {
  //   url: `http://localhost`,
  // },
  // testEnvironment: 'jsdom',
  setupFiles: [`<rootDir>/jestconfs/loadershim.js`],
}

export default config