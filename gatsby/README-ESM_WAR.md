
* suite à l'intégration de gatsby-ts, un problème reste pour l'intégration de modules externes "pur esm"
* Jest fait face au même problème 

# Gatsby 

* l'erreur qui apparait dans ce cas est: 
```
TypeError: Cannot read properties of undefined (reading 'default')
```

2 options de résolution 
## 1/ cas général

* le fichier à modifier est : [voir le fichier](./node_modules/gatsby/dist/utils/parcel/compile-gatsby-files.js)
- il faut rajouter le nom du package en ligne 62

* pour sauvegarde de la modification: 
```

```

## 2/ le cas de p-map 

* la méthode classique ne fonctionne pas. Je ne vois pas pourquoi. Faire un test minimal de reproduction a partir de gatsby-ts



# JEST : 

@TODO: document