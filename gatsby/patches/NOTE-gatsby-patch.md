
* cet élément de patch est supprimé du diff, car je ne vois pas du tout à quoi ça correspond
```
diff --git a/node_modules/gatsby/latest-apis.json b/node_modules/gatsby/latest-apis.json
index 3aa35fe..d9b6c6a 100644
--- a/node_modules/gatsby/latest-apis.json
+++ b/node_modules/gatsby/latest-apis.json
@@ -32,8 +32,8 @@
     "createPagesStatefully": {},
     "sourceNodes": {},
     "onCreateNode": {},
-    "shouldOnCreateNode": {
-      "version": "5.0.0"
+    "unstable_shouldOnCreateNode": {
+      "version": "2.24.80"
     },
     "onCreatePage": {},
     "setFieldsOnGraphQLNodeType": {},
@@ -63,7 +63,6 @@
   "features": [
     "image-cdn",
     "graphql-typegen",
-    "content-file-path",
-    "slices"
+    "content-file-path"
   ]
 }
\ No newline at end of file
```