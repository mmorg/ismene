import { defineConfig } from 'vite';
import react from '@vitejs/plugin-react';
import viteTsconfigPaths from 'vite-tsconfig-paths';
import svgrPlugin from 'vite-plugin-svgr';

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react(), viteTsconfigPaths(), svgrPlugin()],
  build: {
    lib: {
      name: '@mmorg/rdfx-refine',
      entry: 'src/export-utils.ts',
      formats: ['es', 'cjs']
    }
  }
});