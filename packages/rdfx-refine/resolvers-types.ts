import { GraphQLResolveInfo, GraphQLScalarType, GraphQLScalarTypeConfig } from 'graphql';
export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
export type MakeEmpty<T extends { [key: string]: unknown }, K extends keyof T> = { [_ in K]?: never };
export type Incremental<T> = T | { [P in keyof T]?: P extends ' $fragmentName' | '__typename' ? T[P] : never };
export type RequireFields<T, K extends keyof T> = Omit<T, K> & { [P in K]-?: NonNullable<T[P]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: { input: string | number; output: string; }
  String: { input: string; output: string; }
  Boolean: { input: boolean; output: boolean; }
  Int: { input: number; output: number; }
  Float: { input: number; output: number; }
  JSON: { input: any; output: any; }
};

export type Accreditation = {
  __typename?: 'Accreditation';
  id: Scalars['ID']['output'];
};

export type Accreditation_CreateInput = {
  id: Scalars['ID']['input'];
};

export type Accreditation_UpdateInput = {
  id: Scalars['ID']['input'];
};

export type AssociationObject = {
  __typename?: 'AssociationObject';
  id: Scalars['ID']['output'];
  target?: Maybe<Array<Resource>>;
  targetDescription?: Maybe<Array<Literal_Rdfs>>;
  targetFramework?: Maybe<Array<Standard>>;
  targetName?: Maybe<Array<Literal_Rdfs>>;
  targetNotation?: Maybe<Array<Literal_Rdfs>>;
  targetURL?: Maybe<Array<AnyUri>>;
};

export type AssociationObject_CreateInput = {
  id?: InputMaybe<Scalars['ID']['input']>;
  targetURL?: InputMaybe<Array<Scalars['String']['input']>>;
};

export type AssociationObject_UpdateInput = {
  targetURL?: InputMaybe<Array<Scalars['String']['input']>>;
};

export type AwardingActivity = {
  __typename?: 'AwardingActivity';
  id: Scalars['ID']['output'];
};

export type AwardingActivity_CreateInput = {
  id: Scalars['ID']['input'];
};

export type AwardingActivity_UpdateInput = {
  id: Scalars['ID']['input'];
};

export type AwardingBody = {
  __typename?: 'AwardingBody';
  id: Scalars['ID']['output'];
};

export type AwardingBody_CreateInput = {
  id: Scalars['ID']['input'];
};

export type AwardingBody_UpdateInput = {
  id: Scalars['ID']['input'];
};

export type Collection = {
  __typename?: 'Collection';
  altLabel?: Maybe<Array<Literal_Rdfs>>;
  definition?: Maybe<Array<String_Xsd>>;
  id: Scalars['ID']['output'];
  member?: Maybe<Array<Collection>>;
  memberOf?: Maybe<Array<Collection>>;
  prefLabel?: Maybe<Array<Literal_Rdfs>>;
};

export type Collection_CreateInput = {
  id?: InputMaybe<Scalars['ID']['input']>;
  member?: InputMaybe<Array<Scalars['String']['input']>>;
};

export type Collection_UpdateInput = {
  member?: InputMaybe<Array<Scalars['String']['input']>>;
};

export type Concept = {
  __typename?: 'Concept';
  altLabel?: Maybe<Array<Literal_Rdfs>>;
  broader?: Maybe<Array<Concept>>;
  definition?: Maybe<Array<String_Xsd>>;
  id: Scalars['ID']['output'];
  memberOf?: Maybe<Array<Collection>>;
  narrower?: Maybe<Array<Concept>>;
  prefLabel?: Maybe<Array<Literal_Rdfs>>;
  replacedBy?: Maybe<Array<Concept>>;
  replaces?: Maybe<Array<Concept>>;
};

export type ConceptScheme = {
  __typename?: 'ConceptScheme';
  altLabel?: Maybe<Array<Literal_Rdfs>>;
  definition?: Maybe<Array<String_Xsd>>;
  hasPolyHierarchy?: Maybe<Array<Boolean>>;
  id: Scalars['ID']['output'];
  prefLabel?: Maybe<Array<Literal_Rdfs>>;
  supportedLanguage?: Maybe<Array<Language>>;
};

export type ConceptScheme_CreateInput = {
  id?: InputMaybe<Scalars['ID']['input']>;
  prefLabel?: InputMaybe<Array<Scalars['String']['input']>>;
};

export type ConceptScheme_UpdateInput = {
  prefLabel?: InputMaybe<Array<Scalars['String']['input']>>;
};

export type Concept_CreateInput = {
  id?: InputMaybe<Scalars['ID']['input']>;
  memberOf?: InputMaybe<Array<Scalars['String']['input']>>;
};

export type Concept_UpdateInput = {
  memberOf?: InputMaybe<Array<Scalars['String']['input']>>;
};

export enum DimensionEnum {
  Broader = 'broader',
  Narrower = 'narrower',
  Type = 'type'
}

export type EntryRequirement = {
  __typename?: 'EntryRequirement';
  id: Scalars['ID']['output'];
  requirementLevel?: Maybe<Array<Concept>>;
};

export type EntryRequirement_CreateInput = {
  id?: InputMaybe<Scalars['ID']['input']>;
  requirementLevel?: InputMaybe<Array<Scalars['String']['input']>>;
};

export type EntryRequirement_UpdateInput = {
  requirementLevel?: InputMaybe<Array<Scalars['String']['input']>>;
};

export type EscoExtension = {
  __typename?: 'EscoExtension';
  altLabel?: Maybe<Array<Literal_Rdfs>>;
  broader?: Maybe<Array<Concept>>;
  definition?: Maybe<Array<String_Xsd>>;
  id: Scalars['ID']['output'];
  memberOf?: Maybe<Array<Collection>>;
  narrower?: Maybe<Array<Concept>>;
  prefLabel?: Maybe<Array<Literal_Rdfs>>;
};

export type EscoExtension_CreateInput = {
  id?: InputMaybe<Scalars['ID']['input']>;
  memberOf?: InputMaybe<Array<Scalars['String']['input']>>;
};

export type EscoExtension_UpdateInput = {
  memberOf?: InputMaybe<Array<Scalars['String']['input']>>;
};

export type LabelRole = {
  __typename?: 'LabelRole';
  altLabel?: Maybe<Array<Literal_Rdfs>>;
  broader?: Maybe<Array<Concept>>;
  definition?: Maybe<Array<String_Xsd>>;
  id: Scalars['ID']['output'];
  memberOf?: Maybe<Array<Collection>>;
  narrower?: Maybe<Array<Concept>>;
  prefLabel?: Maybe<Array<Literal_Rdfs>>;
};

export type LabelRole_CreateInput = {
  id?: InputMaybe<Scalars['ID']['input']>;
  memberOf?: InputMaybe<Array<Scalars['String']['input']>>;
};

export type LabelRole_UpdateInput = {
  memberOf?: InputMaybe<Array<Scalars['String']['input']>>;
};

export type Language = {
  __typename?: 'Language';
  id: Scalars['ID']['output'];
};

export type Literal_Rdfs = {
  __typename?: 'Literal_rdfs';
  datatype?: Maybe<Scalars['String']['output']>;
  language?: Maybe<Scalars['String']['output']>;
  termType?: Maybe<Scalars['String']['output']>;
  value?: Maybe<Scalars['String']['output']>;
};

export type MemberConcept = {
  __typename?: 'MemberConcept';
  altLabel?: Maybe<Array<Literal_Rdfs>>;
  broader?: Maybe<Array<Concept>>;
  definition?: Maybe<Array<String_Xsd>>;
  id: Scalars['ID']['output'];
  memberOf?: Maybe<Array<Collection>>;
  narrower?: Maybe<Array<Concept>>;
  prefLabel?: Maybe<Array<Literal_Rdfs>>;
};

export type MemberConcept_CreateInput = {
  id?: InputMaybe<Scalars['ID']['input']>;
  memberOf?: InputMaybe<Array<Scalars['String']['input']>>;
};

export type MemberConcept_UpdateInput = {
  memberOf?: InputMaybe<Array<Scalars['String']['input']>>;
};

export type Mutation = {
  __typename?: 'Mutation';
  Accreditation_create?: Maybe<Accreditation>;
  Accreditation_delete?: Maybe<Accreditation>;
  Accreditation_update?: Maybe<Accreditation>;
  AssociationObject_create?: Maybe<AssociationObject>;
  AssociationObject_delete?: Maybe<AssociationObject>;
  AssociationObject_update?: Maybe<AssociationObject>;
  AwardingActivity_create?: Maybe<AwardingActivity>;
  AwardingActivity_delete?: Maybe<AwardingActivity>;
  AwardingActivity_update?: Maybe<AwardingActivity>;
  AwardingBody_create?: Maybe<AwardingBody>;
  AwardingBody_delete?: Maybe<AwardingBody>;
  AwardingBody_update?: Maybe<AwardingBody>;
  Collection_create?: Maybe<Collection>;
  Collection_delete?: Maybe<Collection>;
  Collection_update?: Maybe<Collection>;
  ConceptScheme_create?: Maybe<ConceptScheme>;
  ConceptScheme_delete?: Maybe<ConceptScheme>;
  ConceptScheme_update?: Maybe<ConceptScheme>;
  Concept_create?: Maybe<Concept>;
  Concept_delete?: Maybe<Concept>;
  Concept_update?: Maybe<Concept>;
  EntryRequirement_create?: Maybe<EntryRequirement>;
  EntryRequirement_delete?: Maybe<EntryRequirement>;
  EntryRequirement_update?: Maybe<EntryRequirement>;
  EscoExtension_create?: Maybe<EscoExtension>;
  EscoExtension_delete?: Maybe<EscoExtension>;
  EscoExtension_update?: Maybe<EscoExtension>;
  LabelRole_create?: Maybe<LabelRole>;
  LabelRole_delete?: Maybe<LabelRole>;
  LabelRole_update?: Maybe<LabelRole>;
  MemberConcept_create?: Maybe<MemberConcept>;
  MemberConcept_delete?: Maybe<MemberConcept>;
  MemberConcept_update?: Maybe<MemberConcept>;
  NodeLiteral_create?: Maybe<NodeLiteral>;
  NodeLiteral_delete?: Maybe<NodeLiteral>;
  NodeLiteral_update?: Maybe<NodeLiteral>;
  Occupation_create?: Maybe<Occupation>;
  Occupation_delete?: Maybe<Occupation>;
  Occupation_update?: Maybe<Occupation>;
  Qualification_create?: Maybe<Qualification>;
  Qualification_delete?: Maybe<Qualification>;
  Qualification_update?: Maybe<Qualification>;
  Recognition_create?: Maybe<Recognition>;
  Recognition_delete?: Maybe<Recognition>;
  Recognition_update?: Maybe<Recognition>;
  SkillCompetenceType_create?: Maybe<SkillCompetenceType>;
  SkillCompetenceType_delete?: Maybe<SkillCompetenceType>;
  SkillCompetenceType_update?: Maybe<SkillCompetenceType>;
  Skill_create?: Maybe<Skill>;
  Skill_delete?: Maybe<Skill>;
  Skill_update?: Maybe<Skill>;
  WorkContext_create?: Maybe<WorkContext>;
  WorkContext_delete?: Maybe<WorkContext>;
  WorkContext_update?: Maybe<WorkContext>;
  b0_n44_create?: Maybe<B0_N44>;
  b0_n44_delete?: Maybe<B0_N44>;
  b0_n44_update?: Maybe<B0_N44>;
  b0_n48_create?: Maybe<B0_N48>;
  b0_n48_delete?: Maybe<B0_N48>;
  b0_n48_update?: Maybe<B0_N48>;
  b0_n55_create?: Maybe<B0_N55>;
  b0_n55_delete?: Maybe<B0_N55>;
  b0_n55_update?: Maybe<B0_N55>;
  b0_n59_create?: Maybe<B0_N59>;
  b0_n59_delete?: Maybe<B0_N59>;
  b0_n59_update?: Maybe<B0_N59>;
  b0_n66_create?: Maybe<B0_N66>;
  b0_n66_delete?: Maybe<B0_N66>;
  b0_n66_update?: Maybe<B0_N66>;
};


export type MutationAccreditation_CreateArgs = {
  input?: InputMaybe<CreateAccreditationInput>;
};


export type MutationAccreditation_DeleteArgs = {
  input?: InputMaybe<DeleteAccreditationInput>;
};


export type MutationAccreditation_UpdateArgs = {
  input?: InputMaybe<UpdateAccreditationInput>;
};


export type MutationAssociationObject_CreateArgs = {
  input?: InputMaybe<CreateAssociationObjectInput>;
};


export type MutationAssociationObject_DeleteArgs = {
  input?: InputMaybe<DeleteAssociationObjectInput>;
};


export type MutationAssociationObject_UpdateArgs = {
  input?: InputMaybe<UpdateAssociationObjectInput>;
};


export type MutationAwardingActivity_CreateArgs = {
  input?: InputMaybe<CreateAwardingActivityInput>;
};


export type MutationAwardingActivity_DeleteArgs = {
  input?: InputMaybe<DeleteAwardingActivityInput>;
};


export type MutationAwardingActivity_UpdateArgs = {
  input?: InputMaybe<UpdateAwardingActivityInput>;
};


export type MutationAwardingBody_CreateArgs = {
  input?: InputMaybe<CreateAwardingBodyInput>;
};


export type MutationAwardingBody_DeleteArgs = {
  input?: InputMaybe<DeleteAwardingBodyInput>;
};


export type MutationAwardingBody_UpdateArgs = {
  input?: InputMaybe<UpdateAwardingBodyInput>;
};


export type MutationCollection_CreateArgs = {
  input?: InputMaybe<CreateCollectionInput>;
};


export type MutationCollection_DeleteArgs = {
  input?: InputMaybe<DeleteCollectionInput>;
};


export type MutationCollection_UpdateArgs = {
  input?: InputMaybe<UpdateCollectionInput>;
};


export type MutationConceptScheme_CreateArgs = {
  input?: InputMaybe<CreateConceptSchemeInput>;
};


export type MutationConceptScheme_DeleteArgs = {
  input?: InputMaybe<DeleteConceptSchemeInput>;
};


export type MutationConceptScheme_UpdateArgs = {
  input?: InputMaybe<UpdateConceptSchemeInput>;
};


export type MutationConcept_CreateArgs = {
  input?: InputMaybe<CreateConceptInput>;
};


export type MutationConcept_DeleteArgs = {
  input?: InputMaybe<DeleteConceptInput>;
};


export type MutationConcept_UpdateArgs = {
  input?: InputMaybe<UpdateConceptInput>;
};


export type MutationEntryRequirement_CreateArgs = {
  input?: InputMaybe<CreateEntryRequirementInput>;
};


export type MutationEntryRequirement_DeleteArgs = {
  input?: InputMaybe<DeleteEntryRequirementInput>;
};


export type MutationEntryRequirement_UpdateArgs = {
  input?: InputMaybe<UpdateEntryRequirementInput>;
};


export type MutationEscoExtension_CreateArgs = {
  input?: InputMaybe<CreateEscoExtensionInput>;
};


export type MutationEscoExtension_DeleteArgs = {
  input?: InputMaybe<DeleteEscoExtensionInput>;
};


export type MutationEscoExtension_UpdateArgs = {
  input?: InputMaybe<UpdateEscoExtensionInput>;
};


export type MutationLabelRole_CreateArgs = {
  input?: InputMaybe<CreateLabelRoleInput>;
};


export type MutationLabelRole_DeleteArgs = {
  input?: InputMaybe<DeleteLabelRoleInput>;
};


export type MutationLabelRole_UpdateArgs = {
  input?: InputMaybe<UpdateLabelRoleInput>;
};


export type MutationMemberConcept_CreateArgs = {
  input?: InputMaybe<CreateMemberConceptInput>;
};


export type MutationMemberConcept_DeleteArgs = {
  input?: InputMaybe<DeleteMemberConceptInput>;
};


export type MutationMemberConcept_UpdateArgs = {
  input?: InputMaybe<UpdateMemberConceptInput>;
};


export type MutationNodeLiteral_CreateArgs = {
  input?: InputMaybe<CreateNodeLiteralInput>;
};


export type MutationNodeLiteral_DeleteArgs = {
  input?: InputMaybe<DeleteNodeLiteralInput>;
};


export type MutationNodeLiteral_UpdateArgs = {
  input?: InputMaybe<UpdateNodeLiteralInput>;
};


export type MutationOccupation_CreateArgs = {
  input?: InputMaybe<CreateOccupationInput>;
};


export type MutationOccupation_DeleteArgs = {
  input?: InputMaybe<DeleteOccupationInput>;
};


export type MutationOccupation_UpdateArgs = {
  input?: InputMaybe<UpdateOccupationInput>;
};


export type MutationQualification_CreateArgs = {
  input?: InputMaybe<CreateQualificationInput>;
};


export type MutationQualification_DeleteArgs = {
  input?: InputMaybe<DeleteQualificationInput>;
};


export type MutationQualification_UpdateArgs = {
  input?: InputMaybe<UpdateQualificationInput>;
};


export type MutationRecognition_CreateArgs = {
  input?: InputMaybe<CreateRecognitionInput>;
};


export type MutationRecognition_DeleteArgs = {
  input?: InputMaybe<DeleteRecognitionInput>;
};


export type MutationRecognition_UpdateArgs = {
  input?: InputMaybe<UpdateRecognitionInput>;
};


export type MutationSkillCompetenceType_CreateArgs = {
  input?: InputMaybe<CreateSkillCompetenceTypeInput>;
};


export type MutationSkillCompetenceType_DeleteArgs = {
  input?: InputMaybe<DeleteSkillCompetenceTypeInput>;
};


export type MutationSkillCompetenceType_UpdateArgs = {
  input?: InputMaybe<UpdateSkillCompetenceTypeInput>;
};


export type MutationSkill_CreateArgs = {
  input?: InputMaybe<CreateSkillInput>;
};


export type MutationSkill_DeleteArgs = {
  input?: InputMaybe<DeleteSkillInput>;
};


export type MutationSkill_UpdateArgs = {
  input?: InputMaybe<UpdateSkillInput>;
};


export type MutationWorkContext_CreateArgs = {
  input?: InputMaybe<CreateWorkContextInput>;
};


export type MutationWorkContext_DeleteArgs = {
  input?: InputMaybe<DeleteWorkContextInput>;
};


export type MutationWorkContext_UpdateArgs = {
  input?: InputMaybe<UpdateWorkContextInput>;
};


export type MutationB0_N44_CreateArgs = {
  input?: InputMaybe<Createb0_N44Input>;
};


export type MutationB0_N44_DeleteArgs = {
  input?: InputMaybe<Deleteb0_N44Input>;
};


export type MutationB0_N44_UpdateArgs = {
  input?: InputMaybe<Updateb0_N44Input>;
};


export type MutationB0_N48_CreateArgs = {
  input?: InputMaybe<Createb0_N48Input>;
};


export type MutationB0_N48_DeleteArgs = {
  input?: InputMaybe<Deleteb0_N48Input>;
};


export type MutationB0_N48_UpdateArgs = {
  input?: InputMaybe<Updateb0_N48Input>;
};


export type MutationB0_N55_CreateArgs = {
  input?: InputMaybe<Createb0_N55Input>;
};


export type MutationB0_N55_DeleteArgs = {
  input?: InputMaybe<Deleteb0_N55Input>;
};


export type MutationB0_N55_UpdateArgs = {
  input?: InputMaybe<Updateb0_N55Input>;
};


export type MutationB0_N59_CreateArgs = {
  input?: InputMaybe<Createb0_N59Input>;
};


export type MutationB0_N59_DeleteArgs = {
  input?: InputMaybe<Deleteb0_N59Input>;
};


export type MutationB0_N59_UpdateArgs = {
  input?: InputMaybe<Updateb0_N59Input>;
};


export type MutationB0_N66_CreateArgs = {
  input?: InputMaybe<Createb0_N66Input>;
};


export type MutationB0_N66_DeleteArgs = {
  input?: InputMaybe<Deleteb0_N66Input>;
};


export type MutationB0_N66_UpdateArgs = {
  input?: InputMaybe<Updateb0_N66Input>;
};

export type NodeLiteral = {
  __typename?: 'NodeLiteral';
  altLabel?: Maybe<Array<Literal_Rdfs>>;
  broader?: Maybe<Array<Concept>>;
  definition?: Maybe<Array<String_Xsd>>;
  id: Scalars['ID']['output'];
  memberOf?: Maybe<Array<Collection>>;
  narrower?: Maybe<Array<Concept>>;
  nodeLiteral?: Maybe<Array<Literal_Rdfs>>;
  prefLabel?: Maybe<Array<Literal_Rdfs>>;
};

export type NodeLiteral_CreateInput = {
  id?: InputMaybe<Scalars['ID']['input']>;
  memberOf?: InputMaybe<Array<Scalars['String']['input']>>;
};

export type NodeLiteral_UpdateInput = {
  memberOf?: InputMaybe<Array<Scalars['String']['input']>>;
};

export type Occupation = {
  __typename?: 'Occupation';
  altLabel?: Maybe<Array<Literal_Rdfs>>;
  broader?: Maybe<Array<Concept>>;
  definition?: Maybe<Array<String_Xsd>>;
  id: Scalars['ID']['output'];
  memberOf?: Maybe<Array<Collection>>;
  narrower?: Maybe<Array<Concept>>;
  prefLabel?: Maybe<Array<Literal_Rdfs>>;
};

export type Occupation_CreateInput = {
  id?: InputMaybe<Scalars['ID']['input']>;
  memberOf?: InputMaybe<Array<Scalars['String']['input']>>;
};

export type Occupation_UpdateInput = {
  memberOf?: InputMaybe<Array<Scalars['String']['input']>>;
};

export type PivotTableQuery = {
  __typename?: 'PivotTableQuery';
  broader?: Maybe<Scalars['String']['output']>;
  narrower?: Maybe<Scalars['String']['output']>;
  type?: Maybe<Scalars['String']['output']>;
};

export type PivotTableQueryFilter = {
  broader?: InputMaybe<Scalars['String']['input']>;
  narrower?: InputMaybe<Scalars['String']['input']>;
  type?: InputMaybe<Scalars['String']['input']>;
};

export type Qualification = {
  __typename?: 'Qualification';
  altLabel?: Maybe<Array<Literal_Rdfs>>;
  broader?: Maybe<Array<Concept>>;
  definition?: Maybe<Array<String_Xsd>>;
  expirationPeriod?: Maybe<Array<Duration>>;
  expiryPeriod?: Maybe<Array<Duration>>;
  hasAccreditation?: Maybe<Array<Accreditation>>;
  hasAwardingActivity?: Maybe<Array<AwardingActivity>>;
  hasECTSCreditPoints?: Maybe<Array<Decimal>>;
  hasEntryRequirement?: Maybe<Array<EntryRequirement>>;
  hasRecognition?: Maybe<Array<Recognition>>;
  id: Scalars['ID']['output'];
  isPartialQualification?: Maybe<Array<Boolean>>;
  memberOf?: Maybe<Array<Collection>>;
  narrower?: Maybe<Array<Concept>>;
  prefLabel?: Maybe<Array<Literal_Rdfs>>;
  volumeOfLearning?: Maybe<Array<Duration>>;
  waysToAcquire?: Maybe<Array<Concept>>;
};

export type Qualification_CreateInput = {
  id?: InputMaybe<Scalars['ID']['input']>;
  memberOf?: InputMaybe<Array<Scalars['String']['input']>>;
};

export type Qualification_UpdateInput = {
  memberOf?: InputMaybe<Array<Scalars['String']['input']>>;
};

export type Query = {
  __typename?: 'Query';
  Accreditation: Accreditation;
  AssociationObject: AssociationObject;
  AwardingActivity: AwardingActivity;
  AwardingBody: AwardingBody;
  Collection: Collection;
  Concept: Concept;
  ConceptScheme: ConceptScheme;
  EntryRequirement: EntryRequirement;
  EscoExtension: EscoExtension;
  LabelRole: LabelRole;
  MemberConcept: MemberConcept;
  NodeLiteral: NodeLiteral;
  Occupation: Occupation;
  PivotTableQuery?: Maybe<Array<Maybe<PivotTableQuery>>>;
  Qualification: Qualification;
  Recognition: Recognition;
  Skill: Skill;
  SkillCompetenceType: SkillCompetenceType;
  WorkContext: WorkContext;
  allAccreditation: Array<Accreditation>;
  allAssociationObject: Array<AssociationObject>;
  allAwardingActivity: Array<AwardingActivity>;
  allAwardingBody: Array<AwardingBody>;
  allCollection: Array<Collection>;
  allConcept: Array<Concept>;
  allConceptScheme: Array<ConceptScheme>;
  allEntryRequirement: Array<EntryRequirement>;
  allEscoExtension: Array<EscoExtension>;
  allLabelRole: Array<LabelRole>;
  allMemberConcept: Array<MemberConcept>;
  allNodeLiteral: Array<NodeLiteral>;
  allOccupation: Array<Occupation>;
  allQualification: Array<Qualification>;
  allRecognition: Array<Recognition>;
  allSkill: Array<Skill>;
  allSkillCompetenceType: Array<SkillCompetenceType>;
  allWorkContext: Array<WorkContext>;
  allb0_n44: Array<B0_N44>;
  allb0_n48: Array<B0_N48>;
  allb0_n55: Array<B0_N55>;
  allb0_n59: Array<B0_N59>;
  allb0_n66: Array<B0_N66>;
  b0_n44: B0_N44;
  b0_n48: B0_N48;
  b0_n55: B0_N55;
  b0_n59: B0_N59;
  b0_n66: B0_N66;
};


export type QueryAccreditationArgs = {
  id: Scalars['ID']['input'];
};


export type QueryAssociationObjectArgs = {
  id: Scalars['ID']['input'];
};


export type QueryAwardingActivityArgs = {
  id: Scalars['ID']['input'];
};


export type QueryAwardingBodyArgs = {
  id: Scalars['ID']['input'];
};


export type QueryCollectionArgs = {
  id: Scalars['ID']['input'];
};


export type QueryConceptArgs = {
  id: Scalars['ID']['input'];
};


export type QueryConceptSchemeArgs = {
  id: Scalars['ID']['input'];
};


export type QueryEntryRequirementArgs = {
  id: Scalars['ID']['input'];
};


export type QueryEscoExtensionArgs = {
  id: Scalars['ID']['input'];
};


export type QueryLabelRoleArgs = {
  id: Scalars['ID']['input'];
};


export type QueryMemberConceptArgs = {
  id: Scalars['ID']['input'];
};


export type QueryNodeLiteralArgs = {
  id: Scalars['ID']['input'];
};


export type QueryOccupationArgs = {
  id: Scalars['ID']['input'];
};


export type QueryPivotTableQueryArgs = {
  dimension?: InputMaybe<Array<InputMaybe<DimensionEnum>>>;
  filters?: InputMaybe<PivotTableQueryFilter>;
};


export type QueryQualificationArgs = {
  id: Scalars['ID']['input'];
};


export type QueryRecognitionArgs = {
  id: Scalars['ID']['input'];
};


export type QuerySkillArgs = {
  id: Scalars['ID']['input'];
};


export type QuerySkillCompetenceTypeArgs = {
  id: Scalars['ID']['input'];
};


export type QueryWorkContextArgs = {
  id: Scalars['ID']['input'];
};


export type QueryAllAccreditationArgs = {
  limit?: InputMaybe<Scalars['Int']['input']>;
  page?: InputMaybe<Scalars['Int']['input']>;
  query?: InputMaybe<Scalars['String']['input']>;
  sort?: InputMaybe<Scalars['String']['input']>;
  start?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<Scalars['JSON']['input']>;
};


export type QueryAllAssociationObjectArgs = {
  limit?: InputMaybe<Scalars['Int']['input']>;
  page?: InputMaybe<Scalars['Int']['input']>;
  query?: InputMaybe<Scalars['String']['input']>;
  sort?: InputMaybe<Scalars['String']['input']>;
  start?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<Scalars['JSON']['input']>;
};


export type QueryAllAwardingActivityArgs = {
  limit?: InputMaybe<Scalars['Int']['input']>;
  page?: InputMaybe<Scalars['Int']['input']>;
  query?: InputMaybe<Scalars['String']['input']>;
  sort?: InputMaybe<Scalars['String']['input']>;
  start?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<Scalars['JSON']['input']>;
};


export type QueryAllAwardingBodyArgs = {
  limit?: InputMaybe<Scalars['Int']['input']>;
  page?: InputMaybe<Scalars['Int']['input']>;
  query?: InputMaybe<Scalars['String']['input']>;
  sort?: InputMaybe<Scalars['String']['input']>;
  start?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<Scalars['JSON']['input']>;
};


export type QueryAllCollectionArgs = {
  limit?: InputMaybe<Scalars['Int']['input']>;
  page?: InputMaybe<Scalars['Int']['input']>;
  query?: InputMaybe<Scalars['String']['input']>;
  sort?: InputMaybe<Scalars['String']['input']>;
  start?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<Scalars['JSON']['input']>;
};


export type QueryAllConceptArgs = {
  limit?: InputMaybe<Scalars['Int']['input']>;
  page?: InputMaybe<Scalars['Int']['input']>;
  query?: InputMaybe<Scalars['String']['input']>;
  sort?: InputMaybe<Scalars['String']['input']>;
  start?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<Scalars['JSON']['input']>;
};


export type QueryAllConceptSchemeArgs = {
  limit?: InputMaybe<Scalars['Int']['input']>;
  page?: InputMaybe<Scalars['Int']['input']>;
  query?: InputMaybe<Scalars['String']['input']>;
  sort?: InputMaybe<Scalars['String']['input']>;
  start?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<Scalars['JSON']['input']>;
};


export type QueryAllEntryRequirementArgs = {
  limit?: InputMaybe<Scalars['Int']['input']>;
  page?: InputMaybe<Scalars['Int']['input']>;
  query?: InputMaybe<Scalars['String']['input']>;
  sort?: InputMaybe<Scalars['String']['input']>;
  start?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<Scalars['JSON']['input']>;
};


export type QueryAllEscoExtensionArgs = {
  limit?: InputMaybe<Scalars['Int']['input']>;
  page?: InputMaybe<Scalars['Int']['input']>;
  query?: InputMaybe<Scalars['String']['input']>;
  sort?: InputMaybe<Scalars['String']['input']>;
  start?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<Scalars['JSON']['input']>;
};


export type QueryAllLabelRoleArgs = {
  limit?: InputMaybe<Scalars['Int']['input']>;
  page?: InputMaybe<Scalars['Int']['input']>;
  query?: InputMaybe<Scalars['String']['input']>;
  sort?: InputMaybe<Scalars['String']['input']>;
  start?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<Scalars['JSON']['input']>;
};


export type QueryAllMemberConceptArgs = {
  limit?: InputMaybe<Scalars['Int']['input']>;
  page?: InputMaybe<Scalars['Int']['input']>;
  query?: InputMaybe<Scalars['String']['input']>;
  sort?: InputMaybe<Scalars['String']['input']>;
  start?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<Scalars['JSON']['input']>;
};


export type QueryAllNodeLiteralArgs = {
  limit?: InputMaybe<Scalars['Int']['input']>;
  page?: InputMaybe<Scalars['Int']['input']>;
  query?: InputMaybe<Scalars['String']['input']>;
  sort?: InputMaybe<Scalars['String']['input']>;
  start?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<Scalars['JSON']['input']>;
};


export type QueryAllOccupationArgs = {
  limit?: InputMaybe<Scalars['Int']['input']>;
  page?: InputMaybe<Scalars['Int']['input']>;
  query?: InputMaybe<Scalars['String']['input']>;
  sort?: InputMaybe<Scalars['String']['input']>;
  start?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<Scalars['JSON']['input']>;
};


export type QueryAllQualificationArgs = {
  limit?: InputMaybe<Scalars['Int']['input']>;
  page?: InputMaybe<Scalars['Int']['input']>;
  query?: InputMaybe<Scalars['String']['input']>;
  sort?: InputMaybe<Scalars['String']['input']>;
  start?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<Scalars['JSON']['input']>;
};


export type QueryAllRecognitionArgs = {
  limit?: InputMaybe<Scalars['Int']['input']>;
  page?: InputMaybe<Scalars['Int']['input']>;
  query?: InputMaybe<Scalars['String']['input']>;
  sort?: InputMaybe<Scalars['String']['input']>;
  start?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<Scalars['JSON']['input']>;
};


export type QueryAllSkillArgs = {
  limit?: InputMaybe<Scalars['Int']['input']>;
  page?: InputMaybe<Scalars['Int']['input']>;
  query?: InputMaybe<Scalars['String']['input']>;
  sort?: InputMaybe<Scalars['String']['input']>;
  start?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<Scalars['JSON']['input']>;
};


export type QueryAllSkillCompetenceTypeArgs = {
  limit?: InputMaybe<Scalars['Int']['input']>;
  page?: InputMaybe<Scalars['Int']['input']>;
  query?: InputMaybe<Scalars['String']['input']>;
  sort?: InputMaybe<Scalars['String']['input']>;
  start?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<Scalars['JSON']['input']>;
};


export type QueryAllWorkContextArgs = {
  limit?: InputMaybe<Scalars['Int']['input']>;
  page?: InputMaybe<Scalars['Int']['input']>;
  query?: InputMaybe<Scalars['String']['input']>;
  sort?: InputMaybe<Scalars['String']['input']>;
  start?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<Scalars['JSON']['input']>;
};


export type QueryAllb0_N44Args = {
  limit?: InputMaybe<Scalars['Int']['input']>;
  page?: InputMaybe<Scalars['Int']['input']>;
  query?: InputMaybe<Scalars['String']['input']>;
  sort?: InputMaybe<Scalars['String']['input']>;
  start?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<Scalars['JSON']['input']>;
};


export type QueryAllb0_N48Args = {
  limit?: InputMaybe<Scalars['Int']['input']>;
  page?: InputMaybe<Scalars['Int']['input']>;
  query?: InputMaybe<Scalars['String']['input']>;
  sort?: InputMaybe<Scalars['String']['input']>;
  start?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<Scalars['JSON']['input']>;
};


export type QueryAllb0_N55Args = {
  limit?: InputMaybe<Scalars['Int']['input']>;
  page?: InputMaybe<Scalars['Int']['input']>;
  query?: InputMaybe<Scalars['String']['input']>;
  sort?: InputMaybe<Scalars['String']['input']>;
  start?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<Scalars['JSON']['input']>;
};


export type QueryAllb0_N59Args = {
  limit?: InputMaybe<Scalars['Int']['input']>;
  page?: InputMaybe<Scalars['Int']['input']>;
  query?: InputMaybe<Scalars['String']['input']>;
  sort?: InputMaybe<Scalars['String']['input']>;
  start?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<Scalars['JSON']['input']>;
};


export type QueryAllb0_N66Args = {
  limit?: InputMaybe<Scalars['Int']['input']>;
  page?: InputMaybe<Scalars['Int']['input']>;
  query?: InputMaybe<Scalars['String']['input']>;
  sort?: InputMaybe<Scalars['String']['input']>;
  start?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<Scalars['JSON']['input']>;
};


export type QueryB0_N44Args = {
  id: Scalars['ID']['input'];
};


export type QueryB0_N48Args = {
  id: Scalars['ID']['input'];
};


export type QueryB0_N55Args = {
  id: Scalars['ID']['input'];
};


export type QueryB0_N59Args = {
  id: Scalars['ID']['input'];
};


export type QueryB0_N66Args = {
  id: Scalars['ID']['input'];
};

export type Recognition = {
  __typename?: 'Recognition';
  id: Scalars['ID']['output'];
};

export type Recognition_CreateInput = {
  id: Scalars['ID']['input'];
};

export type Recognition_UpdateInput = {
  id: Scalars['ID']['input'];
};

export type Resource = {
  __typename?: 'Resource';
  altLabel?: Maybe<Array<Literal_Rdfs>>;
  definition?: Maybe<Array<String_Xsd>>;
  id: Scalars['ID']['output'];
  prefLabel?: Maybe<Array<Literal_Rdfs>>;
};

export type Skill = {
  __typename?: 'Skill';
  altLabel?: Maybe<Array<Literal_Rdfs>>;
  broader?: Maybe<Array<Concept>>;
  definition?: Maybe<Array<String_Xsd>>;
  id: Scalars['ID']['output'];
  isEssentialSkillFor?: Maybe<Array<B0_N44>>;
  isOptionalSkillFor?: Maybe<Array<B0_N48>>;
  memberOf?: Maybe<Array<Collection>>;
  narrower?: Maybe<Array<Concept>>;
  prefLabel?: Maybe<Array<Literal_Rdfs>>;
  skillType?: Maybe<Array<SkillCompetenceType>>;
};

export type SkillCompetenceType = {
  __typename?: 'SkillCompetenceType';
  altLabel?: Maybe<Array<Literal_Rdfs>>;
  broader?: Maybe<Array<Concept>>;
  definition?: Maybe<Array<String_Xsd>>;
  id: Scalars['ID']['output'];
  memberOf?: Maybe<Array<Collection>>;
  narrower?: Maybe<Array<Concept>>;
  prefLabel?: Maybe<Array<Literal_Rdfs>>;
};

export type SkillCompetenceType_CreateInput = {
  id?: InputMaybe<Scalars['ID']['input']>;
  memberOf?: InputMaybe<Array<Scalars['String']['input']>>;
};

export type SkillCompetenceType_UpdateInput = {
  memberOf?: InputMaybe<Array<Scalars['String']['input']>>;
};

export type Skill_CreateInput = {
  id?: InputMaybe<Scalars['ID']['input']>;
  memberOf?: InputMaybe<Array<Scalars['String']['input']>>;
};

export type Skill_UpdateInput = {
  memberOf?: InputMaybe<Array<Scalars['String']['input']>>;
};

export type Standard = {
  __typename?: 'Standard';
  id: Scalars['ID']['output'];
};

export type String_Xsd = {
  __typename?: 'String_xsd';
  value: Scalars['String']['output'];
};

export type WorkContext = {
  __typename?: 'WorkContext';
  altLabel?: Maybe<Array<Literal_Rdfs>>;
  broader?: Maybe<Array<Concept>>;
  definition?: Maybe<Array<String_Xsd>>;
  id: Scalars['ID']['output'];
  memberOf?: Maybe<Array<Collection>>;
  narrower?: Maybe<Array<Concept>>;
  prefLabel?: Maybe<Array<Literal_Rdfs>>;
};

export type WorkContext_CreateInput = {
  id?: InputMaybe<Scalars['ID']['input']>;
  memberOf?: InputMaybe<Array<Scalars['String']['input']>>;
};

export type WorkContext_UpdateInput = {
  memberOf?: InputMaybe<Array<Scalars['String']['input']>>;
};

export type AnyUri = {
  __typename?: 'anyURI';
  id: Scalars['ID']['output'];
};

export type B0_N44 = {
  __typename?: 'b0_n44';
  id: Scalars['ID']['output'];
};

export type B0_N44_CreateInput = {
  id: Scalars['ID']['input'];
};

export type B0_N44_UpdateInput = {
  id: Scalars['ID']['input'];
};

export type B0_N48 = {
  __typename?: 'b0_n48';
  id: Scalars['ID']['output'];
};

export type B0_N48_CreateInput = {
  id: Scalars['ID']['input'];
};

export type B0_N48_UpdateInput = {
  id: Scalars['ID']['input'];
};

export type B0_N55 = {
  __typename?: 'b0_n55';
  id: Scalars['ID']['output'];
  relatedEssentialSkill?: Maybe<Array<Skill>>;
};

export type B0_N55_CreateInput = {
  id?: InputMaybe<Scalars['ID']['input']>;
  relatedEssentialSkill?: InputMaybe<Array<Scalars['String']['input']>>;
};

export type B0_N55_UpdateInput = {
  relatedEssentialSkill?: InputMaybe<Array<Scalars['String']['input']>>;
};

export type B0_N59 = {
  __typename?: 'b0_n59';
  id: Scalars['ID']['output'];
  relatedOptionalSkill?: Maybe<Array<Skill>>;
};

export type B0_N59_CreateInput = {
  id?: InputMaybe<Scalars['ID']['input']>;
  relatedOptionalSkill?: InputMaybe<Array<Scalars['String']['input']>>;
};

export type B0_N59_UpdateInput = {
  relatedOptionalSkill?: InputMaybe<Array<Scalars['String']['input']>>;
};

export type B0_N66 = {
  __typename?: 'b0_n66';
  editorialStatus?: Maybe<Array<EditorialStatusDataType>>;
  id: Scalars['ID']['output'];
};

export type B0_N66_CreateInput = {
  editorialStatus?: InputMaybe<Array<Scalars['String']['input']>>;
  id?: InputMaybe<Scalars['ID']['input']>;
};

export type B0_N66_UpdateInput = {
  editorialStatus?: InputMaybe<Array<Scalars['String']['input']>>;
};

export type Boolean = {
  __typename?: 'boolean';
  id: Scalars['ID']['output'];
};

export type CreateAccreditationInput = {
  data: Accreditation_CreateInput;
  where?: InputMaybe<Refine_Where_Locator>;
};

export type CreateAssociationObjectInput = {
  data: AssociationObject_CreateInput;
  where?: InputMaybe<Refine_Where_Locator>;
};

export type CreateAwardingActivityInput = {
  data: AwardingActivity_CreateInput;
  where?: InputMaybe<Refine_Where_Locator>;
};

export type CreateAwardingBodyInput = {
  data: AwardingBody_CreateInput;
  where?: InputMaybe<Refine_Where_Locator>;
};

export type CreateCollectionInput = {
  data: Collection_CreateInput;
  where?: InputMaybe<Refine_Where_Locator>;
};

export type CreateConceptInput = {
  data: Concept_CreateInput;
  where?: InputMaybe<Refine_Where_Locator>;
};

export type CreateConceptSchemeInput = {
  data: ConceptScheme_CreateInput;
  where?: InputMaybe<Refine_Where_Locator>;
};

export type CreateEntryRequirementInput = {
  data: EntryRequirement_CreateInput;
  where?: InputMaybe<Refine_Where_Locator>;
};

export type CreateEscoExtensionInput = {
  data: EscoExtension_CreateInput;
  where?: InputMaybe<Refine_Where_Locator>;
};

export type CreateLabelRoleInput = {
  data: LabelRole_CreateInput;
  where?: InputMaybe<Refine_Where_Locator>;
};

export type CreateMemberConceptInput = {
  data: MemberConcept_CreateInput;
  where?: InputMaybe<Refine_Where_Locator>;
};

export type CreateNodeLiteralInput = {
  data: NodeLiteral_CreateInput;
  where?: InputMaybe<Refine_Where_Locator>;
};

export type CreateOccupationInput = {
  data: Occupation_CreateInput;
  where?: InputMaybe<Refine_Where_Locator>;
};

export type CreateQualificationInput = {
  data: Qualification_CreateInput;
  where?: InputMaybe<Refine_Where_Locator>;
};

export type CreateRecognitionInput = {
  data: Recognition_CreateInput;
  where?: InputMaybe<Refine_Where_Locator>;
};

export type CreateSkillCompetenceTypeInput = {
  data: SkillCompetenceType_CreateInput;
  where?: InputMaybe<Refine_Where_Locator>;
};

export type CreateSkillInput = {
  data: Skill_CreateInput;
  where?: InputMaybe<Refine_Where_Locator>;
};

export type CreateWorkContextInput = {
  data: WorkContext_CreateInput;
  where?: InputMaybe<Refine_Where_Locator>;
};

export type Createb0_N44Input = {
  data: B0_N44_CreateInput;
  where?: InputMaybe<Refine_Where_Locator>;
};

export type Createb0_N48Input = {
  data: B0_N48_CreateInput;
  where?: InputMaybe<Refine_Where_Locator>;
};

export type Createb0_N55Input = {
  data: B0_N55_CreateInput;
  where?: InputMaybe<Refine_Where_Locator>;
};

export type Createb0_N59Input = {
  data: B0_N59_CreateInput;
  where?: InputMaybe<Refine_Where_Locator>;
};

export type Createb0_N66Input = {
  data: B0_N66_CreateInput;
  where?: InputMaybe<Refine_Where_Locator>;
};

export type Decimal = {
  __typename?: 'decimal';
  id: Scalars['ID']['output'];
};

export type DeleteAccreditationInput = {
  where?: InputMaybe<Refine_Where_Locator>;
};

export type DeleteAssociationObjectInput = {
  where?: InputMaybe<Refine_Where_Locator>;
};

export type DeleteAwardingActivityInput = {
  where?: InputMaybe<Refine_Where_Locator>;
};

export type DeleteAwardingBodyInput = {
  where?: InputMaybe<Refine_Where_Locator>;
};

export type DeleteCollectionInput = {
  where?: InputMaybe<Refine_Where_Locator>;
};

export type DeleteConceptInput = {
  where?: InputMaybe<Refine_Where_Locator>;
};

export type DeleteConceptSchemeInput = {
  where?: InputMaybe<Refine_Where_Locator>;
};

export type DeleteEntryRequirementInput = {
  where?: InputMaybe<Refine_Where_Locator>;
};

export type DeleteEscoExtensionInput = {
  where?: InputMaybe<Refine_Where_Locator>;
};

export type DeleteLabelRoleInput = {
  where?: InputMaybe<Refine_Where_Locator>;
};

export type DeleteMemberConceptInput = {
  where?: InputMaybe<Refine_Where_Locator>;
};

export type DeleteNodeLiteralInput = {
  where?: InputMaybe<Refine_Where_Locator>;
};

export type DeleteOccupationInput = {
  where?: InputMaybe<Refine_Where_Locator>;
};

export type DeleteQualificationInput = {
  where?: InputMaybe<Refine_Where_Locator>;
};

export type DeleteRecognitionInput = {
  where?: InputMaybe<Refine_Where_Locator>;
};

export type DeleteSkillCompetenceTypeInput = {
  where?: InputMaybe<Refine_Where_Locator>;
};

export type DeleteSkillInput = {
  where?: InputMaybe<Refine_Where_Locator>;
};

export type DeleteWorkContextInput = {
  where?: InputMaybe<Refine_Where_Locator>;
};

export type Deleteb0_N44Input = {
  where?: InputMaybe<Refine_Where_Locator>;
};

export type Deleteb0_N48Input = {
  where?: InputMaybe<Refine_Where_Locator>;
};

export type Deleteb0_N55Input = {
  where?: InputMaybe<Refine_Where_Locator>;
};

export type Deleteb0_N59Input = {
  where?: InputMaybe<Refine_Where_Locator>;
};

export type Deleteb0_N66Input = {
  where?: InputMaybe<Refine_Where_Locator>;
};

export type Duration = {
  __typename?: 'duration';
  id: Scalars['ID']['output'];
};

export type EditorialStatusDataType = {
  __typename?: 'editorialStatusDataType';
  id: Scalars['ID']['output'];
};

export type Refine_Where_Locator = {
  id?: InputMaybe<Scalars['ID']['input']>;
};

export type UpdateAccreditationInput = {
  data: Accreditation_UpdateInput;
  where?: InputMaybe<Refine_Where_Locator>;
};

export type UpdateAssociationObjectInput = {
  data: AssociationObject_UpdateInput;
  where?: InputMaybe<Refine_Where_Locator>;
};

export type UpdateAwardingActivityInput = {
  data: AwardingActivity_UpdateInput;
  where?: InputMaybe<Refine_Where_Locator>;
};

export type UpdateAwardingBodyInput = {
  data: AwardingBody_UpdateInput;
  where?: InputMaybe<Refine_Where_Locator>;
};

export type UpdateCollectionInput = {
  data: Collection_UpdateInput;
  where?: InputMaybe<Refine_Where_Locator>;
};

export type UpdateConceptInput = {
  data: Concept_UpdateInput;
  where?: InputMaybe<Refine_Where_Locator>;
};

export type UpdateConceptSchemeInput = {
  data: ConceptScheme_UpdateInput;
  where?: InputMaybe<Refine_Where_Locator>;
};

export type UpdateEntryRequirementInput = {
  data: EntryRequirement_UpdateInput;
  where?: InputMaybe<Refine_Where_Locator>;
};

export type UpdateEscoExtensionInput = {
  data: EscoExtension_UpdateInput;
  where?: InputMaybe<Refine_Where_Locator>;
};

export type UpdateLabelRoleInput = {
  data: LabelRole_UpdateInput;
  where?: InputMaybe<Refine_Where_Locator>;
};

export type UpdateMemberConceptInput = {
  data: MemberConcept_UpdateInput;
  where?: InputMaybe<Refine_Where_Locator>;
};

export type UpdateNodeLiteralInput = {
  data: NodeLiteral_UpdateInput;
  where?: InputMaybe<Refine_Where_Locator>;
};

export type UpdateOccupationInput = {
  data: Occupation_UpdateInput;
  where?: InputMaybe<Refine_Where_Locator>;
};

export type UpdateQualificationInput = {
  data: Qualification_UpdateInput;
  where?: InputMaybe<Refine_Where_Locator>;
};

export type UpdateRecognitionInput = {
  data: Recognition_UpdateInput;
  where?: InputMaybe<Refine_Where_Locator>;
};

export type UpdateSkillCompetenceTypeInput = {
  data: SkillCompetenceType_UpdateInput;
  where?: InputMaybe<Refine_Where_Locator>;
};

export type UpdateSkillInput = {
  data: Skill_UpdateInput;
  where?: InputMaybe<Refine_Where_Locator>;
};

export type UpdateWorkContextInput = {
  data: WorkContext_UpdateInput;
  where?: InputMaybe<Refine_Where_Locator>;
};

export type Updateb0_N44Input = {
  data: B0_N44_UpdateInput;
  where?: InputMaybe<Refine_Where_Locator>;
};

export type Updateb0_N48Input = {
  data: B0_N48_UpdateInput;
  where?: InputMaybe<Refine_Where_Locator>;
};

export type Updateb0_N55Input = {
  data: B0_N55_UpdateInput;
  where?: InputMaybe<Refine_Where_Locator>;
};

export type Updateb0_N59Input = {
  data: B0_N59_UpdateInput;
  where?: InputMaybe<Refine_Where_Locator>;
};

export type Updateb0_N66Input = {
  data: B0_N66_UpdateInput;
  where?: InputMaybe<Refine_Where_Locator>;
};

export type WithIndex<TObject> = TObject & Record<string, any>;
export type ResolversObject<TObject> = WithIndex<TObject>;

export type ResolverTypeWrapper<T> = Promise<T> | T;


export type ResolverWithResolve<TResult, TParent, TContext, TArgs> = {
  resolve: ResolverFn<TResult, TParent, TContext, TArgs>;
};
export type Resolver<TResult, TParent = {}, TContext = {}, TArgs = {}> = ResolverFn<TResult, TParent, TContext, TArgs> | ResolverWithResolve<TResult, TParent, TContext, TArgs>;

export type ResolverFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => Promise<TResult> | TResult;

export type SubscriptionSubscribeFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => AsyncIterable<TResult> | Promise<AsyncIterable<TResult>>;

export type SubscriptionResolveFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => TResult | Promise<TResult>;

export interface SubscriptionSubscriberObject<TResult, TKey extends string, TParent, TContext, TArgs> {
  subscribe: SubscriptionSubscribeFn<{ [key in TKey]: TResult }, TParent, TContext, TArgs>;
  resolve?: SubscriptionResolveFn<TResult, { [key in TKey]: TResult }, TContext, TArgs>;
}

export interface SubscriptionResolverObject<TResult, TParent, TContext, TArgs> {
  subscribe: SubscriptionSubscribeFn<any, TParent, TContext, TArgs>;
  resolve: SubscriptionResolveFn<TResult, any, TContext, TArgs>;
}

export type SubscriptionObject<TResult, TKey extends string, TParent, TContext, TArgs> =
  | SubscriptionSubscriberObject<TResult, TKey, TParent, TContext, TArgs>
  | SubscriptionResolverObject<TResult, TParent, TContext, TArgs>;

export type SubscriptionResolver<TResult, TKey extends string, TParent = {}, TContext = {}, TArgs = {}> =
  | ((...args: any[]) => SubscriptionObject<TResult, TKey, TParent, TContext, TArgs>)
  | SubscriptionObject<TResult, TKey, TParent, TContext, TArgs>;

export type TypeResolveFn<TTypes, TParent = {}, TContext = {}> = (
  parent: TParent,
  context: TContext,
  info: GraphQLResolveInfo
) => Maybe<TTypes> | Promise<Maybe<TTypes>>;

export type IsTypeOfResolverFn<T = {}, TContext = {}> = (obj: T, context: TContext, info: GraphQLResolveInfo) => boolean | Promise<boolean>;

export type NextResolverFn<T> = () => Promise<T>;

export type DirectiveResolverFn<TResult = {}, TParent = {}, TContext = {}, TArgs = {}> = (
  next: NextResolverFn<TResult>,
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => TResult | Promise<TResult>;



/** Mapping between all available schema types and the resolvers types */
export type ResolversTypes = ResolversObject<{
  Accreditation: ResolverTypeWrapper<Accreditation>;
  Accreditation_createInput: Accreditation_CreateInput;
  Accreditation_updateInput: Accreditation_UpdateInput;
  AssociationObject: ResolverTypeWrapper<AssociationObject>;
  AssociationObject_createInput: AssociationObject_CreateInput;
  AssociationObject_updateInput: AssociationObject_UpdateInput;
  AwardingActivity: ResolverTypeWrapper<AwardingActivity>;
  AwardingActivity_createInput: AwardingActivity_CreateInput;
  AwardingActivity_updateInput: AwardingActivity_UpdateInput;
  AwardingBody: ResolverTypeWrapper<AwardingBody>;
  AwardingBody_createInput: AwardingBody_CreateInput;
  AwardingBody_updateInput: AwardingBody_UpdateInput;
  Boolean: ResolverTypeWrapper<Scalars['Boolean']['output']>;
  Collection: ResolverTypeWrapper<Collection>;
  Collection_createInput: Collection_CreateInput;
  Collection_updateInput: Collection_UpdateInput;
  Concept: ResolverTypeWrapper<Concept>;
  ConceptScheme: ResolverTypeWrapper<ConceptScheme>;
  ConceptScheme_createInput: ConceptScheme_CreateInput;
  ConceptScheme_updateInput: ConceptScheme_UpdateInput;
  Concept_createInput: Concept_CreateInput;
  Concept_updateInput: Concept_UpdateInput;
  DimensionEnum: DimensionEnum;
  EntryRequirement: ResolverTypeWrapper<EntryRequirement>;
  EntryRequirement_createInput: EntryRequirement_CreateInput;
  EntryRequirement_updateInput: EntryRequirement_UpdateInput;
  EscoExtension: ResolverTypeWrapper<EscoExtension>;
  EscoExtension_createInput: EscoExtension_CreateInput;
  EscoExtension_updateInput: EscoExtension_UpdateInput;
  ID: ResolverTypeWrapper<Scalars['ID']['output']>;
  Int: ResolverTypeWrapper<Scalars['Int']['output']>;
  JSON: ResolverTypeWrapper<Scalars['JSON']['output']>;
  LabelRole: ResolverTypeWrapper<LabelRole>;
  LabelRole_createInput: LabelRole_CreateInput;
  LabelRole_updateInput: LabelRole_UpdateInput;
  Language: ResolverTypeWrapper<Language>;
  Literal_rdfs: ResolverTypeWrapper<Literal_Rdfs>;
  MemberConcept: ResolverTypeWrapper<MemberConcept>;
  MemberConcept_createInput: MemberConcept_CreateInput;
  MemberConcept_updateInput: MemberConcept_UpdateInput;
  Mutation: ResolverTypeWrapper<{}>;
  NodeLiteral: ResolverTypeWrapper<NodeLiteral>;
  NodeLiteral_createInput: NodeLiteral_CreateInput;
  NodeLiteral_updateInput: NodeLiteral_UpdateInput;
  Occupation: ResolverTypeWrapper<Occupation>;
  Occupation_createInput: Occupation_CreateInput;
  Occupation_updateInput: Occupation_UpdateInput;
  PivotTableQuery: ResolverTypeWrapper<PivotTableQuery>;
  PivotTableQueryFilter: PivotTableQueryFilter;
  Qualification: ResolverTypeWrapper<Qualification>;
  Qualification_createInput: Qualification_CreateInput;
  Qualification_updateInput: Qualification_UpdateInput;
  Query: ResolverTypeWrapper<{}>;
  Recognition: ResolverTypeWrapper<Recognition>;
  Recognition_createInput: Recognition_CreateInput;
  Recognition_updateInput: Recognition_UpdateInput;
  Resource: ResolverTypeWrapper<Resource>;
  Skill: ResolverTypeWrapper<Skill>;
  SkillCompetenceType: ResolverTypeWrapper<SkillCompetenceType>;
  SkillCompetenceType_createInput: SkillCompetenceType_CreateInput;
  SkillCompetenceType_updateInput: SkillCompetenceType_UpdateInput;
  Skill_createInput: Skill_CreateInput;
  Skill_updateInput: Skill_UpdateInput;
  Standard: ResolverTypeWrapper<Standard>;
  String: ResolverTypeWrapper<Scalars['String']['output']>;
  String_xsd: ResolverTypeWrapper<String_Xsd>;
  WorkContext: ResolverTypeWrapper<WorkContext>;
  WorkContext_createInput: WorkContext_CreateInput;
  WorkContext_updateInput: WorkContext_UpdateInput;
  anyURI: ResolverTypeWrapper<AnyUri>;
  b0_n44: ResolverTypeWrapper<B0_N44>;
  b0_n44_createInput: B0_N44_CreateInput;
  b0_n44_updateInput: B0_N44_UpdateInput;
  b0_n48: ResolverTypeWrapper<B0_N48>;
  b0_n48_createInput: B0_N48_CreateInput;
  b0_n48_updateInput: B0_N48_UpdateInput;
  b0_n55: ResolverTypeWrapper<B0_N55>;
  b0_n55_createInput: B0_N55_CreateInput;
  b0_n55_updateInput: B0_N55_UpdateInput;
  b0_n59: ResolverTypeWrapper<B0_N59>;
  b0_n59_createInput: B0_N59_CreateInput;
  b0_n59_updateInput: B0_N59_UpdateInput;
  b0_n66: ResolverTypeWrapper<B0_N66>;
  b0_n66_createInput: B0_N66_CreateInput;
  b0_n66_updateInput: B0_N66_UpdateInput;
  boolean: ResolverTypeWrapper<Boolean>;
  createAccreditationInput: CreateAccreditationInput;
  createAssociationObjectInput: CreateAssociationObjectInput;
  createAwardingActivityInput: CreateAwardingActivityInput;
  createAwardingBodyInput: CreateAwardingBodyInput;
  createCollectionInput: CreateCollectionInput;
  createConceptInput: CreateConceptInput;
  createConceptSchemeInput: CreateConceptSchemeInput;
  createEntryRequirementInput: CreateEntryRequirementInput;
  createEscoExtensionInput: CreateEscoExtensionInput;
  createLabelRoleInput: CreateLabelRoleInput;
  createMemberConceptInput: CreateMemberConceptInput;
  createNodeLiteralInput: CreateNodeLiteralInput;
  createOccupationInput: CreateOccupationInput;
  createQualificationInput: CreateQualificationInput;
  createRecognitionInput: CreateRecognitionInput;
  createSkillCompetenceTypeInput: CreateSkillCompetenceTypeInput;
  createSkillInput: CreateSkillInput;
  createWorkContextInput: CreateWorkContextInput;
  createb0_n44Input: Createb0_N44Input;
  createb0_n48Input: Createb0_N48Input;
  createb0_n55Input: Createb0_N55Input;
  createb0_n59Input: Createb0_N59Input;
  createb0_n66Input: Createb0_N66Input;
  decimal: ResolverTypeWrapper<Decimal>;
  deleteAccreditationInput: DeleteAccreditationInput;
  deleteAssociationObjectInput: DeleteAssociationObjectInput;
  deleteAwardingActivityInput: DeleteAwardingActivityInput;
  deleteAwardingBodyInput: DeleteAwardingBodyInput;
  deleteCollectionInput: DeleteCollectionInput;
  deleteConceptInput: DeleteConceptInput;
  deleteConceptSchemeInput: DeleteConceptSchemeInput;
  deleteEntryRequirementInput: DeleteEntryRequirementInput;
  deleteEscoExtensionInput: DeleteEscoExtensionInput;
  deleteLabelRoleInput: DeleteLabelRoleInput;
  deleteMemberConceptInput: DeleteMemberConceptInput;
  deleteNodeLiteralInput: DeleteNodeLiteralInput;
  deleteOccupationInput: DeleteOccupationInput;
  deleteQualificationInput: DeleteQualificationInput;
  deleteRecognitionInput: DeleteRecognitionInput;
  deleteSkillCompetenceTypeInput: DeleteSkillCompetenceTypeInput;
  deleteSkillInput: DeleteSkillInput;
  deleteWorkContextInput: DeleteWorkContextInput;
  deleteb0_n44Input: Deleteb0_N44Input;
  deleteb0_n48Input: Deleteb0_N48Input;
  deleteb0_n55Input: Deleteb0_N55Input;
  deleteb0_n59Input: Deleteb0_N59Input;
  deleteb0_n66Input: Deleteb0_N66Input;
  duration: ResolverTypeWrapper<Duration>;
  editorialStatusDataType: ResolverTypeWrapper<EditorialStatusDataType>;
  refine_where_locator: Refine_Where_Locator;
  updateAccreditationInput: UpdateAccreditationInput;
  updateAssociationObjectInput: UpdateAssociationObjectInput;
  updateAwardingActivityInput: UpdateAwardingActivityInput;
  updateAwardingBodyInput: UpdateAwardingBodyInput;
  updateCollectionInput: UpdateCollectionInput;
  updateConceptInput: UpdateConceptInput;
  updateConceptSchemeInput: UpdateConceptSchemeInput;
  updateEntryRequirementInput: UpdateEntryRequirementInput;
  updateEscoExtensionInput: UpdateEscoExtensionInput;
  updateLabelRoleInput: UpdateLabelRoleInput;
  updateMemberConceptInput: UpdateMemberConceptInput;
  updateNodeLiteralInput: UpdateNodeLiteralInput;
  updateOccupationInput: UpdateOccupationInput;
  updateQualificationInput: UpdateQualificationInput;
  updateRecognitionInput: UpdateRecognitionInput;
  updateSkillCompetenceTypeInput: UpdateSkillCompetenceTypeInput;
  updateSkillInput: UpdateSkillInput;
  updateWorkContextInput: UpdateWorkContextInput;
  updateb0_n44Input: Updateb0_N44Input;
  updateb0_n48Input: Updateb0_N48Input;
  updateb0_n55Input: Updateb0_N55Input;
  updateb0_n59Input: Updateb0_N59Input;
  updateb0_n66Input: Updateb0_N66Input;
}>;

/** Mapping between all available schema types and the resolvers parents */
export type ResolversParentTypes = ResolversObject<{
  Accreditation: Accreditation;
  Accreditation_createInput: Accreditation_CreateInput;
  Accreditation_updateInput: Accreditation_UpdateInput;
  AssociationObject: AssociationObject;
  AssociationObject_createInput: AssociationObject_CreateInput;
  AssociationObject_updateInput: AssociationObject_UpdateInput;
  AwardingActivity: AwardingActivity;
  AwardingActivity_createInput: AwardingActivity_CreateInput;
  AwardingActivity_updateInput: AwardingActivity_UpdateInput;
  AwardingBody: AwardingBody;
  AwardingBody_createInput: AwardingBody_CreateInput;
  AwardingBody_updateInput: AwardingBody_UpdateInput;
  Boolean: Scalars['Boolean']['output'];
  Collection: Collection;
  Collection_createInput: Collection_CreateInput;
  Collection_updateInput: Collection_UpdateInput;
  Concept: Concept;
  ConceptScheme: ConceptScheme;
  ConceptScheme_createInput: ConceptScheme_CreateInput;
  ConceptScheme_updateInput: ConceptScheme_UpdateInput;
  Concept_createInput: Concept_CreateInput;
  Concept_updateInput: Concept_UpdateInput;
  EntryRequirement: EntryRequirement;
  EntryRequirement_createInput: EntryRequirement_CreateInput;
  EntryRequirement_updateInput: EntryRequirement_UpdateInput;
  EscoExtension: EscoExtension;
  EscoExtension_createInput: EscoExtension_CreateInput;
  EscoExtension_updateInput: EscoExtension_UpdateInput;
  ID: Scalars['ID']['output'];
  Int: Scalars['Int']['output'];
  JSON: Scalars['JSON']['output'];
  LabelRole: LabelRole;
  LabelRole_createInput: LabelRole_CreateInput;
  LabelRole_updateInput: LabelRole_UpdateInput;
  Language: Language;
  Literal_rdfs: Literal_Rdfs;
  MemberConcept: MemberConcept;
  MemberConcept_createInput: MemberConcept_CreateInput;
  MemberConcept_updateInput: MemberConcept_UpdateInput;
  Mutation: {};
  NodeLiteral: NodeLiteral;
  NodeLiteral_createInput: NodeLiteral_CreateInput;
  NodeLiteral_updateInput: NodeLiteral_UpdateInput;
  Occupation: Occupation;
  Occupation_createInput: Occupation_CreateInput;
  Occupation_updateInput: Occupation_UpdateInput;
  PivotTableQuery: PivotTableQuery;
  PivotTableQueryFilter: PivotTableQueryFilter;
  Qualification: Qualification;
  Qualification_createInput: Qualification_CreateInput;
  Qualification_updateInput: Qualification_UpdateInput;
  Query: {};
  Recognition: Recognition;
  Recognition_createInput: Recognition_CreateInput;
  Recognition_updateInput: Recognition_UpdateInput;
  Resource: Resource;
  Skill: Skill;
  SkillCompetenceType: SkillCompetenceType;
  SkillCompetenceType_createInput: SkillCompetenceType_CreateInput;
  SkillCompetenceType_updateInput: SkillCompetenceType_UpdateInput;
  Skill_createInput: Skill_CreateInput;
  Skill_updateInput: Skill_UpdateInput;
  Standard: Standard;
  String: Scalars['String']['output'];
  String_xsd: String_Xsd;
  WorkContext: WorkContext;
  WorkContext_createInput: WorkContext_CreateInput;
  WorkContext_updateInput: WorkContext_UpdateInput;
  anyURI: AnyUri;
  b0_n44: B0_N44;
  b0_n44_createInput: B0_N44_CreateInput;
  b0_n44_updateInput: B0_N44_UpdateInput;
  b0_n48: B0_N48;
  b0_n48_createInput: B0_N48_CreateInput;
  b0_n48_updateInput: B0_N48_UpdateInput;
  b0_n55: B0_N55;
  b0_n55_createInput: B0_N55_CreateInput;
  b0_n55_updateInput: B0_N55_UpdateInput;
  b0_n59: B0_N59;
  b0_n59_createInput: B0_N59_CreateInput;
  b0_n59_updateInput: B0_N59_UpdateInput;
  b0_n66: B0_N66;
  b0_n66_createInput: B0_N66_CreateInput;
  b0_n66_updateInput: B0_N66_UpdateInput;
  boolean: Boolean;
  createAccreditationInput: CreateAccreditationInput;
  createAssociationObjectInput: CreateAssociationObjectInput;
  createAwardingActivityInput: CreateAwardingActivityInput;
  createAwardingBodyInput: CreateAwardingBodyInput;
  createCollectionInput: CreateCollectionInput;
  createConceptInput: CreateConceptInput;
  createConceptSchemeInput: CreateConceptSchemeInput;
  createEntryRequirementInput: CreateEntryRequirementInput;
  createEscoExtensionInput: CreateEscoExtensionInput;
  createLabelRoleInput: CreateLabelRoleInput;
  createMemberConceptInput: CreateMemberConceptInput;
  createNodeLiteralInput: CreateNodeLiteralInput;
  createOccupationInput: CreateOccupationInput;
  createQualificationInput: CreateQualificationInput;
  createRecognitionInput: CreateRecognitionInput;
  createSkillCompetenceTypeInput: CreateSkillCompetenceTypeInput;
  createSkillInput: CreateSkillInput;
  createWorkContextInput: CreateWorkContextInput;
  createb0_n44Input: Createb0_N44Input;
  createb0_n48Input: Createb0_N48Input;
  createb0_n55Input: Createb0_N55Input;
  createb0_n59Input: Createb0_N59Input;
  createb0_n66Input: Createb0_N66Input;
  decimal: Decimal;
  deleteAccreditationInput: DeleteAccreditationInput;
  deleteAssociationObjectInput: DeleteAssociationObjectInput;
  deleteAwardingActivityInput: DeleteAwardingActivityInput;
  deleteAwardingBodyInput: DeleteAwardingBodyInput;
  deleteCollectionInput: DeleteCollectionInput;
  deleteConceptInput: DeleteConceptInput;
  deleteConceptSchemeInput: DeleteConceptSchemeInput;
  deleteEntryRequirementInput: DeleteEntryRequirementInput;
  deleteEscoExtensionInput: DeleteEscoExtensionInput;
  deleteLabelRoleInput: DeleteLabelRoleInput;
  deleteMemberConceptInput: DeleteMemberConceptInput;
  deleteNodeLiteralInput: DeleteNodeLiteralInput;
  deleteOccupationInput: DeleteOccupationInput;
  deleteQualificationInput: DeleteQualificationInput;
  deleteRecognitionInput: DeleteRecognitionInput;
  deleteSkillCompetenceTypeInput: DeleteSkillCompetenceTypeInput;
  deleteSkillInput: DeleteSkillInput;
  deleteWorkContextInput: DeleteWorkContextInput;
  deleteb0_n44Input: Deleteb0_N44Input;
  deleteb0_n48Input: Deleteb0_N48Input;
  deleteb0_n55Input: Deleteb0_N55Input;
  deleteb0_n59Input: Deleteb0_N59Input;
  deleteb0_n66Input: Deleteb0_N66Input;
  duration: Duration;
  editorialStatusDataType: EditorialStatusDataType;
  refine_where_locator: Refine_Where_Locator;
  updateAccreditationInput: UpdateAccreditationInput;
  updateAssociationObjectInput: UpdateAssociationObjectInput;
  updateAwardingActivityInput: UpdateAwardingActivityInput;
  updateAwardingBodyInput: UpdateAwardingBodyInput;
  updateCollectionInput: UpdateCollectionInput;
  updateConceptInput: UpdateConceptInput;
  updateConceptSchemeInput: UpdateConceptSchemeInput;
  updateEntryRequirementInput: UpdateEntryRequirementInput;
  updateEscoExtensionInput: UpdateEscoExtensionInput;
  updateLabelRoleInput: UpdateLabelRoleInput;
  updateMemberConceptInput: UpdateMemberConceptInput;
  updateNodeLiteralInput: UpdateNodeLiteralInput;
  updateOccupationInput: UpdateOccupationInput;
  updateQualificationInput: UpdateQualificationInput;
  updateRecognitionInput: UpdateRecognitionInput;
  updateSkillCompetenceTypeInput: UpdateSkillCompetenceTypeInput;
  updateSkillInput: UpdateSkillInput;
  updateWorkContextInput: UpdateWorkContextInput;
  updateb0_n44Input: Updateb0_N44Input;
  updateb0_n48Input: Updateb0_N48Input;
  updateb0_n55Input: Updateb0_N55Input;
  updateb0_n59Input: Updateb0_N59Input;
  updateb0_n66Input: Updateb0_N66Input;
}>;

export type AccreditationResolvers<ContextType = any, ParentType extends ResolversParentTypes['Accreditation'] = ResolversParentTypes['Accreditation']> = ResolversObject<{
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type AssociationObjectResolvers<ContextType = any, ParentType extends ResolversParentTypes['AssociationObject'] = ResolversParentTypes['AssociationObject']> = ResolversObject<{
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  target?: Resolver<Maybe<Array<ResolversTypes['Resource']>>, ParentType, ContextType>;
  targetDescription?: Resolver<Maybe<Array<ResolversTypes['Literal_rdfs']>>, ParentType, ContextType>;
  targetFramework?: Resolver<Maybe<Array<ResolversTypes['Standard']>>, ParentType, ContextType>;
  targetName?: Resolver<Maybe<Array<ResolversTypes['Literal_rdfs']>>, ParentType, ContextType>;
  targetNotation?: Resolver<Maybe<Array<ResolversTypes['Literal_rdfs']>>, ParentType, ContextType>;
  targetURL?: Resolver<Maybe<Array<ResolversTypes['anyURI']>>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type AwardingActivityResolvers<ContextType = any, ParentType extends ResolversParentTypes['AwardingActivity'] = ResolversParentTypes['AwardingActivity']> = ResolversObject<{
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type AwardingBodyResolvers<ContextType = any, ParentType extends ResolversParentTypes['AwardingBody'] = ResolversParentTypes['AwardingBody']> = ResolversObject<{
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type CollectionResolvers<ContextType = any, ParentType extends ResolversParentTypes['Collection'] = ResolversParentTypes['Collection']> = ResolversObject<{
  altLabel?: Resolver<Maybe<Array<ResolversTypes['Literal_rdfs']>>, ParentType, ContextType>;
  definition?: Resolver<Maybe<Array<ResolversTypes['String_xsd']>>, ParentType, ContextType>;
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  member?: Resolver<Maybe<Array<ResolversTypes['Collection']>>, ParentType, ContextType>;
  memberOf?: Resolver<Maybe<Array<ResolversTypes['Collection']>>, ParentType, ContextType>;
  prefLabel?: Resolver<Maybe<Array<ResolversTypes['Literal_rdfs']>>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type ConceptResolvers<ContextType = any, ParentType extends ResolversParentTypes['Concept'] = ResolversParentTypes['Concept']> = ResolversObject<{
  altLabel?: Resolver<Maybe<Array<ResolversTypes['Literal_rdfs']>>, ParentType, ContextType>;
  broader?: Resolver<Maybe<Array<ResolversTypes['Concept']>>, ParentType, ContextType>;
  definition?: Resolver<Maybe<Array<ResolversTypes['String_xsd']>>, ParentType, ContextType>;
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  memberOf?: Resolver<Maybe<Array<ResolversTypes['Collection']>>, ParentType, ContextType>;
  narrower?: Resolver<Maybe<Array<ResolversTypes['Concept']>>, ParentType, ContextType>;
  prefLabel?: Resolver<Maybe<Array<ResolversTypes['Literal_rdfs']>>, ParentType, ContextType>;
  replacedBy?: Resolver<Maybe<Array<ResolversTypes['Concept']>>, ParentType, ContextType>;
  replaces?: Resolver<Maybe<Array<ResolversTypes['Concept']>>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type ConceptSchemeResolvers<ContextType = any, ParentType extends ResolversParentTypes['ConceptScheme'] = ResolversParentTypes['ConceptScheme']> = ResolversObject<{
  altLabel?: Resolver<Maybe<Array<ResolversTypes['Literal_rdfs']>>, ParentType, ContextType>;
  definition?: Resolver<Maybe<Array<ResolversTypes['String_xsd']>>, ParentType, ContextType>;
  hasPolyHierarchy?: Resolver<Maybe<Array<ResolversTypes['boolean']>>, ParentType, ContextType>;
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  prefLabel?: Resolver<Maybe<Array<ResolversTypes['Literal_rdfs']>>, ParentType, ContextType>;
  supportedLanguage?: Resolver<Maybe<Array<ResolversTypes['Language']>>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type EntryRequirementResolvers<ContextType = any, ParentType extends ResolversParentTypes['EntryRequirement'] = ResolversParentTypes['EntryRequirement']> = ResolversObject<{
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  requirementLevel?: Resolver<Maybe<Array<ResolversTypes['Concept']>>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type EscoExtensionResolvers<ContextType = any, ParentType extends ResolversParentTypes['EscoExtension'] = ResolversParentTypes['EscoExtension']> = ResolversObject<{
  altLabel?: Resolver<Maybe<Array<ResolversTypes['Literal_rdfs']>>, ParentType, ContextType>;
  broader?: Resolver<Maybe<Array<ResolversTypes['Concept']>>, ParentType, ContextType>;
  definition?: Resolver<Maybe<Array<ResolversTypes['String_xsd']>>, ParentType, ContextType>;
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  memberOf?: Resolver<Maybe<Array<ResolversTypes['Collection']>>, ParentType, ContextType>;
  narrower?: Resolver<Maybe<Array<ResolversTypes['Concept']>>, ParentType, ContextType>;
  prefLabel?: Resolver<Maybe<Array<ResolversTypes['Literal_rdfs']>>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export interface JsonScalarConfig extends GraphQLScalarTypeConfig<ResolversTypes['JSON'], any> {
  name: 'JSON';
}

export type LabelRoleResolvers<ContextType = any, ParentType extends ResolversParentTypes['LabelRole'] = ResolversParentTypes['LabelRole']> = ResolversObject<{
  altLabel?: Resolver<Maybe<Array<ResolversTypes['Literal_rdfs']>>, ParentType, ContextType>;
  broader?: Resolver<Maybe<Array<ResolversTypes['Concept']>>, ParentType, ContextType>;
  definition?: Resolver<Maybe<Array<ResolversTypes['String_xsd']>>, ParentType, ContextType>;
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  memberOf?: Resolver<Maybe<Array<ResolversTypes['Collection']>>, ParentType, ContextType>;
  narrower?: Resolver<Maybe<Array<ResolversTypes['Concept']>>, ParentType, ContextType>;
  prefLabel?: Resolver<Maybe<Array<ResolversTypes['Literal_rdfs']>>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type LanguageResolvers<ContextType = any, ParentType extends ResolversParentTypes['Language'] = ResolversParentTypes['Language']> = ResolversObject<{
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type Literal_RdfsResolvers<ContextType = any, ParentType extends ResolversParentTypes['Literal_rdfs'] = ResolversParentTypes['Literal_rdfs']> = ResolversObject<{
  datatype?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  language?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  termType?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  value?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type MemberConceptResolvers<ContextType = any, ParentType extends ResolversParentTypes['MemberConcept'] = ResolversParentTypes['MemberConcept']> = ResolversObject<{
  altLabel?: Resolver<Maybe<Array<ResolversTypes['Literal_rdfs']>>, ParentType, ContextType>;
  broader?: Resolver<Maybe<Array<ResolversTypes['Concept']>>, ParentType, ContextType>;
  definition?: Resolver<Maybe<Array<ResolversTypes['String_xsd']>>, ParentType, ContextType>;
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  memberOf?: Resolver<Maybe<Array<ResolversTypes['Collection']>>, ParentType, ContextType>;
  narrower?: Resolver<Maybe<Array<ResolversTypes['Concept']>>, ParentType, ContextType>;
  prefLabel?: Resolver<Maybe<Array<ResolversTypes['Literal_rdfs']>>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type MutationResolvers<ContextType = any, ParentType extends ResolversParentTypes['Mutation'] = ResolversParentTypes['Mutation']> = ResolversObject<{
  Accreditation_create?: Resolver<Maybe<ResolversTypes['Accreditation']>, ParentType, ContextType, Partial<MutationAccreditation_CreateArgs>>;
  Accreditation_delete?: Resolver<Maybe<ResolversTypes['Accreditation']>, ParentType, ContextType, Partial<MutationAccreditation_DeleteArgs>>;
  Accreditation_update?: Resolver<Maybe<ResolversTypes['Accreditation']>, ParentType, ContextType, Partial<MutationAccreditation_UpdateArgs>>;
  AssociationObject_create?: Resolver<Maybe<ResolversTypes['AssociationObject']>, ParentType, ContextType, Partial<MutationAssociationObject_CreateArgs>>;
  AssociationObject_delete?: Resolver<Maybe<ResolversTypes['AssociationObject']>, ParentType, ContextType, Partial<MutationAssociationObject_DeleteArgs>>;
  AssociationObject_update?: Resolver<Maybe<ResolversTypes['AssociationObject']>, ParentType, ContextType, Partial<MutationAssociationObject_UpdateArgs>>;
  AwardingActivity_create?: Resolver<Maybe<ResolversTypes['AwardingActivity']>, ParentType, ContextType, Partial<MutationAwardingActivity_CreateArgs>>;
  AwardingActivity_delete?: Resolver<Maybe<ResolversTypes['AwardingActivity']>, ParentType, ContextType, Partial<MutationAwardingActivity_DeleteArgs>>;
  AwardingActivity_update?: Resolver<Maybe<ResolversTypes['AwardingActivity']>, ParentType, ContextType, Partial<MutationAwardingActivity_UpdateArgs>>;
  AwardingBody_create?: Resolver<Maybe<ResolversTypes['AwardingBody']>, ParentType, ContextType, Partial<MutationAwardingBody_CreateArgs>>;
  AwardingBody_delete?: Resolver<Maybe<ResolversTypes['AwardingBody']>, ParentType, ContextType, Partial<MutationAwardingBody_DeleteArgs>>;
  AwardingBody_update?: Resolver<Maybe<ResolversTypes['AwardingBody']>, ParentType, ContextType, Partial<MutationAwardingBody_UpdateArgs>>;
  Collection_create?: Resolver<Maybe<ResolversTypes['Collection']>, ParentType, ContextType, Partial<MutationCollection_CreateArgs>>;
  Collection_delete?: Resolver<Maybe<ResolversTypes['Collection']>, ParentType, ContextType, Partial<MutationCollection_DeleteArgs>>;
  Collection_update?: Resolver<Maybe<ResolversTypes['Collection']>, ParentType, ContextType, Partial<MutationCollection_UpdateArgs>>;
  ConceptScheme_create?: Resolver<Maybe<ResolversTypes['ConceptScheme']>, ParentType, ContextType, Partial<MutationConceptScheme_CreateArgs>>;
  ConceptScheme_delete?: Resolver<Maybe<ResolversTypes['ConceptScheme']>, ParentType, ContextType, Partial<MutationConceptScheme_DeleteArgs>>;
  ConceptScheme_update?: Resolver<Maybe<ResolversTypes['ConceptScheme']>, ParentType, ContextType, Partial<MutationConceptScheme_UpdateArgs>>;
  Concept_create?: Resolver<Maybe<ResolversTypes['Concept']>, ParentType, ContextType, Partial<MutationConcept_CreateArgs>>;
  Concept_delete?: Resolver<Maybe<ResolversTypes['Concept']>, ParentType, ContextType, Partial<MutationConcept_DeleteArgs>>;
  Concept_update?: Resolver<Maybe<ResolversTypes['Concept']>, ParentType, ContextType, Partial<MutationConcept_UpdateArgs>>;
  EntryRequirement_create?: Resolver<Maybe<ResolversTypes['EntryRequirement']>, ParentType, ContextType, Partial<MutationEntryRequirement_CreateArgs>>;
  EntryRequirement_delete?: Resolver<Maybe<ResolversTypes['EntryRequirement']>, ParentType, ContextType, Partial<MutationEntryRequirement_DeleteArgs>>;
  EntryRequirement_update?: Resolver<Maybe<ResolversTypes['EntryRequirement']>, ParentType, ContextType, Partial<MutationEntryRequirement_UpdateArgs>>;
  EscoExtension_create?: Resolver<Maybe<ResolversTypes['EscoExtension']>, ParentType, ContextType, Partial<MutationEscoExtension_CreateArgs>>;
  EscoExtension_delete?: Resolver<Maybe<ResolversTypes['EscoExtension']>, ParentType, ContextType, Partial<MutationEscoExtension_DeleteArgs>>;
  EscoExtension_update?: Resolver<Maybe<ResolversTypes['EscoExtension']>, ParentType, ContextType, Partial<MutationEscoExtension_UpdateArgs>>;
  LabelRole_create?: Resolver<Maybe<ResolversTypes['LabelRole']>, ParentType, ContextType, Partial<MutationLabelRole_CreateArgs>>;
  LabelRole_delete?: Resolver<Maybe<ResolversTypes['LabelRole']>, ParentType, ContextType, Partial<MutationLabelRole_DeleteArgs>>;
  LabelRole_update?: Resolver<Maybe<ResolversTypes['LabelRole']>, ParentType, ContextType, Partial<MutationLabelRole_UpdateArgs>>;
  MemberConcept_create?: Resolver<Maybe<ResolversTypes['MemberConcept']>, ParentType, ContextType, Partial<MutationMemberConcept_CreateArgs>>;
  MemberConcept_delete?: Resolver<Maybe<ResolversTypes['MemberConcept']>, ParentType, ContextType, Partial<MutationMemberConcept_DeleteArgs>>;
  MemberConcept_update?: Resolver<Maybe<ResolversTypes['MemberConcept']>, ParentType, ContextType, Partial<MutationMemberConcept_UpdateArgs>>;
  NodeLiteral_create?: Resolver<Maybe<ResolversTypes['NodeLiteral']>, ParentType, ContextType, Partial<MutationNodeLiteral_CreateArgs>>;
  NodeLiteral_delete?: Resolver<Maybe<ResolversTypes['NodeLiteral']>, ParentType, ContextType, Partial<MutationNodeLiteral_DeleteArgs>>;
  NodeLiteral_update?: Resolver<Maybe<ResolversTypes['NodeLiteral']>, ParentType, ContextType, Partial<MutationNodeLiteral_UpdateArgs>>;
  Occupation_create?: Resolver<Maybe<ResolversTypes['Occupation']>, ParentType, ContextType, Partial<MutationOccupation_CreateArgs>>;
  Occupation_delete?: Resolver<Maybe<ResolversTypes['Occupation']>, ParentType, ContextType, Partial<MutationOccupation_DeleteArgs>>;
  Occupation_update?: Resolver<Maybe<ResolversTypes['Occupation']>, ParentType, ContextType, Partial<MutationOccupation_UpdateArgs>>;
  Qualification_create?: Resolver<Maybe<ResolversTypes['Qualification']>, ParentType, ContextType, Partial<MutationQualification_CreateArgs>>;
  Qualification_delete?: Resolver<Maybe<ResolversTypes['Qualification']>, ParentType, ContextType, Partial<MutationQualification_DeleteArgs>>;
  Qualification_update?: Resolver<Maybe<ResolversTypes['Qualification']>, ParentType, ContextType, Partial<MutationQualification_UpdateArgs>>;
  Recognition_create?: Resolver<Maybe<ResolversTypes['Recognition']>, ParentType, ContextType, Partial<MutationRecognition_CreateArgs>>;
  Recognition_delete?: Resolver<Maybe<ResolversTypes['Recognition']>, ParentType, ContextType, Partial<MutationRecognition_DeleteArgs>>;
  Recognition_update?: Resolver<Maybe<ResolversTypes['Recognition']>, ParentType, ContextType, Partial<MutationRecognition_UpdateArgs>>;
  SkillCompetenceType_create?: Resolver<Maybe<ResolversTypes['SkillCompetenceType']>, ParentType, ContextType, Partial<MutationSkillCompetenceType_CreateArgs>>;
  SkillCompetenceType_delete?: Resolver<Maybe<ResolversTypes['SkillCompetenceType']>, ParentType, ContextType, Partial<MutationSkillCompetenceType_DeleteArgs>>;
  SkillCompetenceType_update?: Resolver<Maybe<ResolversTypes['SkillCompetenceType']>, ParentType, ContextType, Partial<MutationSkillCompetenceType_UpdateArgs>>;
  Skill_create?: Resolver<Maybe<ResolversTypes['Skill']>, ParentType, ContextType, Partial<MutationSkill_CreateArgs>>;
  Skill_delete?: Resolver<Maybe<ResolversTypes['Skill']>, ParentType, ContextType, Partial<MutationSkill_DeleteArgs>>;
  Skill_update?: Resolver<Maybe<ResolversTypes['Skill']>, ParentType, ContextType, Partial<MutationSkill_UpdateArgs>>;
  WorkContext_create?: Resolver<Maybe<ResolversTypes['WorkContext']>, ParentType, ContextType, Partial<MutationWorkContext_CreateArgs>>;
  WorkContext_delete?: Resolver<Maybe<ResolversTypes['WorkContext']>, ParentType, ContextType, Partial<MutationWorkContext_DeleteArgs>>;
  WorkContext_update?: Resolver<Maybe<ResolversTypes['WorkContext']>, ParentType, ContextType, Partial<MutationWorkContext_UpdateArgs>>;
  b0_n44_create?: Resolver<Maybe<ResolversTypes['b0_n44']>, ParentType, ContextType, Partial<MutationB0_N44_CreateArgs>>;
  b0_n44_delete?: Resolver<Maybe<ResolversTypes['b0_n44']>, ParentType, ContextType, Partial<MutationB0_N44_DeleteArgs>>;
  b0_n44_update?: Resolver<Maybe<ResolversTypes['b0_n44']>, ParentType, ContextType, Partial<MutationB0_N44_UpdateArgs>>;
  b0_n48_create?: Resolver<Maybe<ResolversTypes['b0_n48']>, ParentType, ContextType, Partial<MutationB0_N48_CreateArgs>>;
  b0_n48_delete?: Resolver<Maybe<ResolversTypes['b0_n48']>, ParentType, ContextType, Partial<MutationB0_N48_DeleteArgs>>;
  b0_n48_update?: Resolver<Maybe<ResolversTypes['b0_n48']>, ParentType, ContextType, Partial<MutationB0_N48_UpdateArgs>>;
  b0_n55_create?: Resolver<Maybe<ResolversTypes['b0_n55']>, ParentType, ContextType, Partial<MutationB0_N55_CreateArgs>>;
  b0_n55_delete?: Resolver<Maybe<ResolversTypes['b0_n55']>, ParentType, ContextType, Partial<MutationB0_N55_DeleteArgs>>;
  b0_n55_update?: Resolver<Maybe<ResolversTypes['b0_n55']>, ParentType, ContextType, Partial<MutationB0_N55_UpdateArgs>>;
  b0_n59_create?: Resolver<Maybe<ResolversTypes['b0_n59']>, ParentType, ContextType, Partial<MutationB0_N59_CreateArgs>>;
  b0_n59_delete?: Resolver<Maybe<ResolversTypes['b0_n59']>, ParentType, ContextType, Partial<MutationB0_N59_DeleteArgs>>;
  b0_n59_update?: Resolver<Maybe<ResolversTypes['b0_n59']>, ParentType, ContextType, Partial<MutationB0_N59_UpdateArgs>>;
  b0_n66_create?: Resolver<Maybe<ResolversTypes['b0_n66']>, ParentType, ContextType, Partial<MutationB0_N66_CreateArgs>>;
  b0_n66_delete?: Resolver<Maybe<ResolversTypes['b0_n66']>, ParentType, ContextType, Partial<MutationB0_N66_DeleteArgs>>;
  b0_n66_update?: Resolver<Maybe<ResolversTypes['b0_n66']>, ParentType, ContextType, Partial<MutationB0_N66_UpdateArgs>>;
}>;

export type NodeLiteralResolvers<ContextType = any, ParentType extends ResolversParentTypes['NodeLiteral'] = ResolversParentTypes['NodeLiteral']> = ResolversObject<{
  altLabel?: Resolver<Maybe<Array<ResolversTypes['Literal_rdfs']>>, ParentType, ContextType>;
  broader?: Resolver<Maybe<Array<ResolversTypes['Concept']>>, ParentType, ContextType>;
  definition?: Resolver<Maybe<Array<ResolversTypes['String_xsd']>>, ParentType, ContextType>;
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  memberOf?: Resolver<Maybe<Array<ResolversTypes['Collection']>>, ParentType, ContextType>;
  narrower?: Resolver<Maybe<Array<ResolversTypes['Concept']>>, ParentType, ContextType>;
  nodeLiteral?: Resolver<Maybe<Array<ResolversTypes['Literal_rdfs']>>, ParentType, ContextType>;
  prefLabel?: Resolver<Maybe<Array<ResolversTypes['Literal_rdfs']>>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type OccupationResolvers<ContextType = any, ParentType extends ResolversParentTypes['Occupation'] = ResolversParentTypes['Occupation']> = ResolversObject<{
  altLabel?: Resolver<Maybe<Array<ResolversTypes['Literal_rdfs']>>, ParentType, ContextType>;
  broader?: Resolver<Maybe<Array<ResolversTypes['Concept']>>, ParentType, ContextType>;
  definition?: Resolver<Maybe<Array<ResolversTypes['String_xsd']>>, ParentType, ContextType>;
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  memberOf?: Resolver<Maybe<Array<ResolversTypes['Collection']>>, ParentType, ContextType>;
  narrower?: Resolver<Maybe<Array<ResolversTypes['Concept']>>, ParentType, ContextType>;
  prefLabel?: Resolver<Maybe<Array<ResolversTypes['Literal_rdfs']>>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type PivotTableQueryResolvers<ContextType = any, ParentType extends ResolversParentTypes['PivotTableQuery'] = ResolversParentTypes['PivotTableQuery']> = ResolversObject<{
  broader?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  narrower?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  type?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type QualificationResolvers<ContextType = any, ParentType extends ResolversParentTypes['Qualification'] = ResolversParentTypes['Qualification']> = ResolversObject<{
  altLabel?: Resolver<Maybe<Array<ResolversTypes['Literal_rdfs']>>, ParentType, ContextType>;
  broader?: Resolver<Maybe<Array<ResolversTypes['Concept']>>, ParentType, ContextType>;
  definition?: Resolver<Maybe<Array<ResolversTypes['String_xsd']>>, ParentType, ContextType>;
  expirationPeriod?: Resolver<Maybe<Array<ResolversTypes['duration']>>, ParentType, ContextType>;
  expiryPeriod?: Resolver<Maybe<Array<ResolversTypes['duration']>>, ParentType, ContextType>;
  hasAccreditation?: Resolver<Maybe<Array<ResolversTypes['Accreditation']>>, ParentType, ContextType>;
  hasAwardingActivity?: Resolver<Maybe<Array<ResolversTypes['AwardingActivity']>>, ParentType, ContextType>;
  hasECTSCreditPoints?: Resolver<Maybe<Array<ResolversTypes['decimal']>>, ParentType, ContextType>;
  hasEntryRequirement?: Resolver<Maybe<Array<ResolversTypes['EntryRequirement']>>, ParentType, ContextType>;
  hasRecognition?: Resolver<Maybe<Array<ResolversTypes['Recognition']>>, ParentType, ContextType>;
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  isPartialQualification?: Resolver<Maybe<Array<ResolversTypes['boolean']>>, ParentType, ContextType>;
  memberOf?: Resolver<Maybe<Array<ResolversTypes['Collection']>>, ParentType, ContextType>;
  narrower?: Resolver<Maybe<Array<ResolversTypes['Concept']>>, ParentType, ContextType>;
  prefLabel?: Resolver<Maybe<Array<ResolversTypes['Literal_rdfs']>>, ParentType, ContextType>;
  volumeOfLearning?: Resolver<Maybe<Array<ResolversTypes['duration']>>, ParentType, ContextType>;
  waysToAcquire?: Resolver<Maybe<Array<ResolversTypes['Concept']>>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type QueryResolvers<ContextType = any, ParentType extends ResolversParentTypes['Query'] = ResolversParentTypes['Query']> = ResolversObject<{
  Accreditation?: Resolver<ResolversTypes['Accreditation'], ParentType, ContextType, RequireFields<QueryAccreditationArgs, 'id'>>;
  AssociationObject?: Resolver<ResolversTypes['AssociationObject'], ParentType, ContextType, RequireFields<QueryAssociationObjectArgs, 'id'>>;
  AwardingActivity?: Resolver<ResolversTypes['AwardingActivity'], ParentType, ContextType, RequireFields<QueryAwardingActivityArgs, 'id'>>;
  AwardingBody?: Resolver<ResolversTypes['AwardingBody'], ParentType, ContextType, RequireFields<QueryAwardingBodyArgs, 'id'>>;
  Collection?: Resolver<ResolversTypes['Collection'], ParentType, ContextType, RequireFields<QueryCollectionArgs, 'id'>>;
  Concept?: Resolver<ResolversTypes['Concept'], ParentType, ContextType, RequireFields<QueryConceptArgs, 'id'>>;
  ConceptScheme?: Resolver<ResolversTypes['ConceptScheme'], ParentType, ContextType, RequireFields<QueryConceptSchemeArgs, 'id'>>;
  EntryRequirement?: Resolver<ResolversTypes['EntryRequirement'], ParentType, ContextType, RequireFields<QueryEntryRequirementArgs, 'id'>>;
  EscoExtension?: Resolver<ResolversTypes['EscoExtension'], ParentType, ContextType, RequireFields<QueryEscoExtensionArgs, 'id'>>;
  LabelRole?: Resolver<ResolversTypes['LabelRole'], ParentType, ContextType, RequireFields<QueryLabelRoleArgs, 'id'>>;
  MemberConcept?: Resolver<ResolversTypes['MemberConcept'], ParentType, ContextType, RequireFields<QueryMemberConceptArgs, 'id'>>;
  NodeLiteral?: Resolver<ResolversTypes['NodeLiteral'], ParentType, ContextType, RequireFields<QueryNodeLiteralArgs, 'id'>>;
  Occupation?: Resolver<ResolversTypes['Occupation'], ParentType, ContextType, RequireFields<QueryOccupationArgs, 'id'>>;
  PivotTableQuery?: Resolver<Maybe<Array<Maybe<ResolversTypes['PivotTableQuery']>>>, ParentType, ContextType, Partial<QueryPivotTableQueryArgs>>;
  Qualification?: Resolver<ResolversTypes['Qualification'], ParentType, ContextType, RequireFields<QueryQualificationArgs, 'id'>>;
  Recognition?: Resolver<ResolversTypes['Recognition'], ParentType, ContextType, RequireFields<QueryRecognitionArgs, 'id'>>;
  Skill?: Resolver<ResolversTypes['Skill'], ParentType, ContextType, RequireFields<QuerySkillArgs, 'id'>>;
  SkillCompetenceType?: Resolver<ResolversTypes['SkillCompetenceType'], ParentType, ContextType, RequireFields<QuerySkillCompetenceTypeArgs, 'id'>>;
  WorkContext?: Resolver<ResolversTypes['WorkContext'], ParentType, ContextType, RequireFields<QueryWorkContextArgs, 'id'>>;
  allAccreditation?: Resolver<Array<ResolversTypes['Accreditation']>, ParentType, ContextType, Partial<QueryAllAccreditationArgs>>;
  allAssociationObject?: Resolver<Array<ResolversTypes['AssociationObject']>, ParentType, ContextType, Partial<QueryAllAssociationObjectArgs>>;
  allAwardingActivity?: Resolver<Array<ResolversTypes['AwardingActivity']>, ParentType, ContextType, Partial<QueryAllAwardingActivityArgs>>;
  allAwardingBody?: Resolver<Array<ResolversTypes['AwardingBody']>, ParentType, ContextType, Partial<QueryAllAwardingBodyArgs>>;
  allCollection?: Resolver<Array<ResolversTypes['Collection']>, ParentType, ContextType, Partial<QueryAllCollectionArgs>>;
  allConcept?: Resolver<Array<ResolversTypes['Concept']>, ParentType, ContextType, Partial<QueryAllConceptArgs>>;
  allConceptScheme?: Resolver<Array<ResolversTypes['ConceptScheme']>, ParentType, ContextType, Partial<QueryAllConceptSchemeArgs>>;
  allEntryRequirement?: Resolver<Array<ResolversTypes['EntryRequirement']>, ParentType, ContextType, Partial<QueryAllEntryRequirementArgs>>;
  allEscoExtension?: Resolver<Array<ResolversTypes['EscoExtension']>, ParentType, ContextType, Partial<QueryAllEscoExtensionArgs>>;
  allLabelRole?: Resolver<Array<ResolversTypes['LabelRole']>, ParentType, ContextType, Partial<QueryAllLabelRoleArgs>>;
  allMemberConcept?: Resolver<Array<ResolversTypes['MemberConcept']>, ParentType, ContextType, Partial<QueryAllMemberConceptArgs>>;
  allNodeLiteral?: Resolver<Array<ResolversTypes['NodeLiteral']>, ParentType, ContextType, Partial<QueryAllNodeLiteralArgs>>;
  allOccupation?: Resolver<Array<ResolversTypes['Occupation']>, ParentType, ContextType, Partial<QueryAllOccupationArgs>>;
  allQualification?: Resolver<Array<ResolversTypes['Qualification']>, ParentType, ContextType, Partial<QueryAllQualificationArgs>>;
  allRecognition?: Resolver<Array<ResolversTypes['Recognition']>, ParentType, ContextType, Partial<QueryAllRecognitionArgs>>;
  allSkill?: Resolver<Array<ResolversTypes['Skill']>, ParentType, ContextType, Partial<QueryAllSkillArgs>>;
  allSkillCompetenceType?: Resolver<Array<ResolversTypes['SkillCompetenceType']>, ParentType, ContextType, Partial<QueryAllSkillCompetenceTypeArgs>>;
  allWorkContext?: Resolver<Array<ResolversTypes['WorkContext']>, ParentType, ContextType, Partial<QueryAllWorkContextArgs>>;
  allb0_n44?: Resolver<Array<ResolversTypes['b0_n44']>, ParentType, ContextType, Partial<QueryAllb0_N44Args>>;
  allb0_n48?: Resolver<Array<ResolversTypes['b0_n48']>, ParentType, ContextType, Partial<QueryAllb0_N48Args>>;
  allb0_n55?: Resolver<Array<ResolversTypes['b0_n55']>, ParentType, ContextType, Partial<QueryAllb0_N55Args>>;
  allb0_n59?: Resolver<Array<ResolversTypes['b0_n59']>, ParentType, ContextType, Partial<QueryAllb0_N59Args>>;
  allb0_n66?: Resolver<Array<ResolversTypes['b0_n66']>, ParentType, ContextType, Partial<QueryAllb0_N66Args>>;
  b0_n44?: Resolver<ResolversTypes['b0_n44'], ParentType, ContextType, RequireFields<QueryB0_N44Args, 'id'>>;
  b0_n48?: Resolver<ResolversTypes['b0_n48'], ParentType, ContextType, RequireFields<QueryB0_N48Args, 'id'>>;
  b0_n55?: Resolver<ResolversTypes['b0_n55'], ParentType, ContextType, RequireFields<QueryB0_N55Args, 'id'>>;
  b0_n59?: Resolver<ResolversTypes['b0_n59'], ParentType, ContextType, RequireFields<QueryB0_N59Args, 'id'>>;
  b0_n66?: Resolver<ResolversTypes['b0_n66'], ParentType, ContextType, RequireFields<QueryB0_N66Args, 'id'>>;
}>;

export type RecognitionResolvers<ContextType = any, ParentType extends ResolversParentTypes['Recognition'] = ResolversParentTypes['Recognition']> = ResolversObject<{
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type ResourceResolvers<ContextType = any, ParentType extends ResolversParentTypes['Resource'] = ResolversParentTypes['Resource']> = ResolversObject<{
  altLabel?: Resolver<Maybe<Array<ResolversTypes['Literal_rdfs']>>, ParentType, ContextType>;
  definition?: Resolver<Maybe<Array<ResolversTypes['String_xsd']>>, ParentType, ContextType>;
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  prefLabel?: Resolver<Maybe<Array<ResolversTypes['Literal_rdfs']>>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type SkillResolvers<ContextType = any, ParentType extends ResolversParentTypes['Skill'] = ResolversParentTypes['Skill']> = ResolversObject<{
  altLabel?: Resolver<Maybe<Array<ResolversTypes['Literal_rdfs']>>, ParentType, ContextType>;
  broader?: Resolver<Maybe<Array<ResolversTypes['Concept']>>, ParentType, ContextType>;
  definition?: Resolver<Maybe<Array<ResolversTypes['String_xsd']>>, ParentType, ContextType>;
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  isEssentialSkillFor?: Resolver<Maybe<Array<ResolversTypes['b0_n44']>>, ParentType, ContextType>;
  isOptionalSkillFor?: Resolver<Maybe<Array<ResolversTypes['b0_n48']>>, ParentType, ContextType>;
  memberOf?: Resolver<Maybe<Array<ResolversTypes['Collection']>>, ParentType, ContextType>;
  narrower?: Resolver<Maybe<Array<ResolversTypes['Concept']>>, ParentType, ContextType>;
  prefLabel?: Resolver<Maybe<Array<ResolversTypes['Literal_rdfs']>>, ParentType, ContextType>;
  skillType?: Resolver<Maybe<Array<ResolversTypes['SkillCompetenceType']>>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type SkillCompetenceTypeResolvers<ContextType = any, ParentType extends ResolversParentTypes['SkillCompetenceType'] = ResolversParentTypes['SkillCompetenceType']> = ResolversObject<{
  altLabel?: Resolver<Maybe<Array<ResolversTypes['Literal_rdfs']>>, ParentType, ContextType>;
  broader?: Resolver<Maybe<Array<ResolversTypes['Concept']>>, ParentType, ContextType>;
  definition?: Resolver<Maybe<Array<ResolversTypes['String_xsd']>>, ParentType, ContextType>;
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  memberOf?: Resolver<Maybe<Array<ResolversTypes['Collection']>>, ParentType, ContextType>;
  narrower?: Resolver<Maybe<Array<ResolversTypes['Concept']>>, ParentType, ContextType>;
  prefLabel?: Resolver<Maybe<Array<ResolversTypes['Literal_rdfs']>>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type StandardResolvers<ContextType = any, ParentType extends ResolversParentTypes['Standard'] = ResolversParentTypes['Standard']> = ResolversObject<{
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type String_XsdResolvers<ContextType = any, ParentType extends ResolversParentTypes['String_xsd'] = ResolversParentTypes['String_xsd']> = ResolversObject<{
  value?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type WorkContextResolvers<ContextType = any, ParentType extends ResolversParentTypes['WorkContext'] = ResolversParentTypes['WorkContext']> = ResolversObject<{
  altLabel?: Resolver<Maybe<Array<ResolversTypes['Literal_rdfs']>>, ParentType, ContextType>;
  broader?: Resolver<Maybe<Array<ResolversTypes['Concept']>>, ParentType, ContextType>;
  definition?: Resolver<Maybe<Array<ResolversTypes['String_xsd']>>, ParentType, ContextType>;
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  memberOf?: Resolver<Maybe<Array<ResolversTypes['Collection']>>, ParentType, ContextType>;
  narrower?: Resolver<Maybe<Array<ResolversTypes['Concept']>>, ParentType, ContextType>;
  prefLabel?: Resolver<Maybe<Array<ResolversTypes['Literal_rdfs']>>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type AnyUriResolvers<ContextType = any, ParentType extends ResolversParentTypes['anyURI'] = ResolversParentTypes['anyURI']> = ResolversObject<{
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type B0_N44Resolvers<ContextType = any, ParentType extends ResolversParentTypes['b0_n44'] = ResolversParentTypes['b0_n44']> = ResolversObject<{
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type B0_N48Resolvers<ContextType = any, ParentType extends ResolversParentTypes['b0_n48'] = ResolversParentTypes['b0_n48']> = ResolversObject<{
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type B0_N55Resolvers<ContextType = any, ParentType extends ResolversParentTypes['b0_n55'] = ResolversParentTypes['b0_n55']> = ResolversObject<{
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  relatedEssentialSkill?: Resolver<Maybe<Array<ResolversTypes['Skill']>>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type B0_N59Resolvers<ContextType = any, ParentType extends ResolversParentTypes['b0_n59'] = ResolversParentTypes['b0_n59']> = ResolversObject<{
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  relatedOptionalSkill?: Resolver<Maybe<Array<ResolversTypes['Skill']>>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type B0_N66Resolvers<ContextType = any, ParentType extends ResolversParentTypes['b0_n66'] = ResolversParentTypes['b0_n66']> = ResolversObject<{
  editorialStatus?: Resolver<Maybe<Array<ResolversTypes['editorialStatusDataType']>>, ParentType, ContextType>;
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type BooleanResolvers<ContextType = any, ParentType extends ResolversParentTypes['boolean'] = ResolversParentTypes['boolean']> = ResolversObject<{
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type DecimalResolvers<ContextType = any, ParentType extends ResolversParentTypes['decimal'] = ResolversParentTypes['decimal']> = ResolversObject<{
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type DurationResolvers<ContextType = any, ParentType extends ResolversParentTypes['duration'] = ResolversParentTypes['duration']> = ResolversObject<{
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type EditorialStatusDataTypeResolvers<ContextType = any, ParentType extends ResolversParentTypes['editorialStatusDataType'] = ResolversParentTypes['editorialStatusDataType']> = ResolversObject<{
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type Resolvers<ContextType = any> = ResolversObject<{
  Accreditation?: AccreditationResolvers<ContextType>;
  AssociationObject?: AssociationObjectResolvers<ContextType>;
  AwardingActivity?: AwardingActivityResolvers<ContextType>;
  AwardingBody?: AwardingBodyResolvers<ContextType>;
  Collection?: CollectionResolvers<ContextType>;
  Concept?: ConceptResolvers<ContextType>;
  ConceptScheme?: ConceptSchemeResolvers<ContextType>;
  EntryRequirement?: EntryRequirementResolvers<ContextType>;
  EscoExtension?: EscoExtensionResolvers<ContextType>;
  JSON?: GraphQLScalarType;
  LabelRole?: LabelRoleResolvers<ContextType>;
  Language?: LanguageResolvers<ContextType>;
  Literal_rdfs?: Literal_RdfsResolvers<ContextType>;
  MemberConcept?: MemberConceptResolvers<ContextType>;
  Mutation?: MutationResolvers<ContextType>;
  NodeLiteral?: NodeLiteralResolvers<ContextType>;
  Occupation?: OccupationResolvers<ContextType>;
  PivotTableQuery?: PivotTableQueryResolvers<ContextType>;
  Qualification?: QualificationResolvers<ContextType>;
  Query?: QueryResolvers<ContextType>;
  Recognition?: RecognitionResolvers<ContextType>;
  Resource?: ResourceResolvers<ContextType>;
  Skill?: SkillResolvers<ContextType>;
  SkillCompetenceType?: SkillCompetenceTypeResolvers<ContextType>;
  Standard?: StandardResolvers<ContextType>;
  String_xsd?: String_XsdResolvers<ContextType>;
  WorkContext?: WorkContextResolvers<ContextType>;
  anyURI?: AnyUriResolvers<ContextType>;
  b0_n44?: B0_N44Resolvers<ContextType>;
  b0_n48?: B0_N48Resolvers<ContextType>;
  b0_n55?: B0_N55Resolvers<ContextType>;
  b0_n59?: B0_N59Resolvers<ContextType>;
  b0_n66?: B0_N66Resolvers<ContextType>;
  boolean?: BooleanResolvers<ContextType>;
  decimal?: DecimalResolvers<ContextType>;
  duration?: DurationResolvers<ContextType>;
  editorialStatusDataType?: EditorialStatusDataTypeResolvers<ContextType>;
}>;

