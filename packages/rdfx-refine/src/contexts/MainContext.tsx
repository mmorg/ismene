import { createContext, useContext, useEffect, useState } from "react";

const MainContext = createContext<any>({});

const MainContextProvider = ({
  children,
  editConfigPath,
  tableConfigPath,
  worldOntology
}: any) => {
  const [editConfiguration, setEditConfiguration] = useState<any>(null);
  const [tableConfiguration, setTableConfiguration] = useState<any>(null);

  useEffect(() => {
    (async () => {
      const [editConfig, tableConfig] = await Promise.all([
        fetch(editConfigPath ?? "./data/editConfiguration.jsonld").then((res) => res.json()),
        fetch(tableConfigPath ?? "./data/tableConfiguration.jsonld").then((res) => res.json()),
      ]);

      setEditConfiguration({
        ...editConfig,
        '@graph': [
          ...(editConfig['@graph'] ?? []),
          ...(worldOntology?.graph ?? [])
        ]
      });
      setTableConfiguration({
        ...tableConfig,
        '@graph': [
          ...(tableConfig['@graph'] ?? []),
          ...(worldOntology?.graph ?? [])
        ]
      });
    })();
  }, [
    editConfigPath,
    tableConfigPath,
    worldOntology
  ]);

  return (
    <MainContext.Provider
      value={{
        editConfiguration,
        tableConfiguration,
      }}
    >
      {children}
    </MainContext.Provider>
  );
};

const useMainContext = () => {
  const values = useContext(MainContext);
  if (!values) {
    throw new Error("useMainContext must be used within MainContextProvider");
  }
  return values;
};

export { MainContextProvider, useMainContext };
