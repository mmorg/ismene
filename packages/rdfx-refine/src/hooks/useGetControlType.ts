import { useMemo, useState } from "react";
import { ControlType } from "../types";
import { useMainContext } from "../contexts/MainContext";
import { IResourceItem, useResource } from "@refinedev/core";
import { EditableProp } from "../utils/getEditableProps";

type Props = {
  description: string;
};
const useGetControlType = ({ description }: Props) => {
  const { editConfiguration } = useMainContext();
  const { resources } = useResource();

  const [resource, setResource] = useState<IResourceItem | null>(null);

  // TODO: remove content type calculation, since now it is included EditableProp
  const controlType = useMemo(() => {
    const configurationGraph = editConfiguration?.["@graph"] ?? [];
    const associatedDefinition = configurationGraph.find(
      (definition: any) => definition["id"] === description
    );
    if (!associatedDefinition) return null;
    const propClassName = (associatedDefinition["range"] ?? [])?.[0];
    if (!propClassName) return null;

    if (["rdfs:Literal", "xsd:string"].includes(propClassName)) {
      return ControlType.Literal;
    }

    const associatedClass = configurationGraph.find(
      (definition: any) => definition["id"] === propClassName
    );
    if (!associatedClass) return null;

    const resource = resources.find(
      (resource: any) => resource.resourceType === associatedClass["id"]
    );

    if (resource) {
      setResource(resource);
      return ControlType.Class;
    }
    return null;
  }, [editConfiguration, description, resources]);

  return {
    resource,
    controlType,
  };
};

export default useGetControlType;
