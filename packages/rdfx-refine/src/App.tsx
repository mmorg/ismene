import { Refine } from "@refinedev/core";
import { refineTheme } from "@refinedev/chakra-ui";
import { ChakraProvider } from "@chakra-ui/react";
import dataProvider, { GraphQLClient } from "@refinedev/graphql";
import { useEffect, useState } from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import routerBindings from "@refinedev/react-router-v6";
import Edit from "./components/Edit";
import CommonList from "./components/List";
import Show from "./components/Show";
import getResources from "./utils/getResources";
import { MainContextProvider } from "./contexts/MainContext";
import Create from "./components/Create";
function App() {
  const [resources, setResources] = useState([]);
  const client = new GraphQLClient("http://localhost:5010/graphql");

  useEffect(() => {
    (async () => {
      setResources(await getResources("/data/tableConfiguration.jsonld"));
    })();
  }, []);

  if (!resources.length) {
    return null;
  }
  return (
    <ChakraProvider theme={refineTheme}>
      <BrowserRouter>
        <MainContextProvider>
          <Refine
            routerProvider={routerBindings}
            dataProvider={dataProvider(client)}
            options={{
              syncWithLocation: true,
              warnWhenUnsavedChanges: true,
            }}
            resources={resources}
          >
            <Routes>
              <Route path="concepts">
                <Route index element={<CommonList />} />
                <Route path="create" element={<Create />} />
                <Route path=":id/edit" element={<Edit />} />
                <Route path=":id/show" element={<Show />} />
              </Route>
            </Routes>
          </Refine>
        </MainContextProvider>
      </BrowserRouter>
    </ChakraProvider>
  );
}

export default App;
