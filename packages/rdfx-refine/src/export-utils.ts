import CommonList from "./components/List";
import Edit from "./components/Edit";
import Show from "./components/Show";
import Create from "./components/Create";
import { MainContextProvider, useMainContext } from "./contexts/MainContext";
import getResources from "./utils/getResources";
import { getDataProvider } from "./utils/getDataProvider";

export {
  // utils
  getResources,
  getDataProvider,

  // provider
  MainContextProvider,
  useMainContext,

  // common components
  CommonList,
  Show,
  Edit,
  Create,
}