import { EditableProp } from "../../utils/getEditableProps";
import useGetControlType from "../../hooks/useGetControlType";
import { useFormContext } from "react-hook-form";
import { FormControl, FormErrorMessage, FormLabel, Input } from "@chakra-ui/react";
import Autocomplete from "../Autocomplete";
import { ControlType } from "../../types";

type Props = EditableProp & {
  originalData: any;
};
const EditableControl = ({ isSingular, name, label, description, controlType, originalData }: Props) => {
  const { resource } = useGetControlType({
    description,
  });
  const {
    register,
    formState: { errors },
    watch,
    setValue,
  } = useFormContext();
  const value = watch(name);

  if (controlType === ControlType.Literal) {
    return (
      <FormControl mb="3" isInvalid={!!errors?.title}>
        <FormLabel>{label}</FormLabel>
        <Input
          id="label"
          type="text"
          {...register(`${name}.0.value`, { required: "Label is required" })}
        />
        <FormErrorMessage>{/*{`${errors.title?.message}`}*/}</FormErrorMessage>
      </FormControl>
    );
  }

  if (controlType === ControlType.Class && resource) {
    return (
      <FormControl mb="3" isInvalid={!!errors?.title}>
        <FormLabel>{label}</FormLabel>
        <Autocomplete
          isSingular={isSingular}
          resourceName={resource.name}
          dataSource={(originalData?.[name] ?? []).map((b: any) => ({
            id: b.id,
            // for now, those property as class will always have a prefLabel
            // TODO: get option label from the resource
            label: b.prefLabel?.[0]?.value ?? b.id,
          }))}
          values={value?.map((b: any) => b.id) ?? []}
          onChange={(values) => {
            setValue(
              name,
              values.map((id) => ({ id }))
            );
          }}
        />
        <FormErrorMessage>{/*{`${errors.title?.message}`}*/}</FormErrorMessage>
      </FormControl>
    );
  }

  return null;
};

export default EditableControl;
