import {Spinner, useOutsideClick} from "@chakra-ui/react";
import {
  AutoComplete,
  AutoCompleteInput, AutoCompleteInputProps,
  AutoCompleteItem,
  AutoCompleteList,
  AutoCompleteProps, AutoCompleteTag
} from "@choc-ui/chakra-autocomplete";
import { useList, useResource } from "@refinedev/core";
import {useEffect, useMemo, useRef, useState} from "react";

export type DataSource = {
  id: string;
  label: string;
}

type Props = {
  wrapper?: AutoCompleteProps;
  input?: AutoCompleteInputProps;
  values: string[];
  dataSource?: DataSource[];
  onChange?: (values: string[]) => void;
  resourceName: string;
  isSingular?: boolean;
}
const Autocomplete = ({
  dataSource = [],
  values,
  onChange,
  resourceName,
  isSingular = false
}: Props) => {
  const [source, setSource] = useState<DataSource[]>(dataSource)

  const sourceMap: Record<string, DataSource> = useMemo(() => {
    return source.reduce((acc, item) => ({
      ...acc,
      [item.id]: item
    }), {})
  }, [source])

  const ref = useRef(null)
  const [search, setSearch] = useState<string>('');
  const { resources } = useResource();

  const resource = useMemo(
    () => resources?.find((item) => item.name === resourceName),
    [resources, resourceName]
  )

  const {
    data: filteredSources,
    isFetching,
    isError
  } = useList({
    resource: resource?.name,
    meta: resource?.meta?.getList,
    filters: [{
      field: "query",
      operator: "eq",
      value: search
    }]
  })

  useOutsideClick({
    ref,
    handler: () => {
      setSearch('')
    }
  })

  useEffect(() => {
    setSource(prev => {
      const filteredSourcesData = (filteredSources?.data ?? []).map(item => ({
        id: item.id,
        label: item.prefLabel?.[0]?.value ?? item.id // TODO: check if it works for other models
      }))
      const allSources = [...prev, ...dataSource, ...filteredSourcesData]
      const newSourceIds = [...(new Set(allSources.map(item => item.id as string)))]

      return newSourceIds.map(id => {
        return ({
          id,
          label: allSources.find(item => item.id === id)?.label ?? id
        })
      })
    })
  }, [dataSource, filteredSources?.data])

  return (
    <div ref={ref}>
      <AutoComplete
        openOnFocus
        suggestWhenEmpty
        disableFilter={true}
        multiple
        values={values}
        closeOnSelect={isSingular}
        onChange={(values) => {
          if (!isSingular) {
            onChange?.(values)
            return;
          }
          onChange?.(values.slice(-1))
        }}
      >
        <AutoCompleteInput
          variant="filled"
          sx={{
            backgroundColor: "var(--chakra-colors-chakra-bg)",
            '&:hover': {
              backgroundColor: "var(--chakra-colors-chakra-bg)",
            },
            borderColor: 'var(--chakra-colors-chakra-body-text)'
          }}
          value={search}
          onChange={(e) => {
            setSearch(e.target.value)
          }}
        >
          {
            values.map((value, index) => {
              return (
                <AutoCompleteTag
                  style={{ textTransform: "capitalize" }}
                  key={value + index}
                  label={sourceMap[value]?.label ?? value}
                  onRemove={() => {
                    onChange?.(values.filter((item) => item !== value))
                  }}
                />
              )
            })
          }
        </AutoCompleteInput>
        <AutoCompleteList>
          {isFetching ? (
            <AutoCompleteItem value="" disabled={true}>
              <Spinner />
            </AutoCompleteItem>
           ) : (
            filteredSources?.data?.map((item: any) => {
              return (
                <AutoCompleteItem
                  key={item.id}
                  value={item.id}
                  label={sourceMap[item.id]?.label ?? item.id}
                  textTransform="capitalize"
                >
                  {sourceMap[item.id]?.label ?? item.id}
                </AutoCompleteItem>
              )
            })
          )}
        </AutoCompleteList>
      </AutoComplete>
    </div>
  )
}

export default Autocomplete;
