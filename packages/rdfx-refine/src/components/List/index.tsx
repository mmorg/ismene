import React, { useEffect, useMemo, useState } from "react";
import { prettyString } from "@refinedev/inferencer";
import { useResource } from "@refinedev/core";
import { useTable } from "@refinedev/react-table";
import { ColumnDef, flexRender } from "@tanstack/react-table";
import {
  List,
  EditButton,
  ShowButton,
  DeleteButton,
} from "@refinedev/chakra-ui";
import {
  TableContainer,
  Table,
  Thead,
  Tr,
  Th,
  Tbody,
  Td,
  HStack,
  Text,
  Input, useColorModeValue,
} from "@chakra-ui/react";
import Pagination from "../Pagination";
import useDebounce from "../../hooks/useDebounce";
import Loading from "../Loading";

const concatKey = (...keys: (string | undefined)[]) =>
  keys.filter((k) => !!k).join(".");

const getAccessorKey = (field: any, prefix?: string): string => {
  if (typeof field === "string") {
    return concatKey(prefix, field);
  }
  if (typeof field === "object") {
    const key = Object.keys(field)[0];
    return getAccessorKey(field[key][0], concatKey(prefix, key));
  }
  return prefix ?? "";
};

const CommonList = () => {
  const [searchValue, setSearchValue] = useState("");
  const debounceSearchValue = useDebounce(searchValue, 500);
  const { resource } = useResource();
  const headerColor = useColorModeValue("gray.700", "gray.200");
  const getListMeta = resource?.meta?.getList;
  const fieldTransformer = resource?.meta?.fieldTransformer;
  const fields = useMemo(() => {
    const _fields = getListMeta?.columns ?? [];
    // TODO: typing
    const baseMap = _fields
      .map((field: any) => {
        return {
          key: field.name,
          label: field.label,
          accessorKey: field.dataPath,
          type: field.dataType ?? "text", // TODO: update more type here
        }
      })

    return fieldTransformer ? baseMap.map(fieldTransformer) : baseMap;
  }, [getListMeta]);
  const columns = useMemo<ColumnDef<any>[]>(() => {
    return [
      ...fields.map((f: any) => ({
        id: f.key,
        type: f.type,
        header: f.label ?? prettyString(f.key),
        accessorKey: f.accessorKey,
        ...(f.cell ? { cell: f.cell } : null),
      })),
      {
        id: "actions",
        accessorKey: "id",
        header: "Actions",
        cell: function render({ getValue }) {
          return (
            <HStack>
              <ShowButton hideText recordItemId={getValue() as string} />
              <EditButton hideText recordItemId={getValue() as string} />
              <DeleteButton hideText recordItemId={getValue() as string} meta={resource?.meta?.deleteOne} />
            </HStack>
          );
        },
      },
    ];
  }, []);
  const {
    getHeaderGroups,
    getRowModel,
    setOptions,
    getPageCount,
    refineCore: {
      setFilters,
      setCurrent,
      pageCount,
      current,
      tableQueryResult: { data: tableData, isFetching },
    },
  } = useTable({
    columns,
    refineCoreProps: {
      meta: resource?.meta?.getList,
      filters: {
        initial: [
          {
            field: "query",
            operator: "eq",
            value: debounceSearchValue,
          },
        ],
      },
    },
  });

  setOptions((prev) => ({
    ...prev,
    meta: {
      ...prev.meta,
    },
  }));
  useEffect(() => {
    setFilters([
      {
        field: "query",
        operator: "eq",
        value: debounceSearchValue,
      },
    ]);
  }, [debounceSearchValue]);
  return (
    <List>
      <Text fontWeight={500}>Search</Text>
      <Input
        mb={4}
        value={searchValue}
        onChange={(e) => setSearchValue(e?.target?.value)}
      />
      <div style={{ position: "relative" }}>
        {isFetching && <Loading />}
        <TableContainer whiteSpace="pre-line">
          <Table variant="simple">
            <Thead>
              {getHeaderGroups().map((headerGroup) => (
                <Tr key={headerGroup.id}>
                  {headerGroup.headers.map((header) => (
                    <Th key={header.id} textColor={headerColor}>
                      {!header.isPlaceholder &&
                        flexRender(
                          header.column.columnDef.header,
                          header.getContext()
                        )}
                    </Th>
                  ))}
                </Tr>
              ))}
            </Thead>
            <Tbody>
              {getRowModel().rows.map((row, index) => (
                <Tr key={`${row.id}-${index}`}>
                  {row.getVisibleCells().map((cell) => {
                    return (
                      <Td key={cell.id}>
                        {flexRender(
                          cell.column.columnDef.cell,
                          cell.getContext()
                        )}
                      </Td>
                    );
                  })}
                </Tr>
              ))}
            </Tbody>
          </Table>
        </TableContainer>
      </div>
      <Pagination
        current={current}
        pageCount={getPageCount()}
        setCurrent={setCurrent}
      />
    </List>
  );
};

export default CommonList;
