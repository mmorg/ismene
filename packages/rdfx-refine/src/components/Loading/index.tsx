import { Spinner } from "@chakra-ui/react";

const Loading = () => {
  return (
    <div
      style={{
        position: "absolute",
        inset: 0,
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        background: "grey",
        opacity: 0.3,
      }}
    >
      <Spinner size="xl" />
    </div>
  );
};

export default Loading;
