import { Edit as ChakraEditView, TextField } from "@refinedev/chakra-ui";
import { Heading } from "@chakra-ui/react";
import { useResource, useUpdate } from "@refinedev/core";
import { useForm } from "@refinedev/react-hook-form";
import { useEffect, useMemo } from "react";
import EditableControl from "../EditableControl";
import { FormProvider } from "react-hook-form";
import getEditableProps from "../../utils/getEditableProps";
import { useMainContext } from "../../contexts/MainContext";

import { serializeMutationValues } from "../../utils/serializeMutationValues";

const Edit = () => {
  const { resource } = useResource();
  const getOneMeta = resource?.meta?.getOne;
  const { editConfiguration } = useMainContext();
  const {
    refineCore: { formLoading, queryResult },
    saveButtonProps,
    ...methods
  } = useForm({
    refineCoreProps: {
      meta: getOneMeta,
    },
  });

  const {
    formState: { errors },
    getValues,
    reset,
  } = methods;

  const { mutate, isLoading: isMutating } = useUpdate();

  const editableProperties = useMemo(() => {
    return getEditableProps({
      // @ts-ignore
      resourceType: resource?.resourceType ?? "",
      configData: editConfiguration,
    });
  }, [resource, editConfiguration]);

  const onClick = (e: any) => {
    const _values = serializeMutationValues({
      values: getValues(),
      editableProperties
    })

    mutate({
      resource: resource?.name ?? "",
      meta: {
        fields: getOneMeta.fields,
        // operation: "Concept_update"
      },
      id: queryResult?.data?.data?.id ?? "",
      values: _values,
    });
  };

  useEffect(() => {
    if (queryResult?.data?.data) {
      reset(queryResult?.data?.data);
    }
  }, [queryResult?.data?.data]);

  return (
    <FormProvider {...methods}>
      <ChakraEditView
        isLoading={formLoading || isMutating}
        saveButtonProps={{ ...saveButtonProps, onClick, isLoading: formLoading || isMutating }}
      >
        <Heading as="h5" size="sm" mt={4}>
          Id
        </Heading>
        <TextField value={queryResult?.data?.data?.id} />

        {editableProperties.map((prop, index) => {
          return (
            <EditableControl
              key={prop.name + index}
              {...prop}
              originalData={queryResult?.data?.data}
            />
          );
        })}
      </ChakraEditView>
    </FormProvider>
  );
};

export default Edit;
