import { Create as ChakraCreateView, TextField } from "@refinedev/chakra-ui";
import { nanoid } from "nanoid";
import { useCreate, useResource } from "@refinedev/core";
import { useForm } from "@refinedev/react-hook-form";
import { useEffect, useMemo } from "react";
import EditableControl from "../EditableControl";
import { FormProvider } from "react-hook-form";
import getEditableProps from "../../utils/getEditableProps";
import { useMainContext } from "../../contexts/MainContext";
import { serializeMutationValues } from "../../utils/serializeMutationValues";
import { useNavigate } from "react-router-dom";

const Create = () => {
  const { resource } = useResource();
  const getOneMeta = resource?.meta?.getOne;
  const { editConfiguration } = useMainContext();
  const navigate = useNavigate()
  const {
    refineCore: { formLoading, queryResult },
    saveButtonProps,
    ...methods
  } = useForm({
    refineCoreProps: {
      meta: getOneMeta,
    },
  });

  const {
    formState: { errors },
    getValues,
    reset,
  } = methods;

  const { mutate, isLoading: isMutating } = useCreate();

  const onClick = (e: any) => {
    const _values = serializeMutationValues({
      values: getValues(),
      editableProperties
    })
    const id = [resource?.name, nanoid(10)].join("/")
    mutate({
      resource: resource?.name ?? "",
      meta: {
        fields: getOneMeta.fields,
        // operation: "Concept_update"
      },
      values: {
        id,
        ..._values
      },
    }, {
      onSuccess: () => {
        navigate(`/${resource?.name}/${encodeURIComponent(id)}/edit`)
      }
    });
  };

  const editableProperties = useMemo(() => {
    return getEditableProps({
      // @ts-ignore
      resourceType: resource?.resourceType ?? "",
      configData: editConfiguration,
    });
  }, [resource, editConfiguration]);

  useEffect(() => {
    if (queryResult?.data?.data) {
      reset(queryResult?.data?.data);
    }
  }, [queryResult?.data?.data]);

  return (
    <FormProvider {...methods}>
      <ChakraCreateView
        isLoading={formLoading || isMutating}
        saveButtonProps={{ ...saveButtonProps, onClick, isLoading: formLoading || isMutating }}
      >
        {editableProperties.map((prop, index) => {
          return (
            <EditableControl
              key={prop.name + index}
              {...prop}
              originalData={queryResult?.data?.data}
            />
          );
        })}
      </ChakraCreateView>
    </FormProvider>
  );
};

export default Create;
