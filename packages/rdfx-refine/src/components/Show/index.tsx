import { useResource, useShow } from "@refinedev/core";
import { Show as ChakraShow, TextField } from "@refinedev/chakra-ui";
import { Heading } from "@chakra-ui/react";
import { useMemo } from "react";
import getEditableProps from "../../utils/getEditableProps";
import { useMainContext } from "../../contexts/MainContext";
import DisplayProperty from "./DisplayProperty";

const Show = () => {
  const { resource } = useResource();
  const { editConfiguration } = useMainContext();
  const getOneMeta = resource?.meta?.getOne;

  const { queryResult } = useShow({
    meta: getOneMeta,
  });
  const { data, isLoading } = queryResult;

  const record = data?.data;
  const displayProperties = useMemo(() => {
    return getEditableProps({
      // @ts-ignore
      resourceType: resource?.resourceType ?? "",
      configData: editConfiguration,
    });
  }, [editConfiguration, resource]);

  if (!record) {
    return null;
  }
  return (
    <ChakraShow isLoading={isLoading}>
      <Heading as="h5" size="sm" mt={4}>
        Id
      </Heading>
      <TextField value={record?.id} />
      {displayProperties?.map((property, index) => {
        return <DisplayProperty key={property.name + index} {...property} fullData={record} />;
      })}
    </ChakraShow>
  );
};

export default Show;
