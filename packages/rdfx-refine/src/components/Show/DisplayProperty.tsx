import { EditableProp } from "../../utils/getEditableProps";
import useGetControlType from "../../hooks/useGetControlType";
import { ControlType } from "../../types";
import { Heading, Text } from "@chakra-ui/react";
import { TextField } from "@refinedev/chakra-ui";

type Props = EditableProp & {
  fullData: Record<string, unknown>;
};
const DisplayProperty = ({ name, label, description, controlType, fullData }: Props) => {
  const { resource } = useGetControlType({
    description,
  });
  const value = (fullData?.[name] ?? []) as any[];

  if (controlType === ControlType.Date) {
    return (
      <>
        <Heading as="h5" size="sm" mt={4}>
          {label}
        </Heading>
        {
          value[0]?.value
            ? <TextField value={new Date(value[0]?.value).toLocaleString()} />
            : null
        }
      </>
    );
  }

  if (controlType === ControlType.Literal) {
    return (
      <>
        <Heading as="h5" size="sm" mt={4}>
          {label}
        </Heading>
        <TextField value={value[0]?.value} />
      </>
    );
  }

  if (controlType === ControlType.Class && resource) {
    return (
      <>
        <Heading as="h5" size="sm" mt={4}>
          {label}
        </Heading>
        <div>
          {value?.map((item: any, index) => {
            return <Text key={item.id + index}>{item.prefLabel?.[0]?.value ?? item.id}</Text>;
          })}
        </div>
      </>
    );
  }

  return null;
};

export default DisplayProperty;
