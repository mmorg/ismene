import { ControlType } from "../types";
import { EditableProp } from "./getEditableProps";

type Props = {
  values: Record<string, unknown>;
  editableProperties: EditableProp[]
}
export const serializeMutationValues = ({ values, editableProperties }: Props) => {
  return Object.entries(values).reduce(
    (acc: Record<string, unknown>, [key, value]: [string, any]) => {
      const controlType = editableProperties.find(
        (p) => p.name === key
      )?.controlType;
      if (controlType === ControlType.Class) {
        return {
          ...acc,
          [key]: value?.map((v: any) => v.id) ?? [],
        };
      }
      return {
        ...acc,
        [key]: value,
      };
    },
    {}
  )
}