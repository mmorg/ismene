import dataProvider, { generateFilter, generateSort, GraphQLClient } from "@refinedev/graphql";
import { BaseRecord } from "@refinedev/core";
import camelCase from "camelcase";
import { query as gqlQuery } from "gql-query-builder";
import * as gql from "gql-query-builder";
// @ts-ignore
import pluralize from "pluralize";
import { get } from "./get";

const mapParentGroupToQueryFields = (parentGroup: string, fields: any) => {
  return parentGroup.split('.').reduce((acc, curr) => {
    return [{
      [curr]: acc
    }]
  }, fields)
}

export const getDataProvider = (client: GraphQLClient) => {
  const provider = dataProvider(client);

  return {
    ...provider,
    getList: async ({
      resource,
      pagination,
      sorters,
      filters,
      meta,
    }: any) => {
      const {
        current = 1,
        pageSize = 10,
        mode = "server",
      } = pagination ?? {};

      const sortBy = generateSort(sorters);
      const filterBy = generateFilter(filters);

      const camelResource = camelCase(resource);
      const resourceName = meta?.operation ?? camelResource
      const operation = meta.parentGroup ? meta.parentGroup.split('.')[0] : (resourceName);

      const { query, variables } = gqlQuery({
        operation,
        variables: {
          ...meta?.variables,
          sort: sortBy,
          where: { value: filterBy, type: "JSON" },
          ...(mode === "server"
            ? {
              start: (current - 1) * pageSize,
              limit: pageSize,
            }
            : {}),
        },
        fields: meta.parentGroup ? mapParentGroupToQueryFields(
          meta.parentGroup.split('.').filter((_: any, i: number) => i > 0).join('.'), [{ [resourceName]: meta?.fields }]
        ) : meta.fields,
      });

      const response = await client.request<BaseRecord>(query, variables);
      const actualData = (meta.parentGroup ? get(response, meta.parentGroup) as any : response)?.[resourceName]

      return {
        data: actualData?.data,
        total: actualData?.total ?? 0,
      };
    },

    getOne: async ({ resource, id, meta }: any) => {
      const singularResource = pluralize.singular(resource);
      const camelResource = camelCase(singularResource);
      const parentGroup = meta.parentGroup
      const resourceName = meta?.operation ?? camelResource
      const operation = meta.parentGroup ? meta.parentGroup.split('.')[0] : (resourceName);

      const { query, variables } = gql.query({
        operation,
        variables: {
          id: { value: id, type: "ID", required: true },
        },
        fields: parentGroup ? mapParentGroupToQueryFields(
          parentGroup.split('.').filter((_: any, i: number) => i > 0).join('.'), [{ [resourceName]: meta?.fields }]
        ) : meta.fields,
      });

      const response = await client.request<BaseRecord>(query, variables);
      const actualData = (meta.parentGroup ? get(response, meta.parentGroup) as any : response)?.[resourceName]

      return {
        data: parentGroup ? actualData : response[operation],
      };
    },
  };
}