import {ControlType} from "../types";

type Props = {
  resourceType: string;
  configData: Record<string, unknown>;
};

export type EditableProp = {
  name: string;
  label: string;
  description: string;
  controlType?: ControlType;
  isSingular: boolean;
};

const getEditableProps = ({
  resourceType,
  configData,
}: Props): EditableProp[] => {
  const graph = (configData?.["@graph"] as any[]) ?? [];
  const modelsList =
    graph.find((definition: any) => definition["id"] === "ef:Models")?.models ??
    [];
  const model = modelsList.find(
    (definition: any) => definition["id"] === resourceType
  );

  if (!model) return [];
  const editableProps = model.editableProperties ?? [];

  return editableProps.map((e: any) => {
    const definition = graph.find((d: any) => d.id === e.description)
    const propClassName = (definition?.["range"] ?? [])?.[0];
    let controlType
    if (["xsd:date"].includes(propClassName)) {
      controlType = ControlType.Date;
    } else if (["rdfs:Literal", "xsd:string"].includes(propClassName)) {
      controlType = ControlType.Literal;
    } else if (graph.find((d: any) => d.id === propClassName)?.type?.includes("rdfs:Class")) {
      controlType = ControlType.Class;
    }

    return {
      ...e,
      controlType,
      isSingular: definition.cardinality === 1
    }
  });
};

export default getEditableProps;
