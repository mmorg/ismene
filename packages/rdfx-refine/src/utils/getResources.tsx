import { TableCellProps, Text } from "@chakra-ui/react";
import { DataSourceType } from "../types";
import { get } from "./get";
import { CoreCell } from "@tanstack/react-table";

const recursiveGetGraphQlQuery = (
  column: DataSourceType | DataSourceType[]
): string | Record<string, unknown> | (string | Record<string, unknown>)[] => {
  if (Array.isArray(column)) {
    return column
      .map((item: DataSourceType) => recursiveGetGraphQlQuery(item))
      .filter((a) => !!a) as (string | Record<string, unknown>)[];
  }
  if (!column.propertyPath) return column.name;
  if (typeof column.propertyPath === "string")
    return {
      [`${column.name}`]: [column.propertyPath],
    };
  return {
    [`${column.name}`]: [
      recursiveGetGraphQlQuery(column.propertyPath),
    ].flat(),
  };
};

const getParentGroupMap = (groupsDefinitions: any[]) => {
  const  result: Record<string, string> = {}
  groupsDefinitions.forEach((def: any) => {
    const member = def.member
    if (member?.length) {
      member.forEach((m: any) => {
        result[m] = def.id
      })
    }
  })
  return result
}

// not very optimal
// since this method will run recursively id, and the process might be repeated
// but it will be fine for now since we have few entries now
// TODO: optimize this or change the whole approach to get the parent groups by each id
const getPrefixFromParentGroupMap = (id: string, parentGroupMap: Record<string, string>): string => {
  if (parentGroupMap[id]) {
    return `${getPrefixFromParentGroupMap(parentGroupMap[id], parentGroupMap)}.${id}`
  }
  return id
}

export default async function getResources(source: string, worldOntology?: any) {
  // this must read directly from the jsonld files
  // otherwise waiting for data from MainContext will make the app not working,
  // since resources prop of Refine must be set before rendering
  const res = await fetch(source);
  const tableConfiguration = await res.json();
  const resourceNames = tableConfiguration["models"] ?? [];
  const groupsDefinitions = worldOntology?.graph?.filter((d: any) => d.type?.includes('mms:ApiCollection'))
  const parentGroupMap = getParentGroupMap(groupsDefinitions)
  return resourceNames.map((resourceName: string) => {
    const config = tableConfiguration[resourceName];
    const pluralName = config["plural"] ?? resourceName;
    const tableConfig = config.table;

    const columns = [...(tableConfig.columns ?? [])];
    const dataSources = [...(config.dataSources ?? [])];
    columns.sort((a, b) => (a.weight ?? 0) - (b.weight ?? 0));
    const graph = [
      ...(tableConfiguration["@graph"] ?? []),
      ...(worldOntology?.graph ?? [])
    ];

    const parentGroup = getPrefixFromParentGroupMap(config.description, parentGroupMap)
      .split('.')
      .filter((d: string) => d !== config.description && d !== 'as:root')
      .map((d: string) => {
        const definition = groupsDefinitions.find((def: any) => def.id === d)
        return definition?.label ?? d
      })
      .join('.')

    const fields = dataSources
      .map((ds: DataSourceType) => {
        let sourceType = ds.type;
        if (sourceType === "@id") {
          return "id";
        }

        const columnDefinition = graph.find(
          (definition: any) => definition["id"] === sourceType
        );
        if (!columnDefinition) return null;
        // @TODO: use columnDefinition to get name, description, etc...
        return recursiveGetGraphQlQuery(ds);
      })
      .filter((f) => !!f);
    return {
      name: pluralName,
      identifier: "",
      resourceType: config.description,
      list: `/${pluralName}`,
      edit: `/${pluralName}/:id/edit`,
      create: `/${pluralName}/create`,
      show: `/${pluralName}/:id/show`,
      meta: {
        getList: {
          operation: `all${config.name?.replace(/ /g, "")}`,
          fields: [
            { data: fields },
            'total'
          ],
          columns,
          parentGroup
        },
        getOne: {
          operation: config.name,
          fields,
          parentGroup
        },
        deleteOne: {
          fields: ["id"]
        },
        // TODO: typing properly
        // TODO 2: make it dynamic
        fieldTransformer: (field: any) => {
          if (field.type === "date") {
            return {
              ...field,
              cell: ({ getValue }: CoreCell<any, any>) => {
                const value = getValue();
                if (!value) return null;
                const date = new Date(value);
                return date.toLocaleString();
              }
            }
          }
          if (field.type === "array") {
            return {
              ...field,
              accessorKey: `${field.key}`,
              cell: ({ row }: CoreCell<any, any>) => {
                const dataPath =
                  field.accessorKey?.split("->") ?? [];
                const values = dataPath.length
                  ? dataPath.reduce(
                    (
                      acc: any,
                      cur: string,
                      index: number
                    ) => {
                      if (
                        index >=
                        dataPath.length - 1
                      ) {
                        if (Array.isArray(acc)) {
                          return acc.map((a) =>
                            get(a, cur) ?? a.id
                          );
                        }
                        return [];
                      }
                      return acc[cur];
                    },
                    row.original ?? {}
                  )
                  : [];
                return (
                  <div>
                    {values.map(
                      (item: any, index: number) => {
                        return (
                          <Text key={index}>
                            {item}
                          </Text>
                        );
                      }
                    )}
                  </div>
                );
              },
            };
          }

          return field;
        },
      },
    };
  });
}
