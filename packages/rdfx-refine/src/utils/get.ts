export const get = (object: Record<string, unknown>, path: string): unknown => {
  return path.
    replace(/\[/g, '.').
    replace(/]/g, '').
    split('.').
    reduce((o, k) => (o || {})[k] as any, object);
}