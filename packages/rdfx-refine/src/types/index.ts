export type DataSourceType = {
  type: string | string[];
  name: string;
  propertyPath?: string | DataSourceType;
};

export enum ControlType {
  Date = "date",
  Literal = "literal",
  Class = "class",
}
