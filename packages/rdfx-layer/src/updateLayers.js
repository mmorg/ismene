


import updateStrategies from './updateStrategies.js'
import contextShaper from './contextUtil/contextShaper.js'
import updateEntity from './updateEntity.js'

/**
 * Create a new merged layer. This do not mutate the sourceLayer
 * @param {*} sourceLayer 
 * @param  {...any} otherLayers 
 * @returns 
 * 
 * @todo manage the merge of the @contexts
 */
export default function updateLayers(sourceLayer, ...otherLayers) {

  // 1/ hard copy of the first layer to not mutate it
  const _sourceLayer = JSON.parse(JSON.stringify(sourceLayer))
  let _context = _sourceLayer.ld['@context']
  
  // 2/ flattening the otherLayers input
  const _otherLayers = otherLayers.flat()

  // 2/ create an indexMap of the _sourceLayer's entities
  const sourceMap = _sourceLayer.ld.graph.reduce((acc, v) => {
    acc.set(v.id, v)
    return acc
  }, new Map())

  for (const layer of _otherLayers) {
    // hard merge the contexts 
    // @TODO: a better merge for context 
    Object.assign(_context, layer.ld['@context'])

    // 
    const strategy = layer.updateStrategy ? layer.updateStrategy : updateStrategies.add
    for (const patch of layer.ld.graph) {

      // 0/ The entity don't exist in the source and strategy is no remove
      const exist = sourceMap.get(patch.id)
      if (!exist && strategy !== updateStrategies.remove) {
        sourceMap.set(patch.id, patch)
        continue
      }

      // 1/ The strategy is "remove"
      // @TODO: the remove is only done at the entity level (remove the entity) : make something at the property level ? 
      if (strategy === updateStrategies.remove) {
        sourceMap.delete(patch.id)
        continue
      }

      // 2/ The strategy is "add" or "replace"
      if (strategy === updateStrategies.add || strategy === updateStrategies.replace) {
        const sourceEntity = sourceMap.get(patch.id)
        updateEntity(strategy, sourceEntity, patch)
      }

    }
  }

  const resultld = {
    ['@context'] : _context,
    graph: [...sourceMap.values()],
  }
  contextShaper(resultld)

  // override the graph values of the local copy
  _sourceLayer.ld = resultld 

  return _sourceLayer
}

// // @TODO: ?export them as an updateEntity function ? 
// function add(sourceEntity, targetEntity) {
//   for (const [tKey, tValue] of Object.entries(targetEntity)) {
//     if(tKey === 'id') continue

//     const sourceValue = sourceEntity[tKey]
//     if (!sourceValue) {
//       sourceEntity[tKey] = tValue
//       continue
//     }
//     // there is an existing value
//     if (Array.isArray(sourceValue)) {
//       if (Array.isArray(tValue)) {
//         sourceValue.push(...tValue)
//       } else {
//         sourceValue.push(tValue)
//       }
//     } else {
//       if (Array.isArray(tValue)) {
//         sourceEntity[tKey] = [sourceValue, ...tValue]
//       } else {
//         sourceEntity[tKey] = [sourceValue, tValue]
//       }
//     }
//   }
// }
