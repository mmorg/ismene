
const updateStrategies = {
  add : 'rdfx-gl:add',
  remove: 'rdfx-gl:remove',
  replace: 'rdfx-gl:replace'
}
export default updateStrategies