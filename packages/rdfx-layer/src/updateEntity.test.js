import updateStrategies from './updateStrategies.js'
import updateEntity from './updateEntity.js'

// pnpm test -- --watch updateEntity.test


describe('Do not duplicate entries when multiple add of same scalar', () => {

  const entity1 = {
    label: [
      {
        '@language': 'en',
        '@value': 'Original Label'
      }
    ],
  }

  const patch1 = {
    label: [
      {
        '@language': 'en',
        '@value': 'preferred label'
      }
    ],
    subPropertyOf: [
      'rdfs:label',
      'rdfs:__test__'
    ],
  }

  const addedEntity = updateEntity(updateStrategies.add, entity1, patch1, patch1)

  it('Don\'t duplicate dataTypes entries', () => {
    expect(addedEntity.label.length).toBe(2)
  })

  it('Don\'t duplicate references entries', () => {
    expect(addedEntity.subPropertyOf.length).toBe(2)
  })
})

describe('It replace entry when `updateStrategies.replace`', () => {

  const entity1 = {
    id: 'skos:member',
    range: [
      '_:b0_n0'
    ],
  }

  const patch1 = {
    id: 'skos:member',
    range: [
      'skos:Collection',
      'skos:Concept'
    ]
  }

  const addedEntity = updateEntity(updateStrategies.replace, entity1, patch1, patch1)

  it('Replace a reference array', () => {
    expect(addedEntity.range).toBe(patch1.range)
  })

})

