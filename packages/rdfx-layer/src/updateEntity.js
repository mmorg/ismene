import hash from 'object-hash'
import updateStrategies from './updateStrategies.js'


/**
 * This function mutate the sourceEntity with the patch(es)
 * @param {*} updateStrategy 
 * @param {*} sourceEntity 
 * @param  {...any} patchEntity 
 * @returns 
 */
export default function updateEntity(updateStrategy, sourceEntity, ...patchEntity) {

  const patches = patchEntity.flat()

  for (const patch of patches) {
    // 1/ The strategy is "add"
    if (updateStrategy === updateStrategies.add) {
      add(sourceEntity, patch)
      continue
    }

    // 3/ The strategy is "replace", it do the remplacement at property's value level
    if (updateStrategy === updateStrategies.replace) {
      Object.assign(sourceEntity, patch)
      continue
    }
  }

  return sourceEntity
}


function add(sourceEntity, targetEntity) {

  for (const [tKey, tValue] of Object.entries(targetEntity)) {
    if (tKey === 'id') continue

    const sourceValue = sourceEntity[tKey]
    if (!sourceValue) {
      sourceEntity[tKey] = tValue
      continue
    }
    if(!sourceValue.length && !tValue.length){ // values in both objects are empty arrays, so keep the source value and skip
      sourceEntity[tKey] = sourceValue
      continue
    }
    // there is at least 1 existing value
    if (Array.isArray(sourceValue)) {
      if (Array.isArray(tValue)) {
        // not only merge scalars but also objects
        sourceEntity[tKey] = mergeArrayOfScalars(sourceValue, tValue)
      } else {
        sourceValue.push(tValue)
      }
    } else {
      if (Array.isArray(tValue)) {
        sourceEntity[tKey] = [sourceValue, ...tValue]
      } else {
        sourceEntity[tKey] = [sourceValue, tValue]
      }
    }
  }
}

// merge Arrays of Scalar OR Objects
function mergeArrayOfScalars(source, target) {
  // Scalar type 
  const firstSource = source[0] ? source[0] : null
  const firstTarget = target[0] ? target[0] : null

  if (!firstSource || !firstTarget) {
    console.log('Error during processing of merging Array:', source, target)
  }

  if (typeof firstSource === 'object' && typeof firstTarget === 'object') {
    return mergeArrayOfObjects(source, target) // [...source, ...target]
  }

  if (typeof firstSource === 'string' && typeof firstTarget === 'string') {
    const unique = new Set([...source, ...target])
    return [...unique]
  }

  throw new Error('@TODO: implement: this merge case is not managed')

}

function mergeArrayOfObjects(source, target) {
  // 1/ create the hash set of source
  const sourceHash = new Set(source.map(hash))
  for (const t of target) {
    const tHash = hash(t)
    if (sourceHash.has(tHash)) {
      continue // discard on existing
    }
    source.push(t)
    sourceHash.add(tHash)
  }

  return source
}
