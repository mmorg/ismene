import contextParser from './contextParser.js'
import exampleBigContext from './__fixtures__/exampleBigContext.js'

// docker compose exec gatsby npm run testESM -- --watch contextParser

describe('dev scratchpad', () => {

  it('Generate target context Tree', async () => {
   
    const contextTree = contextParser(exampleBigContext) 

    expect(contextTree.datatypesKeys.has('prefLabel')).toBe(true)

    expect(contextTree).toMatchSnapshot()
  })

})
