
const structureBuilder = () => ({
  idAlias: {},
  typeAlias: {},
  graphAlias: {},

  nsAlias: {},
  nsAliasKeys: {},

  idRefs: {},
  idRefsKeys: new Set(),
  
  datatypes: {},
  datatypesKeys: new Set(),
})

// @TODO: see rdf/js context parser

export default function contextParser(ld) {
  const structure = structureBuilder()

  const context = ld['@context']

  for (const [k, v] of Object.entries(context)) {
    if (isString(v)) {
      if (v === '@id') {
        structure.idAlias = Object.fromEntries([[k, v]])
        continue
      }
      if (v.startsWith('http')) {
        structure.nsAlias[k] = v
        continue
      }
      // else case, throw a warning
      console.warn('@check this alias not detected', Object.fromEntries([[k, v]]))
      continue
    }

    if (typeof v === 'object') {
      if (v['@id'] === '@graph') {
        structure.graphAlias = Object.fromEntries([[k, v]])
        continue
      }
      if (v['@id'] === '@type') {
        structure.typeAlias = Object.fromEntries([[k, v]])
        continue
      }
      if (v['@type'] === '@id') {
        structure.idRefs[k] = v
        continue
      }
      // else case, this is a datatype 
      structure.datatypes[k] = v
    }
    // else: this is an object to check
    // console.log('===============', k)
  }

  // create the accessKeys
  structure.nsAliasKeys = new Set(Object.keys(structure.nsAlias))
  structure.idRefsKeys = new Set(Object.keys(structure.idRefs))
  structure.datatypesKeys = new Set(Object.keys(structure.datatypes))


  return structure
}

function isString(str) {
  return typeof str === 'string' || str instanceof String
}

