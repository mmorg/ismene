export default function updateExternalEngine(sourceOntology, externalEngineOntology) {
  sourceOntology.graph?.forEach?.(entity => {
    const isFromExtraEngine = externalEngineOntology.graph?.find?.(e => e.id === entity.id)
    if (isFromExtraEngine) {
      entity['engine'] = isFromExtraEngine['engine']
    }
  })

  return sourceOntology;
}