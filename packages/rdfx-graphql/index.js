import entities2graphql from './src/rdfx-to-graphql/entities2graphql.js'
import getDataLoader from './src/rdfx-to-graphql/getDataLoader.js'
import getGraph from './src/rdfx-graph/getGraph.js'

export { 
  entities2graphql, getDataLoader,
  getGraph,

}