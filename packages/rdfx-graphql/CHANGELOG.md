# @mmorg/rdfx-graphql

## 1.2.0

### Minor Changes

- Authentication implementation

## 1.1.0

### Minor Changes

- Add multiple features like multiple engines, geoDataType, deletion,...

## 1.0.2

### Patch Changes

- publishing first version
- fix missing dep

## 1.0.1

### Patch Changes

- publishing first version
