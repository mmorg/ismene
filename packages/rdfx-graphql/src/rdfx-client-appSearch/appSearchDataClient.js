import denormForJsonld from "@mmorg/appsearch-utils/normDenorm/denormalisation/denormForJsonld.js";
import queryAppSearch from "@mmorg/appsearch-utils/search/queryAppSearch.js";
// import elasticConf from '../_confs/elasticConf.js'

const defaultResult_fields = { id: { raw: {} } };

export default async function appSearchDataClient(
  args = {},
  result_fields = defaultResult_fields,
  elasticConf
) {
  // console.warn('Start query app-search dataSource', args)
  const { current, size, sort, filters = {}, query = "" } = args;

  const docSize = size || 10; // TODO: confirm?

  const options = {
    filters: {
      all: Object.keys(filters).reduce((acc, k) => {
        return [...acc, { [k]: filters[k] }];
      }, []),
    },
    page: {
      size: docSize,
      current,
    },
    // in case pass null, it will not take default value, and we need that case to return every fields
    ...(!!result_fields && { result_fields })
  };

  // if (facets) {
  //   options.facets = facets
  // }
  if (sort) {
    options.sort = sort;
  }

  // console.log('query options:', JSON.stringify(options, null, 2))
  const configuration = {
    maxDocuments: docSize,
    firstMetaInfo: {},
  };

  let es_docs = [];
  for await (const document of queryAppSearch(
    query,
    options,
    configuration,
    elasticConf
  )) {
    es_docs.push(document);
  }
  // console.log('es_docs: ', es_docs.map(e=> ({
  //   type: e.type?.raw,
  //   broader: e.broader?.raw
  // })))

  const denormed = denormForJsonld(es_docs);
  // console.log('denormed', denormed.map(t => t.prefLabel))
  return {
    data: denormed,
    meta: configuration.firstMetaInfo,
  };
}
