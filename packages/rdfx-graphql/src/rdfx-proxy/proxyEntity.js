

/**
 * WARN: this is a highly experimental script 
 * 
 * This script show the ability to execute function on properties changes
 * With the observer pattern this should allow to propagate change to multiple databases (app-search + elastic + ...)
 * 
 * Complementary notes on the "observer pattern": 
 * // @TODO: rajouter la notion des observers, et notaemment de la librairie rxjs et du subject
 * // https://www.stackchief.com/tutorials/JavaScript%20Observables%20in%205%20Minutes
 * // https://rxjs.dev/guide/subject 
 * 
 */

// documentation: 
// - https://javascript.info/proxy
// - https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Proxy


console.log('==================== testing proxy')



let numbers = [0, 1, 2];

numbers = new Proxy(numbers, {
  get(target, prop, receiver) {
    console.log('in get:', target, prop, receiver)
    if (prop in target) {
      return target[prop];
    } else {
      return 0; // default value
    }
  },
  set(target, prop, val, receiver) {
    console.log('in set', target, prop, val, receiver)
    return Reflect.set(target,prop,val,receiver)
  }
});

numbers.push(555)

console.log( numbers[1] ); // 1
console.log( numbers[123] ); // 0 (no such item)


console.log('========= test proxy on json-ld')


// 1/ define the property 
let narrower = new Proxy(['t:v1', 't:v2'], {
  get(target, prop){
    console.log('B0/ get property level')
    return target[prop]
  },
  set(target,prop,val,receiver){
    if(prop === 'length') return Reflect.set(target,prop,val,receiver)
    console.log('B1/ set property level', target, prop, val)
    return Reflect.set(target,prop,val,receiver)
  },
  deleteProperty(...args){
    console.log('In delete', args)
    return Reflect.deleteProperty(...args)
  }
})

// 2/ define the object 

let entity = {
  id: 'test:v1',
  narrower,
}

entity = new Proxy(entity, {
  get(target, prop){
    console.log('a0/ get entity level')
    return target[prop]
  },
  set(target,prop,val,receiver){
    console.log('a1/ set entity level')
    return Reflect.set(target,prop,val,receiver)
  }
})

// @TODO: restaure theses tests ? 
// entity.narrower = new Proxy(entity.narrower, {
//   get(target, prop){
//     console.log('B0/ get property level')
//     return target[prop]
//   },
//   set(target,prop,val,receiver){
//     console.log('B1/ set property level')
//     return Reflect.set(target,prop,val,receiver)
//   }
// })

//

// associate a new narrower
// entity.narrower.push('anew:value')

// remove one value 
entity.narrower.pop()

// splice some values
// entity.narrower.splice(0,1)

console.log(entity)


