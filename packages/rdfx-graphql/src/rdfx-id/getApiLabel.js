
//@TODO:
// - create a more smart function with jsonld-context parser
// - use entity instead of id and manage the value of rdfs:label
/**
 * @typedef {Object} ApiLabel
 * @property {string} ns 
 * @property {string} label 
 * @property {string} parents
 */

/**
 * @param {object} entity 
 * @returns ApiLabel 
 */
export default function getApiLabel(entity) {

  const [ns, uriPath] = entity.id.split(':')
  const parents = uriPath.split('/')

  // 0/ id label
  let label = ''
  if (parents.length === 1) {
    label = parents[0]
  } else {
    const last = parents.pop()
    label = last
  }
  
  if (label === '' || !label) {
    console.debug('A name for the graphQL field can\'t be found for this id :', entity.id)
    throw new Error('Check for possible errors in you ontology (or in `getApiName` function)')
  }

  // 1/ rdfs:label

  return {label, ns, parents}
}