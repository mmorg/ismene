
const cloudInstance = `https://ce49528ce58a42c0a70dd416ef2baf33.ent-search.europe-west3.gcp.cloud.es.io`
const apiKey = 'private-t2ztrdjejgp426b2ksyxzq5g'

const engineConf = getConf()
const current = engineConf.preprod

const elasticConf = { current, _: engineConf }
export default elasticConf

function getConf() {
  const getC = (engineName) => ({
    apiKey,
    baseUrlFn: cloudInstance,
    engineName: engineName
  })

  const prod = getC('xxxxxx')
  const preprod = getC('rdfx-gen-v1-dev')

  return { prod, preprod }
}