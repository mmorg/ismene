

// @TODO: add export of corresponding graphQl schema ? in gatsby/gatsby-node.js

export default function isRangedBy(entity, graphLD){
    return graphLD.graph.filter( e => e.range ? e.range.includes(entity.id) : null)
}