
// This ontology is an excerpt of a (merged) ontology contains 2 kind of collections : 
// Collection A that contains skos:Concept
// Collection B that contains rome:Jobs
// on the Graphql side, the member property should be a union of the 2 possibles types

// 1/ define the Classes
const skosCollection = {
  id: 'skos:Collection',
  type: ['rdfs:Class', 'owl:Class'],
}

const skosConcept = {
  id: 'skos:Concept',
  type: ['rdfs:Class', 'owl:Class'],
}

const romeJob = {
  id: 'rome:Job',
  type: ['rdfs:Class', 'owl:Class'],
}

// 2/ define the multi-range property 
const skos_member = {
  id: 'skos:member',
  type: ['rdf:Property'],
  domain: [skosCollection.id],
  range: [skosConcept.id, romeJob.id],
}

// 3/ add specific properties to the 2 ranged examples
const skos_specificProperty = {
  id: 'skos:specificSkos',
  type: ['rdf:Property'],
  domain: [skosConcept.id],
  range: ['xsd:string'],
}
const rome_specificProperty = {
  id: 'skos:specificRome',
  type: ['rdf:Property'],
  domain: [romeJob.id],
  range: ['xsd:string'],
}


const ontologyWithUnion = {
  '@context': {},
  graph: [
    skosCollection,
    skosConcept,
    romeJob,
    skos_member,
    skos_specificProperty,
    rome_specificProperty,
  ]
}

export default ontologyWithUnion