import { toElasticPath, toJsonPath } from "@mmorg/appsearch-utils/normDenorm/utils/elasticPathSwitch.js";
// @ts-ignore
import {
  parseResolveInfo,
  simplifyParsedResolveInfoFragmentWithType,
} from 'graphql-parse-resolve-info';
import snakecase from 'snakecase';

/**
 * @typedef {import('../../../node_modules/graphql-parse-resolve-info/node8plus/index').FieldsByTypeName} FieldsByTypeName
 * @typedef {import('../../../node_modules/graphql-parse-resolve-info/node8plus/index').ResolveTree} ResolveTree
 * @typedef {import('../../rdfx-ontology/getRdfxOnto.js').Onto} Onto
 */

export default function queryResolver(rdfxClassType, single) {
  return async (source, args, context, info) => {
    // graphql args to elastic args
    const {
      page,
      limit,
      sort = '',
      where: filters = {},
      start,
      id,
      query,
    } = source?._args ?? args;
    let _query = query;

    // Work around for now, expect document should not have any field name `query`
    if (filters.query !== null && filters.query !== undefined) {
      _query = filters.query ?? '';
      delete filters.query;
    }
    const rdfxOnto = context.dataLoader.rdfxOnto;
    const filtersWithId = Object.keys(filters).filter(p => rdfxOnto.idProperties.has(p))

    const currentPage =
      page ?? (typeof start === 'number' ? Math.floor(start / limit) + 1 : 1);
    const _args = {
      query: _query,
      // facets: {
      //   type: [{ type: 'value' }]
      // },
      // follow refinejs for now
      filters: {
        ...Object.keys(filters).reduce(
          (acc, k) => ({
            ...acc,
            // TODO: confirm if all the filter in refinejs works the same way
            // As I experiment, the filter from refinejs is ended with _in
            [snakecase(k.replace(/_in$/, ''))]: filters[k],
          }),
          {}
        ),
        type: rdfxClassType,
      },
      size: limit,
      current: currentPage,
    };

    // sorts args to elastic
    if (sort) {
      // as in refinejs, sort is a string of comma like 'field1:asc,field2:desc'
      _args.sort = (sort ?? '').split(',').reduce((acc, sortString) => {
        const [field, order] = sortString.split(':');

        return {
          ...acc,
          [field]: order,
        };
      }, {});
    }

    // special case of filtering by id
    if (id) {
      //@ts-ignore
      _args.filters.id = id;
    }

    // resolving requested entities
    /** @type Onto */
    const ontologyGraph = context.dataLoader.ld.graph ?? [];
    const definition = ontologyGraph.find((o) => o.id === rdfxClassType);

    const parsedResolveInfoFragment = parseResolveInfo(info);

    const { fieldsByTypeName } = simplifyParsedResolveInfoFragmentWithType(
      parsedResolveInfoFragment,
      info.returnType
      // ComplexType
    );

    const _fieldsByTypeName = (
      single
      ? fieldsByTypeName
      : (fieldsByTypeName[Object.keys(fieldsByTypeName)[0]]?.data?.fieldsByTypeName ?? {})
    )

    const fullQueryPath = getQueryPathFromFieldsByTypeName(_fieldsByTypeName, rdfxOnto).map(
      toElasticPath
    ) ?? []
    if (!fullQueryPath.includes('id')) {
      fullQueryPath.push('id')
    }
    const esQueryPath = fullQueryPath.filter(p => !rdfxOnto.propertyFunctionMap.get(toJsonPath(p)))
    const extraPath = fullQueryPath.filter(p => !esQueryPath.includes(p))

    const res = await context.dataLoader.getByArgsWithPagination(
      _args,
      esQueryPath.reduce(
        (acc, k) => ({
          ...acc,
          [k]: { raw: {} },
        }),
        {}
      ),
      definition?.engine ? { engineName: definition.engine } : null
    );


    if (single) {
      return { ...res.data[0], _args: args };
    }

    return {
      total: res.meta?.page?.total_results,
      data: res.data.map(d => ({
        ...d,
        _args: args
      })),

    }
  };
}

/**
 * @param {ResolveTree} resolveTree
 * @param {object} ldContext
 * @see graphql-parse-resolve-info
 */
const getQueryPathFromResolveTree = (resolveTree, ldContext, prefix = '') => {
  if (Object.keys(resolveTree.fieldsByTypeName ?? {}).length) {
    return getQueryPathFromFieldsByTypeName(
      resolveTree.fieldsByTypeName,
      ldContext,
      prefix + resolveTree.name + '.'
    );
  }
  return [prefix + resolveTree.name];
};

/**
 * @param {FieldsByTypeName} fieldsByTypeName
 * @param {Onto} rdfxOnto
 * @see graphql-parse-resolve-info
 */
const getQueryPathFromFieldsByTypeName = (
  fieldsByTypeName,
  rdfxOnto,
  prefix = ''
) => {
  if (!Object.keys(fieldsByTypeName).length) return [];

  // For now, assuming that there is only one type in the fieldsByTypeName
  const resolveTrees = fieldsByTypeName[Object.keys(fieldsByTypeName)[0]];

  /** @todo: refactoring */
  // This code should be changed to a 'processing list' of keys, where we can know at the end if some keys are not processed
  // This will help to detect queried fields that are not 'literal', 'flattened' or 'idType'.
  // For example this will be usefull in misconfigured field.
  // multiples calls of `Object.keys(resolveTrees)` is not clear
  if (Object.keys(resolveTrees).length) {
    if (!prefix) {
      // in case !prefix, means that we are at the root of the query,
      // we only flatten fields with exact_match__ or match_with__ prefix or `isLiteral` fields
      const fieldsShouldBeFlatten = Object.keys(resolveTrees).filter(
        (k) =>
          /^exact_match__/g.test(k) ||
          /match_with__/g.test(k) ||
          rdfxOnto.dataTypeProperties.has(k)
      );
      const flattenedFields = fieldsShouldBeFlatten.reduce((acc, cur) => {
        const resolveTree = resolveTrees[cur];

        return [
          ...acc,
          ...getQueryPathFromResolveTree(resolveTree, rdfxOnto, prefix),
        ];
      }, []);

      const nonFlattenFields = Object.keys(resolveTrees).filter(
        (k) => !fieldsShouldBeFlatten.includes(k)
      );

      return [...flattenedFields, ...nonFlattenFields];
    }

    return Object.keys(resolveTrees).reduce((acc, cur) => {
      const resolveTree = resolveTrees[cur];
      return [
        ...acc,
        ...getQueryPathFromResolveTree(resolveTree, rdfxOnto, prefix),
      ];
    }, []);
  }
  return [];
};
