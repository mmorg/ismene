/**
 * @typedef {import('../../rdfx-ontology/getRdfxOnto.js').Onto} Onto
 */

/**
 * This function return a parametrized graphql resolver function
 * @param {*} propertyName
 * @returns
 */
export default function propertyResolver(propertyName) {
  return async (source, args, context, info) => {
    /** @type Onto */
    const rdfxOnto = context.dataLoader.rdfxOnto;
    const ldContext = rdfxOnto.sourceGraph.ldContext;
    const propertiesValues = source[propertyName];
    const propertiesWithEngine = rdfxOnto.propertiesWithEngine;
    const engine = propertiesWithEngine.get(propertyName);

    if (rdfxOnto.propertyFunctionMap.get(propertyName)) {
      // console.log(propertyName, 'propertiesValues', propertiesValues)
      const resolver = rdfxOnto.propertyFunctionMap.get(propertyName).resolver;
      return await resolver(source, args, context, info)
    }

    if (rdfxOnto.dateTypeProperties.has(propertyName)) {
      return propertiesValues?.map((value) => {
        return {
          value
        }
      }) ?? [];
    }
    if (
      rdfxOnto.idProperties.has(propertyName) ||
      ldContext.idContainerProperties.has(propertyName)
    ) {
      /** @todo filter properties retrieved here to limit the result size. There is an example in the "queryResolver" */
      const details = await context.dataLoader.getByIds(
        propertiesValues ?? [],
        engine ? { engineName: engine } : undefined
      );

      return details.filter((d) => !!Object.keys(d ?? {}).length);
    }

    if (
      rdfxOnto.dataTypeProperties.has(propertyName) ||
      ldContext.literalProperties.has(propertyName) ||
      rdfxOnto.geolocationTypeProperties.has(propertyName)
    ) {
      // @TODO: improve this part to process other types of literals, languageTaggued & dataTypes
      const graphqlLiterals = propertiesValues?.map((literal) => {
        return {
          value: literal.value || literal["@value"] || literal,
          language: literal.language || literal["@language"],
        };
      });
      return graphqlLiterals ?? [];
    }

    console.warn(
      "!!!!!! This property was not identified in `rdfxOnto`:",
      propertyName
    );
    return [];
  };
}
