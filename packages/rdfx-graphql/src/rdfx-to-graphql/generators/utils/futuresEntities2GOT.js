import {
  GraphQLObjectType
} from 'graphql'

export default function futuresEntities2GOT(futuresGEntitiesMap) {

  for (const container of futuresGEntitiesMap.values()) {
    const { name, args, fields, resolveUnique: resolve } = container
    container.got = new GraphQLObjectType({ name, args, fields, resolve })
  }

}
