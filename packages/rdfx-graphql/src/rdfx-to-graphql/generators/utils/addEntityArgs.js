import { GraphQLID, GraphQLInputObjectType, GraphQLInt, GraphQLNonNull, GraphQLString } from 'graphql'
import { GraphQLJSON } from 'graphql-scalars'

export default function addEntityArgs(entityInstructionsMap, ld){

  for(const instruction of entityInstructionsMap.values()){

    // @TODO: build the argument object from the rdfs:Property ?
    instruction.argsUnique = {
      id : {
        type: new GraphQLNonNull(GraphQLID),
        // defaultValue: '__test_default_value__'
      }
    }

    // add default argsMultiple
    instruction.argsMultiple = getDefaultRefineMultiple()
  }
}

export function getDefaultRefineMultiple(){
  const argsInstructions = {
    query : {
      type: GraphQLString
    },
    sort : {
      type: GraphQLString
    },
    where: {
      type: GraphQLJSON
    },
    page: {
      type: GraphQLInt
    },
    limit: {
      type: GraphQLInt
    },
    // this may be used when using refine UI inferencer,
    // add this to reduce effort to customize pagination query in FE
    start: {
      type: GraphQLInt
    }
  }

  return argsInstructions
}
