
//@TODO: create a more smart function with jsonld-context parser
/**
 * @deprecated use rdfx-id/getApiName instead. Breaking change: input id entity instead of id & return is object instead of string
 * @param {string} id 
 * @returns string
 */
export default function getApiName(id) {

  const [ns, uriPath] = id.split(':') 
  const path = uriPath.split('/')

  let name = ''
  if (path.length === 1) {
    name = path[0]
  } else {
    const last = path.pop()
    name = last
  }
  
  if (name === '' || !name) {
    console.debug('A name for the graphQL field can\'t be found for this id :', id)
    throw new Error('Check for possible errors in you ontology (or in `getApiName` function)')
  }

  return name
}