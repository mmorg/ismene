import { GraphQLID, GraphQLInputObjectType, GraphQLList, GraphQLNonNull, GraphQLString } from 'graphql'
import filterByDomain from '../../../rdfx-graph/filterByDomain.js'
import getApiName from './getApiName.js'
import propertyResolver from '../../resolvers/propertyResolver.js'


export default function getRdfxFieldsInstructions(entityId, entityInstructionsMap, ontology) {

  // 1/ search for properties
  const properties = filterByDomain(entityId, ontology)

  const entityPropertiesInstructions = entityInstructionsMap.get(entityId)
  if (!entityPropertiesInstructions.deferedFieldsFn) entityPropertiesInstructions.deferedFieldsFn = []


  const idFieldFn = () => {
    const fieldDefinition = {}
    const currentField = { id: fieldDefinition }

    // Type
    fieldDefinition.type = new GraphQLNonNull(GraphQLID)

    // Resolve
    fieldDefinition.resolve = (source, args, context, info) => {
      // console.log('@TODO: define a better id retrieving for ns or full id')
      return source.id
    }
    return currentField
  }
  entityPropertiesInstructions.deferedFieldsFn.push(idFieldFn)

  for (const prop of properties) {
    const propertyName = getApiName(prop.id)

    const ranges = prop.range
    if(!ranges){
      console.log('This property do not have a range defined:', prop)
      continue
    }


    // @TODO: improve to process multiples ranges
    const rangeGraphqlInst = entityInstructionsMap.get(ranges[0])

    // create a callable function to have the .got when avaible during schema parsing
    // a callable function is created for each property
    const currentFieldFn = () => {
      const fieldDescription = {}
      const currentField = {}
      currentField[propertyName] = fieldDescription

      // rdfs:Property.range in Graphql.type
      fieldDescription.type = new GraphQLList(new GraphQLNonNull(rangeGraphqlInst.got))

      // rdfs:Property.range in Graphql Graphql.resolve
      fieldDescription.resolve = propertyResolver(propertyName)

      return currentField
    }

    // assign the field definition to the entity's properties instructions
    entityPropertiesInstructions.deferedFieldsFn.push(currentFieldFn)
  }


}
