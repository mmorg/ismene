import { GraphQLError, GraphQLNonNull, GraphQLString } from 'graphql'
import getApiName from './getApiName.js'
import queryResolver from '../../resolvers/queryResolver.js'


// @TODO: remove this special case with a more generic parsing and literal definitions like https://rdf.js.org/data-model-spec/#literal-interface
// Do the model of rdfs:dataType & rdfs:Literal with the use of interfaces and "automatic values for some properties" object hierarchy
const objectTypeSpecialCase = {
  'xsd:string': {
    name: 'String_xsd',
    fields: () => ({
      // This value field only a string and not an array of value(s)
      value: { type: new GraphQLNonNull(GraphQLString) },
    }),
  },
  'mm:geoDataType': {
    name: 'GeoDataType_mm',
    fields: () => ({
      // This value field only a string and not an array of value(s)
      value: { type: new GraphQLNonNull(GraphQLString) },
    }),
  },
  'xsd:date': {
    name: 'Date_xsd',
    fields: () => ({
      // This value field only a string and not an array of value(s)
      value: { type: new GraphQLNonNull(GraphQLString) },
    }),
  },

  'rdfs:Literal': {
    name: 'Literal_rdfs',
    fields: () => ({
      // @TODO:? pass them all in `new GraphQLNonNull(GraphQLString)`
      termType: { type: GraphQLString },
      datatype: { type: GraphQLString },
      value: { type: GraphQLString },
      language: { type: GraphQLString },
    }),
  }
}

const objectTypeIndex = new Map()

/**
 *
 * @param {*} ld
 * @returns {Map<String, any>}
 */
export default function initEntityInstructions(ld) {

  const propertiesThatRange = ['domain', 'range']
  const entitiesIds = ld.graph.reduce((acc, entity) => {
    for (const p of propertiesThatRange) {
      if (entity[p]) {
        const objects = entity[p]
        if (Array.isArray(objects)) {
          objects.forEach(o => {
            if (!o) {
              console.warn(`Warn: There is a null value for the property "${p}" of:`, entity)
            } else {
              acc.add(o)
            }

          });
        } else {
          acc.add(objects);
        }
      }
    }
    return acc
  }, new Set())

  for (const id of entitiesIds) {
    if (objectTypeSpecialCase[id]) {
      objectTypeIndex.set(id, objectTypeSpecialCase[id])
      continue
    }

    // @TODO: improve with post-prefix and/or context parser : https://github.com/rubensworks/jsonld-context-parser.js
    const name = getApiName(id)

    const gqlInstruction = {
      name,
      resolver: resolveUnique, //@TODO: remove this when ok
      resolveUnique: queryResolver(id, true),
      resolveMultiple: queryResolver(id)
    }

    objectTypeIndex.set(id, gqlInstruction)

  }

  return objectTypeIndex

}

async function resolveUnique(source, args, context, info) {

  const elasticResult = await context.dataLoader.getByArgs(args)
  if (elasticResult.length > 1) {
    console.warn('There is more than one response for theses args', args, 'responses are:', elasticResult)
    return elasticResult[0]
  }
  if (elasticResult[0]) return elasticResult[0]
  // last case, there is no entity result :
  throw new GraphQLError(`No entity found with this args: ${JSON.stringify(args)}. Please check your query.`,
    {
      extensions: {
        code: 'BAD_ID',
        argumentName: 'id',

      }
    })
}
