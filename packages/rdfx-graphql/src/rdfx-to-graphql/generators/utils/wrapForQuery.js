import {
  GraphQLList,
  GraphQLNonNull,
  GraphQLObjectType,
} from 'graphql'
import { GraphQLID, GraphQLInt } from 'graphql';
import { parseResolveInfo, simplifyParsedResolveInfoFragmentWithType } from 'graphql-parse-resolve-info';
import { getDefaultRefineMultiple } from './addEntityArgs.js';

export default function wrapForQuery(futuresGEntitiesMap, ldGraph) {
  const apiCollectionsDefs = ldGraph.filter(def => def.type?.includes('mms:ApiCollection'))
  const queryWrap = {
    root: {}
  }
  const resolverMap = new Map()
  const singleQueryMap = new Map()
  const multipleQueryMap = new Map()
  for (const [key, inst] of futuresGEntitiesMap.entries()){
    const detailInGraph = ldGraph.find(g => g.id === key);
    if (detailInGraph) {
      const types = detailInGraph.type;
      if (types?.includes('owl:Class') || types?.includes("rdfs:Class")) {
        const {
          name: entryName,
          fields,
          got: entityGot,
          argsUnique,
          argsMultiple,
          resolveUnique,
          resolveMultiple
        } = inst
        const singleQueryDetail = {
          type: new GraphQLNonNull(entityGot),
          fields,
          args : argsUnique,
          resolve : resolveUnique
        }
        singleQueryMap.set(entryName, singleQueryDetail)
        resolverMap.set(entryName, resolveUnique)
        const multipleQueryDetail = {
          type: new GraphQLNonNull(new GraphQLObjectType({
            name: `all${entryName}_data`,
            fields: {
              total: {
                type: GraphQLInt
              },
              data: {
                type: new GraphQLList(new GraphQLNonNull(entityGot))
              }
            }
          })),
          fields,
          args: argsMultiple,
          resolve : resolveMultiple
        }

        multipleQueryMap.set(`all${entryName}`, multipleQueryDetail)
        resolverMap.set(`all${entryName}`, resolveMultiple)

        const group = apiCollectionsDefs.find(def => def.member?.includes(detailInGraph.id))

        if (group && !queryWrap[group.label]) queryWrap[group.label] = {}

        const wrapper = group ? queryWrap[group.label] : queryWrap.root

        wrapper.type = {
          ...wrapper.type,
          [entryName]: {
            type: singleQueryDetail.type
          },
          [`all${entryName}`]: {
            type: multipleQueryDetail.type
          }
        }
        wrapper.fields = {
          ...wrapper.fields,
          [entryName]: singleQueryDetail,
          [`all${entryName}`]: multipleQueryDetail
        }
      }
    }
  }
  const result = buildNestedGroup({
    apiCollectionsDefs,
    currentCollectionDef: apiCollectionsDefs.find(def => def.id === 'as:root'),
    fullQueryWrapper: queryWrap,
    resolverMap
  })

  return result.fields
}


const buildNestedGroup = ({ apiCollectionsDefs, currentCollectionDef, fullQueryWrapper, resolverMap }) => {
  const label = currentCollectionDef?.label ?? 'root';
  const queryName = `group_${label}_data`;
  const queryWrapper = fullQueryWrapper[label] ?? {}; // callback for case parent group without any direct children, but contains other groups

  const memberDefs = apiCollectionsDefs
    .filter(def => currentCollectionDef.member?.includes(def.id))
  const groupMembers = memberDefs
    .map(def => ({
      def,
      wrapper: buildNestedGroup({ apiCollectionsDefs, currentCollectionDef: def, fullQueryWrapper, resolverMap })
    }))
    .filter(def => !!def.wrapper)

  queryWrapper.type = new GraphQLObjectType({
    name: queryName,
    fields: {
      ...queryWrapper.type,
      ...groupMembers.reduce((acc, curr) => ({ ...acc, [curr.def.label]: curr.wrapper }), {})
    },
  })

  queryWrapper.fields = {
    ...queryWrapper.fields,
    ...groupMembers.reduce((acc, curr) => ({ ...acc, [curr.def.label]: curr.wrapper }), {})
  }

  queryWrapper.args = {
    id : {
      type: GraphQLID
      // defaultValue: '__test_default_value__'
    },
    ...getDefaultRefineMultiple()
  }

  queryWrapper.resolve = async (source, args, context, info) => {
    const parsedResolveInfoFragment = parseResolveInfo(info);

    const { fieldsByTypeName } = simplifyParsedResolveInfoFragmentWithType(
      parsedResolveInfoFragment
    );
    const result = {}
    await Promise.all(Object.entries(fieldsByTypeName[queryName]).map(async ([_key, _value]) => {
      const childFieldNodes = info.fieldNodes[0].selectionSet.selections.filter(s => s.name.value === _key)
      const childInfo = {
        ...info,
        fieldNodes: childFieldNodes,
        parentType: queryWrapper.type,
      }

      const resolver = resolverMap.get(_key)
      result[_key] = (await resolver?.(source, args, context, childInfo))
    }))

    return { ...result, _args: args };
  }
  return queryWrapper
}