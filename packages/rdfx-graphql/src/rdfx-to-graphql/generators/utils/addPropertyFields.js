import { GraphQLNonNull, GraphQLString } from 'graphql'
import getRdfxFieldsInstructions from './getRdfxFieldsInstructions.js'


export default function addPropertyFields(entityInstructionsMap, ontology) {

  for (const [id, instruction] of entityInstructionsMap) {

    getRdfxFieldsInstructions(id, entityInstructionsMap, ontology)

    // instruction already has a fields definition
    if (instruction.fields) continue

    // Process properties with defer for recursive properties
    Object.assign(
      instruction,
      {
        // fields: () => resolveDeferedFields(instruction)()
        fields: resolveDeferedFields(instruction)
      }
    )
  }
}

function resolveDeferedFields(instruction) {
  return () => {
    const resolvedObject = instruction.deferedFieldsFn.map(deferFn => deferFn())
    return Object.assign({}, ...resolvedObject)
  }
}