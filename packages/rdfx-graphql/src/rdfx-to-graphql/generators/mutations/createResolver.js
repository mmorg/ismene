import mapESMutationData from './mapESMutationData.js';
import camelcase from 'camelcase';

export default function createResolver(detailInGraph, propWithInversePropMap, type = 'create') {
  return async (parent, args, context, info) => {
    const rdfxOnto = context.dataLoader.rdfxOnto
    let data;
    if (type === 'create') {
      data = args.input.data ? [args.input.data] : args.input.bulk ? args.input.bulk : []
    } else if (type === 'update') {
      const { id } = args.input.where ?? {};
      data = args.input.data ? [{
        id,
        ...args.input.data
      }] : []
    }
    if (!data) {
      throw new Error("wrong data type for mutation handler")
    }
    const addDocuments = context.dataLoader.addDocuments;
    const updateDocuments = context.dataLoader.updateDocuments;

    // @TODO: check the properties retrieved by getByIds: this is all property or only some.
    // The getByIds property selection should be the same as the mutation
    const originalData = await context.dataLoader.getByIds(data.map(d => d.id))

    const esDocs = data.map( d => {
      d.type = d.type ?? [detailInGraph.id]
      return mapESMutationData(d, rdfxOnto)
    })
    if (type === 'create') {
      await addDocuments(esDocs)
    } else if (type === 'update') {
      await updateDocuments(esDocs)
    }

    /**
     * manage inverseOf property for all the added entities:
     */
    const allUpdatedDocuments = [];
    await Promise.allSettled(data.map(async d => {
      const res = await Promise.allSettled(
        Object.keys(d).map(async (key) => {
          if (propWithInversePropMap[key]?.handler) {
            const _originalData = originalData.find(_d => _d.id === d.id)
            return propWithInversePropMap[key].handler(
              context.dataLoader,
              d,
              _originalData
            );
          }
          return Promise.resolve([]);
        })
      );

      allUpdatedDocuments.push(
        ...res
          .filter(r => r.status === 'fulfilled')
          .map(r=> r.value)
          .flat()
      )
    }))
    // console.log('allUpdatedDocuments', allUpdatedDocuments)
    await addDocuments(allUpdatedDocuments)

    /**
      handle mirror documents
      this process should run in background, no need to wait for it to finish
     */
    data.forEach(async (d) => {
      await new Promise(resolve => setTimeout(resolve, 3000));
      const latestData = (await context.dataLoader.getByArgs({
        filters: {
          id: d.id,
          type: d.type ?? [detailInGraph.id]
        }
      }, null, detailInGraph.engine ? {
        engineName: detailInGraph.engine
      } : undefined))[0]; // expect there will always be data here
      const dataKeys = Object.keys(latestData).map(k => camelcase(k));
      const mirrorProperties = dataKeys.filter(k => (rdfxOnto.mirrorProperties ?? new Set()).has(k));

      console.log('mirrorProperties', mirrorProperties);
      [...mirrorProperties].forEach(async (propName) => {
        const propDef = rdfxOnto.propertyNameToDef.get(propName);
        console.log('propDef', propDef);
        if (propDef) {
          const propDefModelName = propDef.range[0]; // expect not null and there should only be 1 model for a mirror property
          const externalEngine = rdfxOnto.propertiesWithEngine.get(propName);
          const mirrorId = (latestData[propName])?.[0];

          const mirrorDocuments = await context.dataLoader.getByIds([mirrorId], externalEngine ? {
            engineName: externalEngine
            }: undefined);
          // console.log('found current mirrorDocument', mirrorDocuments[0]);
          const foundMirrorDocument = mirrorDocuments[0] ?? {};
          const dataTypeProperties = dataKeys.filter(
            k => !rdfxOnto.idProperties.has(k) && !rdfxOnto.mirrorProperties.has(k) && k !== 'id' && k !== 'type'
          );
          const mirrorDocument = dataTypeProperties.reduce((acc, cur) => {
            return {
              ...acc,
              [cur]: latestData[cur]
            }
          }, { type: [propDefModelName], id: mirrorId, ...foundMirrorDocument });
          console.log('mirrorDocument', mirrorDocument)
          console.log('mapped mirrorDocument', mapESMutationData(mirrorDocument, rdfxOnto))

          // TODO: update approach in case there are overlapping properties between different models into 1 mirror model
          await addDocuments([mapESMutationData(mirrorDocument, rdfxOnto)], externalEngine ? {
            engineName: externalEngine
          } : undefined)
        }
      })
    })

    return data;
  }
}