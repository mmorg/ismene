// @ts-ignore
import snakecase from "snakecase"

export default function mapESMutationData (data, rdfxOnto){
  const literalProperties = rdfxOnto.literalProperties
  const geolocationTypeProperties = rdfxOnto.geolocationTypeProperties
  return Object.entries(data).reduce((acc, [key, value]) => {
    const snakeCaseKey = snakecase(key)
    if (literalProperties.has(key)) {
      return {
        ...acc,
        [`${snakeCaseKey}__value`]: value[0]?.['@value'] ?? value[0]?.value,
        [`${snakeCaseKey}__language`]: value[0]?.['@language'] ?? value[0]?.language,
      };
    }

    if (geolocationTypeProperties.has(key)) {
      return {
        ...acc,
        [snakeCaseKey]: value[0].value ?? value[0], // different result due to the way data is generated
      };
    }

    return {
      ...acc,
      [snakeCaseKey]: value,
    };
  }, {});
};