import getApiName from '../utils/getApiName.js';
import snakecase from 'snakecase';

export default function createInverseHandler({
  propKey,
  propDefinition,
  ldGraph
}) {
  const inverseOfPropName = getApiName(
    propDefinition.inverseOf[0]
  );
  const inverseOfPropDefinition = ldGraph.find(
    (g) => g.id === propDefinition.inverseOf[0]
  );
  const inverseTarget = inverseOfPropDefinition?.domain?.[0]; // expect not null here
  return async (dataLoader, updatedData, originalData) => {
    if (!updatedData[propKey]) {
      return;
    }
    // list of updated
    const updatedIdsList = updatedData[propKey] ?? [];

    // list of original
    // in case data is created for the first time, there will be no original data
    const originalIdsList = originalData?.[propKey] ?? [];
    const newIds = updatedIdsList.filter(
      (id) => !originalIdsList.includes(id)
    );
    const removedIds = originalIdsList.filter(
      (id) => !updatedIdsList.includes(id)
    );

    try {
      // property with inverseOf property should be a list of ids
      const [newIdsModels, removedIdsModels] =
        await Promise.all([
          dataLoader.getByIds(newIds),
          dataLoader.getByIds(removedIds),
        ]);
      if (inverseOfPropName) {
        const updatedModels = [
          ...newIdsModels,
          ...removedIdsModels,
        ]
          .filter(m=>!!Object.keys(m ?? {}).length)
          .map((m) => {
            // get the value of the inverseOf property in the associated model
            let inverseOfProp = m[inverseOfPropName];

            // if no inverseOf property or inverseOf property is not a list, then create a new list
            if (!inverseOfProp || !Array.isArray(inverseOfProp)) {
              inverseOfProp = [updatedData.id];
            } else {
              // if model is added to the list, then add the updated model id to the inverseOf property
              if (newIds.includes(m.id)) {
                inverseOfProp = [
                  ...new Set([...inverseOfProp, updatedData.id]),
                ];
                // if model is removed from the list, then remove the updated model id from the inverseOf property
              } else if (removedIds.includes(m.id)) {
                inverseOfProp = inverseOfProp.filter(
                  (id) => id !== updatedData.id
                );
              }
            }

            // this below part just to map the results from getByIds in data loader to correct form to update to ES
            const mappedModel = Object.entries(m).reduce(
              (acc, [key, value]) => {
                let _value = value;
                if (key === inverseOfPropName) {
                  _value = inverseOfProp;
                }
                if (dataLoader.rdfxOnto.dataTypeProperties.has(key)) {
                  return {
                    ...acc,
                    [`${snakecase(key)}__value`]: _value[0].value,
                    [`${snakecase(key)}__language`]:
                    _value[0].language,
                  };
                }
                return {
                  ...acc,
                  [snakecase(key)]: _value,
                };
              },
              {}
            );

            // in case original model does not have inverseOf property, then add it to the model
            if (!mappedModel[snakecase(inverseOfPropName)]) {
              mappedModel[snakecase(inverseOfPropName)] = inverseOfProp;
            }

            return mappedModel;
          });

        // in case there is no original data, then create a new model with inverseOf property
        const uncreatedIds = [...newIds, ...removedIds].filter(id => !updatedModels.find(m => m.id === id))
        updatedModels.push(...uncreatedIds.map(id => ({
          id,
          [snakecase(inverseOfPropName)]: [updatedData.id],
          type: [inverseTarget]
        })))

        return updatedModels
      }
    } catch (e) {
      console.log("error when updating inverseOf property", e);
    }
  }
}