import pluralize from "pluralize";
import {
  GraphQLInputObjectType,
  GraphQLID,
  GraphQLList,
  GraphQLNonNull,
  GraphQLString,
} from "graphql";
// @ts-ignore
import camelCase from "camelcase";
import getApiName from "../utils/getApiName.js";
import createResolver from './createResolver.js';
import mapESMutationData from './mapESMutationData.js';
import createInverseHandler from './createInverseHandler.js';
import { isGeolocation, isLiteral } from '../../../rdfx-ontology/getRdfxOnto.js';

const String_xsd_InputType = new GraphQLInputObjectType({
  name: "String_xsd_InputType",
  fields: {
    value: {
      type: new GraphQLNonNull(GraphQLString),
    },
    language: {
      type: GraphQLString, // new GraphQLNonNull(GraphQLString),
    },
  },
});


export default function getMutationFields(entityInstructionsMap, ldGraph) {
  // refine 'where' wraper:
  const refine_where_locator = new GraphQLInputObjectType({
    name: "refine_where_locator",
    fields: {
      id: {
        type: GraphQLID,
      },
    },
  });
  let result = {};
  for (const [key, value] of entityInstructionsMap.entries()) {
    const detailInGraph = ldGraph.find((g) => g.id === key);
    const propWithInversePropMap = {};

    // Just handle if entity is defined in graph
    if (detailInGraph) {
      const types = detailInGraph.type;

      // only entity with type Class should be able to be Create/Update/Delete
      if (types?.includes("owl:Class") || types?.includes("rdfs:Class")) {
        const name = getApiName(key);

        const inputFields = Object.entries(value.fields()).reduce(
          (acc, [key, value]) => {
            const associatedFieldDef = ldGraph.find((g) =>
              g.id?.toLowerCase?.().endsWith(key.toLowerCase()) && g.type?.includes("rdf:Property")
            );

            let inputType;
            if (associatedFieldDef) {
              if (isLiteral(associatedFieldDef?.range)) {
                inputType = new GraphQLList(
                  new GraphQLNonNull(String_xsd_InputType)
                );
              }

              if (isGeolocation(associatedFieldDef?.range)) {
                inputType = new GraphQLList(
                  new GraphQLNonNull(String_xsd_InputType)
                );
              }
              // if (detailInGraph.id === "mms:User") {
              //   console.log('associatedFieldDef', associatedFieldDef)
              // }
              if (
                associatedFieldDef.inverseOf?.length &&
                associatedFieldDef.domain?.length
              ) {
                propWithInversePropMap[key] = {
                  ...associatedFieldDef,
                  handler: createInverseHandler({
                    propKey: key,
                    propDefinition: associatedFieldDef,
                    ldGraph
                  })
                };
              }
            }
            return {
              ...acc,
              [key]: {
                type:
                  inputType ??
                  (value.type instanceof GraphQLList
                    ? new GraphQLList(new GraphQLNonNull(GraphQLString)) // TODO: confirm if this should only be string or else
                    : value.type),
              },
            };
          },
          {}
        );

        /** Create */
        const singularResource = pluralize.singular(name);
        const camelCreateName = camelCase(`create-${singularResource}`);
        const createInput = new GraphQLInputObjectType({
          name: `${name}_createInput`,

          fields: {
            id: {
              type: GraphQLID,
            },
            ...inputFields,
          },
        });

        const refineWrapperCreateFields = {
          data: {
            type: createInput,
          },
          bulk: {
            type: new GraphQLList(new GraphQLNonNull(createInput))
          },
          where: {
            type: refine_where_locator,
          },
        };

        const refineCreateInputType = new GraphQLInputObjectType({
          name: `${camelCreateName}Input`,
          fields: { ...refineWrapperCreateFields },
        });

        /** Update */
        const camelUpdateName = camelCase(`update-${singularResource}`);
        const updateInput = new GraphQLInputObjectType({
          name: `${name}_updateInput`,
          fields: {
            ...inputFields,
          },
        });

        const refineWrapperUpdateFields = {
          data: {
            type: new GraphQLNonNull(updateInput),
          },
          bulk: {
            type: new GraphQLList(new GraphQLNonNull(updateInput))
          },
          where: {
            type: refine_where_locator,
          },
        };

        const refineUpdateInputType = new GraphQLInputObjectType({
          name: `${camelUpdateName}Input`,
          fields: { ...refineWrapperUpdateFields },
        });

        /** Delete */
        // const deleteInput = new GraphQLInputObjectType({
        //   name: `${name}_deleteInput`,
        //   fields: {
        //     id: {
        //       type: GraphQLID
        //     }
        //   }
        // });

        const camelDeleteNAme = camelCase(`delete-${singularResource}`);
        const refineWrapperDeleteFields = {
          // data: {
          //   type: new GraphQLNonNull(deleteInput)
          // },
          where: {
            type: refine_where_locator,
          },
        };

        const refineDeleteInputType = new GraphQLInputObjectType({
          name: `delete${name}Input`,
          fields: { ...refineWrapperDeleteFields },
        });

        result = {
          ...result,
          ...{
              [camelCreateName]: {
                type: new GraphQLList(entityInstructionsMap.get(key).got),
                args: {
                  input: {
                    type: refineCreateInputType,
                  },
                },
                // @TODO: use the Graph function to define if property is a literal, more than this "in-place" initialisation
                resolve: createResolver(detailInGraph, propWithInversePropMap),
              },
              [camelUpdateName]: {
                type: new GraphQLList(entityInstructionsMap.get(key).got),
                args: {
                  input: {
                    type: refineUpdateInputType,
                  },
                },
                resolve: createResolver(detailInGraph, propWithInversePropMap, 'update'),
              },
              // TODO update and test bulk delete
              [camelDeleteNAme]: {
                type: entityInstructionsMap.get(key).got,
                args: {
                  input: {
                    type: refineDeleteInputType,
                  },
                },
                resolve: async (parent, args, context, info) => {
                  const { id } = args.input.where ?? {};
                  const deleteDocuments = context.dataLoader.deleteDocuments;
                  const res = await deleteDocuments([id]);
                  console.log("delete res", res);
                  // TODO: check here why the data is still there right after deletion,
                  // need to wait for few seconds and request again to get new data
                  await new Promise((resolve) => setTimeout(resolve, 2000));
                  return { id };
                },
              },
            },
          };
        }
      }
    }

  return result;
}
