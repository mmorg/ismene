import { GraphQLObjectType } from 'graphql'
import getMutationFields from './generators/mutations/getMutationFields.js'
import addEntityArgs from './generators/utils/addEntityArgs.js'
import addPropertyFields from './generators/utils/addPropertyFields.js'
import futuresEntities2GOT from './generators/utils/futuresEntities2GOT.js'
import initEntityInstructions from './generators/utils/initEntityInstructions.js'
import wrapForQuery from './generators/utils/wrapForQuery.js'
 
export default function entities2graphql(ld) {

  // @TODO: rename to graphql type ?
  const entityInstructionsMap = initEntityInstructions(ld)

  // fields
  addPropertyFields(entityInstructionsMap, ld)
  // args
  addEntityArgs(entityInstructionsMap, ld)

  futuresEntities2GOT(entityInstructionsMap)

  // [...entityInstructionsMap.entries()].map(([key, value]) => {
  //   if (key === "skos:Concept") {
  //     const fields = value.fields();
  //     console.log(fields.memberOf)
  //   }
  // })


  // create queryLevel ObjectType
  const queryFieldsWrap = wrapForQuery(entityInstructionsMap, ld.graph)

  const Query = new GraphQLObjectType({
    name: 'Query',
    fields: {
      ...queryFieldsWrap,
    }
  });

  // Start mutation part
  const mutationFields = getMutationFields(entityInstructionsMap, ld.graph)
  const Mutation = new GraphQLObjectType({
    name: 'Mutation',
    fields: {
      ...mutationFields,
    }
  })

  return {
    query: Query,
    mutation: Mutation
  }

}
