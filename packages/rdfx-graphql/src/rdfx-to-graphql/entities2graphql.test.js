// @ts-ignore
import { describe, expect, it, beforeAll, afterAll } from '@jest/globals'
// @ts-ignore
import { readJson } from '@mmorg/fsutils'
import { graphql, GraphQLSchema, printSchema } from 'graphql'
import getDataLoader from './getDataLoader.js'
import entities2graphql from './entities2graphql.js'
// @ts-ignore
import { jest } from '@jest/globals'
import { addMocksToSchema } from '@graphql-tools/mock'
import ontologyWithUnion from './__fixtures__/ontologyWithUnion.js'

// pnpm test -- --watch entities2graphql.test

describe('Generate Graphql Union on `multi-range property`', () => {
  // This ontology is an excerpt of a (merged) ontology contains 2 kind of collections : 
  // Collection A that contains skos:Concept
  // Collection B that contains rome:Jobs
  // on the graphql side, the member property should be a union of the 2 possibles types
  it.only('create an union for multi-range property', async () => {
    const schemaDefinition = entities2graphql(ontologyWithUnion)
    const schema = new GraphQLSchema(schemaDefinition)

    const schemaWithMocks = addMocksToSchema({ schema })
    // console.log(printSchema(schema))

    const query = `
    query unionMember {
      all_skos {
        allCollection {
          data {
            member {
              id
              ...on Concept {
                specificSkos {
                  value
                }
              }
              ...on Job {
                specificRome {
                  value
                }
              }
            }
          }
          
        }
      }
    }`


    const result = await graphql({
      schema: schemaWithMocks,
      source: query,
      // contextValue,
      // variableValues,
    })

    expect(result.errors).toBeFalsy()

  })

})

it.todo('Review this kind of test with dataloader, should it be reused somewhere or removed ?')
// ontology
const ontoFolder = `skos-1.2.0-test-concept`
const ontoPath = `data/${ontoFolder}/skos-1.2.0.jsonld`
const ontology = readJson(ontoPath)

describe.skip('Experiment 2 : testDynamicLevel1', () => {
  let context
  /** @type GraphQLSchema */
  let schema

  beforeAll(() => {
    context = { '@TODO': 'create a context' }
    const schemaDefinition = entities2graphql(ontology)
    schema = new GraphQLSchema(schemaDefinition)
  })

  afterAll(() => {
    // database.tearDown();
  })

  it('should resolve a Concept Tree', async () => {
    // @TODO: this is a real only call that should be locally saved
    const dataLoader = getDataLoader({
      apiKey: 'mockApiKey',
      baseUrl: 'mockBaseUrl',
      engineName: 'mockEngineName'
    }, ontology);

    jest.spyOn(dataLoader, "getByArgs").mockImplementation(() => {
      return []
    });
    const contextValue = { dataLoader }
    const query = `
    query AllConcept($page: Int, $limit: Int, $sort: String) {
      allConcept(page: $page, limit: $limit, sort: $sort) {
        id
        prefLabel {
          value
          language
        }
      }
    }`

    const variableValues = {
      "page": 1,
      "limit": 10,
      "sort": "id:asc",
    }


    // see: https://graphql.org/graphql-js/
    const result = await graphql({
      schema,
      source: query,
      contextValue,
      variableValues,
    })

    expect(result.errors).toBeFalsy();
    expect(result).toEqual({ "data": { "allConcept": [] } });
    expect(dataLoader.getByArgs).toBeCalledWith({
      size: 10,
      current: 1,
      sort: {
        id: "asc"
      },
      filters: {
        type: "skos:Concept"
      }
    }, {
      id: { raw: {} },
      pref_label__value: { raw: {} },
      pref_label__language: { raw: {} },
    })
  })

})

