// @ts-ignore
import createDocAppSearch from "@mmorg/appsearch-utils/cud/createDocAppSearch.js";
import deleteDocAppSearch from "@mmorg/appsearch-utils/cud/deleteDocAppSearch.js";
import updateDocAppSearch from "@mmorg/appsearch-utils/cud/updateDocAppSearch.js";
import batchIdsSearch from "@mmorg/appsearch-utils/batch/batchIdsSearch.js";
import appSearchDataClient from '../rdfx-client-appSearch/appSearchDataClient.js'
import getRdfxOnto from '../rdfx-ontology/getRdfxOnto.js'
import getAppSearchClient from '@mmorg/appsearch-utils/getAppSearchClient.js';
// @ts-ignore
import snakecase from 'snakecase';
import jwt from 'jsonwebtoken'

// @TODO: the dataloader spec to be a 'real and only' elastic data loader
//    => remove the 'static file' reading and pass elastic configuration only'

export default async function getDataLoader(elasticConf, ontology, worldGraph, options = { throttleConf : {}, secret: '' }) {
  let ld = ontology;
  if (!ontology) {
    console.warn(
      '@Deprecated: this will use a deprecated `ontology properties` index'
    );
    throw new Error(
      'Pass your source ontology as a parameter of the `getDataLoader` function '
    );
  }
  const rdfxOnto = getRdfxOnto(ld, worldGraph)
  await updateSchemaForSpecificTypes(elasticConf, rdfxOnto)

  const dataSourceFn = async (className) =>
    (await appSearchDataClient({ type: className }, undefined, elasticConf))?.data ?? [];
  const dataSourceByArgs = async (args, resultFields, _elasticConf) =>
    (await appSearchDataClient(args, resultFields, {
      ...elasticConf,
      ..._elasticConf,
    })).data ?? [];
  const dataSourceByArgsWithPagination = async (args, resultFields, _elasticConf) =>
    (await appSearchDataClient(args, resultFields, {
      ...elasticConf,
      ..._elasticConf,
    }));

  // TODO: add type of documents to the query
  const getByIds = async (ids, _elasticConf) => {
    const cudOptions = {}; // keep the default limits for now
    const bodyOption = {
      search_fields: {
        id: {},
      },
    };

    // It use the default throttle interval of 100ms, and limit 4, can be shortened if needed
    return await batchIdsSearch(ids, {
      ...elasticConf,
      ..._elasticConf,
    }, cudOptions, bodyOption);
  };

  const addDocuments = async (documents) => {
    const _elasticConf = getEngineForDocuments(documents, elasticConf, rdfxOnto)
    const response = await createDocAppSearch(documents, _elasticConf, options.throttleConf)

    return response;
  };
  const updateDocuments = async (documents) => {
    const _elasticConf = getEngineForDocuments(documents, elasticConf, rdfxOnto)
    const response = await updateDocAppSearch(documents, _elasticConf, options.throttleConf)

    return response;
  };

  const deleteDocuments = async (documentIds) => {
    // @TODO: how do we define the source engine in this case (only ids) ?
    const response = await deleteDocAppSearch(documentIds, elasticConf);

    return response;
  }

  const dataLoader = {
    ld,
    // rdfxGraph: Graph(defaultLD),
    rdfxOnto, //: getRdfxOnto(ld, worldGraph),
    getByType: dataSourceFn,
    getByArgs: dataSourceByArgs,
    getByArgsWithPagination: dataSourceByArgsWithPagination,
    getByIds,
    addDocuments,
    updateDocuments,
    deleteDocuments,
  };

  return dataLoader;
}

// set engine schema for specific types
const updateSchemaForSpecificTypes = async (elasticConf, rdfxOnto) => {
  const client = getAppSearchClient(elasticConf)
  const newSchema = {};

  // geolocation type
  [...(rdfxOnto.geolocationTypeProperties)].forEach((prop) => {
    newSchema[snakecase(prop)] = "geolocation"
  });

  // date type
  [...(rdfxOnto.dateTypeProperties)].forEach((prop) => {
    newSchema[snakecase(prop)] = "date"
  });

  await client.putSchema({
    engine_name: elasticConf.engineName,
    schema: newSchema
  })
}

/** @todo: add this to the `rdfxOnto` like .propertiesWithEngine ; it can be named like .entitiesWithEngine */
function getEngineForDocuments(documents, elasticConf, rdfxOnto) {
  if (documents.length > 1) {
    console.warn('!!!! @TODO: the `addDocuments` function  may not work in case of multiple documents that target multiples engines')
  }

  if(!documents.length) return elasticConf
  const entityType = documents[0].type[0]
  const entityDef = rdfxOnto.sourceGraph.findById(entityType)
  // console.log('getEngineForDocuments:', rdfxOnto.propertiesWithEngine)
  const engineName = entityDef?.engine ? { engineName: entityDef.engine } : {}
  return Object.assign({}, elasticConf, engineName)
}

// @TODO: make it an array of dataClient to call (multi-points sync)
async function dataSourceFn(className) {
  return await appSearchDataClient({ type: className });
}

async function dataSourceByArgs(args) {
  return await appSearchDataClient(args);
}
