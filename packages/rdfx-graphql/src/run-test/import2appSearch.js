import fs from 'fs'
import createDocAppSearch from '../appSearchUtils/cud/createDocAppSearch.js'
import normForAppSearchWithDepth from '../appSearchUtils/normDenorm/normalisation/normForAppSearchWithDepth.js'
import elasticConf from '../_confs/elasticConf.js'

// node src/run-test/import2appSearch.js

console.log('start import')


const path = 'data/genTerms-1.1.0/genTerms-v1.1.0.jsonld'
const ld = JSON.parse(fs.readFileSync(path).toString())

// const sendedEntities = ld.graph.slice(0,5)
const elasticDocs = normForAppSearchWithDepth(ld.graph, ld, 2)

// console.log(elasticDocs[2])
// console.log(elasticConf.current)

await createDocAppSearch(elasticDocs, elasticConf.current)
