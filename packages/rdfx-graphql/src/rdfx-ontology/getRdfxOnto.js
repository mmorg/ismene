// @ts-ignore
import { readJson } from "@mmorg/fsutils";
import getGraph from "../rdfx-graph/getGraph.js";
/**
 * @typedef {Object.<string, object|object[]>} LD
 *  // property {object} '@context'
 *  // property {object[]} graph
 * @todo There is a way to define object with "key with special caracters" (like "@context") ?.
 *    Here there is a (woking?) example, but for typescript: https://github.com/toba/json-ld/blob/master/src/types.ts
 *    For now LD is only a generic definition like a dictionary: https://jsdoc.app/tags-type.html
 */

/**
 * @typedef Onto
 * @property {Set<string>} idProperties - Index of properties that contains an ID
 * @property {Set<string>} dataTypeProperties - Index of properties that contains a DataType
 * @todo Define if a "literalProperties" is required, as DataType is a subClass of Literal and there is no example of type=rdfs:Literal
 */


const loadDefaultWorldGraph = (onto) => {
  const ontoFolder = '../../gatsby/data'
  const dataTypes = readJson(`${ontoFolder}/xmlDataTypes/dataTypeDefinitions.jsonld`)
  const rdfsOnto = readJson(`${ontoFolder}/rdfs/rdf-schema-1.1.0.jsonld`)
  const rdfOnto = readJson(`${ontoFolder}/rdf/22-rdf-syntax-ns-1.1.0.jsonld`)
  return getGraph(onto, dataTypes, rdfsOnto, rdfOnto)
}

const verbose = 0; // true

/**
 * @param {LD} onto - The source ontology
 * @param {object} worldGraph - The rdfx-graph of the "world ontologies" that are reused in `onto`
 * @param {object} [context] - The context associated with the data. This will give "real" property name defined in the context
 * @return Onto
 * @todo implement the context parameter
 */
export default function getRdfxOnto(onto, worldGraph, context) {
  // console.log(onto)
  const sourceGraph = getGraph(onto);
  const properties = sourceGraph.filterByType("rdf:Property");
  const propertyFunctions = sourceGraph.filterByType("met:PropertyFunction");
  const propertiesDefs = properties.reduce((acc, p) => {
    acc.set(p.id, getPropertyDescription(p, worldGraph, context))
    return acc;
  }, new Map())
  const mirrorClasses = sourceGraph.filterByType("mms:Mirror");
  const classes = sourceGraph.filterByType("rdfs:Class");
  const propertiesByClass = classes.reduce((acc, cur) => {
    const props = properties
      .filter(p => p.domain?.includes(cur.id));
    acc.set(cur.id, props);
    return acc;
  }, new Map())
  if (!worldGraph) {
    console.info('The default world graph is loaded')
    worldGraph = loadDefaultWorldGraph(onto)
  }

  const propertyFunctionMap = new Map()
  const idProperties = new Set()
  const dataTypeProperties = new Set()
  const geolocationTypeProperties = new Set()
  const dateTypeProperties = new Set()
  const literalProperties = new Set()
  const mirrorProperties = new Set()
  const propertiesWithEngine = new Map()
  const propertyNameToDef = new Map()
  const undefined = []

  // const worldGraph = getGraph(onto, dataTypes, rdfsOnto, rdfOnto)
  properties.forEach((p) => {
    const description = propertiesDefs.get(p.id)
    propertyNameToDef.set(description.label, p)
    const propertyEngine = getPropertyEngine(p, worldGraph)
    if (propertyEngine) {
      propertiesWithEngine.set(description.label, propertyEngine)
    }

    const _propertyFunctions = propertyFunctions.filter(pf => pf.predicate === p.id)
    if (_propertyFunctions.length) {
      propertyFunctionMap.set(description.label, _propertyFunctions[0]); // expect there should be only 1 property function per property
    }

    if (!description.parent.length) {
      undefined.push(description)
    } else if (isMirror([...description.parent, ...p.type])) {
      mirrorProperties.add(description.label)
    }  else if (isGeolocation(description.parent)) {
      geolocationTypeProperties.add(description.label)
    } else if (isDate(description.parent)) {
      dateTypeProperties.add(description.label)
    } else if (isLiteral(description.parent) || isDatatype(description.parent)) {
      // TODO: confirm usage of dataTypeProperties
      dataTypeProperties.add(description.label)
      // TODO: split this condition out after confirmed above concern
      if (isLiteral(description.parent)) {
        literalProperties.add(description.label)
      }
    } else {
      idProperties.add(description.label);
    }
  });

  if (undefined.length && verbose) {
    console.log('The property type of range was not found for theses ones:', undefined)
  }

  // TODO: revise later for mirror documents
  // console.log('mirrorProperties', mirrorProperties)
  // // handle Mirror classes
  // const mirrorClassesMap = new Map();
  // for (const mirrorClass of mirrorClasses) {
  //   const props = propertiesByClass.get(mirrorClass.id) ?? []
  //
  //   // expect properties of mirror class to be related to a single class
  //   for (const prop of props) {
  //     const range = prop.range[0]
  //     const rangeProps = propertiesByClass.get(range) ?? []
  //     const mirrorClassProps = (mirrorClassesMap.get(mirrorClass.id) ?? [])
  //     mirrorClassProps.push(...rangeProps
  //       .map(p => propertiesDefs.get(p.id))
  //       .filter(p => {
  //         if (p.id === "mmm:mission") {
  //           console.log('mirrorProperties', mirrorProperties, mirrorProperties.has(p.label))
  //         }
  //         return !mirrorProperties.has(p.label) && !idProperties.has(p.label)
  //       })
  //     )
  //     mirrorClassesMap.set(mirrorClass.id, mirrorClassProps)
  //   }
  // }
  // console.log('mirrorClassesMap', mirrorClassesMap)

  return {
    idProperties,
    dataTypeProperties, // literal type only for now
    geolocationTypeProperties,
    dateTypeProperties,
    propertiesWithEngine,
    literalProperties,
    mirrorProperties,
    propertyNameToDef,
    propertyFunctionMap,
    classes,
    sourceGraph, // @TODO: review literalProperties and dataTypeProperties
  };
}

function getPropertyDescription(property, graph, context) {
  const id = property.id;
  const parent = getRangeParentHood(property, graph);
  /** @todo implement here the context parsing to get the real property name */
  const label = id.split(":")[1];
  return {
    id,
    parent,
    label,
  };
}

/**
 *
 * @param {Object} property
 * @param {Object} graph
 * @todo define the graphType at least, from the `Graph` function
 */
function getRangeParentHood(property, graph) {
  const ranges = property.range;
  const parent = [];
  for (const r of ranges) {

    const rangeValue = graph.findById(r);

    if (!rangeValue) {
      console.log("This Id was not found in graph", r);
      continue;
    }

    // 1/ keep the range in parent
    parent.push(rangeValue.id);

    // 2/ recursive finding of heritance parent's
    const tree = getParents(rangeValue, graph);
    parent.push(...tree);
  }

  return parent;
}

function getParents(entity, graph) {
  if (!entity.subClassOf) return []
  const tree = []
  for (const s of entity.subClassOf) {
    const parentValue = graph.findById(s)
    if (!parentValue) {
      if (verbose) console.log('(getParents) This Id was not found in graph', s)
      continue
    }
    tree.push(parentValue.id)
    tree.push(...getParents(parentValue, graph))
  }
  return tree
}

function getPropertyEngine(property, graph) {
  const ranges = property.range;
  // expect only one range here for property type id
  let rangeDef = graph.findById(ranges[0]);
  for (let i = 0; i < ranges.length; i++) {
    const range = ranges[i];
    rangeDef = graph.findById(range);
    if (rangeDef) break;
  }
  return rangeDef?.engine
}

// TODO: confirm rdfs:Datatype to be literal??
export function isLiteral(propertyParent = []) {
  return ["rdfs:Literal", "rdfs:Datatype", "xsd:string"].some((p) => propertyParent.includes(p));
}
export function isDatatype(propertyParent = []) {
  return ["rdfs:Datatype"].some((p) => propertyParent.includes(p));
}
export function isGeolocation(propertyParent = []) {
  return ["mm:geoDataType"].some((p) => propertyParent.includes(p));
}
function isDate(propertyParent = []) {
  return ["xsd:date"].some((p) => propertyParent.includes(p));
}
function isMirror(propertyParent = []) {
  return ["mms:mirroredBy"].some((p) => propertyParent.includes(p));
}