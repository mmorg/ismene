

// @TODO: merge this context parser with this one : packages/rdfx-layer/src/contextUtil/contextParser.js
//      : extract contextUtil into a dedicated package.

export default function contextParser(ld) {
  const _localContext = ld["@context"];
  const _graphTypeMapping = {};

  let idContainerProperties = new Set();
  let literalProperties = new Set();
  try {
    Object.keys(_localContext).forEach((propertyName) => {
      if (isIdContainer(propertyName)) idContainerProperties.add(propertyName);
    });
  } catch (e) {}
  try {
    Object.keys(_localContext).forEach((propertyName) => {
      if (isLiteral(propertyName)) literalProperties.add(propertyName);
    });
  } catch (e) {}

  return {
    isIdContainer,
    isLiteral,
    idContainerProperties,
    literalProperties,
    addTypeMapping,
    getTypeMapping,
  };

  function isIdContainer(propertyName) {
    const ldConfig = _localContext[propertyName];

    if (!ldConfig) return false;

    const isId = ldConfig["@type"] ? ldConfig["@type"] === "@id" : false;
    const isContainer = ldConfig["@container"]
      ? ldConfig["@container"] === "@set"
      : false;

    return isId && isContainer;
  }

  // @TODO: this test as to be improved
  function isLiteral(propertyName) {
    const ldConfig = _localContext[propertyName];

    if (!ldConfig) return false;

    const isLiteral = ldConfig["@type"] ? ldConfig["@type"] !== "@id" : true;
    return isLiteral;
  }

  function addTypeMapping(graphqlType, rdfxType) {
    if (getTypeMapping(graphqlType) && getTypeMapping(graphqlType) !== rdfxType)
      throw new Error("Another mapping exist for the same key.");
    _graphTypeMapping[graphqlType] = rdfxType;
    return rdfxType;
  }

  function getTypeMapping(graphqlType) {
    return _graphTypeMapping[graphqlType];
  }
}
