import getApiLabel from '../rdfx-id/getApiLabel.js'
import contextParser from './contextParser.js'
import filterBy from './filterBy.js'
import filterByType from './filterByType.js'
import {default as findById} from './findById.js'

/**
 * @typedef {Object} GraphReader
 * @property {function(string) : object} findById
 * @property {function(string) : object} filterBy
 * @property {function(string) : object} filterByType
 * @property {function(string) : object} ldContext
 * @todo define more precise type for the `ldContext` property
 */

/**
 * 
 * @param {Object} ld 
 * @param  {...any} worldLd 
 * @returns GraphReader
 * @todo import the definition of LD from packages/rdfx-graphql/src/rdfx-ontology/getRdfxOnto.js
 */
export default function getGraph(ld, ...worldLd) {
  const agg_ld = { graph: [...ld.graph] }
  for (const wld of worldLd) {
    agg_ld.graph.push(...wld.graph)
  }

  /** @type GraphReader */
  return {
    findById: (id) => findById(id, agg_ld),
    filterBy: (property, value) => filterBy(property, value, agg_ld),
    filterByType: (rdfxType) => filterByType(rdfxType, agg_ld),
    ldContext: contextParser(ld), // @TODO: review this and make it works on agg_ld
    indexes: {
      apiLabelMap : getApiLabelMap(agg_ld)
    }
  }

}

const modelProperties = ['rdf:Property', 'rdfs:Class', 'owl:Class']
function getApiLabelMap(ld){

  const index = new Map()
  for(const e of ld.graph){
    // @TODO: reuse the Graph functions instead
    if(!e.type) continue
    if(!modelProperties.some( modelId => e.type.includes(modelId))) continue

    const apiLabel = getApiLabel(e)
    const found = index.get(apiLabel.label)
    if(found){
      const foundLabel = getApiLabel(found)
      const message = `
      There is a conflic in api label name. 
      Existing one : ${JSON.stringify(foundLabel)}
      New one : ${JSON.stringify(apiLabel)}
      `
      console.log(message)
      throw new Error('This case should be fixed in the sources Ontologies')
    }else {
      index.set(apiLabel.label, e)
    }

  }
  
  return index
}