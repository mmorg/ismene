import contextParser from './contextParser.js'
import filterBy from './filterBy.js'
import filterByType from './filterByType.js'
import findById from './findById.js'

/** @deprecated use `getGraph` instead */
export default function Graph(ld) {

  return {
    findById: (id) => findById(id,ld),
    filterBy: (property, value) => filterBy(property, value, ld),
    filterByType: (rdfxType) => filterByType(rdfxType, ld),
    ldContext: contextParser(ld)

  }

}
