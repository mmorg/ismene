import filterBy from './filterBy.js'

export default function filterByType(typeValue, ld){
  return filterBy('type', typeValue, ld)
}
