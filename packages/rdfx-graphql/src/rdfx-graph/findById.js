
export default function findById(id, ld) {
    const filtered = ld.graph.filter(node => node.id === id)
    if (filtered.length > 1) console.warn('Multiple entities with this id in the document')
    return filtered[0]
}
