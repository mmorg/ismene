import { dirname } from 'path'
import { fileURLToPath } from 'url'
const __dirname = dirname(fileURLToPath(import.meta.url))
import { readJson } from '@mmorg/fsutils'
import getStringValues from './getStringValues.js'

// docker compose exec gatsby npm test -- --watch getStringValues.test

const fixturesFolder = `${__dirname}/__fixtures__`

describe('getStringValues tests', () => {

  it('Extract strings from localized literals', () => {
    const hasAptitude = readJson(`${fixturesFolder}/hasAptitude.json`)

    // test fr flocalised comment
    const commentString = getStringValues(hasAptitude.comment)
    expect(commentString).toStrictEqual(['Aptitudes dont dispose l\'utilisateur'])

    const nullValue = getStringValues(hasAptitude.example)
    expect(nullValue).toBe(null)

    const unlanguedLiteral = getStringValues(['a value'])
    expect(unlanguedLiteral).toStrictEqual(['a value'])

    const uniqueLiteralUnlangued = getStringValues('unique literal')
    expect(uniqueLiteralUnlangued).toStrictEqual(['unique literal'])

    // @TODO: faire un test sur des numbers
  })
  

  it('Works for a test on the last fallback language', () => {
    const test = [
      {
        "_language": "en",
        "_value": "yearsKnown"
      }
    ]
    expect(getStringValues(test)).toStrictEqual(['yearsKnown'])

  })

})
