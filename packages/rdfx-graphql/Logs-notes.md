@TODO: 

* check why some properties values are duplicated by the merge (on jas and other): check: 
`node src/bin/compactLayers export -p 'data/esco' 'data/skos' -t 'data/jas' -l 'jasOnto-1.0.0' -u`



==> other level 
* class heritance management for values of "property_to_class"
  - extract an example from gatsby : multiple range for a property 


* create an ontology and AllOntology query to retrive the model (on another engine ?)

* dynamic filter values for some entities ==> retrive the possible values for a filter with a request
==> examples `isPartialQualification` list the existing qualifications 
==> can be also applied on mutation.


=> to me : 
==> esco: 
- attention / filter les terms deprecated 
- regarder la classe 

==> graphql:
* fail fast in elastic for unknow fields: 
query AllConcept($start: Int, $limit: Int, $sort: String, $where: JSON) {
  allConcept(start: $start, limit: $limit, sort: $sort, where: $where) {
    id
    prefLabel {
      value
      language
    }
    broader {
      id
    },
    narrower {
      id
    }
  }
}


* autres issues: 
  - gérer les requêtes d'aggrégations à partir de graphql ? 

* Possibility to get collections members and filter on this kind of property. 
  - for now, there is no collections in the jobs&skills data
  - example query:
http://localhost:5010/?explorerURLState=N4IgJg9gxgrgtgUwHYBcQC4QEcYIE4CeABAIIA2ZAwhBQlCgJYRIAUAJGQ3AyukQJKoANETYBnCHl5EAyijwMkAcwCURYAB0kRIgEMK1WvSatO3aRy48REqX3GSUazdp1EGYLW6IAHPAgAzABldACMEMnUvbyIAN31caJ0AXyEkokQ4cLwo128PdJ0-QJDwyJcYt3iyRLy3ZPSG1ybkkCEQeIUwsgQxDBAKog0QMx5hvgBGAAY012HbFHGhkA90XTEoYZEhpFbkoA


# @TODO on rdfx 

* 1/ add "path traversal" & "Patch emission" from functions in https://gitlab.com/mmorg/ismene/-/blob/c38e925a90085136e9bb1d0d4b510e6d3a986606/gatsby/src/jsonldUtils/graphUtils/


