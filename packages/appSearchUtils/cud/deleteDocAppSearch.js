import getAppSearchClient from '../getAppSearchClient.js'
import cudUtils from './cudUtils.js'

export default async function deleteDocAppSearch(documentIds, conf) {
  const { engineName } = conf
  const client = getAppSearchClient(conf)

  const res = await cudUtils(async (docIds) => {
    return await client.deleteDocuments({
      engine_name: engineName,
      documentIds: docIds
    })
  }, documentIds)

  console.log('** Deletion is okay')

  return res;
}



