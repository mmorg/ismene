import getAppSearchClient from '../getAppSearchClient.js'
import cudUtils from './cudUtils.js'


export default async function updateDocAppSearch(documents, conf, loadConfig) {
  const { engineName } = conf
  const client = getAppSearchClient(conf)

  const res = await cudUtils(async (docs) => {
    return await client.putDocuments({
      engine_name: engineName,
      documents: docs
    })
  }, documents, loadConfig)

  return res;
}
