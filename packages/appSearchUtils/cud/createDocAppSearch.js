import getAppSearchClient from '../getAppSearchClient.js'
import cudUtils from './cudUtils.js'


export default async function createDocAppSearch(documents, conf, throttleConf = {}) {
  const { engineName } = conf
  const client = getAppSearchClient(conf)

  console.log('** Will import documents to:', engineName)
  const res = await cudUtils(async (docs) => {
      return await client.indexDocuments({
        engine_name: engineName,
        documents: docs
      })
    },
    documents,
    throttleConf,
  )

  console.log('** Import okay to:', engineName)
  return res;
}
