import progress from 'cli-progress'
import chunkArray from '../arrayUtils/chunkArray.js'
import pThrottle from 'p-throttle'
import chalk from 'chalk'
import genericSaveCatcher from '../errorCatchers/genericSaveCatcher.js'

// @TODO?: merge this function with: genscan/rml/src/translator-data-wrapper
//       : the translator-data-wrapper as generator & abort-signal implementation


// @TODO: make this configuration available to hight level caller
// @TODO: remplace the p-map defaultConfig by the p-throttle ones
const defaultConfig = {
  limit: 4, // 2/1000 is okay, but long
  interval: 1000, // 1000
  docsCount: 20, // 80 not ok / doc per request / default limits = max 100 documents per request but also 235.7mb size max, so go down per request
  showLogs: true,
}

// Conservative conf that works but is long 
// const defaultConfig = {
//   limit: 2, 
//   interval: 2000, 
//   docsCount: 10, 
// }



export default async function cudUtils(appSearchFn, documents, config = {}) {
  const { limit, interval, docsCount, showLogs } = Object.assign({}, defaultConfig, config)

  const throttle = pThrottle({ limit, interval })
  if (showLogs) console.log('Processing configured with a docsCount =', docsCount, 'a limit =', limit, 'and an interval =', interval)

  // configure progress-bar
  let bar = undefined
  if (showLogs) {
    bar = new progress.SingleBar({}, progress.Presets.shades_classic)
  }


  if (showLogs) console.log('Start processing documents. Total documents length', documents.length)

  // create an array split
  const documentsLength = documents.length
  const documentChunks = chunkArray(documents, docsCount)
  if (showLogs) console.log(documentsLength, 'documents to be processed in', documentChunks.length, 'chunks')
  if (bar) bar.start(documentChunks.length, 0)


  const throttled = throttle(async documents => cudExec(documents, appSearchFn, bar))
  const tts = documentChunks.map(async documents => await throttled(documents))
  const results = await Promise.all(tts)


  if (bar) bar.stop()
  return results
}

async function cudExec(documents, appSearchFn, bar) {
  let response
  try {
    // response = await appSearchFn(documents)
    response = await waitAndRetry(0, 5, documents, appSearchFn)

  } catch (e) {
    console.log(chalk.bgRed('\n\nError during processing:'))
    throw e
  }

  // errors management
  if (Array.isArray(response)) {
    const errors = response.reduce((acc, resp) => {
      if (resp.errors) acc.push(...resp.errors)
      return acc
    }, [])
    if (errors.length) {
      console.log('\n')
      console.log('@TODO: manage this errors :', errors)
      // @TODO: do it more like a "service" that starts / end and be ping by errors 
      genericSaveCatcher(errors, documents)
    }
  }else {
    // @TODO: check for Errors in case of reponse as object
    // console.warn('@TODO: this is a case of response as object. Check for errors')
  }


  if (bar) bar.increment()
  return response
}

async function waitAndRetry(current, max, documents, appSearchFn) {
  // console.log(current, '===========', max)
  if (current > max) throw new Error(`\n\n\n ${max} EAI append for this request. Stop process here`)
  let response
  try {
    response = await appSearchFn(documents)
  } catch (e) {
    if (e.code === 'EAI_AGAIN') {
      console.log('\n==> EAI_AGAIN case')
      await new Promise(resolve => setTimeout(resolve, 1000))
      return await waitAndRetry(current + 1, max, documents, appSearchFn)
    }
    if (e.code === 'ECONNRESET') {
      console.log('\n==> Connexion reset case')
      await new Promise(resolve => setTimeout(resolve, 1000))
      return await waitAndRetry(current + 1, max, documents, appSearchFn)
    }
    if (e.code === 'ECONNREFUSED') {
      console.log('\n==> ECONNREFUSED')
      await new Promise(resolve => setTimeout(resolve, 10000))
      return await waitAndRetry(current + 1, max, documents, appSearchFn)
    }

    // last case: there is another error
    console.log(
      chalk.red('\n==> cudUtils: This is another case to manage. See error: \n'),
      e,
      'query-details',
      // e.meta.meta
    )
    // wait longer
    await new Promise(resolve => setTimeout(resolve, 10000))
    return await waitAndRetry(current + 1, max, documents, appSearchFn)
  }

  if (!response) {
    console.log(chalk.bgRed(`After ${max} retries, there is still an error`))
    throw new Error('Don\'t achieve the request.')
  }

  return response
}

