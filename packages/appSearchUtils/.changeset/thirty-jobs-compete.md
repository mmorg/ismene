---
"@mmorg/appsearch-utils": patch
---

Fix batchIdSearch for id that not exist in the engine. It now return an empty object instead of an Error
