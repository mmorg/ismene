---
"@mmorg/appsearch-utils": patch
---

Remove buggy cycle detection during the denorm phase
