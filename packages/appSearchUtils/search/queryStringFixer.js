import queryWordsRemover from './queryWordsRemover.js'


const maxLength = 128

// clean : multiple spaces and trim
const spaceCleaner = (query) => query.replace(/  +/g, ' ').trim()

// if length > appSearch max length, try to 'reduce' the competency
export default function queryStringFixer(query, debug = false) {
    const originalString = query

    query = spaceCleaner(query)

    // processings if string > MaxLength
    // step M1 : remove text in ( ... )
    if (query.length >= maxLength) {
        const regexp = /\(.*?\)/gi
        query = query.replace(regexp, '')
    }

    // step M2 : remove words
    if (query.length >= maxLength) {
        query = queryWordsRemover(query)
    }

    // spaceClean before others steps
    query = spaceCleaner(query)

    // step M3 : remove the last word(s)
    if (query.length >= maxLength) {

        while (query.length >= maxLength) {
            const lastIndex = query.lastIndexOf(" ");
            query = query.substring(0, lastIndex);
        }

        if(debug){
            console.warn(
                '------------',
                ' \n This string was shorten with last words removal. See if other remove can be done.',
                ' \n original string : ', originalString,
                ' \n changed string  : ', query,
                ' \n ------------',
            )
        }

    }

    if(query.length != originalString.length && debug){
        console.log(`===> String shortened to "${query}"
        source : ${originalString.slice(0,80)}...
        ` )
    }

    // before the return another spaceCleaner
    query = spaceCleaner(query)

    return query
}

