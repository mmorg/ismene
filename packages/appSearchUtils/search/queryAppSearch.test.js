import {jest} from '@jest/globals'
import elasticConf from '../../configurations/elasticConf.js.js'
import queryAppSearch from './queryAppSearch.js'

// docker-compose exec rml npm test -- --watch queryAppSearch.test

// run the tests on the "selected" endpoint
const {selected } = elasticConf

jest.setTimeout(10000)

describe('Test about the generator function of queryAppSearch', () => {

  const result_fields = {id: { raw: {} },}

  it('Parse the max doc size', async () => {
    const query = 'Administrateur réseau'
    const options = {
      page: { size: 5, current: 30 },
      result_fields,
    }
    const configuration = {
      maxDocuments: 2,
      firstMetaInfo: {}
    }

    let count = 0
    for await (const document of queryAppSearch(query, options, configuration,selected)) {
      count += 1
    }

    expect(count).toBe(2)

  })

  it('Browse pages until doc size', async () => {
    const query = 'Administrateur réseau'
    const options = {
      page: { size: 1, current: 1 },
      result_fields,
    }
    const configuration = {
      maxDocuments: 5,
      firstMetaInfo: {}
    }

    let count = 0
    for await (const document of queryAppSearch(query, options, configuration,selected)) {
      count += 1
    }

    expect(count).toBe(5)

  })

  it('Stop when the last page reached', async () => {
    const query = 'Administrateur réseau'
    const options = {
      page: { size: 100, current: 1 },
      result_fields,
    }
    const configuration = {
      maxDocuments: 1,
      firstMetaInfo: {}
    }

    // 1/ do a 1st query to get the dynamic page size
    const pageNumber = queryAppSearch(query, options, configuration,selected)
    await pageNumber.next()

    // get max number of pages
    const { firstMetaInfo: { page: { total_pages, total_results } } } = configuration
    if (total_pages > 100) console.error('options.page.size need to be adjusted to get less than 100 pages.')

    // define maxNumber of page we want to fetch
    const maxPages = 2
    // define starting page
    const startingPage = total_pages - maxPages

    // calculate the number of docs on the last page
    // (total_pages -1) = number of documents until the last page
    const lastPageDocs = total_results - ((total_pages - 1) * options.page.size)

    // define a maxDocument greater than documents we can retrive (effectiveDocuments)
    const effectiveDocuments = (options.page.size * maxPages) + lastPageDocs
    const maxDocuments = effectiveDocuments + 500

    // 2/ change the config for a new request at the end of the pages
    options.page.current = startingPage
    configuration.maxDocuments = maxDocuments

    let count = 0
    for await (const document of queryAppSearch(query, options, configuration)) {
      count += 1
    }

    // we will just have the results for the 2 last pages
    expect(count).toBe(effectiveDocuments)
  })

})

