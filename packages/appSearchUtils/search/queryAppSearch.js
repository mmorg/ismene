import chalk from 'chalk'
import getAppSearchClient from '../getAppSearchClient.js'

// this is scroll implementation for app-search
// for elastic search scroll see : `const scrollDocs = client.helpers.scrollSearch(searchParam)`
//      in : https://gitlab.com/mindmatcher.org/stats-addviseo/blob/d117d5d6be2c002744fe8a6e8bb2bc1f2b802683/gatsby/src/statsAksis/fetchers/competencesFetcher.js


// call it like :
/*
let count = 0
for await (const document of queryAppSearch(query, options, configuration)) {
  count += 1
}
*/
// or :
/*
const pageNumber = queryAppSearch(query, options, configuration)
await pageNumber.next()
*/

const debug = false
if (debug) console.warn('--> Debug function activated in queryAppSearch')

// caching
let client = null
let engineName = null

export default async function* queryAppSearch(query, bodyOptions, configuration = {}, engineConf) {

  let countDocs = 0

  const optionsClone = JSON.parse(JSON.stringify(bodyOptions))
  const localOptions = Object.assign({ page: { size: 51, current: 1 } }, optionsClone)

  let response = await makeSearchQuery(engineConf, query, localOptions)

  const { meta } = response
  configuration.firstMetaInfo = meta

  generatorLoop:
  while (true) {
    const { results, meta } = response

    for (const doc of results) {
      yield doc
      countDocs += 1
      if (configuration.maxDocuments && countDocs >= configuration.maxDocuments) break generatorLoop
    }

    // check if next page exist
    const { page: { current, total_pages } } = meta
    const nextPage = current + 1
    if (debug) console.log(meta)
    if (nextPage > total_pages) break

    // update option for the next page
    localOptions.page.current = nextPage
    response = await makeSearchQuery(engineConf, query, localOptions)
  }
}

async function makeSearchQuery(engineConf, query, bodyOptions) {

  if (!client || engineName !== engineConf.engineName) {
    client = getAppSearchClient(engineConf)
    engineName = engineConf.engineName
  }

  if (bodyOptions?.page.size && (bodyOptions.page.size < 1 || bodyOptions.page.size > 1000)) throw new Error('Invalid range for `options.page.size`. See: https://www.elastic.co/guide/en/app-search/current/search.html#search-common-malformed-api-requests')
  if (bodyOptions?.page.current && (bodyOptions.page.current < 1 || bodyOptions.page.current > 100)) throw new Error('Invalid range for `options.page.current`. See: https://www.elastic.co/guide/en/app-search/current/search.html#search-common-malformed-api-requests')

  return await strongSearch(0, 5, engineName, query, bodyOptions)
}

// search that fallback and retry on some kind of errors
async function strongSearch(current, max, engineName, query, options) {
  const retry = async (tm = 1000) => {
    await new Promise(resolve => setTimeout(resolve, tm))
    return await strongSearch(current + 1, max, engineName, query, options)
  }
  if (current > max) throw new Error(`\n\n\n ${max} EAI append for this request. Stop process here`)
  let response
  try {
    response = await client.search({
      engine_name: engineName,
      body: {
        query,
        ...options,
      }
    });

    // console.log('response', response)
  } catch (e) {
    if (e.code === 'EAI_AGAIN') {
      console.log('\n==> EAI_AGAIN case')
      return await retry()
    }
    if (e.code === 'ECONNRESET') {
      console.log('\n==> Connexion reset case')
      return await retry()
    }
    if (e.code === 'ECONNREFUSED') {
      console.log('\n==> ECONNREFUSED')
      return await retry(10000)
    }

    if(e.body.errors.length){
      // check on first one. Allow to manage fail fast
      const [err] = e.body.errors
      if(err.startsWith('Filters contains invalid key')) throw e
    }

    // last case: there is another error
    console.log(
      chalk.red('\n==> queryAppSearch: This is another case to manage. See error: \n'),
      e
    )
    // wait longer
    return await retry(10000)
  }

  if (!response) {
    console.log(chalk.bgRed(`After ${max} retries, there is still an error`))
    throw new Error('Don\'t achieve the request.')
  }

  return response
}
