
import queryStringFixer from './queryStringFixer'

// docker-compose exec rml npm test -- --watch queryStringFixer.test

describe('Fix the string length for too big strings', () => {

    it('dev tests ', async () => {

        const tests = [
            {
                source : "Sélectionner les outillages (racles, rouleaux, ...) et les cadres d'impression et déterminer les besoins en produits (couleurs, ...)",
                target : "Sélectionner les outillages et les cadres d'impression et déterminer les besoins en produits"
            },
            {
                source : "Recenser les données sur le territoire (spécificités culturelles, centres d'intérêt, ...) et déterminer les axes d'intervention socioculturelle",
                target : "Recenser les données sur le territoire et déterminer les axes d'intervention socioculturelle"
            },
            {
                source: "Surveiller des zones terrestres, maritimes, aériennes et orienter des interventions ou des opérations de recherche, d'assistance, de secours",
                target : 'Surveiller zones terrestres maritimes aériennes orienter interventions opérations recherche assistance secours'
            },
            {
                // length < 128 so no processing
                source : 'Organisation des structures sanitaires et sociales',
                target : 'Organisation des structures sanitaires et sociales'
            },
            {
                source: "Déployer les directives du ministère et des collectivités pour la protection et la valorisation des milieux naturels par réintroduction d'espèces, restauration de plages ...",
                target: 'Déployer directives ministère collectivités protection valorisation milieux naturels réintroduction espèces restauration plages'
            },
            {
                source: "Surveiller l'état de l'embarcation pendant la navigation, détecter les anomalies (avaries, ...) et en informer le maître d'équipage ou l'officier de quart",
                target : 'Surveiller état embarcation navigation détecter anomalies informer maître équipage officier quart'
            },
            {
                source: "Veiller au respect de la loi Informatique et Libertés et du RGPD dans l'entreprise, gérer la liste des traitements de données à caractère personnel, faire l'interface avec la Commission Nationale de l'Informatique et des Libertés - CNIL",
                target: 'Veiller respect loi Informatique Libertés RGPD entreprise liste traitements données caractère personnel interface Commission'
            }

        ]

        for(const test of tests){
            const r = queryStringFixer(test.source)
            expect(r.length).toBeLessThan(128)
            expect(r).toBe(test.target)
        }

    })

})

