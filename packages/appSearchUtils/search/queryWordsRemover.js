
const toRemove = [
  ' des ', ' de ', ' du ', ' d\'',
  ' les ', ' le ', ' la ', ' l\'',
  ' un ',
  ' et ',
  ' pour ', ' par ',
  ' en ', ' ou ', ' pendant ',
  ' dans ', ' aux ', ' au ', ' a ', ' à ',
  ' avec ',
  '\\.\\.\\.', // corresponding to '...'
  ', ',
  ' gérer ', ' faire ', ' vers ', ' auprès '

]

export default function queryWordsRemover(query) {
  toRemove.forEach(tr => {
    const r = new RegExp(tr, 'gi')
    query = query.replace(r, ' ')
  })
  return query
}
