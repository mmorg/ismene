import { toElasticPath } from '../utils/elasticPathSwitch.js'
import { toEmptyPower } from '../utils/emptyPower.js'
import stopMarker from '../utils/stopMarker.js'
import debugIsEntityEntry from './utils/debugIsEntityEntry.js'


export const EMPTY_CELL = '___EMPTY_CELL___'


// console.log('@TODO: make predicateToIRI automatic from ontology')
// ==> make an exception for `type` property (maybe some others ?)

const _predicateToIRI_ref = [
  {
    id: 'skos:broader',
    ldAlias: 'broader',
    label: [
      {
        '@language': 'en',
        '@value': 'has broader'
      }
    ],
  },
  {
    id: 'skos:narrower',
    ldAlias: 'narrower',
    label: [
      {
        '@language': 'en',
        '@value': 'has narrower'
      }
    ],
  },
  { // this is a "hard add" for hashes management
    id: 'mm:signatures',
    ldAlias: 'signatures',
    label: [
      {
        '@language': 'en',
        '@value': 'The added Signatures'
      }
    ],
  },
]
// const predicateToIRI = predicateToIRI_ref.reduce((acc, v) => { acc.add(v.ldAlias); return acc; }, new Set())

const predicateToDatatype_ref = [
  {
    id: 'skos:prefLabel',
    ldAlias: 'prefLabel',
  },
  {
    id: 'skos:altLabel',
    ldAlias: 'altLabel',
  },

]
const predicateToDatatype = predicateToDatatype_ref.reduce((acc, v) => { acc.add(v.ldAlias); return acc; }, new Set())

let entity_cache = null

// @TODO: make it optional and with bar
const _default_progress = 15000

export default function normForAppSearchWithDepth(
  entities,
  ld = { graph: [] },
  maxDepth = 2,
  predicateToIRI_ref = _predicateToIRI_ref,
  showLogs = true,
  compliantElasticFields = true,
  showProgress = false,
) {

  const predicateToIRI = predicateToIRI_ref.reduce((acc, v) => { acc.add(v.ldAlias); return acc; }, new Set())

  entity_cache = ld.graph.reduce((acc, e) => { acc.set(e.id, e); return acc }, new Map())

  // 1/ make the normalization with dot notation
  let entities_entries = normForAppSearchRecurse(1, entities, maxDepth, predicateToIRI, showLogs, [], showProgress)
  
  // 2/ transform keys to compliant elastic fields
  if (compliantElasticFields) entities_entries = entities_entries.map(
    entries => entries.map(([key, value]) => [toElasticPath(key), value])
  )

  // 3/ rebuilt the objects
  const normedDocs = entities_entries.map(Object.fromEntries)
  return normedDocs
}

let count = 0
let audit_perf = false
let debug_id = undefined //'gendj:e34f480ea2cf2a201a726cc3506f7d6b'
let debug_entity = false //true
let debug_entry = false //true
function normForAppSearchRecurse(
  depth,
  entities,
  maxDepth,
  predicateToIRI,
  showLogs = true,
  parentPredicate = [],
  showProgress = false,
) {

  if (depth > maxDepth) return []

  const normedDocsEntries = []

  for (const e of entities) {
    count += 1
    if (audit_perf) console.time()
    if (showProgress && count % _default_progress === 0) {
      console.log(count, 'Processed')
      console.timeStamp()
    }
    if (debug_entity) {
      if (e.id === debug_id) console.log('Entity_Source:', e)
    }

    // 1/ spread the properties & extract the call of recursion to a sub-function
    const normEntries = getNormedEntries(e, depth, maxDepth, predicateToIRI, parentPredicate, showLogs, showProgress)
    normedDocsEntries.push(normEntries)

    if (debug_entity) {
      // this is a special test for the last leaf. Implement filter by id for more advanced debug 
      if (debugIsEntityEntry(normEntries, 'narrower.narrower.id')) {
        console.log('Entity_Entries:', normEntries)
        // console.log('NormedDocsEntries :', normedDocsEntries[0])
      }
      if(normedDocsEntries[0].filter(([k,v]) => k === 'narrower.narrower.id')){
        console.log('NormedDocsEntries :', normedDocsEntries[0].filter(([k,v]) => k === 'narrower.narrower.id'))
        console.log('entity',e)
        console.log('normEntries', normedDocsEntries)
        console.log('====')
      }
    }
    
    
    if (audit_perf) console.timeEnd()
  }

  return normedDocsEntries
}

function getNormedEntries(e, depth, maxDepth, predicateToIRI, parentPredicate, showLogs, showProgress) {
  const lastDepth = depth === maxDepth
  const normEntries = []
  for (const [key, value] of Object.entries(e)) {
    if (audit_perf) console.timeLog()
    // Datatype 
    if (predicateToDatatype.has(key)) {
      normEntries.push(...getDatatypeEntries(parentPredicate, key, value))
      continue
    }

    // IRI : 2 cases: there are to unref or not 
    // 1/ not To Unref 
    if (lastDepth) {
      normEntries.push(...normIRIwithoutUnref(parentPredicate, key, value))
      // console.log('================ to the unref here !!!')
      continue
    }
    // else case, go to unref 
    if (predicateToIRI.has(key)) {
      const unrefed_entities = []

      for (const v of value) {
        // 1/ get the id
        let id = null
        if (typeof v === 'object') id = v.id
        if (typeof v === 'string') id = v

        if (!id) {
          if (showLogs) console.warn('No id found for this value:', v)
          continue
        }
        if (audit_perf) console.timeLog(undefined, '==> find before')
        // 2/ check if entity can be unref
        const unrefE = entity_cache.get(id)
        if (!unrefE) {
          if (showLogs) console.warn(id, 'for key', key, ': can\'t be uref, So it\'s discarded')
          throw new Error('@NotImplemented: create test for partial documents')
          continue
        }
        if (audit_perf) console.timeLog(undefined, '==> find after')
        unrefed_entities.push(unrefE)
      }

      // 3/ Norm the unrefed entities 
      const recurce_parentPredicate = [...parentPredicate, key]
      const unref_Docs_Entries = normForAppSearchRecurse(
        depth + 1, unrefed_entities,
        maxDepth, predicateToIRI,
        showLogs, recurce_parentPredicate,
        showProgress,
      )

      const concat_entities_entries = horizontalConcat(unref_Docs_Entries)

      // check a before/after horizontalConcat 
      let debug_level3 = undefined
      if (debug_entry) {
        if (debugIsEntityEntry(unref_Docs_Entries[0], 'narrower.narrower.id')) {
          debug_level3 = unref_Docs_Entries[0][0]
          console.log('Before Horizontal Concat:', debug_level3)
          console.log('After Horizontal Concat:', concat_entities_entries)
        }
      }

      // add child entries to the current entity
      normEntries.push(...concat_entities_entries)
      continue
    }

    // Others
    const parentPath = getParentPredicatePath(parentPredicate)
    normEntries.push([`${parentPath}${key}`, value])
  }
  return normEntries
}



//// Indirection for concatenation 



function horizontalConcat(entitiesEntries) {

  const debug = (key) => key === undefined // 'narrower.narrower.id'

  const keyIndexes = entitiesEntries.reduce((keyMap, entity_entries) => {

    // 1/ index all as array
    for (const entry of entity_entries) {
      const [key, value] = entry
      const _value = Array.isArray(value) ? value : [value]

      let found = keyMap.get(key)
      if (!found) {
        found = []
        keyMap.set(key, found)
      }
      found.push(_value)
    }

    return keyMap
  }, new Map())

  // 2/ add stop Marker after each object component
  const concact_entities_entries = []
  for (const [key, concat_values] of keyIndexes.entries()) {
    if (debug(key)){ 
      console.log(key, '\n', concat_values,'\n ========================')

    }
    const oneLineValue = []    
    // naive fix of 'endend by 2 marker' bug. The problem reside where horizontalConcat is called (maybe restructure the functions)
    // aggregate all the "object.property chunks" into one line 
    concat_values.forEach(values => {
      oneLineValue.push(...values)

      // is this horizontal property not already stopped by the marker ? 
      const lastItem = values.flat().slice(-1)[0]
      if(lastItem !== stopMarker)  oneLineValue.push(stopMarker)
    })

    concact_entities_entries.push([key, oneLineValue])
  }

  return concact_entities_entries
}

// @TODO: extract theses indirections ? They are others options for serialization

function horizontalConcatWithPower(entitiesEntries) {

  // 1/ Create an index & find the more lengthy array value 
  let max = 0

  const keyIndexes = entitiesEntries.reduce((keyMap, entity_entries) => {

    for (const entry of entity_entries) {
      const [key, value] = entry
      const _value = Array.isArray(value) ? value : [value]

      // 1/ check max value 
      if (_value.length > max) max = _value.length

      // 2/ create the key index
      let found = keyMap.get(key)
      if (!found) {
        found = []
        keyMap.set(key, found)
      }
      found.push(_value)
    }

    return keyMap

  }, new Map())

  const concact_entities_entries = [...keyIndexes.entries()].map(([key, concat_values]) => {
    // do the "fill blank concat" with emptyPower
    const filled_values = concat_values.map(value => toEmptyPower(value, max))
    return [key, filled_values.flat()]
  })

  return concact_entities_entries
}

function horizontalConcatWithFill(entitiesEntries) {

  // 1/ Create an index & find the more lengthy array value 
  let max = 0

  const keyIndexes = entitiesEntries.reduce((keyMap, entity_entries) => {

    for (const entry of entity_entries) {
      const [key, value] = entry
      const _value = Array.isArray(value) ? value : [value]

      // 1/ check max value 
      if (_value.length > max) max = _value.length

      // 2/ create the key index
      let found = keyMap.get(key)
      if (!found) {
        found = []
        keyMap.set(key, found)
      }
      found.push(_value)
    }

    return keyMap

  }, new Map())


  const concact_entities_entries = [...keyIndexes.entries()].map(([key, concat_values]) => {
    const filled_values = concat_values.map(value => {
      const object_like = {
        length: max,
        ...value
      }

      const filled = Array.from(object_like, (v) => v ? v : EMPTY_CELL)
      return filled
    })

    return [key, filled_values.flat()]
  })
  return concact_entities_entries
}

/////  @TODO: end of extract theses indirections ? 

function getParentPredicatePath(parentPredicate) {
  let ppp = parentPredicate.reduce((acc, pp) => {
    return acc === `` ? pp : `${acc}.${pp}`
  }, ``)

  if (ppp !== ``) ppp = `${ppp}.`
  return ppp
}

function getDatatypeEntries(parentPredicate, key, value) {
  const template = {
    //@TODO: add others datatype properties
    language: [],
    _value: [],
  }

  const { language, _value } = value.reduce((acc, datatype) => {
    const lang = datatype['@language']
    acc.language.push(lang)
    const val = datatype['@value']
    acc._value.push(val)
    return acc
  }, template)

  const parentPath = getParentPredicatePath(parentPredicate)
  const langEntry = [`${parentPath}${key}.language`, language]
  const valEntry = [`${parentPath}${key}.value`, _value]
  return [langEntry, valEntry]
}

function normIRIwithoutUnref(parentPredicate, predicate, value) {
  const parentPath = getParentPredicatePath(parentPredicate)
  return [[`${parentPath}${predicate}`, value]]
}