import { dirname } from 'path'
import { fileURLToPath } from 'url'
const __dirname = dirname(fileURLToPath(import.meta.url))
import { getComplete3Levels } from '../__fixtures__/testEntities.js'
import normForAppSearchWithDepth, { EMPTY_CELL } from './normForAppSearchWithDepth.js'
// @ts-ignore
import { readJson } from '@mmorg/fsutils'
import { getPowerString } from '../utils/emptyPower.js'
import stopMarker from '../utils/stopMarker.js'
// import rawBigSource from '../__fixtures__/rawBigSource.json' assert { type: `json` }

const rawBigSource = readJson(`${__dirname}/../__fixtures__/rawBigSource.json`)

// cd rdfx-graphql
// pnpm test -- --watch normForAppSearchWithDepth.test


const EMPTY_1 = getPowerString(1)
const EMPTY_2 = getPowerString(2)


it.todo('remove the ld2imatrice dependency ?')
describe('AppSearch normalisation in `one child to unref` case', () => {
  // test graph with 3 depth of unreferencing
  const c3ld = getComplete3Levels()

  // Examples on AppSearch flattening: https://www.elastic.co/guide/en/app-search/current/elasticsearch-engines.html#elasticsearch-engines-object-fields
  it('Flatten a 2 levels object tree', async () => {

    const unrefEntities = [c3ld.graph[0]]
    const r = normForAppSearchWithDepth(unrefEntities, c3ld, 2, undefined, false, false)
    const normEntity = r[0]
    // console.log(normEntity)

    expect(normEntity.id).toBe('gendj:050ad8d9a61484b5f48325aa31876c9f')
    expect(normEntity.type).toStrictEqual(['skos:Concept', 'gendj:Sector'])
    expect(normEntity[`prefLabel.language`]).toStrictEqual(['fr'])
    expect(normEntity[`prefLabel.value`]).toStrictEqual(['Sécurité, réseau et cloud'])

    // not dereferencable IRI are discarded
    expect(normEntity[`narrower.id`]).toStrictEqual([
      'gendj:3810407a71b2a6b3b51a64231aafd4c8', stopMarker, 
      'gendj:83aed3ceaee337d456cebfcc47a81e56', stopMarker,
    ])

    expect(normEntity[`narrower.prefLabel.value`]).toStrictEqual([
      'Cloud', stopMarker,
      'Cybersécurité', stopMarker,
    ])

    // the source `predicateToIRI` of an unref predicate should not be in the entity 
    expect(normEntity.narrower).toBeFalsy()

  })

  it('Flatten a 3 levels object tree', async () => {

    const unrefEntities = [c3ld.graph[0]]
    const r = normForAppSearchWithDepth(unrefEntities, c3ld, 3, undefined, false, false)
    const normEntity = r[0]

    // console.log(normEntity)

    // focus tests on the 3rd level 
    expect(normEntity[`narrower.narrower.id`]).toStrictEqual([
      'gendj:e34f480ea2cf2a201a726cc3506f7d6b',stopMarker,
    ])
    expect(normEntity[`narrower.narrower.prefLabel.value`]).toStrictEqual([
      'Architecte Cloud', stopMarker,
    ])

    expect(normEntity['narrower.narrower.broader']).toStrictEqual([
      'gendj:3810407a71b2a6b3b51a64231aafd4c8', stopMarker,
    ])

    // should not have a .broader
    expect(normEntity['broader']).toStrictEqual(undefined)

  })

  it('Return `elastic compliant` properties name by default', () => {
    const unrefEntities = [c3ld.graph[0]]
    const r = normForAppSearchWithDepth(unrefEntities, c3ld, 2, undefined, false)
    const normEntity = r[0]

    // works at the entity level
    expect(normEntity.id).toBe('gendj:050ad8d9a61484b5f48325aa31876c9f')
    expect(normEntity.pref_label__value).toStrictEqual(['Sécurité, réseau et cloud'])
    // works at the unref entity level
    expect(normEntity[`pref_label__language`]).toStrictEqual(['fr'])
  })

})

describe('AppSearch normalisation in `multiple children to unref` case', () => {
  // test graph with 3 depth of unreferencing
  const c3ld = getComplete3Levels()
  
  // Examples on AppSearch flattening: https://www.elastic.co/guide/en/app-search/current/elasticsearch-engines.html#elasticsearch-engines-object-fields
  it('Create predicate lines of the same size', async () => {
    const unrefEntities = [c3ld.graph[0]]
    const r = normForAppSearchWithDepth(unrefEntities, c3ld, 2, undefined, false, false)
    const normEntity = r[0]

    expect(normEntity.id).toBe('gendj:050ad8d9a61484b5f48325aa31876c9f')

    expect(normEntity[`narrower.id`]).toStrictEqual([
      'gendj:3810407a71b2a6b3b51a64231aafd4c8',
      stopMarker,
      'gendj:83aed3ceaee337d456cebfcc47a81e56',
      stopMarker,
    ])

    // console.log(normEntity)
    expect(normEntity[`narrower.narrower`]).toStrictEqual([
      'gendj:e34f480ea2cf2a201a726cc3506f7d6b',
      stopMarker,
    ])
  })

  it('Do not generate arrays of same size for all properties', () => {
    // config to process only matchWith property
    const predicateToIRI_ref = [
      {
        id: 'rdfx_ia:matchWith',
        ldAlias: 'matchWith',
      },
    ]
    const r = normForAppSearchWithDepth([rawBigSource[0]], { graph: rawBigSource }, 2, predicateToIRI_ref)
    expect(JSON.stringify(r).length < 10000).toBe(true)
    expect(r).toMatchSnapshot()
  })

})

