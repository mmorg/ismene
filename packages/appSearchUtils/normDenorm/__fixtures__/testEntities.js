
// This is a complete graph with 3 levels deep

export { getComplete3Levels }

const level_1 = {
  id: 'gendj:050ad8d9a61484b5f48325aa31876c9f',
  type: [
    'skos:Concept',
    'gendj:Sector'
  ],
  narrower: [
    'gendj:3810407a71b2a6b3b51a64231aafd4c8',
    'gendj:83aed3ceaee337d456cebfcc47a81e56',
  ],
  prefLabel: [
    {
      '@language': 'fr',
      '@value': 'Sécurité, réseau et cloud'
    }
  ],
  inScheme: [
    'gendj:scheme_for_Concept_generated'
  ]
}

const narrower_1_1 = {
  id: 'gendj:3810407a71b2a6b3b51a64231aafd4c8',
  type: [
    'skos:Concept',
    'gendj:Job'
  ],
  broader: [
    'gendj:050ad8d9a61484b5f48325aa31876c9f'
  ],
  narrower: [
    'gendj:e34f480ea2cf2a201a726cc3506f7d6b',
  ],
  prefLabel: [
    {
      '@language': 'fr',
      '@value': 'Cloud'
    }
  ],
  inScheme: [
    'gendj:scheme_for_Concept_generated'
  ]
}

const narrower_1_2 = {
  id: "gendj:83aed3ceaee337d456cebfcc47a81e56",
  type: [
    "skos:Concept",
    "gendj:Job"
  ],
  broader: [
    "gendj:050ad8d9a61484b5f48325aa31876c9f"
  ],
  prefLabel: [
    {
      "@language": "fr",
      "@value": "Cybersécurité"
    }
  ]
}


const narrower_1_1_1 = {
  id: 'gendj:e34f480ea2cf2a201a726cc3506f7d6b',
  type: [
    'skos:Concept',
    'gendj:Position'
  ],
  broader: [
    'gendj:3810407a71b2a6b3b51a64231aafd4c8'
  ],
  prefLabel: [
    {
      '@language': 'fr',
      '@value': 'Architecte Cloud'
    }
  ],
  inScheme: [
    'gendj:scheme_for_Concept_generated'
  ]
}

function getComplete3Levels() {

  const graph = [
    level_1,

    narrower_1_1,
    narrower_1_1_1,
    narrower_1_2,
  ]

  return {
    '@context': {
      id: '@id',
      'graph': {
        '@id': '@graph',
        '@container': '@set'
      },
      type: {
        '@id': '@type',
        '@container': '@set'
      },
      'skos': 'http://www.w3.org/2004/02/skos/core#',
      'gendj': 'https://gen.competencies.be/terms/digitalJobs/',
      inScheme: {
        '@id': 'skos:inScheme',
        '@type': '@id',
        '@container': '@set'
      },
      prefLabel: {
        '@id': 'skos:prefLabel',
        '@container': '@set'
      },
      'altLabel': {
        '@id': 'skos:altLabel',
        '@container': '@set'
      },
      narrower: {
        '@id': 'skos:narrower',
        '@type': '@id',
        '@container': '@set'
      },
      broader: {
        '@id': 'skos:broader',
        '@type': '@id',
        '@container': '@set'
      }
    },
    graph,
  }
}