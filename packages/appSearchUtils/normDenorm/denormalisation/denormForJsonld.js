import { EMPTY_CELL } from '../normalisation/normForAppSearchWithDepth.js'
import { toJsonPath } from '../utils/elasticPathSwitch.js'
import { isPowerString } from '../utils/emptyPower.js'
import stopMarker from '../utils/stopMarker.js'

const SEP_JSONPATH = '.'

export default function denormForJsonld(entities, removeElasticRawWrapper = true, keepDatatypeAt = true) {

  if (Array.isArray(entities)) return entities.map(e => denormEntity(e, removeElasticRawWrapper, keepDatatypeAt))
  return denormEntity(entities, removeElasticRawWrapper, keepDatatypeAt)
}

function denormEntity(entity, removeElasticRawWrapper, keepDatatypeAt) {
  if (!entity) return null;

  let entity_entries = null
  // 1/ remove raw wrapper
  if (removeElasticRawWrapper) {
    const { _meta, ...others } = entity
    const appSearch_entries = Object.entries(others)
    entity_entries = appSearch_entries.map(([k, v]) => [k, v.raw])

  } else {
    entity_entries = Object.entries(entity)
  }

  // 2/ get key as jsonPath
  const denorm_entity_entries = entity_entries.map(([key, value]) => [
    toJsonPath(key), Array.isArray(value) ? value : [value]
  ])
  // console.log("denorm_entity_entries", denorm_entity_entries, '~~~~~~')

  // 3/ do the denormalisation for each entity
  const parentIndexes = new Set()
  const denorm_entities = recurseDenormEntries(denorm_entity_entries, [], parentIndexes, keepDatatypeAt)
  // console.log('denormed entity', denorm_entities)
  return denorm_entities
}


const datatypeSpecialPredicates = ['language', 'value']
const isDatatypeJsonPredicate = (keyPath) => keyPath.length === 2 && (datatypeSpecialPredicates.includes(keyPath[1]))

const debug_Id = null //'gendj:050ad8d9a61484b5f48325aa31876c9f' // 'gendj:83aed3ceaee337d456cebfcc47a81e56'

const debug_recurse = false
let debug_recurse_id = 0

function recurseDenormEntries(entity, parentPredicate = [], parentIndexes, keepDatatypeAt) {

  const getEntityId = () => entity[0][1][0]

  debug_recurse_id += 1
  const fnID = `${debug_recurse_id}`
  if(debug_recurse) console.log(fnID, '/ START', '  EntityId:', getEntityId(), '\n  parentPath:', parentPredicate, '  parentIndexes:', parentIndexes)

  // 0/ Detect cycles with parent (inverse relation) and create a tag
  // if (parentIndexes.has(getEntityId())) {
  //   if(debug_recurse) console.log(fnID, '/ END - early ending for cycle case')
  //   return [`__Cycle-${getEntityId()}__`]
  // }
  // parentIndexes.add(getEntityId())

  // 1/ split the entries into the current_entity and the sub_entity
  const [current_entity_predicates, sub_entity_predicates] = entity.reduce((acc, [key, value]) => {
    const keyPath = key.split(SEP_JSONPATH)
    // simple case, just the predicate
    if (keyPath.length === 1) {
      acc[0].push([key, value])
      return acc
    }
    // special case of dataType predicates
    if (isDatatypeJsonPredicate(keyPath)) {
      acc[0].push([key, value])
      return acc
    }

    // else case : this is a sub_object property
    acc[1].push([key, value])

    return acc
  }, [[], []])

  // 2/ process the current entries
  const currentPredicateObject = {}
  let objectValues = null
  if (current_entity_predicates.length) {
    // 1/ "un-concat" the multiples entities entries
    const resultEntities_entries = unConcatEntries(current_entity_predicates, keepDatatypeAt);
    const entities_object_values = resultEntities_entries.map(Object.fromEntries)

    // @TODO: check why the previous one send array, and here just use the 1st one
    objectValues = entities_object_values[0]
  }

  // 3/ process the sub_entity entries
  const subEntityObject = {}
  if (sub_entity_predicates.length) {

    const parentPedicate_map = new Map()

    // 1/ split by predicate index
    for (const [key, value] of sub_entity_predicates) {
      const sepIndex = key.indexOf(SEP_JSONPATH)
      const parentPredicate = key.slice(0, sepIndex)
      const childJsonPath = key.slice(sepIndex + 1)

      let found = parentPedicate_map.get(parentPredicate)
      if (!found) {
        found = []
        parentPedicate_map.set(parentPredicate, found)
      }
      found.push([childJsonPath, value])
    }

    // get the entries split for entities by key
    const parentKeys = new Map()
    for (const [key, entries] of parentPedicate_map) {
      let entitiesSplits = []

      for (const entry of entries) {
        // console.log('ààààààààààààà', entry)
        // split the entries by values
        const [entryKey, entryValues] = entry

        // get the split by values
        const valueSplit = []
        let currentSplit = []
        for (const val of entryValues) {
          if (val === stopMarker) {
            valueSplit.push([...currentSplit])
            currentSplit = []
            continue // do not push the marker
          }
          currentSplit.push(val)
        }
        // add the last iteration if something in it
        if (currentSplit.length) valueSplit.push([...currentSplit])


        // console.warn(valueSplit)

        valueSplit.forEach((valSplit, i) => {
          let entity = entitiesSplits[i]
          if (!entity) {
            entity = []
            entitiesSplits[i] = entity
          }
          entity.push([entryKey, valSplit])
        })

      }
      parentKeys.set(key, entitiesSplits)
    }

    //
    for (const [parentKey, entities] of parentKeys) {
      const denormed = []
      for (const entries of entities) {

        const recursed = recurseDenormEntries(entries, [...parentPredicate, parentKey], parentIndexes, keepDatatypeAt)
        const _r = Array.isArray(recursed) ? recursed : [recursed]
        denormed.push(..._r)

      }
      Object.assign(subEntityObject, { [parentKey]: denormed })
    }

  }

  // final object
  let final_entity = Object.assign({}, objectValues, subEntityObject)

  if(debug_recurse) console.log(fnID, '/ END',
    // '\n predicates_values', objectValues,
    // '\n subEntity', subEntityObject,
    '\n final:', final_entity
  )
  return final_entity
}

function unConcatEntries(entity_predicate_entries, keepDatatypeAt) {

  const debug = false // idEntry[1][0] === 'gendj:3810407a71b2a6b3b51a64231aafd4c8' ? true : false

  if (debug) console.log('full slice:', entity_predicate_entries)
  // 2/ do the "object Level" split
  const object_entries = entity_predicate_entries.reduce((acc, [k, v]) => {
    let objectIndex = 0

    for (const value of v) {
      if (value === stopMarker) { // prepare to send the next one to another object
        objectIndex += 1
        continue
      }

      // assign the entry to the good Object (array of entries)
      let targetEntries = acc.get(objectIndex)
      if (!targetEntries) {
        targetEntries = []
        acc.set(objectIndex, targetEntries)
      }

      // get the good entry
      let targetEntry = targetEntries.find(([ktarget, v]) => ktarget === k)
      let targetValue = targetEntry ? targetEntry[1] ?? undefined : undefined
      if (!targetEntry) {
        targetValue = []
        targetEntry = [k, targetValue]
        targetEntries.push(targetEntry)
      }
      targetValue.push(value)
    }

    return acc
  }, new Map())

  if (debug) console.log('Map Result', object_entries.get(0))
  // 3/ do multiple object level value cleaning
  let entities_entries = [...object_entries.values()].map(entries => {
    return getCleanEntityEntries(entries, keepDatatypeAt)
  })

  if (debug) console.log('Entities_entries', entities_entries)

  return entities_entries
}

function getCleanEntityEntries(entries, keepDatatypeAt) {
  // a) Create the split of dataType_Entries and the others
  //   & make sure all values are array(s)
  const [datatype_entries, other_entries] = entries.reduce((acc, [k, v]) => {

    const _v = !Array.isArray(v) ? [v] : v

    // Slice datatypes and the others
    if (isDatatypeJsonPredicate(k.split(SEP_JSONPATH))) {
      acc[0].push([k, _v])
    } else {
      acc[1].push([k, _v])
    }

    return acc
  }, [[], []])


  // b) Denorm datatype entries
  const clean_datatype_entries_map = datatype_entries.reduce((acc, [k, v]) => {
    const keySplit = k.split(SEP_JSONPATH)

    v.forEach((val, index) => {
      let dataTypes = acc.get(keySplit[0])
      if (!dataTypes) {
        dataTypes = []
        acc.set(keySplit[0], dataTypes)
      }
      let dt = dataTypes[index]
      if (!dt) {
        dt = {}
        dataTypes.push(dt)
      }
      // @TODO: make it more generic for others dataTypes names
      const propertyName = keepDatatypeAt ? `@${keySplit[1]}` : keySplit[1]
      dt[propertyName] = val
    })
    return acc
  }, new Map())
  const clean_datatype_entries = [...clean_datatype_entries_map.entries()]


  // c) Denorm other entries
  const clean_other_entries = other_entries.map(([k, v]) => {
    if (k === 'id') return [k, v[0]]
    return [k, v]
  })

  return [
    ...clean_other_entries,
    ...clean_datatype_entries,
  ]
}

function getMaxLengthInValue(value) {
  const notEmptyCell = v => v != EMPTY_CELL
  const lastValueIndex = findLastIndex(value, notEmptyCell)
  const entityLength = value.length - lastValueIndex
  return entityLength
}

// @TODO: use the native 'lastIndexOf' when on node-18+
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/lastIndexOf
export function findLastIndex(array, predicate) {
  let l = array.length;
  while (l--) {
    if (predicate(array[l], l, array))
      return l;
  }
  return -1;
}
