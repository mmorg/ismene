import normForAppSearchWithDepth from '../normalisation/normForAppSearchWithDepth.js'
import { getComplete3Levels } from '../__fixtures__/testEntities.js'
import denormForJsonld from './denormForJsonld.js'
// @ts-ignore
import elastic_result from '../__fixtures__/result-from-appSearch.json'
import as_same_sibbling from '../__fixtures__/as-results-same-sibbling.json'
// pnpm test -- --watch denormForJsonld.test




describe('Denorm outputs from `normForAppSearchWithDepth`', () => {
  describe('On the complete 3 levels ld', () => {
    const c3ld = getComplete3Levels()

    // build the target entity with denormed childrens 
    const [level_1, narrower_1_1, narrower_1_1_1, narrower_1_2,] = c3ld.graph
    const getSub_entityTarget = (level) => {
      // @ts-ignore
      const target = { ...level_1 }
      let _narrower_1_1 = {...narrower_1_1}
      if(level === 3) {
        // @ts-ignore
        _narrower_1_1.narrower = narrower_1_1_1
      }
      // @ts-ignore
      target.narrower = [_narrower_1_1, narrower_1_2]
      return target
    }

    // build the target entity 
    // @ts-ignore
    const target_level_2 = getSub_entityTarget()

    const unrefEntities = [c3ld.graph[0]]
    // console.log(unrefEntities[0])
    it('Denorm on the first 2 levels', () => {
      const normed_entities = normForAppSearchWithDepth(unrefEntities, c3ld, 2, undefined, false)
      const denormed = denormForJsonld(normed_entities, false, true)
      // console.log(denormed)
      // it works correctly for a sub-entity
      // @ts-ignore
      const target_sub = target_level_2.narrower[0]
      const denormed_sub = denormed[0].narrower[0]
      expect(denormed_sub).toStrictEqual(target_sub)

      // it works for all the tree
      expect(denormed[0]).toStrictEqual(target_level_2)
    })

    it('Denorm on 3 levels', () => {
      const normed_entities = normForAppSearchWithDepth(unrefEntities, c3ld, 3, undefined, false)
      const denormed = denormForJsonld(normed_entities, false, true)
      
      const [level_1,
        narrower_1_1,
        narrower_1_1_1,
        narrower_1_2,
      ] = c3ld.graph

      // 1st level properties checks
      const d = denormed[0]
      expect(d.broader).toBe(undefined)
      expect(d.id).toBe(level_1.id)
      expect(d.type).toStrictEqual(level_1.type)
      expect(d.narrower.length).toBe(2)
      
      
      const [d_1_1, d_1_2] = d.narrower
      // 2nd level 1 : first one with 3rd sub-level
      expect(d_1_1.id).toBe(narrower_1_1.id)
  
      // 3nd level :
      const d_1_1_1 = d_1_1.narrower[0]
      expect(d_1_1_1).toStrictEqual(narrower_1_1_1)

      // 2nd level 2 
      expect(d_1_2).toMatchSnapshot()
    })

  })
})

describe('It Denormalize from elastic source values', () => {

  it('Do not add `Cycle` notation for properties at the same level with same normed content', async () => {
    
    const denormed = denormForJsonld(as_same_sibbling.results, undefined, true) //, false, true)
    const [entity] = denormed
    // denormed property 1
    expect(entity.exactMatch[0].id).toBe('occupation:76bded28-876a-40b2-9efb-3123c8f14cf6')
    // denormed property 2
    expect(entity.matchWith[0]).not.toBe('__Cycle-occupation:76bded28-876a-40b2-9efb-3123c8f14cf6__')
    expect(entity.matchWith[0].id).toBe('occupation:76bded28-876a-40b2-9efb-3123c8f14cf6')
    
  })
})


