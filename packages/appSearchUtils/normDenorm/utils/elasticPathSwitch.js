// rules for transformation, from most specific to more generic
// ___ (three underscores): replace an existing `_` in the predicate name
// __ (two underscores) : replace the dot notation (`.`) in a predicate path 
// _ (one underscore): replace an uppercase letter L with a `_l` lowerCase letter
export const SEP_UNDERSCORE = '___'
export const SEP_PATH = '__' // '.' is only valid in elastic "internals"
export const SEP_UPPER = '_'

const SEP_UNDERSCORE_R = '(?<!_)_{3}(?!_)'
const SEP_PATH_R = '(?<!_)_{2}(?!_)' // @(?!gin) --> Lookahead negative assertion: Matches '@' only if '@' is not followed by 'gin'.
const SEP_UPPER_R = '(?<!_)_[a-z]' // (?<!gin)@ --> Lookbehind negative assertion: Matches '@' only if 'gin' is not followed by '@'.

// _R for Regexp
const SOURCE_UNDERSCORE_R = '_'
const SOURCE_PATH_R = '\\.' // double escape in string for 'new RegExp'
const SOURCE_UPPER_R = '[A-Z]'

// _V for source string Value
const SOURCE_UNDERSCORE_V = '_'
const SOURCE_PATH_V = '.'
const SOURCE_UPPER_V = s => s.replace('_', '').toUpperCase()


// Go from more specific (big) to more generic (little)
export function toElasticPath(jsonPath) {
  return jsonPath.replace(new RegExp(SOURCE_UNDERSCORE_R, 'g'), SEP_UNDERSCORE)
    .replace(new RegExp(SOURCE_PATH_R, 'g'), SEP_PATH)
    .replace(new RegExp(SOURCE_UPPER_R, 'g'), s => SEP_UPPER + s.toLowerCase())
}

// Go from more generic (little) to more specific (big)
export function toJsonPath(elasticPath) {
  return elasticPath.replace(new RegExp(SEP_UPPER_R, 'g'), SOURCE_UPPER_V)
    .replace(new RegExp(SEP_PATH_R, 'g'), SOURCE_PATH_V)
    .replace(new RegExp(SEP_UNDERSCORE_R, 'g'), SOURCE_UNDERSCORE_V)
}