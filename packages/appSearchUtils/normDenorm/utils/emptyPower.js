
const powerPrefix = '___EMPTY-CELL-'
const powerSuffix = '___'

const regexp = new RegExp(`${powerPrefix}(\\d*)${powerSuffix}`, 'igm')
const getPowerNumber = (empty) => +regexp.exec(empty)[1] // get the 1st group as number


export function toEmptyPower(value, maxLength) {
  const power = maxLength - value.length
  const valueWithEmpty = [...value]
  if (power) valueWithEmpty.push(getPowerString(power))
  return valueWithEmpty
}

export function fromEmptyPower() {

  const emptyPower = '___EMPTY-CELL-6___'
  console.log('<=', getPowerNumber(emptyPower))
}

export function isPowerString(str) { return regexp.test(str) }

export function getPowerString(power) { return `${powerPrefix}${power}${powerSuffix}` }