import {toElasticPath, toJsonPath} from './elasticPathSwitch.js'

// npm test -- --watch elasticPathSwitch.test


const j2e = toElasticPath
const e2j = toJsonPath

describe('`jsonPath` tranformation to `elasticPath`', () => {
  it('Do the standard transformations', () => {
    const tests = [
      ['', ''],
      ['prefLabel', 'pref_label'],
      ['prefLabel.language', 'pref_label__language'],
      ['narrower.test_with_underscore', 'narrower__test___with___underscore'],
    ]
    tests.forEach(([source,target])=> expect(j2e(source)).toBe(target))
    
  })
})

describe('`elasticPath` tranformation to `jsonPath`', () => {
  it('Do the standard transformations', () => {
    const tests = [
      ['', ''],
      ['pref_label', 'prefLabel' ],
      ['pref_label__language', 'prefLabel.language'],
      ['narrower__test___with___underscore', 'narrower.test_with_underscore']
    ]
    tests.forEach(([source,target])=> expect(e2j(source)).toBe(target))
    
  })
})
