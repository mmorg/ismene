import getAppSearchClient from '../getAppSearchClient.js'

const defaultSize = 10 // [1,20] limits

const defaultBody = {
  query: '',
  size: defaultSize,
  types: { // is this required with relevance configuration ? In doub, keep it like in docs
    documents: {
      fields: [
        "pref_label__value",
        "alt_label__value"
      ]
    }
  },

}

export default async function autocomplete(queryConf, confElastic) {
  const { engineName } = confElastic
  const client = getAppSearchClient(confElastic)

  const body = Object.assign({}, defaultBody, queryConf)
  if(!body.size || body.size > 20) throw new Error(`size parameter should be in [1,20]. Actual value is ${body.size}`)

  // documentation: https://www.elastic.co/guide/en/app-search/current/query-suggestion.html
  const response = await client.querySuggestion({
    engine_name: engineName,
    body,
  })

  return response.results.documents.map( d => d.suggestion)

}