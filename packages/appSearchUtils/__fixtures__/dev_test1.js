
export default [
  {
    id: '6d49484f-6923-4f6d-9bd7-4d03d0f34b54',
    title: 'Titre professionnel Développeur web et web mobile',
    coordinates: '47.282002, -2.211694',
    price: 9240,
    objectif_formation: `La formation vise l'acquisition des blocs de compétences de la certification ""Titre professionnel Développeur web et web mobile"" (du Ministère de l'Emploi, de niveau 5 - Bac+2) : CCP 1 - ""DEVELOPPER LA PARTIE FRONT-END D'UNE APPLICATION WEB OU WEB MOBILE EN INTEGRANT LES RECOMMANDATIONS DE SECURITE"" CCP 2 - ""DEVELOPPER LA PARTIE BACK-END D'UNE APPLICATION WEB OU WEB MOBILE EN INTEGRANT LES RECOMMANDATIONS DE SECURITE""`,
    resultats_attendus: 'Pour obtenir le titre professionnel, le candidat doit réussir les épreuves de certification : mise en situation professionnelle, contrôle continu et résultats de formation, dossier professionnel et entretien devant jury.',
    contenu_formation: "A l'issue de la formation, le candidat est capable de : Maquetter une application Réaliser une interface utilisateur web statique et adaptable Développer une interface utilisateur web dynamique Réaliser une interface utilisateur avec une solution de gestion de contenu ou e-commerce Créer une base de données Développer les composants d'accès aux données Développer la partie back-end d'une application web ou web mobile Élaborer et mettre en ¿uvre des composants dans une application de gestion de contenu ou e-commerce Les notions et outils abordés en formation : MÉTHODOLOGIE (gestion de projet ; méthode Scrum Agile) LANGAGE WEB (HTML / CSS ; Saas ; Boostrap ; JavaScript / JQuery ; Ajax / PHP / MySQL) CONTENT MANAGEMENT SYSTEM (CMS) (Wordpress ; Prestashop ; Joomla) CONCEPTION WEB (Photoshop ; Illustrator orienté web)",
    code_niveau_entree: '-1) Aucun niveau indiqué',
    code_niveau_sortie: '6) niveau III (BTS, DUT)',
    niveau_entree_obligatoire: '-1) Aucun niveau indiqué',
    modalites_enseignement: '0) formation entièrement présentielle',
    startdates: [ '2022-03-21T00:00:00.000Z', undefined ],
    enddates: [ '2022-10-10T00:00:00.000Z', undefined ],
    months_duration: [ 7 ],
    bac_level_out: [ 'Bac+2' ],
    accepted_by: 'Ajout niv.Entrée = `Bac+2` => _Bac+2_ avec durée moyenne inférieure à 8 mois',
    bac_level_in: [ 'Bac+2' ]
  },
  {
    id: '29332131-4294-4b40-ad4e-b74177449972',
    title: 'Titre professionnel Développeur web et web mobile',
    coordinates: '46.677402, -1.437341',
    price: 9240,
    objectif_formation: `La formation vise l'acquisition des blocs de compétences de la certification ""Titre professionnel Développeur web et web mobile"" (du Ministère de l'Emploi, de niveau 5 - Bac+2) : CCP 1 - ""DEVELOPPER LA PARTIE FRONT-END D'UNE APPLICATION WEB OU WEB MOBILE EN INTEGRANT LES RECOMMANDATIONS DE SECURITE"" CCP 2 - ""DEVELOPPER LA PARTIE BACK-END D'UNE APPLICATION WEB OU WEB MOBILE EN INTEGRANT LES RECOMMANDATIONS DE SECURITE""`,
    resultats_attendus: 'Pour obtenir le titre professionnel, le candidat doit réussir les épreuves de certification : mise en situation professionnelle, contrôle continu et résultats de formation, dossier professionnel et entretien devant jury.',
    contenu_formation: "A l'issue de la formation, le candidat est capable de : Maquetter une application Réaliser une interface utilisateur web statique et adaptable Développer une interface utilisateur web dynamique Réaliser une interface utilisateur avec une solution de gestion de contenu ou e-commerce Créer une base de données Développer les composants d'accès aux données Développer la partie back-end d'une application web ou web mobile Élaborer et mettre en ¿uvre des composants dans une application de gestion de contenu ou e-commerce Les notions et outils abordés en formation : MÉTHODOLOGIE (gestion de projet ; méthode Scrum Agile) LANGAGE WEB (HTML / CSS ; Saas ; Boostrap ; JavaScript / JQuery ; Ajax / PHP / MySQL) CONTENT MANAGEMENT SYSTEM (CMS) (Wordpress ; Prestashop ; Joomla) CONCEPTION WEB (Photoshop ; Illustrator orienté web)",
    code_niveau_entree: '-1) Aucun niveau indiqué',
    code_niveau_sortie: '6) niveau III (BTS, DUT)',
    niveau_entree_obligatoire: '-1) Aucun niveau indiqué',
    modalites_enseignement: '0) formation entièrement présentielle',
    startdates: [ '2022-06-07T00:00:00.000Z', undefined ],
    enddates: [ '2022-12-09T00:00:00.000Z', undefined ],
    months_duration: [ 7 ],
    bac_level_out: [ 'Bac+2' ],
    accepted_by: 'Ajout niv.Entrée = `Bac+2` => _Bac+2_ avec durée moyenne inférieure à 8 mois',
    bac_level_in: [ 'Bac+2' ]
  },

]
