## 1.4.0

## 1.7.0

### Minor Changes

- fix error on empty & improve search filters

### Patch Changes

- Remove buggy cycle detection during the denorm phase
- fix engine cache invalidation
- Fix batchIdSearch for id that not exist in the engine. It now return an empty object instead of an Error

## 1.6.0

### Minor Changes

- fix error on empty & improve search filters

### Patch Changes

- Remove buggy cycle detection during the denorm phase
- Fix batchIdSearch for id that not exist in the engine. It now return an empty object instead of an Error

## 1.5.0

### Minor Changes

- fix error on empty & improve search filters

### Patch Changes

- Fix batchIdSearch for id that not exist in the engine. It now return an empty object instead of an Error

### Minor Changes

- fix error on empty & improve search filters

## 1.3.1

### Patch Changes

- publish test

## 1.3.1

### Patch Changes

- validate npm publish conf

## 1.3.0

### Minor Changes

- New normDenorm algorithme

## 1.2.0

### Minor Changes

- feat: add `autocomplete` & `batchIdsSearch` functions. Improve configurations.

## 1.1.3

### Patch Changes

- typo

## 1.1.2

### Patch Changes

- remove logs

## 1.1.1

### Patch Changes

- publish fix

## 1.1.0

### Minor Changes

- move denormalisation processing from array to entity or array

## 1.0.4

### Patch Changes

- missing to-be-published dep

## 1.0.3

### Patch Changes

- missing dep

## 1.0.2

### Patch Changes

- old dependency clean

### Bug Fixes

- internal function params ([67b7a00](https://gitlab.com/mmorg/ismene/commit/67b7a003ec9db6379752ffb61e8a0df4fb612211))

## [1.0.1](https://gitlab.com/mmorg/ismene/compare/v1.0.0...v1.0.1) (2023-03-10)

### Bug Fixes

- internal function params ([4bb49a5](https://gitlab.com/mmorg/ismene/commit/4bb49a5fb4f42cbd60788f5a1307a5a144588cc4))

# 1.0.0 (2023-03-10)

### Bug Fixes

- activate po & create a dev folder for input data ([82efb4b](https://gitlab.com/mmorg/ismene/commit/82efb4b22269322eced93310d00f1c67be53e022))
- add publish ([eb791b0](https://gitlab.com/mmorg/ismene/commit/eb791b07877cc6b88c22981bce39296f4f0e773a))
- Add skos in final document ([97d71a0](https://gitlab.com/mmorg/ismene/commit/97d71a0fa206ab5153c19e2cfc34a1aecba2dead))
- auto-app client only loading ([76861f4](https://gitlab.com/mmorg/ismene/commit/76861f4753faf4467fab7a13c7ade98f3dd5526a))
- better naming ([42a93e6](https://gitlab.com/mmorg/ismene/commit/42a93e6986ac00f42e543a48faf13ab61821b59a))
- better post processing ([cb51d82](https://gitlab.com/mmorg/ismene/commit/cb51d82aea3b69a8233ec7adc1f0d5fc3405106c))
- dependency ([569442a](https://gitlab.com/mmorg/ismene/commit/569442a037f50a762085436c1176a3aa1afca92f))
- docker files ([6a8a282](https://gitlab.com/mmorg/ismene/commit/6a8a28221d4e4fa0888fafb5597ca4643d5743ac))
- docker-build & integration of command ([9bcfa72](https://gitlab.com/mmorg/ismene/commit/9bcfa7293f9036b95d5c90aece44318f932561fe))
- gatsby files ([c23314f](https://gitlab.com/mmorg/ismene/commit/c23314f8d28e4997d705a92ffc5cc3cb089a292d))
- improve graphLayer feature ([54cf35e](https://gitlab.com/mmorg/ismene/commit/54cf35e82e8ac2a1b7009e1f4d320a1ea6f02c92))
- improve implementation of rdfx-layers ([791e0f3](https://gitlab.com/mmorg/ismene/commit/791e0f37e53db5c79bf87cf65027b3cf8685b9a6))
- improve of fn & change texts ([4428d63](https://gitlab.com/mmorg/ismene/commit/4428d63aff665f4ab7107def3319d257d2dcaf3a))
- langString in ([bda64ef](https://gitlab.com/mmorg/ismene/commit/bda64ef6dcccf6883730d9047fdcd5a911fb63cf))
- make in-place and browser app editing okay ([0107358](https://gitlab.com/mmorg/ismene/commit/0107358eedff6cb2824e802c1e06fcf27141d26e))
- mapping typos [@bart](https://gitlab.com/bart) : have a look ! ([32389ac](https://gitlab.com/mmorg/ismene/commit/32389ac3d2a4913521d18c2d3d99f50c789bc2aa))
- merging branches ([1dddafa](https://gitlab.com/mmorg/ismene/commit/1dddafa69f191d5c93b1822ac54c73827fd1ab9c))
- new generation ([7e3ff9d](https://gitlab.com/mmorg/ismene/commit/7e3ff9d32854ba14efb8d9d40506dbbd347a5857))
- new structure ([6530ba5](https://gitlab.com/mmorg/ismene/commit/6530ba5e341999f3efcb93a66422a093d44740fc))
- owl:ontology ([e36e359](https://gitlab.com/mmorg/ismene/commit/e36e35911a61caf6f46a5d1db8d731276feee67e))
- refine display ([4f7d456](https://gitlab.com/mmorg/ismene/commit/4f7d456feed13952378d093faf30420a3cf5e90b))
- remove old code ([9fb9d7c](https://gitlab.com/mmorg/ismene/commit/9fb9d7c0541c962718daa991c605170f0a9a6840))
- remove some props ([eee69d0](https://gitlab.com/mmorg/ismene/commit/eee69d00fef37d80b426d1b9b349a7a483d9b7dc))
- resolve bugs in layers exec and generators ([c38e925](https://gitlab.com/mmorg/ismene/commit/c38e925a90085136e9bb1d0d4b510e6d3a986606))
- solve little things ([674dade](https://gitlab.com/mmorg/ismene/commit/674dade5bd728a8182a2ea7c64b25d33fee3fca7))
- test after file path change ([0005e26](https://gitlab.com/mmorg/ismene/commit/0005e265f7f81eca7aa4d0b432b8d72ca71b0f5f))
- tests works again ([03465e9](https://gitlab.com/mmorg/ismene/commit/03465e9f2d298f1fa592a602c6b81a915d8c1f42))
- update version number ([0f86e62](https://gitlab.com/mmorg/ismene/commit/0f86e62783a60e1382217169d9863d8b404157ad))

### Features

- 1st implementation of refine integration ([c7a9272](https://gitlab.com/mmorg/ismene/commit/c7a927248fd8eb40117ea211cdba2fddb10caed0))
- add api first draft & rml processing library integration ([2750837](https://gitlab.com/mmorg/ismene/commit/27508374cac7fc7f3771854f57c9d3360e19b6b6))
- add Collectoin display page ([73c64b1](https://gitlab.com/mmorg/ismene/commit/73c64b102f04f74a263c38210c4b09a7f4e8f8ee))
- add complete file generation function ([932cbb9](https://gitlab.com/mmorg/ismene/commit/932cbb905ea56571c9143c8f7dc2517816043754))
- add configurable url endpoint ([0a6d87b](https://gitlab.com/mmorg/ismene/commit/0a6d87b595e874c1c13cd75d38b29e5a177c6e75))
- add context lighter ([10145b5](https://gitlab.com/mmorg/ismene/commit/10145b5811cdc0885bff3f6133f4f91ea631b8e6))
- add contribution images and update of Paper to Card ([1e439c2](https://gitlab.com/mmorg/ismene/commit/1e439c2d0c9c797c069964ea3becfd44c15cad58))
- add dataCube ([0f892d0](https://gitlab.com/mmorg/ismene/commit/0f892d088975968b3be678b7b35fd7fdfc83e455))
- add deviationLayer Generation & update Skos ([dbc8883](https://gitlab.com/mmorg/ismene/commit/dbc8883400a55c052da04c6ba7d79a03adbe013c))
- add deviationType to continue to test ([0471d61](https://gitlab.com/mmorg/ismene/commit/0471d61ea16953ed30f2ce1209752c1718615c59))
- add explicitInherit layer generation ([ab075ed](https://gitlab.com/mmorg/ismene/commit/ab075eda523e15ff10f476dfdc21576ed0c0873b))
- add export of compacted layers ([e4f9a85](https://gitlab.com/mmorg/ismene/commit/e4f9a855b83df26ec1f5761d7f270ad0c7b3ea65))
- add first version of contribution logos management ([10a9ff2](https://gitlab.com/mmorg/ismene/commit/10a9ff2c115d1d5e29ed4eb0e23c447a28163149))
- add global editor sub-app ([80153a0](https://gitlab.com/mmorg/ismene/commit/80153a0adfb8a7058a8f776933cf57a059ea0abc))
- add graphLayers generators execution and Layers save ([c1b65d9](https://gitlab.com/mmorg/ismene/commit/c1b65d95254edc6bcbe1e97d2486326ee13383aa))
- add jest conf for EMS ([9cf7469](https://gitlab.com/mmorg/ismene/commit/9cf74699d48f5c5f5fed710cea090a0134985183))
- add layers save ([db71ff1](https://gitlab.com/mmorg/ismene/commit/db71ff19edd4424740572b6409178f9d115b6341))
- add menu for conceptContainers ([66e8f03](https://gitlab.com/mmorg/ismene/commit/66e8f03e9f4ea8dd4026f973566ac9737ce8ebf6))
- add MM ontology ([98e59c3](https://gitlab.com/mmorg/ismene/commit/98e59c32edb2ac75d031142ebec76f1fcb66d2a6))
- add new header ([c18afe4](https://gitlab.com/mmorg/ismene/commit/c18afe4e4be32555896e3240aa6d638a01150540))
- add new header ([c163ed1](https://gitlab.com/mmorg/ismene/commit/c163ed1c607705193ec315128b56fc138ee87722))
- add new ontologies ([b840bf6](https://gitlab.com/mmorg/ismene/commit/b840bf6d7a76dc29e27d5ae81aed86b3441adcc7))
- add Onto-Terms Translators throw Visons API ([5a96408](https://gitlab.com/mmorg/ismene/commit/5a96408840b3eb2415bf602c22e93fcff71141b1))
- add openBadges std context ([9c8a340](https://gitlab.com/mmorg/ismene/commit/9c8a3409c9937664d5d4d1ef40efa6c0a8a6cb8d))
- add organisation entities generation ([9a8bb1b](https://gitlab.com/mmorg/ismene/commit/9a8bb1b6d9f20fcfd7e2e07124b2588b21f86fc9))
- add processing to apiPF ([6590447](https://gitlab.com/mmorg/ismene/commit/6590447e8e1b51e7b2937e6872896b51057e825b))
- add rdf-graph serveur ([97653e7](https://gitlab.com/mmorg/ismene/commit/97653e785734d01e781b14eda86b3128d52fcf8a))
- add rdfs ([82fce6f](https://gitlab.com/mmorg/ismene/commit/82fce6f74bb65d5403467a0859acf542bb18eefc))
- add shacl model ([9b3c16d](https://gitlab.com/mmorg/ismene/commit/9b3c16dc6731cb240d5d0f44feb9189f7224fbbf))
- add skos ontology ([a3d5d38](https://gitlab.com/mmorg/ismene/commit/a3d5d3848cfc2eb69c409d60d5a1b1e44de35cfa))
- add Test on pre-push ([6c29d10](https://gitlab.com/mmorg/ismene/commit/6c29d1013f2ab3199ee9b4ffd2784f2ee7dcb8e5))
- add updateEntity with strategies ([b2e24dd](https://gitlab.com/mmorg/ismene/commit/b2e24ddafb3be40501d2ac2562ae8f7da5ddb6aa))
- add vision interop ([c403337](https://gitlab.com/mmorg/ismene/commit/c403337d4b8d98cc8c52a1ee7d1df4407402faaa))
- add xml to json-ld for skos ([9ae5bef](https://gitlab.com/mmorg/ismene/commit/9ae5befd1e528f78c14f8275608e55cf39d0d7bf))
- first integration of Oriane ontology ([4d3f2b6](https://gitlab.com/mmorg/ismene/commit/4d3f2b626cfeb6de0d4647100f4a4a95081be7b8))
- implementation of sch:Organisations generation ([12361fc](https://gitlab.com/mmorg/ismene/commit/12361fc95c838f820657f93ede338fc396b81022))
- import openEmploi & mnx ontologies ([ec617e5](https://gitlab.com/mmorg/ismene/commit/ec617e5b70c2c177b488a0d34e52232e4bbded09))
- initial commit ([0287d16](https://gitlab.com/mmorg/ismene/commit/0287d169b8de18a75442062f04b1725c54b755ed))
- multi-ontology menu display ([541ccf6](https://gitlab.com/mmorg/ismene/commit/541ccf61c12e8705da4c4513ef526053f6ceb7d2))
- new clean gen_v1 export ([43dfc98](https://gitlab.com/mmorg/ismene/commit/43dfc98b4d73d08c52dcf098322d6c1ee479b37a))
- new version ([67a5374](https://gitlab.com/mmorg/ismene/commit/67a537490fae13fbd00bd44465205cecd0e1ae59))
- publish a the first version ([76935b1](https://gitlab.com/mmorg/ismene/commit/76935b14533ce38f3327631bc62828b875feceb4))
- start add ts+esm support ([2ca8e65](https://gitlab.com/mmorg/ismene/commit/2ca8e658266e52296ce8b7c12f4fd2e14a502b32))
- start gl implementation ([70dd085](https://gitlab.com/mmorg/ismene/commit/70dd085c8ad5977c91e7d1f5bff661a2ac4dfdeb))
- working intermediate version ([20cbff8](https://gitlab.com/mmorg/ismene/commit/20cbff802ffedde52f19f8d435d0635172c3d9af))

# 1.0.0 (2023-03-10)

### Bug Fixes

- activate po & create a dev folder for input data ([82efb4b](https://gitlab.com/mmorg/ismene/commit/82efb4b22269322eced93310d00f1c67be53e022))
- add publish ([eb791b0](https://gitlab.com/mmorg/ismene/commit/eb791b07877cc6b88c22981bce39296f4f0e773a))
- Add skos in final document ([97d71a0](https://gitlab.com/mmorg/ismene/commit/97d71a0fa206ab5153c19e2cfc34a1aecba2dead))
- auto-app client only loading ([76861f4](https://gitlab.com/mmorg/ismene/commit/76861f4753faf4467fab7a13c7ade98f3dd5526a))
- better naming ([42a93e6](https://gitlab.com/mmorg/ismene/commit/42a93e6986ac00f42e543a48faf13ab61821b59a))
- better post processing ([cb51d82](https://gitlab.com/mmorg/ismene/commit/cb51d82aea3b69a8233ec7adc1f0d5fc3405106c))
- dependency ([569442a](https://gitlab.com/mmorg/ismene/commit/569442a037f50a762085436c1176a3aa1afca92f))
- docker files ([6a8a282](https://gitlab.com/mmorg/ismene/commit/6a8a28221d4e4fa0888fafb5597ca4643d5743ac))
- docker-build & integration of command ([9bcfa72](https://gitlab.com/mmorg/ismene/commit/9bcfa7293f9036b95d5c90aece44318f932561fe))
- gatsby files ([c23314f](https://gitlab.com/mmorg/ismene/commit/c23314f8d28e4997d705a92ffc5cc3cb089a292d))
- improve graphLayer feature ([54cf35e](https://gitlab.com/mmorg/ismene/commit/54cf35e82e8ac2a1b7009e1f4d320a1ea6f02c92))
- improve implementation of rdfx-layers ([791e0f3](https://gitlab.com/mmorg/ismene/commit/791e0f37e53db5c79bf87cf65027b3cf8685b9a6))
- improve of fn & change texts ([4428d63](https://gitlab.com/mmorg/ismene/commit/4428d63aff665f4ab7107def3319d257d2dcaf3a))
- langString in ([bda64ef](https://gitlab.com/mmorg/ismene/commit/bda64ef6dcccf6883730d9047fdcd5a911fb63cf))
- make in-place and browser app editing okay ([0107358](https://gitlab.com/mmorg/ismene/commit/0107358eedff6cb2824e802c1e06fcf27141d26e))
- mapping typos [@bart](https://gitlab.com/bart) : have a look ! ([32389ac](https://gitlab.com/mmorg/ismene/commit/32389ac3d2a4913521d18c2d3d99f50c789bc2aa))
- merging branches ([1dddafa](https://gitlab.com/mmorg/ismene/commit/1dddafa69f191d5c93b1822ac54c73827fd1ab9c))
- new generation ([7e3ff9d](https://gitlab.com/mmorg/ismene/commit/7e3ff9d32854ba14efb8d9d40506dbbd347a5857))
- new structure ([6530ba5](https://gitlab.com/mmorg/ismene/commit/6530ba5e341999f3efcb93a66422a093d44740fc))
- owl:ontology ([e36e359](https://gitlab.com/mmorg/ismene/commit/e36e35911a61caf6f46a5d1db8d731276feee67e))
- refine display ([4f7d456](https://gitlab.com/mmorg/ismene/commit/4f7d456feed13952378d093faf30420a3cf5e90b))
- remove old code ([9fb9d7c](https://gitlab.com/mmorg/ismene/commit/9fb9d7c0541c962718daa991c605170f0a9a6840))
- remove some props ([eee69d0](https://gitlab.com/mmorg/ismene/commit/eee69d00fef37d80b426d1b9b349a7a483d9b7dc))
- resolve bugs in layers exec and generators ([c38e925](https://gitlab.com/mmorg/ismene/commit/c38e925a90085136e9bb1d0d4b510e6d3a986606))
- solve little things ([674dade](https://gitlab.com/mmorg/ismene/commit/674dade5bd728a8182a2ea7c64b25d33fee3fca7))
- test after file path change ([0005e26](https://gitlab.com/mmorg/ismene/commit/0005e265f7f81eca7aa4d0b432b8d72ca71b0f5f))
- tests works again ([03465e9](https://gitlab.com/mmorg/ismene/commit/03465e9f2d298f1fa592a602c6b81a915d8c1f42))
- update version number ([0f86e62](https://gitlab.com/mmorg/ismene/commit/0f86e62783a60e1382217169d9863d8b404157ad))

### Features

- 1st implementation of refine integration ([c7a9272](https://gitlab.com/mmorg/ismene/commit/c7a927248fd8eb40117ea211cdba2fddb10caed0))
- add api first draft & rml processing library integration ([2750837](https://gitlab.com/mmorg/ismene/commit/27508374cac7fc7f3771854f57c9d3360e19b6b6))
- add Collectoin display page ([73c64b1](https://gitlab.com/mmorg/ismene/commit/73c64b102f04f74a263c38210c4b09a7f4e8f8ee))
- add complete file generation function ([932cbb9](https://gitlab.com/mmorg/ismene/commit/932cbb905ea56571c9143c8f7dc2517816043754))
- add configurable url endpoint ([0a6d87b](https://gitlab.com/mmorg/ismene/commit/0a6d87b595e874c1c13cd75d38b29e5a177c6e75))
- add context lighter ([10145b5](https://gitlab.com/mmorg/ismene/commit/10145b5811cdc0885bff3f6133f4f91ea631b8e6))
- add contribution images and update of Paper to Card ([1e439c2](https://gitlab.com/mmorg/ismene/commit/1e439c2d0c9c797c069964ea3becfd44c15cad58))
- add dataCube ([0f892d0](https://gitlab.com/mmorg/ismene/commit/0f892d088975968b3be678b7b35fd7fdfc83e455))
- add deviationLayer Generation & update Skos ([dbc8883](https://gitlab.com/mmorg/ismene/commit/dbc8883400a55c052da04c6ba7d79a03adbe013c))
- add deviationType to continue to test ([0471d61](https://gitlab.com/mmorg/ismene/commit/0471d61ea16953ed30f2ce1209752c1718615c59))
- add explicitInherit layer generation ([ab075ed](https://gitlab.com/mmorg/ismene/commit/ab075eda523e15ff10f476dfdc21576ed0c0873b))
- add export of compacted layers ([e4f9a85](https://gitlab.com/mmorg/ismene/commit/e4f9a855b83df26ec1f5761d7f270ad0c7b3ea65))
- add first version of contribution logos management ([10a9ff2](https://gitlab.com/mmorg/ismene/commit/10a9ff2c115d1d5e29ed4eb0e23c447a28163149))
- add global editor sub-app ([80153a0](https://gitlab.com/mmorg/ismene/commit/80153a0adfb8a7058a8f776933cf57a059ea0abc))
- add graphLayers generators execution and Layers save ([c1b65d9](https://gitlab.com/mmorg/ismene/commit/c1b65d95254edc6bcbe1e97d2486326ee13383aa))
- add jest conf for EMS ([9cf7469](https://gitlab.com/mmorg/ismene/commit/9cf74699d48f5c5f5fed710cea090a0134985183))
- add layers save ([db71ff1](https://gitlab.com/mmorg/ismene/commit/db71ff19edd4424740572b6409178f9d115b6341))
- add menu for conceptContainers ([66e8f03](https://gitlab.com/mmorg/ismene/commit/66e8f03e9f4ea8dd4026f973566ac9737ce8ebf6))
- add MM ontology ([98e59c3](https://gitlab.com/mmorg/ismene/commit/98e59c32edb2ac75d031142ebec76f1fcb66d2a6))
- add new header ([c18afe4](https://gitlab.com/mmorg/ismene/commit/c18afe4e4be32555896e3240aa6d638a01150540))
- add new header ([c163ed1](https://gitlab.com/mmorg/ismene/commit/c163ed1c607705193ec315128b56fc138ee87722))
- add new ontologies ([b840bf6](https://gitlab.com/mmorg/ismene/commit/b840bf6d7a76dc29e27d5ae81aed86b3441adcc7))
- add Onto-Terms Translators throw Visons API ([5a96408](https://gitlab.com/mmorg/ismene/commit/5a96408840b3eb2415bf602c22e93fcff71141b1))
- add openBadges std context ([9c8a340](https://gitlab.com/mmorg/ismene/commit/9c8a3409c9937664d5d4d1ef40efa6c0a8a6cb8d))
- add organisation entities generation ([9a8bb1b](https://gitlab.com/mmorg/ismene/commit/9a8bb1b6d9f20fcfd7e2e07124b2588b21f86fc9))
- add processing to apiPF ([6590447](https://gitlab.com/mmorg/ismene/commit/6590447e8e1b51e7b2937e6872896b51057e825b))
- add rdf-graph serveur ([97653e7](https://gitlab.com/mmorg/ismene/commit/97653e785734d01e781b14eda86b3128d52fcf8a))
- add rdfs ([82fce6f](https://gitlab.com/mmorg/ismene/commit/82fce6f74bb65d5403467a0859acf542bb18eefc))
- add shacl model ([9b3c16d](https://gitlab.com/mmorg/ismene/commit/9b3c16dc6731cb240d5d0f44feb9189f7224fbbf))
- add skos ontology ([a3d5d38](https://gitlab.com/mmorg/ismene/commit/a3d5d3848cfc2eb69c409d60d5a1b1e44de35cfa))
- add Test on pre-push ([6c29d10](https://gitlab.com/mmorg/ismene/commit/6c29d1013f2ab3199ee9b4ffd2784f2ee7dcb8e5))
- add updateEntity with strategies ([b2e24dd](https://gitlab.com/mmorg/ismene/commit/b2e24ddafb3be40501d2ac2562ae8f7da5ddb6aa))
- add vision interop ([c403337](https://gitlab.com/mmorg/ismene/commit/c403337d4b8d98cc8c52a1ee7d1df4407402faaa))
- add xml to json-ld for skos ([9ae5bef](https://gitlab.com/mmorg/ismene/commit/9ae5befd1e528f78c14f8275608e55cf39d0d7bf))
- first integration of Oriane ontology ([4d3f2b6](https://gitlab.com/mmorg/ismene/commit/4d3f2b626cfeb6de0d4647100f4a4a95081be7b8))
- implementation of sch:Organisations generation ([12361fc](https://gitlab.com/mmorg/ismene/commit/12361fc95c838f820657f93ede338fc396b81022))
- import openEmploi & mnx ontologies ([ec617e5](https://gitlab.com/mmorg/ismene/commit/ec617e5b70c2c177b488a0d34e52232e4bbded09))
- initial commit ([0287d16](https://gitlab.com/mmorg/ismene/commit/0287d169b8de18a75442062f04b1725c54b755ed))
- multi-ontology menu display ([541ccf6](https://gitlab.com/mmorg/ismene/commit/541ccf61c12e8705da4c4513ef526053f6ceb7d2))
- new clean gen_v1 export ([43dfc98](https://gitlab.com/mmorg/ismene/commit/43dfc98b4d73d08c52dcf098322d6c1ee479b37a))
- new version ([67a5374](https://gitlab.com/mmorg/ismene/commit/67a537490fae13fbd00bd44465205cecd0e1ae59))
- publish a the first version ([76935b1](https://gitlab.com/mmorg/ismene/commit/76935b14533ce38f3327631bc62828b875feceb4))
- start add ts+esm support ([2ca8e65](https://gitlab.com/mmorg/ismene/commit/2ca8e658266e52296ce8b7c12f4fd2e14a502b32))
- start gl implementation ([70dd085](https://gitlab.com/mmorg/ismene/commit/70dd085c8ad5977c91e7d1f5bff661a2ac4dfdeb))
- working intermediate version ([20cbff8](https://gitlab.com/mmorg/ismene/commit/20cbff802ffedde52f19f8d435d0635172c3d9af))

# 1.0.0 (2023-03-10)

### Bug Fixes

- activate po & create a dev folder for input data ([82efb4b](https://gitlab.com/mmorg/ismene/commit/82efb4b22269322eced93310d00f1c67be53e022))
- add publish ([eb791b0](https://gitlab.com/mmorg/ismene/commit/eb791b07877cc6b88c22981bce39296f4f0e773a))
- Add skos in final document ([97d71a0](https://gitlab.com/mmorg/ismene/commit/97d71a0fa206ab5153c19e2cfc34a1aecba2dead))
- auto-app client only loading ([76861f4](https://gitlab.com/mmorg/ismene/commit/76861f4753faf4467fab7a13c7ade98f3dd5526a))
- better naming ([42a93e6](https://gitlab.com/mmorg/ismene/commit/42a93e6986ac00f42e543a48faf13ab61821b59a))
- better post processing ([cb51d82](https://gitlab.com/mmorg/ismene/commit/cb51d82aea3b69a8233ec7adc1f0d5fc3405106c))
- dependency ([569442a](https://gitlab.com/mmorg/ismene/commit/569442a037f50a762085436c1176a3aa1afca92f))
- docker files ([6a8a282](https://gitlab.com/mmorg/ismene/commit/6a8a28221d4e4fa0888fafb5597ca4643d5743ac))
- docker-build & integration of command ([9bcfa72](https://gitlab.com/mmorg/ismene/commit/9bcfa7293f9036b95d5c90aece44318f932561fe))
- gatsby files ([c23314f](https://gitlab.com/mmorg/ismene/commit/c23314f8d28e4997d705a92ffc5cc3cb089a292d))
- improve graphLayer feature ([54cf35e](https://gitlab.com/mmorg/ismene/commit/54cf35e82e8ac2a1b7009e1f4d320a1ea6f02c92))
- improve implementation of rdfx-layers ([791e0f3](https://gitlab.com/mmorg/ismene/commit/791e0f37e53db5c79bf87cf65027b3cf8685b9a6))
- improve of fn & change texts ([4428d63](https://gitlab.com/mmorg/ismene/commit/4428d63aff665f4ab7107def3319d257d2dcaf3a))
- langString in ([bda64ef](https://gitlab.com/mmorg/ismene/commit/bda64ef6dcccf6883730d9047fdcd5a911fb63cf))
- make in-place and browser app editing okay ([0107358](https://gitlab.com/mmorg/ismene/commit/0107358eedff6cb2824e802c1e06fcf27141d26e))
- mapping typos [@bart](https://gitlab.com/bart) : have a look ! ([32389ac](https://gitlab.com/mmorg/ismene/commit/32389ac3d2a4913521d18c2d3d99f50c789bc2aa))
- merging branches ([1dddafa](https://gitlab.com/mmorg/ismene/commit/1dddafa69f191d5c93b1822ac54c73827fd1ab9c))
- new generation ([7e3ff9d](https://gitlab.com/mmorg/ismene/commit/7e3ff9d32854ba14efb8d9d40506dbbd347a5857))
- new structure ([6530ba5](https://gitlab.com/mmorg/ismene/commit/6530ba5e341999f3efcb93a66422a093d44740fc))
- owl:ontology ([e36e359](https://gitlab.com/mmorg/ismene/commit/e36e35911a61caf6f46a5d1db8d731276feee67e))
- refine display ([4f7d456](https://gitlab.com/mmorg/ismene/commit/4f7d456feed13952378d093faf30420a3cf5e90b))
- remove old code ([9fb9d7c](https://gitlab.com/mmorg/ismene/commit/9fb9d7c0541c962718daa991c605170f0a9a6840))
- remove some props ([eee69d0](https://gitlab.com/mmorg/ismene/commit/eee69d00fef37d80b426d1b9b349a7a483d9b7dc))
- resolve bugs in layers exec and generators ([c38e925](https://gitlab.com/mmorg/ismene/commit/c38e925a90085136e9bb1d0d4b510e6d3a986606))
- solve little things ([674dade](https://gitlab.com/mmorg/ismene/commit/674dade5bd728a8182a2ea7c64b25d33fee3fca7))
- test after file path change ([0005e26](https://gitlab.com/mmorg/ismene/commit/0005e265f7f81eca7aa4d0b432b8d72ca71b0f5f))
- tests works again ([03465e9](https://gitlab.com/mmorg/ismene/commit/03465e9f2d298f1fa592a602c6b81a915d8c1f42))
- update version number ([0f86e62](https://gitlab.com/mmorg/ismene/commit/0f86e62783a60e1382217169d9863d8b404157ad))

### Features

- 1st implementation of refine integration ([c7a9272](https://gitlab.com/mmorg/ismene/commit/c7a927248fd8eb40117ea211cdba2fddb10caed0))
- add api first draft & rml processing library integration ([2750837](https://gitlab.com/mmorg/ismene/commit/27508374cac7fc7f3771854f57c9d3360e19b6b6))
- add Collectoin display page ([73c64b1](https://gitlab.com/mmorg/ismene/commit/73c64b102f04f74a263c38210c4b09a7f4e8f8ee))
- add complete file generation function ([932cbb9](https://gitlab.com/mmorg/ismene/commit/932cbb905ea56571c9143c8f7dc2517816043754))
- add configurable url endpoint ([0a6d87b](https://gitlab.com/mmorg/ismene/commit/0a6d87b595e874c1c13cd75d38b29e5a177c6e75))
- add context lighter ([10145b5](https://gitlab.com/mmorg/ismene/commit/10145b5811cdc0885bff3f6133f4f91ea631b8e6))
- add contribution images and update of Paper to Card ([1e439c2](https://gitlab.com/mmorg/ismene/commit/1e439c2d0c9c797c069964ea3becfd44c15cad58))
- add dataCube ([0f892d0](https://gitlab.com/mmorg/ismene/commit/0f892d088975968b3be678b7b35fd7fdfc83e455))
- add deviationLayer Generation & update Skos ([dbc8883](https://gitlab.com/mmorg/ismene/commit/dbc8883400a55c052da04c6ba7d79a03adbe013c))
- add deviationType to continue to test ([0471d61](https://gitlab.com/mmorg/ismene/commit/0471d61ea16953ed30f2ce1209752c1718615c59))
- add explicitInherit layer generation ([ab075ed](https://gitlab.com/mmorg/ismene/commit/ab075eda523e15ff10f476dfdc21576ed0c0873b))
- add export of compacted layers ([e4f9a85](https://gitlab.com/mmorg/ismene/commit/e4f9a855b83df26ec1f5761d7f270ad0c7b3ea65))
- add first version of contribution logos management ([10a9ff2](https://gitlab.com/mmorg/ismene/commit/10a9ff2c115d1d5e29ed4eb0e23c447a28163149))
- add global editor sub-app ([80153a0](https://gitlab.com/mmorg/ismene/commit/80153a0adfb8a7058a8f776933cf57a059ea0abc))
- add graphLayers generators execution and Layers save ([c1b65d9](https://gitlab.com/mmorg/ismene/commit/c1b65d95254edc6bcbe1e97d2486326ee13383aa))
- add jest conf for EMS ([9cf7469](https://gitlab.com/mmorg/ismene/commit/9cf74699d48f5c5f5fed710cea090a0134985183))
- add layers save ([db71ff1](https://gitlab.com/mmorg/ismene/commit/db71ff19edd4424740572b6409178f9d115b6341))
- add menu for conceptContainers ([66e8f03](https://gitlab.com/mmorg/ismene/commit/66e8f03e9f4ea8dd4026f973566ac9737ce8ebf6))
- add MM ontology ([98e59c3](https://gitlab.com/mmorg/ismene/commit/98e59c32edb2ac75d031142ebec76f1fcb66d2a6))
- add new header ([c18afe4](https://gitlab.com/mmorg/ismene/commit/c18afe4e4be32555896e3240aa6d638a01150540))
- add new header ([c163ed1](https://gitlab.com/mmorg/ismene/commit/c163ed1c607705193ec315128b56fc138ee87722))
- add new ontologies ([b840bf6](https://gitlab.com/mmorg/ismene/commit/b840bf6d7a76dc29e27d5ae81aed86b3441adcc7))
- add Onto-Terms Translators throw Visons API ([5a96408](https://gitlab.com/mmorg/ismene/commit/5a96408840b3eb2415bf602c22e93fcff71141b1))
- add openBadges std context ([9c8a340](https://gitlab.com/mmorg/ismene/commit/9c8a3409c9937664d5d4d1ef40efa6c0a8a6cb8d))
- add organisation entities generation ([9a8bb1b](https://gitlab.com/mmorg/ismene/commit/9a8bb1b6d9f20fcfd7e2e07124b2588b21f86fc9))
- add processing to apiPF ([6590447](https://gitlab.com/mmorg/ismene/commit/6590447e8e1b51e7b2937e6872896b51057e825b))
- add rdf-graph serveur ([97653e7](https://gitlab.com/mmorg/ismene/commit/97653e785734d01e781b14eda86b3128d52fcf8a))
- add rdfs ([82fce6f](https://gitlab.com/mmorg/ismene/commit/82fce6f74bb65d5403467a0859acf542bb18eefc))
- add shacl model ([9b3c16d](https://gitlab.com/mmorg/ismene/commit/9b3c16dc6731cb240d5d0f44feb9189f7224fbbf))
- add skos ontology ([a3d5d38](https://gitlab.com/mmorg/ismene/commit/a3d5d3848cfc2eb69c409d60d5a1b1e44de35cfa))
- add Test on pre-push ([6c29d10](https://gitlab.com/mmorg/ismene/commit/6c29d1013f2ab3199ee9b4ffd2784f2ee7dcb8e5))
- add updateEntity with strategies ([b2e24dd](https://gitlab.com/mmorg/ismene/commit/b2e24ddafb3be40501d2ac2562ae8f7da5ddb6aa))
- add vision interop ([c403337](https://gitlab.com/mmorg/ismene/commit/c403337d4b8d98cc8c52a1ee7d1df4407402faaa))
- add xml to json-ld for skos ([9ae5bef](https://gitlab.com/mmorg/ismene/commit/9ae5befd1e528f78c14f8275608e55cf39d0d7bf))
- first integration of Oriane ontology ([4d3f2b6](https://gitlab.com/mmorg/ismene/commit/4d3f2b626cfeb6de0d4647100f4a4a95081be7b8))
- implementation of sch:Organisations generation ([12361fc](https://gitlab.com/mmorg/ismene/commit/12361fc95c838f820657f93ede338fc396b81022))
- import openEmploi & mnx ontologies ([ec617e5](https://gitlab.com/mmorg/ismene/commit/ec617e5b70c2c177b488a0d34e52232e4bbded09))
- initial commit ([0287d16](https://gitlab.com/mmorg/ismene/commit/0287d169b8de18a75442062f04b1725c54b755ed))
- multi-ontology menu display ([541ccf6](https://gitlab.com/mmorg/ismene/commit/541ccf61c12e8705da4c4513ef526053f6ceb7d2))
- new clean gen_v1 export ([43dfc98](https://gitlab.com/mmorg/ismene/commit/43dfc98b4d73d08c52dcf098322d6c1ee479b37a))
- new version ([67a5374](https://gitlab.com/mmorg/ismene/commit/67a537490fae13fbd00bd44465205cecd0e1ae59))
- publish a the first version ([76935b1](https://gitlab.com/mmorg/ismene/commit/76935b14533ce38f3327631bc62828b875feceb4))
- start add ts+esm support ([2ca8e65](https://gitlab.com/mmorg/ismene/commit/2ca8e658266e52296ce8b7c12f4fd2e14a502b32))
- start gl implementation ([70dd085](https://gitlab.com/mmorg/ismene/commit/70dd085c8ad5977c91e7d1f5bff661a2ac4dfdeb))
- working intermediate version ([20cbff8](https://gitlab.com/mmorg/ismene/commit/20cbff802ffedde52f19f8d435d0635172c3d9af))

# 1.0.0 (2023-03-10)

### Bug Fixes

- activate po & create a dev folder for input data ([82efb4b](https://gitlab.com/mmorg/ismene/commit/82efb4b22269322eced93310d00f1c67be53e022))
- add publish ([eb791b0](https://gitlab.com/mmorg/ismene/commit/eb791b07877cc6b88c22981bce39296f4f0e773a))
- Add skos in final document ([97d71a0](https://gitlab.com/mmorg/ismene/commit/97d71a0fa206ab5153c19e2cfc34a1aecba2dead))
- auto-app client only loading ([76861f4](https://gitlab.com/mmorg/ismene/commit/76861f4753faf4467fab7a13c7ade98f3dd5526a))
- better naming ([42a93e6](https://gitlab.com/mmorg/ismene/commit/42a93e6986ac00f42e543a48faf13ab61821b59a))
- better post processing ([cb51d82](https://gitlab.com/mmorg/ismene/commit/cb51d82aea3b69a8233ec7adc1f0d5fc3405106c))
- dependency ([569442a](https://gitlab.com/mmorg/ismene/commit/569442a037f50a762085436c1176a3aa1afca92f))
- docker files ([6a8a282](https://gitlab.com/mmorg/ismene/commit/6a8a28221d4e4fa0888fafb5597ca4643d5743ac))
- docker-build & integration of command ([9bcfa72](https://gitlab.com/mmorg/ismene/commit/9bcfa7293f9036b95d5c90aece44318f932561fe))
- gatsby files ([c23314f](https://gitlab.com/mmorg/ismene/commit/c23314f8d28e4997d705a92ffc5cc3cb089a292d))
- improve graphLayer feature ([54cf35e](https://gitlab.com/mmorg/ismene/commit/54cf35e82e8ac2a1b7009e1f4d320a1ea6f02c92))
- improve implementation of rdfx-layers ([791e0f3](https://gitlab.com/mmorg/ismene/commit/791e0f37e53db5c79bf87cf65027b3cf8685b9a6))
- improve of fn & change texts ([4428d63](https://gitlab.com/mmorg/ismene/commit/4428d63aff665f4ab7107def3319d257d2dcaf3a))
- langString in ([bda64ef](https://gitlab.com/mmorg/ismene/commit/bda64ef6dcccf6883730d9047fdcd5a911fb63cf))
- make in-place and browser app editing okay ([0107358](https://gitlab.com/mmorg/ismene/commit/0107358eedff6cb2824e802c1e06fcf27141d26e))
- mapping typos [@bart](https://gitlab.com/bart) : have a look ! ([32389ac](https://gitlab.com/mmorg/ismene/commit/32389ac3d2a4913521d18c2d3d99f50c789bc2aa))
- merging branches ([1dddafa](https://gitlab.com/mmorg/ismene/commit/1dddafa69f191d5c93b1822ac54c73827fd1ab9c))
- new generation ([7e3ff9d](https://gitlab.com/mmorg/ismene/commit/7e3ff9d32854ba14efb8d9d40506dbbd347a5857))
- new structure ([6530ba5](https://gitlab.com/mmorg/ismene/commit/6530ba5e341999f3efcb93a66422a093d44740fc))
- owl:ontology ([e36e359](https://gitlab.com/mmorg/ismene/commit/e36e35911a61caf6f46a5d1db8d731276feee67e))
- refine display ([4f7d456](https://gitlab.com/mmorg/ismene/commit/4f7d456feed13952378d093faf30420a3cf5e90b))
- remove old code ([9fb9d7c](https://gitlab.com/mmorg/ismene/commit/9fb9d7c0541c962718daa991c605170f0a9a6840))
- remove some props ([eee69d0](https://gitlab.com/mmorg/ismene/commit/eee69d00fef37d80b426d1b9b349a7a483d9b7dc))
- resolve bugs in layers exec and generators ([c38e925](https://gitlab.com/mmorg/ismene/commit/c38e925a90085136e9bb1d0d4b510e6d3a986606))
- solve little things ([674dade](https://gitlab.com/mmorg/ismene/commit/674dade5bd728a8182a2ea7c64b25d33fee3fca7))
- test after file path change ([0005e26](https://gitlab.com/mmorg/ismene/commit/0005e265f7f81eca7aa4d0b432b8d72ca71b0f5f))
- tests works again ([03465e9](https://gitlab.com/mmorg/ismene/commit/03465e9f2d298f1fa592a602c6b81a915d8c1f42))
- update version number ([0f86e62](https://gitlab.com/mmorg/ismene/commit/0f86e62783a60e1382217169d9863d8b404157ad))

### Features

- 1st implementation of refine integration ([c7a9272](https://gitlab.com/mmorg/ismene/commit/c7a927248fd8eb40117ea211cdba2fddb10caed0))
- add api first draft & rml processing library integration ([2750837](https://gitlab.com/mmorg/ismene/commit/27508374cac7fc7f3771854f57c9d3360e19b6b6))
- add Collectoin display page ([73c64b1](https://gitlab.com/mmorg/ismene/commit/73c64b102f04f74a263c38210c4b09a7f4e8f8ee))
- add complete file generation function ([932cbb9](https://gitlab.com/mmorg/ismene/commit/932cbb905ea56571c9143c8f7dc2517816043754))
- add configurable url endpoint ([0a6d87b](https://gitlab.com/mmorg/ismene/commit/0a6d87b595e874c1c13cd75d38b29e5a177c6e75))
- add context lighter ([10145b5](https://gitlab.com/mmorg/ismene/commit/10145b5811cdc0885bff3f6133f4f91ea631b8e6))
- add contribution images and update of Paper to Card ([1e439c2](https://gitlab.com/mmorg/ismene/commit/1e439c2d0c9c797c069964ea3becfd44c15cad58))
- add dataCube ([0f892d0](https://gitlab.com/mmorg/ismene/commit/0f892d088975968b3be678b7b35fd7fdfc83e455))
- add deviationLayer Generation & update Skos ([dbc8883](https://gitlab.com/mmorg/ismene/commit/dbc8883400a55c052da04c6ba7d79a03adbe013c))
- add deviationType to continue to test ([0471d61](https://gitlab.com/mmorg/ismene/commit/0471d61ea16953ed30f2ce1209752c1718615c59))
- add explicitInherit layer generation ([ab075ed](https://gitlab.com/mmorg/ismene/commit/ab075eda523e15ff10f476dfdc21576ed0c0873b))
- add export of compacted layers ([e4f9a85](https://gitlab.com/mmorg/ismene/commit/e4f9a855b83df26ec1f5761d7f270ad0c7b3ea65))
- add first version of contribution logos management ([10a9ff2](https://gitlab.com/mmorg/ismene/commit/10a9ff2c115d1d5e29ed4eb0e23c447a28163149))
- add global editor sub-app ([80153a0](https://gitlab.com/mmorg/ismene/commit/80153a0adfb8a7058a8f776933cf57a059ea0abc))
- add graphLayers generators execution and Layers save ([c1b65d9](https://gitlab.com/mmorg/ismene/commit/c1b65d95254edc6bcbe1e97d2486326ee13383aa))
- add jest conf for EMS ([9cf7469](https://gitlab.com/mmorg/ismene/commit/9cf74699d48f5c5f5fed710cea090a0134985183))
- add layers save ([db71ff1](https://gitlab.com/mmorg/ismene/commit/db71ff19edd4424740572b6409178f9d115b6341))
- add menu for conceptContainers ([66e8f03](https://gitlab.com/mmorg/ismene/commit/66e8f03e9f4ea8dd4026f973566ac9737ce8ebf6))
- add MM ontology ([98e59c3](https://gitlab.com/mmorg/ismene/commit/98e59c32edb2ac75d031142ebec76f1fcb66d2a6))
- add new header ([c18afe4](https://gitlab.com/mmorg/ismene/commit/c18afe4e4be32555896e3240aa6d638a01150540))
- add new header ([c163ed1](https://gitlab.com/mmorg/ismene/commit/c163ed1c607705193ec315128b56fc138ee87722))
- add new ontologies ([b840bf6](https://gitlab.com/mmorg/ismene/commit/b840bf6d7a76dc29e27d5ae81aed86b3441adcc7))
- add Onto-Terms Translators throw Visons API ([5a96408](https://gitlab.com/mmorg/ismene/commit/5a96408840b3eb2415bf602c22e93fcff71141b1))
- add openBadges std context ([9c8a340](https://gitlab.com/mmorg/ismene/commit/9c8a3409c9937664d5d4d1ef40efa6c0a8a6cb8d))
- add organisation entities generation ([9a8bb1b](https://gitlab.com/mmorg/ismene/commit/9a8bb1b6d9f20fcfd7e2e07124b2588b21f86fc9))
- add processing to apiPF ([6590447](https://gitlab.com/mmorg/ismene/commit/6590447e8e1b51e7b2937e6872896b51057e825b))
- add rdf-graph serveur ([97653e7](https://gitlab.com/mmorg/ismene/commit/97653e785734d01e781b14eda86b3128d52fcf8a))
- add rdfs ([82fce6f](https://gitlab.com/mmorg/ismene/commit/82fce6f74bb65d5403467a0859acf542bb18eefc))
- add shacl model ([9b3c16d](https://gitlab.com/mmorg/ismene/commit/9b3c16dc6731cb240d5d0f44feb9189f7224fbbf))
- add skos ontology ([a3d5d38](https://gitlab.com/mmorg/ismene/commit/a3d5d3848cfc2eb69c409d60d5a1b1e44de35cfa))
- add Test on pre-push ([6c29d10](https://gitlab.com/mmorg/ismene/commit/6c29d1013f2ab3199ee9b4ffd2784f2ee7dcb8e5))
- add updateEntity with strategies ([b2e24dd](https://gitlab.com/mmorg/ismene/commit/b2e24ddafb3be40501d2ac2562ae8f7da5ddb6aa))
- add vision interop ([c403337](https://gitlab.com/mmorg/ismene/commit/c403337d4b8d98cc8c52a1ee7d1df4407402faaa))
- add xml to json-ld for skos ([9ae5bef](https://gitlab.com/mmorg/ismene/commit/9ae5befd1e528f78c14f8275608e55cf39d0d7bf))
- first integration of Oriane ontology ([4d3f2b6](https://gitlab.com/mmorg/ismene/commit/4d3f2b626cfeb6de0d4647100f4a4a95081be7b8))
- implementation of sch:Organisations generation ([12361fc](https://gitlab.com/mmorg/ismene/commit/12361fc95c838f820657f93ede338fc396b81022))
- import openEmploi & mnx ontologies ([ec617e5](https://gitlab.com/mmorg/ismene/commit/ec617e5b70c2c177b488a0d34e52232e4bbded09))
- initial commit ([0287d16](https://gitlab.com/mmorg/ismene/commit/0287d169b8de18a75442062f04b1725c54b755ed))
- multi-ontology menu display ([541ccf6](https://gitlab.com/mmorg/ismene/commit/541ccf61c12e8705da4c4513ef526053f6ceb7d2))
- new clean gen_v1 export ([43dfc98](https://gitlab.com/mmorg/ismene/commit/43dfc98b4d73d08c52dcf098322d6c1ee479b37a))
- new version ([67a5374](https://gitlab.com/mmorg/ismene/commit/67a537490fae13fbd00bd44465205cecd0e1ae59))
- publish a the first version ([76935b1](https://gitlab.com/mmorg/ismene/commit/76935b14533ce38f3327631bc62828b875feceb4))
- start add ts+esm support ([2ca8e65](https://gitlab.com/mmorg/ismene/commit/2ca8e658266e52296ce8b7c12f4fd2e14a502b32))
- start gl implementation ([70dd085](https://gitlab.com/mmorg/ismene/commit/70dd085c8ad5977c91e7d1f5bff661a2ac4dfdeb))
- working intermediate version ([20cbff8](https://gitlab.com/mmorg/ismene/commit/20cbff802ffedde52f19f8d435d0635172c3d9af))

# 1.0.0 (2023-03-10)

### Bug Fixes

- activate po & create a dev folder for input data ([82efb4b](https://gitlab.com/mmorg/ismene/commit/82efb4b22269322eced93310d00f1c67be53e022))
- add publish ([eb791b0](https://gitlab.com/mmorg/ismene/commit/eb791b07877cc6b88c22981bce39296f4f0e773a))
- Add skos in final document ([97d71a0](https://gitlab.com/mmorg/ismene/commit/97d71a0fa206ab5153c19e2cfc34a1aecba2dead))
- auto-app client only loading ([76861f4](https://gitlab.com/mmorg/ismene/commit/76861f4753faf4467fab7a13c7ade98f3dd5526a))
- better naming ([42a93e6](https://gitlab.com/mmorg/ismene/commit/42a93e6986ac00f42e543a48faf13ab61821b59a))
- better post processing ([cb51d82](https://gitlab.com/mmorg/ismene/commit/cb51d82aea3b69a8233ec7adc1f0d5fc3405106c))
- dependency ([569442a](https://gitlab.com/mmorg/ismene/commit/569442a037f50a762085436c1176a3aa1afca92f))
- docker files ([6a8a282](https://gitlab.com/mmorg/ismene/commit/6a8a28221d4e4fa0888fafb5597ca4643d5743ac))
- docker-build & integration of command ([9bcfa72](https://gitlab.com/mmorg/ismene/commit/9bcfa7293f9036b95d5c90aece44318f932561fe))
- gatsby files ([c23314f](https://gitlab.com/mmorg/ismene/commit/c23314f8d28e4997d705a92ffc5cc3cb089a292d))
- improve graphLayer feature ([54cf35e](https://gitlab.com/mmorg/ismene/commit/54cf35e82e8ac2a1b7009e1f4d320a1ea6f02c92))
- improve implementation of rdfx-layers ([791e0f3](https://gitlab.com/mmorg/ismene/commit/791e0f37e53db5c79bf87cf65027b3cf8685b9a6))
- improve of fn & change texts ([4428d63](https://gitlab.com/mmorg/ismene/commit/4428d63aff665f4ab7107def3319d257d2dcaf3a))
- langString in ([bda64ef](https://gitlab.com/mmorg/ismene/commit/bda64ef6dcccf6883730d9047fdcd5a911fb63cf))
- make in-place and browser app editing okay ([0107358](https://gitlab.com/mmorg/ismene/commit/0107358eedff6cb2824e802c1e06fcf27141d26e))
- mapping typos [@bart](https://gitlab.com/bart) : have a look ! ([32389ac](https://gitlab.com/mmorg/ismene/commit/32389ac3d2a4913521d18c2d3d99f50c789bc2aa))
- merging branches ([1dddafa](https://gitlab.com/mmorg/ismene/commit/1dddafa69f191d5c93b1822ac54c73827fd1ab9c))
- new generation ([7e3ff9d](https://gitlab.com/mmorg/ismene/commit/7e3ff9d32854ba14efb8d9d40506dbbd347a5857))
- new structure ([6530ba5](https://gitlab.com/mmorg/ismene/commit/6530ba5e341999f3efcb93a66422a093d44740fc))
- owl:ontology ([e36e359](https://gitlab.com/mmorg/ismene/commit/e36e35911a61caf6f46a5d1db8d731276feee67e))
- refine display ([4f7d456](https://gitlab.com/mmorg/ismene/commit/4f7d456feed13952378d093faf30420a3cf5e90b))
- remove old code ([9fb9d7c](https://gitlab.com/mmorg/ismene/commit/9fb9d7c0541c962718daa991c605170f0a9a6840))
- remove some props ([eee69d0](https://gitlab.com/mmorg/ismene/commit/eee69d00fef37d80b426d1b9b349a7a483d9b7dc))
- resolve bugs in layers exec and generators ([c38e925](https://gitlab.com/mmorg/ismene/commit/c38e925a90085136e9bb1d0d4b510e6d3a986606))
- solve little things ([674dade](https://gitlab.com/mmorg/ismene/commit/674dade5bd728a8182a2ea7c64b25d33fee3fca7))
- test after file path change ([0005e26](https://gitlab.com/mmorg/ismene/commit/0005e265f7f81eca7aa4d0b432b8d72ca71b0f5f))
- tests works again ([03465e9](https://gitlab.com/mmorg/ismene/commit/03465e9f2d298f1fa592a602c6b81a915d8c1f42))
- update version number ([0f86e62](https://gitlab.com/mmorg/ismene/commit/0f86e62783a60e1382217169d9863d8b404157ad))

### Features

- 1st implementation of refine integration ([c7a9272](https://gitlab.com/mmorg/ismene/commit/c7a927248fd8eb40117ea211cdba2fddb10caed0))
- add api first draft & rml processing library integration ([2750837](https://gitlab.com/mmorg/ismene/commit/27508374cac7fc7f3771854f57c9d3360e19b6b6))
- add Collectoin display page ([73c64b1](https://gitlab.com/mmorg/ismene/commit/73c64b102f04f74a263c38210c4b09a7f4e8f8ee))
- add complete file generation function ([932cbb9](https://gitlab.com/mmorg/ismene/commit/932cbb905ea56571c9143c8f7dc2517816043754))
- add configurable url endpoint ([0a6d87b](https://gitlab.com/mmorg/ismene/commit/0a6d87b595e874c1c13cd75d38b29e5a177c6e75))
- add context lighter ([10145b5](https://gitlab.com/mmorg/ismene/commit/10145b5811cdc0885bff3f6133f4f91ea631b8e6))
- add contribution images and update of Paper to Card ([1e439c2](https://gitlab.com/mmorg/ismene/commit/1e439c2d0c9c797c069964ea3becfd44c15cad58))
- add dataCube ([0f892d0](https://gitlab.com/mmorg/ismene/commit/0f892d088975968b3be678b7b35fd7fdfc83e455))
- add deviationLayer Generation & update Skos ([dbc8883](https://gitlab.com/mmorg/ismene/commit/dbc8883400a55c052da04c6ba7d79a03adbe013c))
- add deviationType to continue to test ([0471d61](https://gitlab.com/mmorg/ismene/commit/0471d61ea16953ed30f2ce1209752c1718615c59))
- add explicitInherit layer generation ([ab075ed](https://gitlab.com/mmorg/ismene/commit/ab075eda523e15ff10f476dfdc21576ed0c0873b))
- add export of compacted layers ([e4f9a85](https://gitlab.com/mmorg/ismene/commit/e4f9a855b83df26ec1f5761d7f270ad0c7b3ea65))
- add first version of contribution logos management ([10a9ff2](https://gitlab.com/mmorg/ismene/commit/10a9ff2c115d1d5e29ed4eb0e23c447a28163149))
- add global editor sub-app ([80153a0](https://gitlab.com/mmorg/ismene/commit/80153a0adfb8a7058a8f776933cf57a059ea0abc))
- add graphLayers generators execution and Layers save ([c1b65d9](https://gitlab.com/mmorg/ismene/commit/c1b65d95254edc6bcbe1e97d2486326ee13383aa))
- add jest conf for EMS ([9cf7469](https://gitlab.com/mmorg/ismene/commit/9cf74699d48f5c5f5fed710cea090a0134985183))
- add layers save ([db71ff1](https://gitlab.com/mmorg/ismene/commit/db71ff19edd4424740572b6409178f9d115b6341))
- add menu for conceptContainers ([66e8f03](https://gitlab.com/mmorg/ismene/commit/66e8f03e9f4ea8dd4026f973566ac9737ce8ebf6))
- add MM ontology ([98e59c3](https://gitlab.com/mmorg/ismene/commit/98e59c32edb2ac75d031142ebec76f1fcb66d2a6))
- add new header ([c18afe4](https://gitlab.com/mmorg/ismene/commit/c18afe4e4be32555896e3240aa6d638a01150540))
- add new header ([c163ed1](https://gitlab.com/mmorg/ismene/commit/c163ed1c607705193ec315128b56fc138ee87722))
- add new ontologies ([b840bf6](https://gitlab.com/mmorg/ismene/commit/b840bf6d7a76dc29e27d5ae81aed86b3441adcc7))
- add Onto-Terms Translators throw Visons API ([5a96408](https://gitlab.com/mmorg/ismene/commit/5a96408840b3eb2415bf602c22e93fcff71141b1))
- add openBadges std context ([9c8a340](https://gitlab.com/mmorg/ismene/commit/9c8a3409c9937664d5d4d1ef40efa6c0a8a6cb8d))
- add organisation entities generation ([9a8bb1b](https://gitlab.com/mmorg/ismene/commit/9a8bb1b6d9f20fcfd7e2e07124b2588b21f86fc9))
- add processing to apiPF ([6590447](https://gitlab.com/mmorg/ismene/commit/6590447e8e1b51e7b2937e6872896b51057e825b))
- add rdf-graph serveur ([97653e7](https://gitlab.com/mmorg/ismene/commit/97653e785734d01e781b14eda86b3128d52fcf8a))
- add rdfs ([82fce6f](https://gitlab.com/mmorg/ismene/commit/82fce6f74bb65d5403467a0859acf542bb18eefc))
- add shacl model ([9b3c16d](https://gitlab.com/mmorg/ismene/commit/9b3c16dc6731cb240d5d0f44feb9189f7224fbbf))
- add skos ontology ([a3d5d38](https://gitlab.com/mmorg/ismene/commit/a3d5d3848cfc2eb69c409d60d5a1b1e44de35cfa))
- add Test on pre-push ([6c29d10](https://gitlab.com/mmorg/ismene/commit/6c29d1013f2ab3199ee9b4ffd2784f2ee7dcb8e5))
- add updateEntity with strategies ([b2e24dd](https://gitlab.com/mmorg/ismene/commit/b2e24ddafb3be40501d2ac2562ae8f7da5ddb6aa))
- add vision interop ([c403337](https://gitlab.com/mmorg/ismene/commit/c403337d4b8d98cc8c52a1ee7d1df4407402faaa))
- add xml to json-ld for skos ([9ae5bef](https://gitlab.com/mmorg/ismene/commit/9ae5befd1e528f78c14f8275608e55cf39d0d7bf))
- first integration of Oriane ontology ([4d3f2b6](https://gitlab.com/mmorg/ismene/commit/4d3f2b626cfeb6de0d4647100f4a4a95081be7b8))
- implementation of sch:Organisations generation ([12361fc](https://gitlab.com/mmorg/ismene/commit/12361fc95c838f820657f93ede338fc396b81022))
- import openEmploi & mnx ontologies ([ec617e5](https://gitlab.com/mmorg/ismene/commit/ec617e5b70c2c177b488a0d34e52232e4bbded09))
- initial commit ([0287d16](https://gitlab.com/mmorg/ismene/commit/0287d169b8de18a75442062f04b1725c54b755ed))
- multi-ontology menu display ([541ccf6](https://gitlab.com/mmorg/ismene/commit/541ccf61c12e8705da4c4513ef526053f6ceb7d2))
- new clean gen_v1 export ([43dfc98](https://gitlab.com/mmorg/ismene/commit/43dfc98b4d73d08c52dcf098322d6c1ee479b37a))
- new version ([67a5374](https://gitlab.com/mmorg/ismene/commit/67a537490fae13fbd00bd44465205cecd0e1ae59))
- publish a the first version ([76935b1](https://gitlab.com/mmorg/ismene/commit/76935b14533ce38f3327631bc62828b875feceb4))
- start add ts+esm support ([2ca8e65](https://gitlab.com/mmorg/ismene/commit/2ca8e658266e52296ce8b7c12f4fd2e14a502b32))
- start gl implementation ([70dd085](https://gitlab.com/mmorg/ismene/commit/70dd085c8ad5977c91e7d1f5bff661a2ac4dfdeb))
- working intermediate version ([20cbff8](https://gitlab.com/mmorg/ismene/commit/20cbff802ffedde52f19f8d435d0635172c3d9af))

# @mmorg/appsearch-utils

## 1.1.0

### Minor Changes

- Setup of lib publishing
