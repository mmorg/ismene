import { saveJson } from "@mmorg/fsutils";
/**
 * @warning : manual creation of folder until fs-utils next version with parent folder creation
 * Analyses can be done with this script: ismene/rdfx-graphql/src/appSearchUtils/errorCatchers/genericSaveCatcher.js
 * @param {*} error
 */
export default function genericSaveCatcher(error, documents) {
  try {
    const errorFolder = `${process.cwd()}/__errors__`;
    const d = new Date();
    const tms = d.getTime();

    const errMessage = {
      error,
      body: {
        documents,
      },
    };
    saveJson(errMessage, `${errorFolder}/${tms}.json`);
  } catch (e) {}

  // throw new Error('@TODO: this should be tested !!')
}
