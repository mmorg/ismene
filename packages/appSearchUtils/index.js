import createDocAppSearch from './cud/createDocAppSearch.js'
import updateDocAppSearch from './cud/updateDocAppSearch.js'
import deleteDocAppSearch from './cud/deleteDocAppSearch.js'

import queryAppSearch from './search/queryAppSearch.js'
import queryStringFixer from './search/queryStringFixer.js'
import queryWordsRemover from './search/queryWordsRemover.js'

import getAppSearchClient from './getAppSearchClient.js'
import autocomplete from './autocomplete/autocomplete.js'
import batchIdsSearch from './batch/batchIdsSearch.js'

import normForAppSearchWithDepth from './normDenorm/normalisation/normForAppSearchWithDepth.js'
import denormForJsonld from './normDenorm/denormalisation/denormForJsonld.js'

export { 
  // cud
  createDocAppSearch, 
  updateDocAppSearch,
  deleteDocAppSearch,

  // search 
  queryAppSearch,
  autocomplete,
  batchIdsSearch,

  // utilities
  getAppSearchClient,
  normForAppSearchWithDepth,
  denormForJsonld,
  queryStringFixer,
  queryWordsRemover,


}