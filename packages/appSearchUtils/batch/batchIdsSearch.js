import getAppSearchClient from '../getAppSearchClient.js'
import { Client } from '@elastic/enterprise-search'
import cudUtils from '../cud/cudUtils.js'
import denormForJsonld from '../normDenorm/denormalisation/denormForJsonld.js'

// @TODO: implement this function as a multi-search https://www.elastic.co/guide/en/app-search/current/multi-search.html
// function to call : https://www.elastic.co/guide/en/enterprise-search-clients/enterprise-search-node/master/app-search-api.html#app-search-api-search-apis-multi
// This function is actuall buggy because of https://github.com/elastic/enterprise-search-js/issues/38
/** implementation example: 
 * 
const chunk_size = 10
async function run() {
  const q1 = {
    query: 'bouche',
    page: { size: pageSize },
    filters: {
      all: [
        {
          type: ['romeModel:Skill', 'romeModel:Occupation']
        }
      ]
    }
  }

  const q2 = {
    query: 'numérique',
    page: { size: pageSize },
    filters: {
      all: [
        {
          type: ['esco:Occupation'] // for skill: 'esco:Occupation'
        }
      ]
    }
  }

  console.log(confElastic)
  const { engineName, url, apiKey } = confElastic
  // const client = getAppSearchClient(confElastic)


  const client = new Client({
    url,
    auth: {
      token: apiKey
    }
  })


  console.log(client)
  const r = await client.app.multiSearch({
    engine_name: engineName,
    queries: [q1, q2]
  })
  console.log(t)
}
*/ 


// const pageSize = 2 // can be 1

const default_cudOptions = {
  // really low page size here as we only check by id
  docsCount : 1,
  interval : 100,
  showLogs : false,
}

// @TODO: move it to a generator when multi-search okay
export default async function batchIdsSearch(ids, confElastic, cudOptions = {}, bodyOptions = {}) {
  const _cudOps = Object.assign({}, default_cudOptions, cudOptions)
  const denorm_keepDatatypeAt = false


  const { engineName } = confElastic
  const client = getAppSearchClient(confElastic)


  return await cudUtils(async (docs) => {
    const response = await client.search({
      engine_name: engineName,
      body: {
        query: docs[0],
        ...bodyOptions,
      }
    })

    let entity = {}
    if(response.results[0]) entity = denormForJsonld(response.results[0], true, denorm_keepDatatypeAt)
    return entity
  },
    ids,
    _cudOps,
  )


}