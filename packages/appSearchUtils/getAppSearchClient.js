import { Client } from '@elastic/enterprise-search'

export default function getAppSearchClient(conf) {
    const { apiKey, baseUrl, url } = conf
    if (!url && !baseUrl) throw new Error('ElasticConf : At least one property required')
    let _url = url
    if (!_url && baseUrl) _url = baseUrl
    if (baseUrl) console.log('ElasticConf : @TODO: baseUrl conf is deprecated, use url instead')
    
    const client = new Client({
        url: _url,
        auth: {
            token: apiKey
        }
    })

    return client.app
}
