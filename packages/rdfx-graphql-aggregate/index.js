const exampleAggregateQuery = {
  size: 0,
  aggs: {
    count_by_broader: {
      terms: {
        field: "broader.keyword"
      },
      aggs: {
        filter: {
          match: {
            type: 'skos:Concept'
          }
        },
      }
    }
  }
}

import ld2aggregate from "./src/rdfx-to-graphql-aggregate/ld2aggregate.js";

export {ld2aggregate}
