// Get aggregation query for multi dimensions as multi buckets
export const getMultiAggQuery = (dimensions = [], filters = {}) => {
  return dimensions.reduce((acc, cur) => {
    let query = {
      terms: {
        field: `${cur}.enum`,
        size: 100000
      }
    }

    // if (filters[cur]) {
    let finalQuery = {
        filter: {
          term: Object.keys(filters).reduce((acc, cur) => ({
            ...acc,
            [`${cur}.enum`]: filters[cur]
          }), {})
          // term: {
          //   [`${cur}.enum`]: filters[cur]
          // }
        },
        aggs: {
          [`count_by_${cur}`]: {
            ...query
          }
        }
      }
    // }

    return ({
      ...acc,
      [`count_by_${cur}`]: {
        ...finalQuery
      }
    })
  }, {})
}
