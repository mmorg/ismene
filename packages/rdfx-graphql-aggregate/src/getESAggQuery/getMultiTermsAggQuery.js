// Get aggregation query for multi dimensions as 1 bucket only
export const getMultiTermsAggQuery = (dimensions = [], filters = {}) => {
  const multiTermsQuery = {
    multi_terms: {
      terms: dimensions.map((dimension) => ({
        field: `${dimension}.enum`
      })),
      size: 100000
    },
  }
  let query = multiTermsQuery

  if (Object.keys(filters).length) {
    query = {
      filter: {
        term: Object.keys(filters).reduce((acc, cur) => ({
          ...acc,
          [`${cur}.enum`]: filters[cur]
        }), {}),
      },
      aggs: {
        // [`count_by_${dimensions.join("_and_")}`]: {
        //   ...multiTermsQuery
        // }
        count: {
          ...multiTermsQuery
        }
      }
    }
  }



  return ({
    myAggregation: query
  })
}
