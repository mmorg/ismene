import fs from "fs";
import { GraphQLEnumType, GraphQLList, GraphQLObjectType, GraphQLString } from "graphql/index.js";
import { GraphQLInputObjectType } from "graphql/type/index.js";
import { getMultiTermsAggQuery } from "../getESAggQuery/getMultiTermsAggQuery.js";
import getAppSearchClient from "../../../rdfx-graphql/src/appSearchUtils/getAppSearchClient.js";
import getDimensions from "../utils/dimensions/getDimensions.js";
import getMeasures from "../utils/measures/getMeasures.js";

import { fileURLToPath } from 'url';
import { dirname } from 'path';

const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

const isTesting = true;

const ld2aggregate = (ld) => {
  const { dimensionProperty, dimensionComponent, dimensionLdKey } = getDimensions(ld)
  const { measureProperty, measureComponent, measureLdKey } = getMeasures(ld)

  const dimensions = dimensionProperty.map((property) => property.id.split(":")[1]);
  console.log('dimensions: ', dimensions);
  const DimensionEnumType = new GraphQLEnumType({
    name: "DimensionEnum",
    values: dimensions.reduce((acc, cur) => ({
      ...acc,
      [cur]: {
        value: cur
      }
    }), {})
  })
  const query = {
    "PivotTableQuery": {
      type: new GraphQLList(new GraphQLObjectType({
        name: "PivotTableQuery",
        fields: {
          ...dimensions.reduce((acc, cur) => ({
            ...acc,
            [cur]: {
              type: GraphQLString
            },
          }), {}),
        }
      })),
      args: {
        dimension: {
          type: new GraphQLList(DimensionEnumType)
        },
        filters: {
          type: new GraphQLInputObjectType({
            name: "PivotTableQueryFilter",
            fields: {
              ...dimensions.reduce((acc, cur) => ({
                ...acc,
                [cur]: {
                  type: GraphQLString
                }
              }), {})
            }
          })
        }
      },
      resolve: async (_, agrs) => {
        const { dimension, filters } = agrs;

        const client = getAppSearchClient({
          "baseUrl": "https://dashemploi.ent.europe-west1.gcp.cloud.es.io",
          "apiKey": "private-7474uh6ns96rkm1gtd9abt1u",
          "engineName": 'jobs-skills-fr-dev-4'
        });

        const aggsQuery = getMultiTermsAggQuery(dimension, filters);

        const res = await client.searchEsSearch({
          engine_name: 'jobs-skills-fr-dev-4',
          body: {
            size: 0,
            aggs: {
              ...aggsQuery
            }
          }
        });

        const results = res.aggregations?.myAggregation?.count?.buckets ?? res.aggregations?.myAggregation?.buckets ?? [];

        if (isTesting) {
          fs.writeFileSync(
            __dirname + '/../exampleResults/aggregations.json',
            JSON.stringify(results, null, 2)
          )
        }
      }
    }
  }

  return query;
}

export default ld2aggregate;
