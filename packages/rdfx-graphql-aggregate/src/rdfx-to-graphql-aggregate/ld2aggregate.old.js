import * as fs from "fs";
import { GraphQLInt, GraphQLList, GraphQLObjectType, GraphQLString } from "graphql";
import getAppSearchClient from "../../../rdfx-graphql/src/appSearchUtils/getAppSearchClient.js";
import { fileURLToPath } from 'url';
import { dirname } from 'path';

const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

const isTesting = true;
const ld2aggregate = (ld) => {
  // const { dimensionProperty, dimensionComponent, dimensionLdKey } = getDimensions(ld)
  // const { measureProperty, measureComponent, measureLdKey } = getMeasures(ld)
  //
  // console.log('dimensionProperty', dimensionProperty)
  //
  // const dimensionMap = {};
  // dimensionProperty.forEach((property) => {
  //   const id = property.id.split(":");
  //   const fields = property["qb:measure"] ?? [];
  //   fields.forEach(field => {
  //     const fieldId = field.split(":");
  //   })
  // })

  const cubeStructures = ld.graph.filter((graph) => graph.type.includes("qb:DataStructureDefinition"));
  const queries = {}
  cubeStructures.forEach(cubeStructure => {
    const dimensions = cubeStructure["qb:dimension"];
    const measures = cubeStructure["qb:measure"];
    const name = cubeStructure.id.split(":")[1];
    queries[name] = {
      type: new GraphQLList(new GraphQLObjectType({
        name,
        fields: {
          ...dimensions.reduce((acc, cur) => ({
            ...acc,
            [cur.replace(".", "")]: {
              type: GraphQLString
            }, // TODO: normalize the name
          }), {}),
          ...measures.reduce((acc, cur) => ({
            ...acc,
            [cur]: {
              type: GraphQLInt
            },
          }), {}),
        }
      })),
      args: {
        dimension: {
          type: new GraphQLList(GraphQLString)
        },
        measure: {
          type: new GraphQLList(GraphQLString)
        }
      },
      resolve: async () => {
        const client = getAppSearchClient({
          "baseUrl": "https://dashemploi.ent.europe-west1.gcp.cloud.es.io",
          "apiKey": "private-7474uh6ns96rkm1gtd9abt1u",
          "engineName": 'jobs-skills-fr-dev-4'
        });
        const aggsQuery = getMultiAggQuery(dimensions);
        console.log('aggsQuery', JSON.stringify(aggsQuery, null, 2))
        const res = await client.searchEsSearch({
          engine_name: 'jobs-skills-fr-dev-4',
          body: {
            size: 0,
            ...aggsQuery
            // aggs: {
            //   count_by_broader: {
            //     scripted_metric: {
            //       init_script: `state.values = [];`,
            //       map_script: `
            //         if (doc.containsKey('broader') && doc.broader.raw != null) {
            //           if (doc['broader.keyword'] instanceof String) {
            //             state.values.add(doc['broader.keyword']);
            //           } else if (doc['broader.keyword'] instanceof List) {
            //             state.values.addAll(doc['broader.keyword']);
            //           }
            //         }
            //       `,
            //       combine_script: `return state.values`,
            //       reduce_script: `
            //         Set values = new HashSet();
            //         for (int i = 0; i < states.size(); i++) {
            //           values.addAll(states[i]);
            //         }
            //
            //         return values;
            //       `,
            //     },
            //   }
            // }
          }
        })
        if (isTesting) {
          fs.writeFileSync(
            __dirname + '/../exampleResults/aggregations.json',
            JSON.stringify(res.aggregations, null, 2)
          )
        }
      }
    }
  })

  return queries;
}

export default ld2aggregate;

const script = `
  def values = new HashSet();
  def broader = doc['broader'];
  if (broader instanceof String) {
    values.add(broader);
  } else if (broader instanceof List) {
    values.addAll(broader);
  }
  return values;
`;
const getAggsQueryAsSubAggsQuery = (dimensions = []) => {
  const result = {};
  let cur = result;

  for (const dimension of dimensions) {
    const aggs = {
      [`count_by_${dimension}`]: {
        terms: {
          field: `${dimension}.enum`,
        },
      }
    }
    cur.aggs = aggs;

    cur = aggs[`count_by_${dimension}`];
  }

  return result;
}

const getMultiAggQuery = (dimensions = []) => {
  const result = {}

  for (const dimension of dimensions) {
    result[`${dimension}_filtered_by_end_of_object`] = {
      filter: {
        bool: {
          must_not: {
            term: {
              [`${dimension}.enum`]: "__end_of_object__"
            }
          }
        },
      },
      aggs: {
        [`count_by_${dimension}`]: {
          terms: {
            field: `${dimension}.enum`,
            size: 100000
          },
        }
      }
    }
  }

  return {
    aggs: result
  }
}
