import comptesDatacubeStructure from '../../../../data/statsAksis/comptesDatacubeStructure.js'
import initNewStructure from '../../../statsAksis/processors/utils/initNewStructure.js'
import getDimensions from 'packages/rdfx-graphql-aggregate/src/utils/dimensions/getDimensions.js'


// docker-compose exec gatsby npm test -- --watch getDimensions.test


describe('Dimensions extraction from ldstd datacubes', () => {

  // init a demo datacube (with no observations)
  let structureDataCube = null
  beforeAll( async () => {
    const {ld } = await initNewStructure(comptesDatacubeStructure)
    structureDataCube = ld
  })

  it('Extract the dimensions entities and the corresponding LD keys', async () => {
    const {dimensionProperty, dimensionComponent, dimensionLdKey} = getDimensions(structureDataCube)

    expect(dimensionComponent).toMatchSnapshot()
    expect(dimensionProperty).toMatchSnapshot()
    expect(dimensionLdKey).toStrictEqual(['annee', 'mois', 'monthNumber', 'categorie'])

  })

})
