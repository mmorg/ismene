import getComponentSpecification from '../componentSpecification/getComponentSpecification.js'

export default function getDimensions(ld) {
  return getComponentSpecification('dimension', ld)
}
