import getComponentSpecification from '../componentSpecification/getComponentSpecification.js'

export default function getMeasures(ld) {
  return getComponentSpecification('measure', ld)
}

