import comptesDatacubeStructure from '../../../../data/statsAksis/comptesDatacubeStructure.js'
import initNewStructure from '../../../statsAksis/processors/utils/initNewStructure.js'
import getMeasures from 'packages/rdfx-graphql-aggregate/src/utils/measures/getMeasures.js'


// docker-compose exec gatsby npm test -- --watch getMeasures.test


describe('Dimensions extraction from ldstd datacubes', () => {

  // init a demo datacube (with no observations)
  let structureDataCube = null
  beforeAll(async () => {
    const { ld } = await initNewStructure(comptesDatacubeStructure)
    structureDataCube = ld
  })

  it('Extract the dimensions entities and the corresponding LD keys', async () => {
    const { measureProperty, measureComponent, measureLdKey } = getMeasures(structureDataCube)
    expect(measureProperty).toMatchSnapshot()
    expect(measureComponent).toMatchSnapshot()

    expect(measureLdKey).toStrictEqual(['nombreReleve'])

  })

})
