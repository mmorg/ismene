
const capitalize = (s) => s[0].toUpperCase() + s.substring(1)

const contextFilter = (context, predicate) => {
  return Object.fromEntries(Object.entries(context).filter(predicate))
}


// this is a generic function to retrive specifics types
//    -> see definitions here: https://www.w3.org/TR/vocab-data-cube/#reference-compspec

// this should works for :
// - qb:dimension, qb:measure, qb:attribute
// - qb:measureDimension : but this need to be checked

export default function getComponentSpecification(componentPropertyName, ld) {

  const { '@context': ldContext } = ld

  const cpn = componentPropertyName // only to reduce var name
  const compPropId = `qb:${capitalize(cpn)}Property`

  const bag = {}

  bag[`${cpn}Property`] = ld.graph.filter(node => node.type.includes(compPropId))

  const propertyComponent = ld.graph.filter(node => node.type.includes('qb:ComponentSpecification') && node[cpn])
  bag[`${cpn}Component`] = propertyComponent
  // const dimensionComponent = ld.graph.filter(node => node.type.includes('qb:ComponentSpecification') && node.dimension)

  bag[`${cpn}LdKey`] = propertyComponent.reduce((acc, compSpec) => {
    // @TODO: use optional chaining when on node16. This works on jest but not on node command line...
    // this is the original implementation with 'dimension', need to be adjusted with the actual genericity
    // const dimensionKey = dimProp.dimension?.[0]
    const propertyKeyId = compSpec[cpn] ? compSpec[cpn][0] : null
    const ldAlias = contextFilter(ldContext,
      ([aliasKey, aliasValue]) => {
        // @TODO: use optional chaining when on node16. Strange error see below.
        // return dimensionKey && aliasValue?.['@id'] === dimensionKey
        const aliasId = aliasValue['@id'] ? aliasValue['@id'] : null
        return propertyKeyId && aliasId === propertyKeyId
      })

    const keys = Object.keys(ldAlias)
    if(!keys.length) throw new Error(`No ld alias defined for ${propertyKeyId} in the datacube structure. Please review the structure`)
    if (keys.length > 1) throw new Error(`Multiple LD aliases match for this dimension id ${compSpec.id}`)

    // @TODO: step 2 : search for "unalized keys" for more genericity on ld structure

    // end assign
    if(!keys[0]){
      console.warn(`Can't retrive ldKey for dimension`,ldAlias)
      throw new Error('Missing information in cube definition')
    }

    acc.push(keys[0])
    return acc
  }, [])

  return bag

}
