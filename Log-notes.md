
# rdfx-graphql : 

* Status: intégration de bout en bout presque ok. 
  - connexion refine - graphql : ok 
  - connexion graphql - app-search en lecture : ok

* Reste à faire  
  - graphql: generation complète à partir d'un modèle : a vérifier 
  - graphql: connexion en écriture a app-search
  - refine: autogeneration de l'interface à partir du modèle : a continuer 

* pour le restart : 
  - faire tourner cette requête graphql en la complétant avec : type, narrower, broader, ...
```graphql
query integrateRefineList ($sort: String, $where: JSON, $start: Int, $limit: Int) {
  allConcept(sort: $sort, where: $where, start: $start, limit: $limit) {
    id
    prefLabel {
      value
    }
  }
}
```
  - test l'affichage refine "live from app-search"
  - test de la modification d'entitées
   



# intégration rdfx-editor 

* A rajouter au multi-runner ? dans workspace "web3-tbl" `refine-explo/data-provider-strapi-graphql`

* acheter rdfx.eu 

* documenter la commande générale : `npm run run:dev`


# Jest / gatsby watch : 

* pour le problème de watch dans jest, voir : https://github.com/facebook/jest/issues/4531
  * qui indique le package en question.

  * ou la technique qui remplace avec nodemon 

  * !!! se met à jour lors de la maj de gatsby !!! 

==> vérifier la configuration faite et la revoir car: 
- pour jest:  pas totalement fonctionnelle. 
- pour gatsby: fonctionne pour les api via un patch. Voir: 
  - gatsby/patches/gatsby+4.22.0.patch

# ESM War: gatsby 'type:module'
- voir la fin de ce patch, et la signification des `targets` dans le fonctionnement de parcel.


# remove / update : 
* ajoute beaucoup de problèmes: react-mermaid2
* a mettre à jour upstream: @datalab-s/rml-js-ontology-elevator

# move : 
* to dashUtils/core/
- test-ts-gatsby/src/components/dashorm/hastTree2ReactCompiler.js
- test-ts-gatsby/src/components/dashorm/dashTree2HastTree.js


# cas spécial de diagram à revoir : 
* http://localhost:1111/rdf/Statement/



# composant à reprendre (pour le FAP, et les sankey de distance entre métiers):
dans le projet région-sud 
gatsby/src/components/dashorm/displays



# notes to clean: 

==> cleaner et exporter le logs-notes-2.md


==> Pour le test d'affichage de l'ontologie ori:
http://localhost:1111/ori/AptitudeRating/
http://localhost:1111/ori/hasAptitude/

==> test d'affichage de propriétés cibles xsd:, rdf, internes et de référentiels (à venir) : 
http://localhost:1111/nfr/OffreDeFormation/

==> layers management 
./__graphLayers__
    /generator
        skos:Ontology-1.2.0_gl_[n].jsonld
        nfr:ontology_gl_full.jsonld
    /gatsby
        nfr:on... 

==> explicit inherit 
==> Type override 



# Issues to fix :

* ajout de la notion de contribution 
  * mettre à jour le lien dans footer et page contribution 
  faire une doc sur le rdfs declaration : 
  ```
  * propertyName[lang] = text as it comes
  * range = ex:ExampleClass
  ```


* améliorer la gestion des "subpropertiesof", voir par exemple skos, avec http://localhost:1111/skos/broader/ qui est une sous-sous-sous-property de http://localhost:1111/skos/semanticRelation/


* general: tester le owl:Ontology.id en tant que prefix uniquement. Voir si ca fonctionne sur le reste du processing et pour le jsonld.compact

* ER représentation : 
  * nfr:EntiteDuMondeDeLaFormation est subclass of plusieurs classes: voir la représentation mermaid

* Visualisation avec un bug : http://localhost:1111/hrrdf/BinaryObjectType/

# Libs to clean
* remove:  
"@mui-treasury/components": "^1.10.1",
"@mui-treasury/styles": "^1.13.1",

* use a tool to detect unused deps


# projects to create / note to export:
## graph-diff, rdfx-graph-diff

* ==> archéologie : définition de rdf-diff : 
  * voir ce fichier de test : https://github.com/florent-andre/LinkedHeritage/blob/master/visualisation/resources/js/test/lh.histo.js-test.html#L130
  * et cette implémentation : 
  * ==> A extraire, refaire tourner et faire évoluer 


## stdld
* standardLD / stdLD : 
  * read it as : stardard Contexts for Json-LD
  * ==> Std : Standard Typings for Developpers using Json-LD contexts
  * contextes individuels, et possibbilité de les merge avec une API

 

# others ontologies to import: 

 * existing lists: 
  * ontologies : https://gitlab.com/mmorg/ismene/-/issues/?sort=created_asc&state=opened&label_name%5B%5D=new%20ontology&first_page_size=20
  * terminologies : https://gitlab.com/mmorg/ismene/-/issues/?sort=created_asc&state=opened&label_name%5B%5D=new%20term&first_page_size=20

  

* ==> import de shacl: 
  * a faire après: importer le shacl-shacl : https://github.com/w3c/data-shapes/blob/gh-pages/shacl/shacl-shacl.ttl
  * debug de l'import du owl:Ontology.label
    => tester en rajoutant dans le modèle graphql (gatsby-node) la déclaration du label 

* ==> import de Foaf : 
  * et d'un lié = https://www.w3.org/2000/10/swap/pim/contact#Person
  * ==> élément intéressants avec l'usage de vs:term_status="testing", "stable",...


* ==> import sioc http://rdfs.org/sioc/ns

* ==> ajouter prov-o
  * attention en format owl/xml, voir si autres versions sources: https://gitlab.com/ami-paca/ami-models/-/blob/master/prov-o.owl

* doap : http://usefulinc.com/ns/doap
  * ==> chercher le doap utilisé par Apache si c'est le même 

* earl: pour les résultats de tests : https://www.w3.org/ns/earl#
  * contient des "instances" ou "référentiels"; A voir ce que ça donne.

* mls : Machine Learning Schema : https://github.com/ML-Schema/core/blob/master/MLSchema.ttl
 
* ==> ajouter rdf-diff (et rdf-math) au visualisateur : faire de l'archéologie

* ==> ajouter l'ontologie europass : voir ici pour mettre à jour : https://gitlab.com/mmorg/europass
  * repartir du mapping xsd "Classes" de hrrdf ?

* ==> ajouter schema.org (mettre à jour le fichier)

* ==> ajouter ontologie sur les CV (pour comparaison avec Europass) : http://rdfs.org/resume-rdf/

* ==> ajouter ontologie sur les tags : http://rdfs.org/scot/spec/




# inspiration notes

- cible d'exemple: https://www.schemastore.org/json/
  - ce qui est intéressant c'est l'intégration dans les éditeurs de textes
  - c'est aussi une source de jsonSchema a remettre en rdfs



# question gatsby : 

* Graphql : how to transform the "last win" to the "merge win" during node processing 
  * => faire un example minimal
  * question ouverte: https://github.com/gatsbyjs/gatsby/discussions/35852





## RESTART MultiLevel Menu : 

==> faire tourner le script d'indexation pour hrrdf

* Indexation d'un model : 
```
docker-compose exec gatsby node src/bin/integrate mu -f '**/oriane-rdf-release/*.jsonld'
```

* pour un model de description d'ontologie avec des instructions : `gatsby/data/ontoModelTemplate`

* pour définir le mapping "clé" -> "React" des icones, il faut modifier le fichier : `gatsby/src/components/dashorm/menuHelpers/node2menuItem.js` -> iconsList
  * ==> @TODO : export to a config file
  * ==> @TODO : make icon @Ontology level dynamic


***pour le test et le dev***
* repartir de cette page de test pour la gestion des multi-ontologies : http://localhost:1111/devTests/MultiOntologyTest/

* Jest tests :  `docker-compose exec gatsby npm test -- --watch ontoIndexer.test`



## RDFs explorator docs : 

* Main configuration parameters 
  * homepage : `gatsby/src/pages/index.js`
  * website name : 
    * `gatsby/gatsby-config.js`
    * `gatsby/src/components/dashibiden/DashLayout.js`
    * `gatsby/src/components/dashibiden/DashBase.js`
  * website footer : `gatsby/src/components/dashibiden/Copyright.js`
  * primary color : @TODO: to find
  * 



